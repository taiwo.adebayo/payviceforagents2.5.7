package com.iisysgroup.payviceforagents.fundwallet

import android.app.Activity
import android.app.ProgressDialog
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.google.gson.Gson
import com.iisysgroup.payviceforagents.PinAlertUtils
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.activities.PaymentOptionActivity
import com.iisysgroup.payviceforagents.activities.ResultVasActivity
import com.iisysgroup.payviceforagents.activities.UserPinEntryActivity
import com.iisysgroup.payviceforagents.base.presenter.FundWalletPresenter
import com.iisysgroup.payviceforagents.baseimpl.viewmodel.FundWalletViewModel
import com.iisysgroup.payviceforagents.entities.VasResult
import com.iisysgroup.payviceforagents.external.CardUtils
import com.iisysgroup.payviceforagents.external.CreditCardUtils
import com.iisysgroup.payviceforagents.external.MyCardEditActivity
import com.iisysgroup.payviceforagents.fragments.LinkedCardsFragment
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.payviceservices.Requests
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.*
import kotlinx.android.synthetic.main.fragment_fund_amount.*
import org.jetbrains.anko.alert
import java.util.HashMap

class FundWalletActivity : AppCompatActivity() {
    private var progressDialog: ProgressDialog? = null

    private var encryptedUserPin = ""

    private val viewModel: FundWalletPresenter by lazy {
        FundWalletViewModel()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fund_wallet)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        proceedBtn.setOnClickListener {
            if (isValidInput()) {
                showPinEntry()
            }
        }

        viewModel.onError().observe(this, Observer {
            it?.let(this::showError)
        })

        viewModel.onProgressUpdate().observe(this, Observer {
            it?.let {
                showProgressDialog(it.first, it.second)
            }
        })

        viewModel.onFundResponse().observe(this, Observer {

            it?.let {
                val response = it

                val intent = Intent(this@FundWalletActivity, ResultVasActivity::class.java)
                intent.putExtra(ResultVasActivity.EXTRA_RESULT, it)
                startActivity(intent)
            }
            //it?.let(this::showResultScreen)
        })

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_cancel || id == android.R.id.home) {
            finish()
        }
        return true
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == UserPinEntryActivity.PIN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                encryptedUserPin = data!!.getStringExtra(UserPinEntryActivity.PIN_RESPONSE_DATA)

                val amount = Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
                showPaymentOption(PaymentOption.Mode.FUND, amount, "Self")
            }
        }

        if (requestCode == PaymentOptionActivity.REQUEST_CODE) {
            Log.i("Debit card ", "REQUEST_CODE ")
            if (resultCode == Activity.RESULT_OK) {
                val paymentOption = data?.getSerializableExtra(PaymentOptionActivity.RESULT_OPTION) as PaymentOption
                val card = data.getSerializableExtra(PaymentOptionActivity.RESULT_CARD) as? Card

                continuePayment(paymentOption, card)
            }
        }

        if (requestCode == LinkedCardsFragment.LINK_CARD_REQUEST_CODE) {
            Log.i("Debit card ", "LINK_CARD_REQUEST_CODE ")
            if (resultCode == Activity.RESULT_OK && data!!.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY) != null) {

                val cardHolderName = data.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME)
                val cardNumber = data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER)
                val expiry = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY)
                val cvv = data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV)
                Log.i("curcard.exp", Gson().toJson(expiry))
                // Your processing goes here.

                val currentCard = Card()

                currentCard.cardHolder = cardHolderName
                currentCard.PAN = cardNumber
                currentCard.CVV = cvv
                currentCard.expiryMonth = expiry.split("\\/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
                currentCard.expiryYear = expiry.split("\\/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]

                Log.i("curCard.exp", Gson().toJson(currentCard.expiryYear ))
                Log.i("Card Details ", Gson().toJson(currentCard))

                alert {
                    title ="Payvice"
                    message="Do you want to save card details ?"
                    positiveButton("Yes") {
                        showPinpad(currentCard,"yes",true)

                    }

                    negativeButton("No") {
                        showPinpad(currentCard,"no",false)

                    }
                }.show()




            }
        }
    }

    private fun showPinpad(currentCard:Card,isOnetime: String,linkcard:Boolean){
        val view = View.inflate(this, R.layout.activity_enter_pin, null)
        PinAlertUtils.getPin(this, view) { enteredPin ->
            currentCard.pin = enteredPin

            storeDetailsOnline(currentCard, isOnetime,linkcard)

        }
    }

    private fun continuePayment(paymentOption: PaymentOption, card: Card?) {
        when (paymentOption) {
            PaymentOption.LINKED_CARD -> {
                val amount = Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
                val processor = DebitCardProcessor(this, encryptedUserPin)
                viewModel.fund(amount, card, processor)
            }

            PaymentOption.DEBIT_CARD -> {
                startActivityForResult(Intent(this, MyCardEditActivity::class.java), LinkedCardsFragment.LINK_CARD_REQUEST_CODE)
            }

            //            else -> {
            //
            //            }
        }


    }

    private fun showProgressDialog(show: Boolean, message: String = "Processing...") {
        progressDialog?.dismiss()

        if (show) {
            progressDialog = ProgressDialog.show(this, title, message, true)
        }
    }

    private fun storeDetailsOnline(card: Card, isOneTimeUse: String,linkCard :Boolean) {

        var canContinue = false

        val progressDialog = ProgressDialog.show(this@FundWalletActivity, null, "Processing transaction", true, false)


        Helper.ThreadService.execute {

            try {

                if(linkCard) {
                    Log.i("Link card   ", "Yes")
                    val terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID, "")
                    val salt = Helper.getClearKey()
                    val onlineData = CardUtils.generateOnlineCard(terminalID, card, salt)
                    val userId = SecureStorage.retrieve(Helper.USER_ID, "")
                    val url = getString(R.string.tams_url)

                    val action = getString(R.string.tams_webapi_action)
                    val control = "DO_UPDATE"

                    val params = HashMap<String, String>()
                    params["action"] = action
                    params["userid"] = userId
                    params["termid"] = terminalID
                    params["onetime"] = isOneTimeUse
                    params["control"] = control
                    params["value"] = onlineData

                    val tamsResult = Requests.processRequest(url, params)


                    if (tamsResult.result == VasResult.Result.APPROVED) {
                        canContinue = true
                    } else {
                        canContinue = false
                        Helper.showInfoDialog(this, "Request failed", "Could not link this card. Please try again")
                    }
                }else{
                    Log.i("Link card   ", "NO")

                    doTransaction(card,progressDialog)
                }

            } catch (e: Exception) {
                e.printStackTrace()
                Helper.showDefaultComError(this)
            } finally {
                if (canContinue) {
                    doTransaction(card,progressDialog)
                }


            }
        }
    }

    private fun doTransaction(card: Card, progressDialog:ProgressDialog){
        Helper.runOnUiThread {

            val amount = Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
            val processor = DebitCardProcessor(this, encryptedUserPin)
            viewModel.fund(amount, card, processor)
            progressDialog?.dismiss()
        }
    }


    private fun isValidInput(): Boolean {
        if (amountEdit.text.isNullOrBlank() ||
                Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt() < 50) {

            Helper.showErrorAnim(amountEdit)
            amountEdit.error = "Invalid amount"
            return false
        }
        return true
    }

}

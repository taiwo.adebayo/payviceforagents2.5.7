package com.iisysgroup.payviceforagents.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.fragments.ChangePasswordFragment;
import com.iisysgroup.payviceforagents.fragments.ChangePinFragment;
import com.iisysgroup.payviceforagents.fragments.LoginSettingsActivityFragment;
import com.iisysgroup.payviceforagents.fragments.MainSettingsActivityFragment;
import com.iisysgroup.payviceforagents.fragments.PrivacyPolicyFragment;
import com.iisysgroup.payviceforagents.util.Helper;

public class MainSettingsActivity extends BaseActivity implements LoginSettingsActivityFragment.OnFragmentInteractionListener {

    FragmentManager fm;
    FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fm = getSupportFragmentManager();

        loadFragment(new MainSettingsActivityFragment());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if (fm.getBackStackEntryCount() > 1) {
                fm.popBackStack();
            } else
                NavUtils.navigateUpFromSameTask(this);
        }


        return true;
    }

    @Override
    public void onBackPressed() {
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();
        } else
            NavUtils.navigateUpFromSameTask(this);

    }

    public void loadFragment(Fragment fragment) {
        ft = fm.beginTransaction();
        if (fm.findFragmentByTag(fragment.getTag()) != null)
            ft.remove(fragment);

        ft.replace(R.id.container, fragment, fragment.getClass().getSimpleName());
        ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

    @Override
    public void onSettingSelected(int pos) {
        switch (pos) {
            case 0:
                loadFragment(new ChangePinFragment());
                break;
            case 1:
                loadFragment(new ChangePasswordFragment());
                break;
            case 2:
                Helper.showInfoDialog(this, "Pin Reset", "You can do a pin reset when you are in a situation where you cannot" +
                        " recall your pin. To do a pin reset:\n1. Log out\n2. Go to Help\n3.Do a password recovery" +
                        "\n4. Follow the link sent to your mail and change your password.\n5. Log in again and change your pin when you are required to.");
                break;
            case 3:
                loadFragment(new PrivacyPolicyFragment());
                break;

        }
    }
}

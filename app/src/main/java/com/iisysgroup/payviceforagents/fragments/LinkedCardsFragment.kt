package com.iisysgroup.payviceforagents.fragments

import android.app.Activity
import android.app.ProgressDialog
import android.arch.lifecycle.LiveDataReactiveStreams
import android.arch.lifecycle.MediatorLiveData
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.entities.VasResult
import com.iisysgroup.payviceforagents.external.CardUtils
import com.iisysgroup.payviceforagents.external.CreditCardUtils
import com.iisysgroup.payviceforagents.external.CreditCardView
import com.iisysgroup.payviceforagents.external.MyCardEditActivity
import com.iisysgroup.payviceforagents.payviceservices.Requests
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
//import com.iisysgroup.payviceforagents.util.db
import com.iisysgroup.payviceforagents.util.runSafelyOnBackground
import io.reactivex.Flowable
import kotlinx.android.synthetic.main.fragment_linked_cards.*
import org.jetbrains.anko.support.v4.alert
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.thread


open class LinkedCardsFragment : Fragment() {
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private var progressDialog: ProgressDialog? = null
    internal var cards: MutableList<Card> = ArrayList()

    internal var cardAdapter = CardAdapter(cards)
    internal var userId = SecureStorage.retrieve(Helper.USER_ID, "")

    internal var terminalId = SecureStorage.retrieve(Helper.TERMINAL_ID, "")


//    private val db by lazy {
//        activity!!.db
//    }


    private val mediator = MediatorLiveData<List<Card>>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_linked_cards, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        list.layoutManager = layoutManager
        list.adapter = cardAdapter

        addCardBtn.setOnClickListener {
            startActivityForResult(Intent(activity, MyCardEditActivity::class.java), LINK_CARD_REQUEST_CODE)
        }



//        val itemClicked : View.OnLongClickListener =

//        list

        mediator.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                cards.clear()
                cards.addAll(it)
                cardAdapter.notifyDataSetChanged()
            }

            showAppropriateUI()
        })
    }

    override fun onStart() {
        super.onStart()
      //  getCards()
    }

//    private fun getCards() {
//        val dbCardLiveData = db.cardDao().getAll()
//        Log.i("After deleting   ", "After here   ")
//
//        mediator.addSource(dbCardLiveData) {
//
//            mediator.removeSource(dbCardLiveData)
//            if (it != null && it.isNotEmpty()) {
//                Log.i("After deleting   ", "Reused   ")
//                mediator.postValue(it)
//            } else {
//                Log.i("After deleting   ", "Fresh download    ")
//                val downloadCardLiveData = LiveDataReactiveStreams.fromPublisher(downloadCards())
//
//                mediator.addSource(downloadCardLiveData) {
//                    mediator.removeSource(downloadCardLiveData)
//                    it?.let {
////                        thread {
////                            for (card in it) {
////                                db.cardDao().insert(card)
////                            }
////                        }
//                        mediator.postValue(it)
//                    }
//                }
//            }
//        }
//
//    }

    private fun downloadCards(): Flowable<List<Card>> {
        progressDialog = ProgressDialog.show(activity, null, "Please wait...")

        return Flowable.fromCallable {
            getLinkedCards()
        }.runSafelyOnBackground().onErrorReturn {
            emptyList()
        }
    }


    private fun getLinkedCards(): List<Card> {
        val url = getString(R.string.tams_url)
        val action = getString(R.string.tams_webapi_action)
        val control = "DO_RETRIEVE"

        val params = HashMap<String, String>()
        params["action"] = action
        params["userid"] = userId
        params["termid"] = terminalId
        params["control"] = control

        val tamsResult = Requests.processRequest(url, params)

        return processCards(tamsResult.message.trim { it <= ' ' })
    }

    private fun processCards(data: String): List<Card> {
        val cards = ArrayList<Card>()
        val key = Helper.getClearKey()
        for (onlineCard in data.split("\\|".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {

            if (onlineCard.trim { it <= ' ' }.isEmpty()) {
                continue
            }
            val card = CardUtils.getCardData(onlineCard, key)
            cards.add(card)
        }

        return cards
    }

    private fun showAppropriateUI() {
        if (!cards.isEmpty()) {
            emptyLayout.visibility = View.GONE
            cardAdapter.notifyDataSetChanged()
        } else {
            emptyLayout.visibility = View.VISIBLE
        }

        progressDialog?.dismiss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == LINK_CARD_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data!!.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY) != null) {

                val cardHolderName = data.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME)
                val cardNumber = data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER)
                val expiry = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY)
                val cvv = data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV)

                // Your processing goes here.

                val currentCard = Card()
                currentCard.cardHolder = cardHolderName
                currentCard.PAN = cardNumber
                currentCard.CVV = cvv
                currentCard.expiryMonth = expiry.split("\\/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
                currentCard.expiryYear = expiry.split("\\/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]


                storeDetailsOnline(currentCard, "no")

            }
        }
    }

    private fun storeDetailsOnline(card: Card, isOneTimeUse : String) {
        val progressDialog = ProgressDialog.show(activity, null, "Linking card. Please wait", true, false)

        Helper.ThreadService.execute {
            try {
                val terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID, "")
                val salt = Helper.getClearKey()
                val onlineData = CardUtils.generateOnlineCard(terminalID, card, salt)
                val userId = SecureStorage.retrieve(Helper.USER_ID, "")
                val url = activity!!.getString(R.string.tams_url)

                val action = activity!!.getString(R.string.tams_webapi_action)
                val control = "DO_UPDATE"

                val params = HashMap<String, String>()
                params["action"] = action
                params["userid"] = userId
                params["termid"] = terminalID
                params["onetime"] = isOneTimeUse
                params["control"] = control
                params["value"] = onlineData

                val tamsResult = Requests.processRequest(url, params)

                if (tamsResult.result == VasResult.Result.APPROVED) {

                    //db.cardDao().insert(card)
                    Helper.runOnUiThread {
                        cards.add(card)
                        cardAdapter.notifyDataSetChanged()
                    }
                } else {
                    Helper.showInfoDialog(activity, "Request failed", "Could not link this card. Please try again")
                }

            } catch (e: Exception) {
                e.printStackTrace()
                Helper.showDefaultComError(activity)
            } finally {
                Helper.runOnUiThread {
                    progressDialog?.dismiss()
                }
            }
        }

    }
    private fun deleteDetailsonline(card: Card, isOneTimeUse: String) {

//        var canContinue = false

        val progressDialog = ProgressDialog.show(context, null, "Deleting Card Details ", true, false)


        Helper.ThreadService.execute {
            try {

//                if(linkCard) {
//                Log.i("Link card   ", "Yes")
                val terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID, "")
                val salt = Helper.getClearKey()
                val onlineData = CardUtils.generateOnlineCard(terminalID, card, salt)
                val userId = SecureStorage.retrieve(Helper.USER_ID, "")
                val url = getString(R.string.tams_url)

                val action = getString(R.string.tams_webapi_action)
                val control = "DO_UPDATE"
                val delete = "DELETE"

                val params = HashMap<String, String>()
                params["action"] = action
                params["userid"] = userId
                params["termid"] = terminalID
                params["onetime"] = isOneTimeUse
                params["control"] = control
                params["value"] = onlineData
                params["option"] = delete
                Log.i("url   ", Gson().toJson(url))
                Log.i("params   ", Gson().toJson(params))

                val tamsResult = Requests.processRequest(url, params)


                if (tamsResult.result == VasResult.Result.APPROVED) {
                    Log.i("After deleting   ", Gson().toJson(tamsResult))

                    //getCards()
                } else {

                    Log.i("Delete card   ", "Failed")

//                        canContinue = false
                    Helper.showInfoDialog(context, "Request failed", "Could not link this card. Please try again")
                }
//                }else{
//                Log.i("Link card   ", "NO")
//
//                doTransaction(card,progressDialog)
//                }

            } catch (e: Exception) {
                progressDialog.dismiss()

                e.printStackTrace()
                Helper.showDefaultComError(activity)
            }
            finally {

                progressDialog.dismiss()

            }
        }
    }

    internal open inner class CardAdapter(var cardList: List<Card>) : RecyclerView.Adapter<CardAdapter.CardViewHolder>(){


        override fun getItemCount(): Int {
            return cardList.size
        }



        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {


            val view = LayoutInflater.from(parent.context).inflate(R.layout.card_list_item, parent, false)

            return CardViewHolder(view)
        }

        override fun onBindViewHolder(holder: CardViewHolder, position: Int) {

            val card = cardList[position]
            val cvv = card.CVV
            val cardHolderName = card.cardHolder
            val cardNumber = card.PAN
            val expiry =card.expiryMonth + card.expiryYear


            holder.creditCardView.cvv = cvv
            holder.creditCardView.setCardExpiry(expiry)
            holder.creditCardView.cardNumber = card.maskedPan
            holder.creditCardView.visibility = View.VISIBLE
            holder.creditCardView.cardHolderName = "---"




            // Your processing goes here.

            val currentCard = Card()
            currentCard.cardHolder = cardHolderName
            currentCard.PAN = cardNumber
            currentCard.CVV = cvv
            currentCard.expiryMonth = card.expiryMonth
            currentCard.expiryYear = card.expiryYear

            holder.creditCardView.setOnLongClickListener(object :View.OnLongClickListener {
                override fun onLongClick(v: View?): Boolean {
//                        v!!.context!!.toast("last")
//                        Log.i("Yeah", "clicked")
                    alert {
                        title ="Payvice"
                        message="Do you want to delete this card ? "
                        positiveButton("Yes") {
                            deleteDetailsonline(currentCard,"Yes")
                            cards.clear()
                            cardAdapter.notifyDataSetChanged()
                        }

                        negativeButton("No") {
                            //                                showPinpad(currentCard,"no",false)

                        }
                    }.show()
                    return false
                }
            })




        }



        internal inner class CardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var creditCardView: CreditCardView
            var view: View


            init {
                this.view = itemView.findViewById(R.id.selectAction)
                creditCardView = itemView.findViewById(R.id.credit_card_view)



            }




        }


    }




    companion object {
        const val LINK_CARD_REQUEST_CODE = 1002
    }

}



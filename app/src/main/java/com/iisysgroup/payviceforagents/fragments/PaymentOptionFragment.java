package com.iisysgroup.payviceforagents.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.PaymentOption;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

import static com.iisysgroup.payviceforagents.util.PaymentOption.Mode.FUND;
import static com.iisysgroup.payviceforagents.util.PaymentOption.Mode.PAY;
import static com.iisysgroup.payviceforagents.util.PaymentOption.Mode.REPEAT;


public class PaymentOptionFragment extends BaseBalanceFragment {

    static final String EXTRA_BENEFICIARY = "beneficiary",
            EXTRA_CONVENIENCE = "convenience", EXTRA_MODE = "mode";
    String modeText = "";
    @BindView(R.id.walletBalanceText)
    TextView walletBalanceText;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @Nullable
    @BindView(R.id.fundButton)
    CardView fundButton;
    @BindView(R.id.phoneText)
    TextView phoneText;
    @BindView(R.id.recipientText)
    TextView recipientText;
    @BindView(R.id.productOptionText)
    TextView productOptionText;
    @BindView(R.id.amountText)
    TextView amountText;
    @BindView(R.id.convenienceFeeText)
    TextView convenienceFeeText;
    @BindView(R.id.radPayWithWallet)
    RadioButton radPayWithWallet;
    @BindView(R.id.radPayWithLinkedCard)
    RadioButton radPayWithLinkedCard;
    @BindView(R.id.radPayWithDebitCard)
    RadioButton radPayWithDebitCard;
    @BindView(R.id.proceedBtn)
    Button proceedButton;
    @BindView(R.id.modeTextView)
    TextView modeTextView;
    @BindView(R.id.radWalletLayout)
    View walletLayout;
    @BindView(R.id.radDebitCardLayout)
    View debitCardLayout;
    @BindView(R.id.radLinkedCardLayout)
    View linkedCardLayout;
    PaymentOption paymentOption = PaymentOption.WALLET;
    Beneficiary beneficiary;
    int amount = 0;
    int convenienceFee = 0;
    PaymentOption.Mode mode;
    Handler handler;
    private OnFragmentInteractionListener mListener;

    public static PaymentOptionFragment getInstance(Beneficiary beneficiary, int amount, int convenienceFee) {
        return getInstance(beneficiary, amount, convenienceFee, PAY);
    }

    public static PaymentOptionFragment getInstance(Beneficiary beneficiary, int amount, int convenienceFee, PaymentOption.Mode mode) {
        PaymentOptionFragment paymentOptionFragment = new PaymentOptionFragment();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_BENEFICIARY, beneficiary);
        args.putInt(EXTRA_AMOUNT, amount);
        args.putInt(EXTRA_CONVENIENCE, convenienceFee);
        args.putSerializable(EXTRA_MODE, mode);

        paymentOptionFragment.setArguments(args);

        return paymentOptionFragment;
    }

    @OnCheckedChanged(R.id.radPayWithDebitCard)
    void selectPayWithDebitCard() {
        if (radPayWithDebitCard.isChecked()) {
            paymentOption = PaymentOption.DEBIT_CARD;

            radPayWithLinkedCard.setChecked(false);
            radPayWithWallet.setChecked(false);
        }

    }

    @OnCheckedChanged(R.id.radPayWithLinkedCard)
    void selectPayWithLinkedCard() {
        if (radPayWithLinkedCard.isChecked()) {
            paymentOption = PaymentOption.LINKED_CARD;

            radPayWithWallet.setChecked(false);
            radPayWithDebitCard.setChecked(false);
        }

    }

    @OnCheckedChanged(R.id.radPayWithWallet)
    void selectPayWithWallet() {
        if (radPayWithWallet.isChecked()) {
            paymentOption = PaymentOption.WALLET;

            radPayWithDebitCard.setChecked(false);
            radPayWithLinkedCard.setChecked(false);
        }

    }

    @OnClick(R.id.radWalletLayout)
    void walletLayoutSelected() {
        radPayWithWallet.setChecked(true);
    }

    @OnClick(R.id.radLinkedCardLayout)
    void linkedCardLayoutSelected() {
        radPayWithLinkedCard.setChecked(true);
    }

    @OnClick(R.id.radDebitCardLayout)
    void debitCardLayoutSelected() {
        radPayWithDebitCard.setChecked(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            beneficiary = (Beneficiary) getArguments().getSerializable(EXTRA_BENEFICIARY);
            amount = getArguments().getInt(EXTRA_AMOUNT);
            convenienceFee = getArguments().getInt(EXTRA_CONVENIENCE);
            mode = (PaymentOption.Mode) getArguments().getSerializable(EXTRA_MODE);
        }

        handler = new Handler();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Payment Option");
        return inflater.inflate(R.layout.payment_option_item, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        walletBalanceText.setText(Helper.getPreference(getActivity(), Helper.BALANCE, ""));
        convenienceFeeText.setText("\u20A6 " + convenienceFee);
        if (beneficiary != null) {
            recipientText.setText(beneficiary.getDisplayInfo());
        } else {
            recipientText.setText("Self");
        }

        amountText.setText("\u20A6 " + amount);

        if (mode == FUND) {
            modeText = "Fund";
            walletLayout.setEnabled(false);
            radPayWithWallet.setEnabled(false);

            fundButton.setEnabled(false);
            fundButton.setVisibility(View.INVISIBLE);
        } else {
            modeText = "Pay";

            if (mode == REPEAT) {
                linkedCardLayout.setEnabled(false);
                radPayWithLinkedCard.setEnabled(false);
                debitCardLayout.setEnabled(false);
                radPayWithDebitCard.setEnabled(false);
            }
        }

        modeTextView.setText(modeText + " With");

        proceedButton.setText(String.format("%s %c %d", modeText, '\u20A6', amount + convenienceFee));


    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    boolean isValidInputs() {
        if (radPayWithLinkedCard.isChecked() ||
                radPayWithWallet.isChecked() || radPayWithDebitCard.isChecked()) return true;

        Helper.showSnackBar(walletBalanceText, "Please select a payment method");
        return false;
    }

    @OnClick(R.id.proceedBtn)
    void proceed() {
        if (isValidInputs()) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.app_name)
                    .setMessage("Proceed with payment?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mListener.onPaymentOptionFragmentInteraction(paymentOption);
                        }
                    }).show();
        }

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onPaymentOptionFragmentInteraction(PaymentOption paymentOption);
    }


}

package com.iisysgroup.payviceforagents.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName





class GetTidModel{

    inner class MainModel {

        @SerializedName("error")
        @Expose
        private var error: Boolean? = null
        @SerializedName("tid")
        @Expose
        private var tid: String? = null
        @SerializedName("address")
        @Expose
        private var address: String? = null
        @SerializedName("Name")
        @Expose
        private var name: String? = null
        @SerializedName("bank_logo")
        @Expose
        private var bankLogo: String? = null
        @SerializedName("screensaver_logo1")
        @Expose
        private var screensaverLogo1: String? = null
        @SerializedName("callhome")
        @Expose
        private var callhome: Int? = null
        @SerializedName("hosts")
        @Expose
        private var hosts: List<Host> = ArrayList()

        fun getError(): Boolean? {
            return error
        }

        fun setError(error: Boolean?) {
            this.error = error
        }

        fun getTid(): String? {
            return tid
        }

        fun setTid(tid: String) {
            this.tid = tid
        }

        fun getAddress(): String? {
            return address
        }

        fun setAddress(address: String) {
            this.address = address
        }

        fun getName(): String? {
            return name
        }

        fun setName(name: String) {
            this.name = name
        }

        fun getBankLogo(): String? {
            return bankLogo
        }

        fun setBankLogo(bankLogo: String) {
            this.bankLogo = bankLogo
        }

        fun getScreensaverLogo1(): String? {
            return screensaverLogo1
        }

        fun setScreensaverLogo1(screensaverLogo1: String) {
            this.screensaverLogo1 = screensaverLogo1
        }

        fun getCallhome(): Int? {
            return callhome
        }

        fun setCallhome(callhome: Int?) {
            this.callhome = callhome
        }

        fun getHosts(): List<Host> {
            return hosts
        }

        fun setHosts(hosts: List<Host>) {
            this.hosts = hosts
        }

    }

    inner class Host {

        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("host")
        @Expose
        var host: String? = null
        @SerializedName("port")
        @Expose
        var port: String? = null
        @SerializedName("security")
        @Expose
        var security: String? = null

    }
}
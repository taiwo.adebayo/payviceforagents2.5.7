package com.iisysgroup.payviceforagents.baseimpl.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.iisysgroup.payviceforagents.base.view.RegistrationView

class RegistrationActivityViewModel : ViewModel(), RegistrationView {

    private val showProgressLiveData = MutableLiveData<Boolean>()
    val onProgressUpdate: LiveData<Boolean>
        get() = showProgressLiveData

    private val uiValidationLiveData = MutableLiveData<ValidationType>()
    val onValidationError: LiveData<ValidationType>
        get() = uiValidationLiveData

    private val referralMutableLiveData = MutableLiveData<String>()
    val onSetReferral: LiveData<String>
        get() = referralMutableLiveData

    private val resultMutableLiveData = MutableLiveData<Boolean>()
    val onRegistrationSuccessful: LiveData<Boolean>
        get() = resultMutableLiveData

    private val errorMutableLiveData = MutableLiveData<Throwable>()
    val onRegistrationError: LiveData<Throwable>
        get() = errorMutableLiveData


    override fun onNameValidationError() {
        uiValidationLiveData.value = ValidationType.USERNAME
    }

    override fun onEmailValidationError() {
        uiValidationLiveData.value = ValidationType.EMAIL
    }

    override fun onPhoneValidationError() {
        uiValidationLiveData.value = ValidationType.PHONE
    }

    override fun onPasswordValidationError() {
        uiValidationLiveData.value = ValidationType.PASSWORD
    }

    override fun onPinValidationError() {
        uiValidationLiveData.value = ValidationType.PIN
    }

    override fun onShowProgress() {
        showProgressLiveData.value = true
    }

    override fun onDismissProgress() {
        showProgressLiveData.value = false
    }

    override fun setRegistrationError(error: Throwable?) {
        errorMutableLiveData.postValue(error)
    }

    override fun onDisplayReferral(code: String) {
        referralMutableLiveData.value = code
    }

    override fun onRegistrationSuccess() {
        resultMutableLiveData.postValue(true)
    }

    enum class ValidationType {
        USERNAME, EMAIL, PHONE, PASSWORD, PIN
    }
}
package com.iisysgroup.payviceforagents;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;

public class PinAlertUtils {
    Context context;
    ProgressDialog progressDialog = new ProgressDialog(context);
    public interface PinEnteredListener {
        void onPinEntered(String pin);
    }
    public static void getPin(Context context, View view, final PinEnteredListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        ProgressDialog progressDialog = new ProgressDialog(context);

        builder.setCancelable(false);
        builder.setView(view);

        PinLockView pinLockView = view.findViewById(R.id.pin_lock_view);
        IndicatorDots dots = view.findViewById(R.id.indicator_dots);
        ImageView cancel = view.findViewById(R.id.cancelButton);
        pinLockView.attachIndicatorDots(dots);
        pinLockView.setPinLength(4);
        final Dialog dialog = builder.create();
        pinLockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                dialog.dismiss();
                progressDialog.setTitle("Transferring");
                progressDialog.setMessage("Now Debitting Wallet");
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(true);
                progressDialog.show();

                listener.onPinEntered(pin);

            }

            @Override
            public void onEmpty() {

            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
    public void getProgressDialog(Context context){

        progressDialog.setTitle("Transferring");
        progressDialog.setMessage("Now Debitting Wallet");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
}


// android.view.WindowLeaked: Activity com.iisysgroup.payvice.activities.MainActivity has leaked window DecorView@87322[] that was originally added here
//        at android.view.ViewRootImpl.<init>(ViewRootImpl.java:511)
//        at android.view.WindowManagerGlobal.addView(WindowManagerGlobal.java:338)
//        at android.view.WindowManagerImpl.addView(WindowManagerImpl.java:93)
//        at android.app.Dialog.show(Dialog.java:331)
//        at org.jetbrains.anko.AndroidDialogsKt.progressDialog(AndroidDialogs.kt:172)
//        at org.jetbrains.anko.AndroidDialogsKt.indeterminateProgressDialog(AndroidDialogs.kt:158)
//        at com.iisysgroup.payvice.fragments.WalletFragment$mProgressDialog$2.invoke(WalletFragment.kt:197)
//        at com.iisysgroup.payvice.fragments.WalletFragment$mProgressDialog$2.invoke(WalletFragment.kt:37)
//        at kotlin.SynchronizedLazyImpl.getValue(LazyJVM.kt:74)
//        at com.iisysgroup.payvice.fragments.WalletFragment.getMProgressDialog(WalletFragment.kt)
//        at com.iisysgroup.payvice.fragments.WalletFragment.access$getMProgressDialog$p(WalletFragment.kt:37)
//        at com.iisysgroup.payvice.fragments.WalletFragment$commissionTransfer$1$1$$special$$inlined$verticalLayout$lambda$1$1.onPinEntered(WalletFragment.kt:159)
//        at com.iisysgroup.payvice.PinAlertUtils$1.onComplete(PinAlertUtils.java:31)
//        at com.andrognito.pinlockview.PinLockView$1.onNumberClicked(PinLockView.java:56)
//        at com.andrognito.pinlockview.PinLockAdapter$NumberViewHolder$1.onClick(PinLockAdapter.java:191)
//        at android.view.View.performClick(View.java:6199)
//        at android.view.View$PerformClick.run(View.java:23235)
//        at android.os.Handler.handleCallback(Handler.java:836)
//        at android.os.Handler.dispatchMessage(Handler.java:103)
//        at android.os.Looper.loop(Looper.java:203)
//        at android.app.ActivityThread.main(ActivityThread.java:6251)
//        at java.lang.reflect.Method.invoke(Native Method)
//        at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:1073)
//        at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:934)
//        03-05 17:22:21.600 806-18455/? I/PPSWrapper: PPS onAfterAc

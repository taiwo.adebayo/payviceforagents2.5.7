package com.iisysgroup.payviceforagents.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.iisysgroup.payviceforagents.R;

public class TransactionHistoryActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_transaction_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}



package com.iisysgroup.payviceforagents.external;

import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.iisysgroup.payviceforagents.R;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */
public class CardCVVFragment extends CreditCardFragment {
    private EditText mCardCVVView;

    public CardCVVFragment() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle state) {
        View v = inflater.inflate(R.layout.lyt_card_cvv, group, false);
        this.mCardCVVView = (EditText) v.findViewById(com.cooltechworks.creditcarddesign.R.id.card_cvv);
        this.mCardCVVView.addTextChangedListener(this);
        String cvv = "";
        if (this.getArguments() != null && this.getArguments().containsKey("card_cvv")) {
            cvv = this.getArguments().getString("card_cvv");
        }

        if (cvv == null) {
            cvv = "";
        }

        this.mCardCVVView.setText(cvv);
        return v;
    }

    public void afterTextChanged(Editable s) {
        this.onEdit(s.toString());
        if (s.length() == 3) {
            this.onComplete();
        }

    }

    public void focus() {
        if (this.isAdded()) {
            this.mCardCVVView.selectAll();
        }

    }
}

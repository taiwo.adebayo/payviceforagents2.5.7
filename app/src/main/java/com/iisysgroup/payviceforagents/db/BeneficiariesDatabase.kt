//package com.iisysgroup.payviceforagents.db
//
//import android.arch.persistence.room.Database
//import android.arch.persistence.room.RoomDatabase
//import com.iisysgroup.payviceforagents.vas.airtime_and_data.DataBeneficiariesModel
//import com.iisysgroup.payviceforagents.vas.cable.DstvBeneficiariesModelimport
//import com.itex.richard.payviceconnect.model.AirtimeModel
//
//com.iisysgroup.payviceforagents.db.AirtimeBeneficiariesDaoimport com.iisysgroup.payviceforagents.db.DataBeneficiariesDaoimport com.iisysgroup.payviceforagents.db.DstvBeneficiariesDaoimport com.iisysgroup.payviceforagents.db.GotvBeneficiariesDao
//
//@Database(entities = [(AirtimeModel::class), (DataBeneficiariesModel::class), (DstvBeneficiariesModel::class)], version = 1, exportSchema = false)
//abstract class BeneficiariesDatabase() : RoomDatabase() {
//    abstract fun getAirtimeBeneficiariesDao() : AirtimeBeneficiariesDao
//
//    abstract fun getDataBeneficiariesDao() : DataBeneficiariesDao
//
//    //abstract fun getBankAccountBeneficiariesDao() : DstvBeneficiariesDao
//
//
//    abstract fun getDstvBeneficiariesDao() : DstvBeneficiariesDao
//
//    abstract fun getGotvBeneficiariesDao() : GotvBeneficiariesDao
//
//    /*
//    abstract fun getIkejaElectricBeneficiariesDao() : IkejaElecticBeneficiaries */
//
//}
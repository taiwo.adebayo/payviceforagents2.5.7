package com.iisysgroup.payviceforagents

import android.arch.lifecycle.LifecycleOwner
import android.content.Context
import android.util.Log
import com.iisysgroup.androidlite.cardpaymentprocessors.VasCommunicator
import com.iisysgroup.newland.NewlandDevice
import com.iisysgroup.poslib.commons.emv.EmvTransactionType
import com.iisysgroup.poslib.deviceinterface.interactors.EmvInteractor
import com.iisysgroup.poslib.host.HostInteractor
import com.iisysgroup.poslib.host.dao.PosLibDatabase
import com.iisysgroup.poslib.host.entities.ConfigData
import com.iisysgroup.poslib.host.entities.ConnectionData
import com.iisysgroup.poslib.host.entities.KeyHolder
import com.iisysgroup.poslib.host.entities.TransactionResult
import com.iisysgroup.poslib.utils.InputData
import com.iisysgroup.poslib.utils.TransactionData
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import org.jetbrains.anko.doAsync

class VasPurchase(owner: LifecycleOwner, val db: PosLibDatabase, val inputData: InputData,
                  val hostInteractor: HostInteractor, var connData: ConnectionData,
                  val emvInteractor: EmvInteractor, var configData: ConfigData, var keyHolder: KeyHolder, var context : Context)  {



    init {
        emvInteractor.observeStatus().subscribe {
            Log.d("VAS_PURCHASE", it.state.toString())
        }
    }


    fun getTransactionResult(): Single<TransactionResult> {

        doAsync {
            val mainkey = keyHolder.masterKey
            val pinkey = keyHolder.pinKey
            Log.d("okh", "$mainkey and $pinkey")

            //load the masterKey and the pinKey
            NewlandDevice.loadKeys(mainkey, pinkey)
        }

        val cardData = emvInteractor.startEmvTransaction(inputData.amount,
                inputData.additionalAmount, EmvTransactionType.EMV_PURCHASE).subscribeOn(Schedulers.io())

        return cardData.flatMap {
            val transactionData = TransactionData(inputData, it, configData, keyHolder)
            val  varsr = VasCommunicator(context, transactionData, connData, keyHolder)
                Single.fromCallable{
                    runBlocking {
                        varsr.processOnlineTransaction()
                }

            }
        }
    }





}



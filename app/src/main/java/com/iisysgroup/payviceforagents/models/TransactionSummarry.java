package com.iisysgroup.payviceforagents.models;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Itex-PC on 07/11/2018.
 */

public class TransactionSummarry {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("creditCount")
    @Expose
    private Integer creditCount;
    @SerializedName("creditAmount")
    @Expose
    private String creditAmount;
    @SerializedName("debitCount")
    @Expose
    private Integer debitCount;
    @SerializedName("debitAmount")
    @Expose
    private String debitAmount;
    @SerializedName("currentPage")
    @Expose
    private Integer currentPage;
    @SerializedName("lastPage")
    @Expose
    private Integer lastPage;
//    @SerializedName("products")
//    @Expose
//    private Product products;

    @SerializedName("products")
    @Expose
    private JsonElement products;

    public JsonElement getproduct() {

        return products;
    }

    public List<AllProducts> getResultsList() {
        List<AllProducts> resultList = new ArrayList<>(); // Initializing here just to cover the null pointer exception
        Gson gson = new Gson();
        if (getproduct() instanceof JsonObject) {
            resultList.add(gson.fromJson(getproduct(), AllProducts.class));
        } else if (getproduct() instanceof JsonArray) {
            Type founderListType = new TypeToken<ArrayList<AllProducts>>() {
            }.getType();
            resultList = gson.fromJson(getproduct(), founderListType);
        }
       return resultList;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Integer getCreditCount() {
        return creditCount;
    }

    public void setCreditCount(Integer creditCount) {
        this.creditCount = creditCount;
    }

    public String getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
        this.creditAmount = creditAmount;
    }

    public Integer getDebitCount() {
        return debitCount;
    }

    public void setDebitCount(Integer debitCount) {
        this.debitCount = debitCount;
    }

    public String getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(String debitAmount) {
        this.debitAmount = debitAmount;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }
//
//    public Product getProducts() {
//        return products;
//    }
//
//    public void setProducts(Product products) {
//        this.products = products;
//    }



}

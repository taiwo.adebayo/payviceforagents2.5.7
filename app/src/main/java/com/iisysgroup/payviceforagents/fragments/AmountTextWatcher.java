package com.iisysgroup.payviceforagents.fragments;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.DecimalFormat;

/**
 * Created by Bamitale@Itex on 16/05/2017.
 */
public class AmountTextWatcher implements TextWatcher {

    String temp;
    private EditText amountEdit;

    public AmountTextWatcher(EditText amountEdit) {
        this.amountEdit = amountEdit;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        temp = s.toString().replaceAll(",", "").trim();
    }

    @Override
    public void afterTextChanged(Editable s) {
        amountEdit.removeTextChangedListener(this);
        if (temp.length() > 0 && temp.charAt(0) != '\u20A6') {
            temp = DecimalFormat.getInstance().format(Integer.parseInt(temp));
        } else if (temp.length() == 1 && temp.charAt(0) == '\u20A6')
            temp = "";

        amountEdit.setText(temp);
        amountEdit.addTextChangedListener(this);
        amountEdit.setSelection(amountEdit.length());
    }
}

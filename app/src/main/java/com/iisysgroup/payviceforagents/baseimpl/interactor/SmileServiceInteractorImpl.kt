package com.iisysgroup.payviceforagents.baseimpl.interactor

import android.app.Application
import android.util.Log
import com.iisysgroup.payviceforagents.base.interactor.SmileServiceInteractor
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
import com.iisysgroup.payviceforagents.util.StringUtils
import com.iisysgroup.payviceforagents.util.payviceServices
import com.iisysgroup.payviceforagents.util.runSafelyOnBackground
import com.itex.richard.payviceconnect.model.SmileModel
import io.reactivex.Single


class SmileServiceInteractorImpl(private val context: Application) : SmileServiceInteractor {
    private val walletId by lazy {
        SecureStorage.retrieve(Helper.TERMINAL_ID, "")
    }

    private val userId by lazy {
        SecureStorage.retrieve(Helper.USER_ID, "")
    }

    private val userToken by lazy {
        SecureStorage.retrieve(Helper.STORED_PASSWORD, "")
    }

    override fun validateUser(accountId: String, walletId : String): Single<SmileModel.SmileValidateCustomerResponse> {
        val requestData = SmileModel.SmileValidateCustomerRequest(accountId, userId, userToken, null, walletId)

        return Single.fromObservable(context.payviceServices.SmileValidateCustomer(requestData))
                .runSafelyOnBackground()
    }

    override fun getBundles(): Single<SmileModel.SmileGetBundleResponse> {
        val requestDetails = SmileModel.SmileGetBudlesRequest(walletId, userId,
                userToken, null)

        return Single.fromObservable(context.payviceServices.SmileGetBundles(requestDetails))
                .runSafelyOnBackground()
    }

    override fun topUpAccount(accountId: String, amountInKobo: Int, encryptedPin: String): Single<SmileModel.SmileSuccessResponse> {
        val clientReference = StringUtils.getClientRef(context, "")
        val requestDetails = SmileModel.SmileTopUpRequestDetails(accountId,
                amountInKobo.toString(), walletId, userId, null, encryptedPin, "Cash", clientReference)
        Log.d("okh", requestDetails.toString())
        return Single.fromObservable(context.payviceServices.SmileTopUp(requestDetails))
                .runSafelyOnBackground()
    }

    override fun subscribe(accountId: String, plan: SmileModel.Bundle, encryptedPin: String): Single<SmileModel.SmileSuccessResponse> {
        val clientReference = StringUtils.getClientRef(context, "")
        val requestDetails = SmileModel.SmileSubscribe(accountId, walletId, plan.code.toString(),
                plan.price.toString(), userId, null, encryptedPin, "Cash", clientReference)

        return Single.fromObservable(context.payviceServices.SmileSubscribe(requestDetails)).runSafelyOnBackground()
    }
}
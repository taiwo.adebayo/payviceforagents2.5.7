package com.iisysgroup.payviceforagents.external;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

//import com.cooltechworks.creditcarddesign.pager.*;

//import com.cooltechworks.creditcarddesign.pager.CardNumberFragment;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */
public class CardFragmentAdapter extends FragmentStatePagerAdapter implements IActionListener {
    CardNumberFragment mCardNumberFragment;

    private CardExpiryFragment mCardExpiryFragment;
    private CardCVVFragment mCardCVVFragment = new CardCVVFragment();
    private CardNameFragment mCardNameFragment;
    private CardFragmentAdapter.ICardEntryCompleteListener mCardEntryCompleteListener;

    public CardFragmentAdapter(FragmentManager fm, Bundle args) {
        super(fm);
        this.mCardCVVFragment.setArguments(args);
        this.mCardNameFragment = new CardNameFragment();
        this.mCardNameFragment.setArguments(args);
        this.mCardNumberFragment = new CardNumberFragment();
        this.mCardNumberFragment.setArguments(args);
        this.mCardExpiryFragment = new CardExpiryFragment();
        this.mCardExpiryFragment.setArguments(args);
        this.mCardNameFragment.setActionListener(this);
        this.mCardNumberFragment.setActionListener(this);
        this.mCardExpiryFragment.setActionListener(this);
        this.mCardCVVFragment.setActionListener(this);
    }

    public void focus(int position) {
        ((IFocus) this.getItem(position)).focus();
    }

    public void setOnCardEntryCompleteListener(CardFragmentAdapter.ICardEntryCompleteListener listener) {
        this.mCardEntryCompleteListener = listener;
    }

    public Fragment getItem(int position) {
        return (new Fragment[]{this.mCardNumberFragment, this.mCardExpiryFragment, this.mCardCVVFragment, this.mCardNameFragment})[position];
    }

    public int getCount() {
        return 4;
    }

    public void onActionComplete(CreditCardFragment fragment) {
        int index = this.getIndex(fragment);
        if (index >= 0 && this.mCardEntryCompleteListener != null) {
            this.mCardEntryCompleteListener.onCardEntryComplete(index);
        }

    }

    public int getIndex(CreditCardFragment fragment) {
        byte index = -1;
        if (fragment == this.mCardNumberFragment) {
            index = 0;
        } else if (fragment == this.mCardExpiryFragment) {
            index = 1;
        } else if (fragment == this.mCardCVVFragment) {
            index = 2;
        } else if (fragment == this.mCardNameFragment) {
            index = 3;
        }

        return index;
    }

    public void onEdit(CreditCardFragment fragment, String edit) {
        int index = this.getIndex(fragment);
        if (index >= 0 && this.mCardEntryCompleteListener != null) {
            this.mCardEntryCompleteListener.onCardEntryEdit(index, edit);
        }

    }

    public interface ICardEntryCompleteListener {
        void onCardEntryComplete(int var1);

        void onCardEntryEdit(int var1, String var2);
    }


}

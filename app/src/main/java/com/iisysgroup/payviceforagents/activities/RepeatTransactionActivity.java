package com.iisysgroup.payviceforagents.activities;

import android.os.Bundle;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.fragments.AuthorizePinFragment;
import com.iisysgroup.payviceforagents.fragments.PaymentOptionFragment;
import com.iisysgroup.payviceforagents.fragments.RepeatTransactionFragment;
import com.iisysgroup.payviceforagents.fragments.StatusFragment;
import com.iisysgroup.payviceforagents.payviceservices.VasTransactionManager;
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.EmailHandler;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.PaymentOption;
import com.iisysgroup.payviceforagents.util.TransactionHistotyAdapter;

import java.util.Map;
import java.util.TreeMap;

public class RepeatTransactionActivity extends DefaultVasActivity implements
        RepeatTransactionFragment.OnFragmentInteractionListener,
        PaymentOptionFragment.OnFragmentInteractionListener {


    public static final String EXTRA_HISTORY = "history";


    TransactionHistotyAdapter.History history;
    String productString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repeat_transaction);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().hasExtra(EXTRA_HISTORY)) {
            history = (TransactionHistotyAdapter.History) getIntent().getSerializableExtra(EXTRA_HISTORY);
        } else {
            finish();
            return;
        }

        beneficiary = new Beneficiary(history.beneficiary, history.beneficiary,
                "");
        amount = Helper.sanitizeStringAmount(history.amount);
        productString = history.product;

        if (!history.isRecharge()) {
            is_bill_payment = true;
            convenienceFee = SecureStorage.retrieve(Helper.COMMISSION_KEY, Helper.default_commission);

        }

        loadFragment(RepeatTransactionFragment.getInstance(history));
    }


    @Override
    public void onFragmentInteraction(String amount) {
        this.amount = Helper.sanitizeStringAmount(amount);
        loadFragment(new AuthorizePinFragment());
    }


    @Override
    public void onAuthorizePinFragmentInteraction(String pin) {
        userPin = Helper.preparePin(pin);
        loadFragment(PaymentOptionFragment.getInstance(beneficiary, Integer.parseInt(amount), convenienceFee, PaymentOption.Mode.REPEAT));

    }

    @Override
    public void onPaymentOptionFragmentInteraction(PaymentOption paymentOption) {
        if (paymentOption == PaymentOption.WALLET) {

            if (Helper.hasInternetConnectivity(this)) {
//                new AlertDialog.Builder(this)
//                        .setTitle("Repeat Transaction")
//                        .setMessage("Proceed with transaction?")
//                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                validateCardRequest = null;
//                                purchaseRequest = null;
//                                new Thread(new Runnable() {
//                                    @Override
//                                    public void run() {
                doTransaction();
//                                    }
//                                }).start();
//
//                            }
//                        })
//                        .setNegativeButton(android.R.string.cancel, null)
//                        .show();
            }
        } else {
            super.onPaymentOptionFragmentInteraction(paymentOption);
        }

    }

    void doTransaction() {
        Helper.savePreference(getApplicationContext(), Helper.DOWNLOAD_BALANCE, true);
        showProgressDialog("Payvice", "Processing.... Please wait", false);

        final Map<String, String> params = new TreeMap<>();

        String action = getString(R.string.tams_webapi_action);
        String termID = SecureStorage.retrieve(Helper.TERMINAL_ID, "");
        String userId = SecureStorage.retrieve(Helper.USER_ID, "");


        params.put("action", action);
        params.put("amount", amount);
        params.put("control", "");
        params.put("pin", userPin);
        params.put("phoneno", beneficiary.getData());

        params.put("network", productString);
        params.put("termid", termID);
        params.put("userid", userId);
        params.put("op", "EXCHANGE");


        String url = context.getString(R.string.tams_url);
        transactionManager = new VasTransactionManager(url, params);

        statusObject.setStatus(false);
        statusObject.setStatusAmount(Double.parseDouble(amount));

        statusObject.statusRecipient = beneficiary.getDisplayInfo();
        statusObject.statusService = productString;

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    VasResult result = transactionManager.processTransaction();
                    String message = result.message, balance = result.balance;

                    statusObject.statusReason = message;
                    if (result.result == VasResult.Result.APPROVED) {
                        statusObject.setStatus(true);
                        EmailHandler.sendNotificationMail(RepeatTransactionActivity.this, statusObject);
                    }

                } catch (IllegalStateException e) {
                    statusObject.statusReason = e.getMessage();
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    statusObject.statusReason = "An error occurred. Please try again later";
                } finally {
                    dismissProgressDialog();
                    Helper.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadFragment(StatusFragment.getInstance(statusObject));
                        }
                    });
                }
            }
        }).start();

    }
}

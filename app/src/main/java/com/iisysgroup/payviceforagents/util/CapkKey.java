package com.iisysgroup.payviceforagents.util;

import com.newland.mtype.util.ISOUtils;

/**
 * Created by Bamitale@Itex on 31/05/2017.
 */

public class CapkKey {

     byte [] exponent = {},
             modulus = {},
             checkSum = {},
             keyIndex = {},
             expiryDate = {} ;
     int keyLength;

    /**
     *
     * @param exponent
     * @param modulus
     * @param checkSum
     * @param keyIndex
     * @param expiryDate
     */
    public CapkKey(byte[] modulus,byte[] exponent,  byte[] checkSum, byte[] keyIndex, byte[] expiryDate, int keyLength) {
        this.modulus = modulus;
        this.exponent = exponent;
        this.checkSum = checkSum;
        this.keyIndex = keyIndex;
        this.expiryDate = expiryDate;
        this.keyLength = keyLength;
    }

    /**
     *
     * @param exponent
     * @param modulus
     * @param checkSum
     * @param keyIndex
     * @param expiryDate
     */
    public CapkKey(String modulus, String exponent, String checkSum, String keyIndex, String expiryDate, int keyLength) {
        this(ISOUtils.hex2byte(sanitize(modulus)), ISOUtils.hex2byte(sanitize(exponent)),
                ISOUtils.hex2byte(sanitize(checkSum).substring(0,16)),
                ISOUtils.hex2byte(sanitize(keyIndex)), ISOUtils.hex2byte(sanitize(expiryDate)), keyLength);
    }


    static String sanitize(String string){
        return string.replaceAll("\n", "").replaceAll("\t","").replaceAll(" ","").trim();
    }

    public byte[] getExponent() {
        return exponent;
    }

    public byte[] getModulus() {
        return modulus;
    }

    public byte[] getCheckSum() {
        return checkSum;
    }

    public byte[] getKeyIndex() {
        return keyIndex;
    }

    public byte[] getExpiryDate() {
        return expiryDate;
    }

    public int getKeyLength() {
        return keyLength;
    }
}

package com.iisysgroup.payviceforagents.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.iisysgroup.payviceforagents.activities.MainActivity;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.securestorage.SecureStorageUtils;
import com.iisysgroup.payviceforagents.util.Helper;

import java.util.HashMap;
import java.util.Map;


public class ActivateUserFragment extends Fragment {


    EditText passwordEdit, confirmEdit;
    TextInputLayout passwordTIL, confirmTIL;
    EditText userIdEdit, pinEdit, confirmPinEdit;
    TextInputLayout pinTIL, confirmPinTIL, userIdTIL;

    ProgressDialog progressDialog;
    Handler handler = new Handler();
    DialogInterface.OnClickListener action = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            NavUtils.navigateUpTo(getActivity(), new Intent(getActivity(), MainActivity.class));
        }
    };


    public ActivateUserFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_activate_user, container, false);

        getActivity().setTitle("Account Activation");

        userIdEdit = view.findViewById(R.id.userIdEdit);
        userIdTIL = view.findViewById(R.id.userIdTIL);

        passwordEdit = view.findViewById(R.id.passwordEdit);
        confirmEdit = view.findViewById(R.id.confirmPasswordEdit);

        passwordTIL = view.findViewById(R.id.passwordTIL);
        confirmTIL = view.findViewById(R.id.confirmPasswordTIL);

        pinEdit = view.findViewById(R.id.pinEdit);
        confirmPinEdit = view.findViewById(R.id.confirmPinEdit);

        pinTIL = view.findViewById(R.id.pinTIL);
        confirmPinTIL = view.findViewById(R.id.confirmPinTIL);

        view.findViewById(R.id.activate_account_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Helper.hasInternetConnectivity(getActivity()))
                    doActivation();

            }
        });

        return view;
    }

    boolean isValidated() {

        if (userIdEdit.getText().toString().isEmpty()) {
            userIdTIL.setErrorEnabled(true);
            userIdTIL.setError("User ID cannot be left empty");
            Helper.showErrorAnim(userIdEdit);
            userIdEdit.requestFocus();

            return false;
        }

        if (userIdEdit.getText().toString().contains(" ")) {
            userIdTIL.setErrorEnabled(true);
            userIdTIL.setError("User ID cannot contain empty spaces");
            Helper.showErrorAnim(userIdEdit);
            userIdEdit.requestFocus();

            return false;
        }

        String password = passwordEdit.getText().toString();

        if (password.length() < 8) {
            passwordTIL.setErrorEnabled(true);
            passwordTIL.setError("password should not be less than 8 characters");
            Helper.showErrorAnim(passwordEdit);
            passwordEdit.requestFocus();
            return false;
        }

        if (!Helper.checkPasswordStrength(password)) {
            passwordTIL.setErrorEnabled(true);
            passwordTIL.setError("Password should contain at least an upper case letter and a number");
            Helper.showErrorAnim(passwordEdit);
            passwordEdit.requestFocus();
            return false;
        }

        if (!password.equals(confirmEdit.getText().toString())) {
            confirmTIL.setErrorEnabled(true);
            confirmTIL.setError("password doesn't match");
            Helper.showErrorAnim(confirmEdit);
            confirmEdit.requestFocus();
            return false;
        }

        String pin = pinEdit.getText().toString();

        if (pin.length() < 4) {
            pinTIL.setErrorEnabled(true);
            pinTIL.setError("pin should not be less than 4 characters");
            Helper.showErrorAnim(pinEdit);
            pinEdit.requestFocus();
            return false;
        }

        if (!pin.equals(confirmPinEdit.getText().toString())) {
            confirmPinTIL.setErrorEnabled(true);
            confirmPinTIL.setError("pin doesn't match");
            Helper.showErrorAnim(confirmPinEdit);
            confirmPinEdit.requestFocus();
            return false;
        }


        return true;
    }

    void doActivation() {
        if (isValidated()) {
            progressDialog = ProgressDialog.show(getActivity(), "Activating Account", "Please wait...", true, false);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        activate();
                    } catch (IllegalAccessException e) {
                        Helper.showInfoDialog(getActivity(), "Account activation failed",
                                e.getMessage());
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Helper.showInfoDialog(getActivity(), "Account activation failed",
                                "Please check your details and try again.");

                    } finally {
                        dismissProgressDialog();
                    }
                }
            }).start();
        }
    }

    void activate() throws Exception {
        Map<String, String> params = new HashMap<>();

        String url = getResources().getString(R.string.tams_url);

        String TAMS_LOGIN_ACTION = getString(R.string.tams_login_action);
        String initControl = getString(R.string.tams_init_action);
        String terminalID = getString(R.string.tams_default_terminal_id);

        String key = null;
        String mUserId = userIdEdit.getText().toString();

        //for init set CONTROL = INIT
        params.put("action", TAMS_LOGIN_ACTION);
        params.put("termid", terminalID);
        params.put("userid", mUserId);
        params.put("control", initControl);

        VasResult tamsResult = Requests.processRequest(url, params);

        terminalID = tamsResult.macrosTID;

        if (tamsResult.result == VasResult.Result.APPROVED)
            key = tamsResult.message.trim();

        if (key != null && !key.isEmpty()) {
            params.clear();

            String[] temp = key.split("\\|");
            key = temp[0] + "|" + temp[1];

            String PIN_ACTION = "TAMS_PIN_UPDATE";
            String password = passwordEdit.getText().toString();
            String pin = pinEdit.getText().toString();
            String clrKey = Helper.decryptUserKey(terminalID, key);


            String ePassword = SecureStorageUtils.hashIt(password, clrKey);

            String pinSalt = SecureStorageUtils.hashIt(terminalID + key, clrKey);
            String hashedPin = SecureStorageUtils.hashIt(pin, pinSalt);
            String ePin = SecureStorageUtils.hashIt(pin, hashedPin);


            params.put("action", PIN_ACTION);
            params.put("userid", mUserId);
            params.put("termid", terminalID);
            params.put("password", "1234");
            params.put("newpassword", ePassword);
            params.put("pin", "1234");
            params.put("newpin", ePin);

            tamsResult = Requests.processRequest(url, params);

            if (tamsResult.result == VasResult.Result.APPROVED) {
                Helper.showInfoDialogWithAction(getActivity(),
                        "Account Activation Successful", "Your account has been successfully activated", action);
            } else
                throw new IllegalAccessException(tamsResult.message);

        } else
            throw new IllegalAccessException("Activation failed. Please check your input and try again");
    }

    void dismissProgressDialog() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        });
    }


}

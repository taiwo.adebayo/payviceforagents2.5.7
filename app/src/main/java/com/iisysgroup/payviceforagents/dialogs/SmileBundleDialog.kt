package com.iisysgroup.payviceforagents.dialogs

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.iisysgroup.payviceforagents.R
import com.itex.richard.payviceconnect.model.SmileModel
import kotlinx.android.synthetic.main.fragment_plan_list_dialog_item.view.*


class SmileBundleDialog : BasePlanDialogFragment<SmileBundleDialog.ViewHolder>() {
    var list: List<SmileModel.Bundle> = ArrayList()

    override fun getAdapter() = Adapter(list)


    inner class ViewHolder internal constructor(inflater: LayoutInflater, parent: ViewGroup)
        : RecyclerView.ViewHolder(inflater.inflate(R.layout.fragment_plan_list_dialog_item, parent, false)) {

        private val descriptionTextView = itemView.descriptionText
        private val amountTextView = itemView.amountText

        init {
            itemView.setOnClickListener {

                selector?.invoke(adapterPosition)
            }
        }

        fun bindView(item: SmileModel.Bundle) {
            descriptionTextView.text = item.name
            amountTextView.text = "\u20A6 ${item.displayPrice / 100}"
        }
    }

    inner class Adapter(private val list: List<SmileModel.Bundle>) :
            RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            holder.bindView(list[position])
        }

        override fun getItemCount(): Int {
            return list.size
        }


    }


    companion object {
        fun newInstance(dataList: List<SmileModel.Bundle>,
                        selectionHandler: (Int) -> Unit): SmileBundleDialog =
                SmileBundleDialog().apply {
                    this.list = dataList
                    this.selector = selectionHandler
                }

    }
}

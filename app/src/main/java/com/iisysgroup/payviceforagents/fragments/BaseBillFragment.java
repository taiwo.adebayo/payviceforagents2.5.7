package com.iisysgroup.payviceforagents.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.callbacks.SelectedItemCallback;
import com.iisysgroup.payviceforagents.entities.PinValue;
import com.iisysgroup.payviceforagents.entities.PlanValue;
import com.iisysgroup.payviceforagents.entities.Service;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.HistoryAdapter;
import com.iisysgroup.payviceforagents.util.SingleImageTitleObject;
import com.iisysgroup.payviceforagents.util.VasServices;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Bamitale@Itex on 05/05/2017.
 */

public class BaseBillFragment extends Fragment {

    protected static final String SERVICE = "param1";

    @BindView(R.id.serviceImage)
    ImageView serviceImage;
    @BindView(R.id.subTitleText)
    TextView subTitleText;
    @BindView(R.id.productText)
    TextView serviceText;
    @BindView(R.id.selectProductBtn)
    View selectServiceLayout;
    @BindView(R.id.proceedBtn)
    Button proceedButton;

    @Nullable
    @BindView(R.id.amountTIL)
    TextInputLayout amountTIL;
    @Nullable
    @BindView(R.id.amountEdit)
    EditText amountEdit;

    @Nullable
    @BindView(R.id.selectAmountLayout)
    View selectPlanLayout;
    @Nullable
    @BindView(R.id.selectableAmountText)
    TextView pinAmountText;

    @BindView(R.id.historyLayout)
    LinearLayout historyLayout;
    @BindView(android.R.id.list)
    RecyclerView list;

    List<HistoryAdapter.History> historyList = new ArrayList<>();
    HistoryAdapter historyAdapter;

    Service service;
    Service.Product product;
    PlanValue planValue;
    PinValue pinValue;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String serviceString = getArguments().getString(SERVICE);
            service = VasServices.SERVICES.get(serviceString);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        historyAdapter = new HistoryAdapter(getActivity(), historyList);
        list.setAdapter(historyAdapter);
        subTitleText.setText(service.name);
        serviceImage.setImageResource(service.icon);

        if (service.products.length == 1) {
            product = service.products[0];
            serviceText.setText(product.name);
            selectServiceLayout.setEnabled(false);
        }

        if (amountEdit != null) {
            amountEdit.addTextChangedListener(new AmountTextWatcher(amountEdit));
        }

        previousRecharge();
    }


    @OnClick(R.id.selectProductBtn)
    void selectProduct() {
        final BottomSheetDialog networkDialog = new BottomSheetDialog(getActivity());

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.bottom_sheet_list_layout, null, false);
        networkDialog.setContentView(view);

        TextView titleView = view.findViewById(R.id.titleText);
        titleView.setText(String.format("Select %s Product", service));

        ListView listView = view.findViewById(android.R.id.list);
        final List<SingleImageTitleObject> itemList = new ArrayList<>();

        for (Service.Product product : service.products) {
            itemList.add(new SingleImageTitleObject(product.name, service.icon));
        }

        listView.setAdapter(new SingleImageTitleObject.SingleImageTitleAdapter(itemList, getActivity(),
                R.layout.bottom_sheet_list_item));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                product = service.products[position];
                serviceText.setText(product.name);
                networkDialog.dismiss();
            }
        });

        networkDialog.show();
    }

    void previousRecharge() {
        final String historySerial = Helper.getPreference(getActivity(), Helper.HISTORY_SERIAL, "");

        if (!historySerial.trim().isEmpty()) {
            final List<HistoryAdapter.History> tempList = HistoryAdapter.processRecords(historySerial);
            historyList.clear();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (HistoryAdapter.History history : tempList) {
                        if (historyList.size() > 2) break;
                        for (Service.Product product : service.products) {
                            if (history.product.equals(product.requestCode)) {
                                historyList.add(history);
                                break;
                            }
                        }
                    }

                    if (historyList.isEmpty()) {
                        hideHistory();
                    } else {
                        historyAdapter.notifyDataSetChanged();
                    }

                }
            }).start();


        } else {
            hideHistory();
        }

    }

    void hideHistory() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                historyLayout.setVisibility(View.GONE);
            }
        });
    }


    protected void selectPlan() {
        final PlanListDialog planListDialog = new PlanListDialog(getActivity(), service.type, product,
                new SelectedItemCallback<PlanValue>() {
                    @Override
                    public void onItemSelected(PlanValue item) {
                        planValue = item;
                        pinAmountText.setText(String.format("%d - %s", item.value, item.description));
                    }
                });


        planListDialog.show();
    }

    protected void selectPin() {
        final PinListDialog pinListDialog = new PinListDialog(getActivity(), service.type, product,
                new SelectedItemCallback<PinValue>() {
                    @Override
                    public void onItemSelected(PinValue item) {
                        pinValue = item;
                        pinAmountText.setText(String.valueOf(pinValue.value));
                    }
                });

        pinListDialog.show();
    }


}

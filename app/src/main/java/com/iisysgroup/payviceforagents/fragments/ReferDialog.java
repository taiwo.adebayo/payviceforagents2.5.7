package com.iisysgroup.payviceforagents.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.activities.BaseActivity;
import com.iisysgroup.payviceforagents.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Bamitale@Itex on 03/08/2017.
 */

public class ReferDialog extends DialogFragment implements RadioButton.OnCheckedChangeListener {

    @BindView(R.id.referPromoLayout)
    LinearLayout referPromoLayout;
    @BindView(R.id.prImageb)
    ImageView prImageb;
    @BindView(R.id.fontTextView2)
    TextView fontTextView2;
    @BindView(R.id.facebookCheckBox)
    RadioButton facebookCheckBox;
    @BindView(R.id.contactCheckBox)
    RadioButton contactCheckBox;
    @BindView(R.id.showQrCheckBox)
    RadioButton showQrCheckBox;
    ImageView imageView;

    @OnClick(R.id.cancel_button)
    void cancel() {
        this.dismiss();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_referral_option, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        facebookCheckBox.setOnCheckedChangeListener(this);
        contactCheckBox.setOnCheckedChangeListener(this);
        showQrCheckBox.setOnCheckedChangeListener(this);

        view.findViewById(R.id.cancel_button_ic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            switch (buttonView.getId()) {
                case R.id.facebookCheckBox:
                    ((BaseActivity) getActivity()).sendFacebookInvite();
                    break;
                case R.id.contactCheckBox:
                    ((BaseActivity) getActivity()).sendGoogleAppInvite();
                    break;
                case R.id.showQrCheckBox:
                    ((BaseActivity) getActivity()).showReferralQrDialog();
                    break;
            }

            dismiss();
        }
    }


}

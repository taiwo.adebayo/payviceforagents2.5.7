package com.iisysgroup.payviceforagents.base.presenter

import android.arch.lifecycle.LiveData
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.itex.richard.payviceconnect.model.DataModel

interface AirtimePresenter : BaseServicePresenter {
    fun loadPlans(): LiveData<List<DataModel.DataResponseElements>>
    fun setSelectedPlan(position: Int)

    fun purchaseVtu(amount: Int, phoneNumber: String, authPin: String)
    fun purchaseVtWithCard(amount: Int, phoneNumber: String, card: Card?, processor: DebitCardProcessor)

    fun purchasePin(amount: Int, phoneNumber: String, authPin: String)
    fun purchasePinWithCard(amount: Int, phoneNumber: String, card: Card?, processor: DebitCardProcessor)


    fun purchaseData(phoneNumber: String, authPin: String)
    fun purchaseDataWithCard(phoneNumber: String, card: Card?, processor: DebitCardProcessor)

    fun purchasePinToSms(phoneNumber: String, authPin: String)
    fun purchasePinToSmsWithCard(phoneNumber: String, card: Card?, processor: DebitCardProcessor)
}
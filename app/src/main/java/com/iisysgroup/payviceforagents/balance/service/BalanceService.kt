package com.iisysgroup.payviceforagents.balance.service

import com.iisysgroup.payviceforagents.balance.model.CommissionTransferResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface BalanceService {
    @GET("transactionadvice.php")
//    fun transferCommissionToBalance(@Query("action") action : String = "TAMS_WEBAPI", @Query("control") control : String = "ComTrans", @Query("userid") userid : String, @Query("pin") pin : String, @Query("amount") amountInKobo : String, @Query("termid") termid : String) : Call<CommissionTransferResponse>

    fun transferCommissionToBalance(@Query("action") action : String = "TAMS_WEBAPI", @Query("control") control : String = "ComTrans", @Query("userid") userid : String, @Query("pin") pin : String, @Query("amount") amountInKobo : String, @Query("termid") termid : String) : Deferred<CommissionTransferResponse>

    @GET()
    fun getBalance()

    companion object {
        private val base_url = "http://197.253.19.75/ctms/eftpos/devinterface/"
        //private val base_url = "http://197.253.19.78/ctms/eftpos/devinterface/"


        private val logging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        private val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()

        fun getService() : BalanceService {

            val interceptor = HttpLoggingInterceptor()

            interceptor.level = HttpLoggingInterceptor.Level.BODY
             var client = OkHttpClient.Builder ()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                     .addInterceptor( object : Interceptor{
                         override fun intercept(chain: Interceptor.Chain?): Response {
                             var original : Request = chain!!.request()

                             var request  = original.newBuilder()
                                     .header("User-Agent","ANDROIDPHONE")
                                     .method(original.method(),original.body())
                                     .build()
                             var  response : Response = chain.proceed(request)

                             return  response

                         }
                     })
                     .build()


            val retrofit = Retrofit.Builder()
                    .baseUrl(base_url)
                    .client(client)
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(BalanceService::class.java)
        }
    }


}
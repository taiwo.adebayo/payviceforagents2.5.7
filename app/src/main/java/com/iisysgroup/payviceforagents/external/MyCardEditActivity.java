package com.iisysgroup.payviceforagents.external;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.activities.BaseActivity;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.persistence.entitiy.Card;
import com.iisysgroup.payviceforagents.util.Helper;


public class MyCardEditActivity extends BaseActivity {
    int mLastPageSelected = 0;
    private CreditCardView mCreditCardView;
    private String mCardNumber;
    private String mCVV;
    private String mCardHolderName;
    private String mExpiry;
    private CardFragmentAdapter mCardAdapter;

    public MyCardEditActivity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.setContentView(R.layout.activity_card_edit);
        this.findViewById(R.id.next).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ViewPager pager = MyCardEditActivity.this.findViewById(R.id.card_field_container_pager);
                int max = pager.getAdapter().getCount();
                if (pager.getCurrentItem() == max - 1) {
                    MyCardEditActivity.this.onDoneTapped();
                } else {
                    MyCardEditActivity.this.showNext();
                }
            }
        });


        this.findViewById(R.id.previous).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                MyCardEditActivity.this.showPrevious();
            }
        });
        this.setKeyboardVisibility(true);
        this.mCreditCardView = this.findViewById(R.id.credit_card_view);
        if (savedInstanceState != null) {
            this.checkParams(savedInstanceState);
        } else {
            this.checkParams(this.getIntent().getExtras());
        }

        this.loadPager();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.default_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cancel:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void checkParams(Bundle bundle) {
        if (bundle != null) {
            this.mCardHolderName = bundle.getString("card_holder_name");
            this.mCVV = bundle.getString("card_cvv");
            this.mExpiry = bundle.getString("card_expiry");
            this.mCardNumber = bundle.getString("card_number");
            this.mCreditCardView.setCVV(this.mCVV);
            this.mCreditCardView.setCardHolderName(this.mCardHolderName);
            this.mCreditCardView.setCardExpiry(this.mExpiry);
            this.mCreditCardView.setCardNumber(this.mCardNumber);
            if (this.mCardAdapter != null) {
                this.mCardAdapter.notifyDataSetChanged();
            }

        }
    }

    public void refreshNextButton() {
        ViewPager pager = this.findViewById(R.id.card_field_container_pager);
        int max = pager.getAdapter().getCount();
        int text = R.string.next;
        if (pager.getCurrentItem() == max - 1) {
            text = R.string.done;
        }

        ((TextView) this.findViewById(R.id.next)).setText(text);
    }

    public void loadPager() {
        ViewPager pager = this.findViewById(R.id.card_field_container_pager);
        pager.addOnPageChangeListener(new OnPageChangeListener() {
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                MyCardEditActivity.this.mCardAdapter.focus(position);
                if (position == 2) {
                    MyCardEditActivity.this.mCreditCardView.showBack();
                } else if (position == 1 && MyCardEditActivity.this.mLastPageSelected == 2 || position == 3) {
                    MyCardEditActivity.this.mCreditCardView.showFront();
                }

                MyCardEditActivity.this.mLastPageSelected = position;
                MyCardEditActivity.this.refreshNextButton();
            }

            public void onPageScrollStateChanged(int state) {
            }
        });
        pager.setOffscreenPageLimit(4);
        this.mCardAdapter = new CardFragmentAdapter(this.getSupportFragmentManager(), this.getIntent().getExtras());
        this.mCardAdapter.setOnCardEntryCompleteListener(new CardFragmentAdapter.ICardEntryCompleteListener() {
            public void onCardEntryComplete(int currentIndex) {
                MyCardEditActivity.this.showNext();
            }

            public void onCardEntryEdit(int currentIndex, String entryValue) {
                switch (currentIndex) {
                    case 0:
                        MyCardEditActivity.this.mCardNumber = entryValue.replace(" ", "");
                        MyCardEditActivity.this.mCreditCardView.setCardNumber(MyCardEditActivity.this.mCardNumber);
                        break;
                    case 1:
                        MyCardEditActivity.this.mExpiry = entryValue;
                        MyCardEditActivity.this.mCreditCardView.setCardExpiry(entryValue);
                        break;
                    case 2:
                        MyCardEditActivity.this.mCVV = entryValue;
                        MyCardEditActivity.this.mCreditCardView.setCVV(entryValue);
                        break;
                    case 3:
                        MyCardEditActivity.this.mCardHolderName = entryValue;
                        MyCardEditActivity.this.mCreditCardView.setCardHolderName(entryValue);
                }

            }
        });
        pager.setAdapter(this.mCardAdapter);
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString("card_cvv", this.mCVV);
        outState.putString("card_holder_name", this.mCardHolderName);
        outState.putString("card_expiry", this.mExpiry);
        outState.putString("card_number", this.mCardNumber);
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        this.checkParams(inState);
    }

    public void showPrevious() {
        ViewPager pager = this.findViewById(R.id.card_field_container_pager);
        int currentIndex = pager.getCurrentItem();
        if (currentIndex - 1 >= 0) {
            pager.setCurrentItem(currentIndex - 1);
        }

        this.refreshNextButton();
    }

    public void showNext() {
        ViewPager pager = this.findViewById(R.id.card_field_container_pager);
        CardFragmentAdapter adapter = (CardFragmentAdapter) pager.getAdapter();
        int max = adapter.getCount();
        int currentIndex = pager.getCurrentItem();
        if (currentIndex + 1 < max) {
            pager.setCurrentItem(currentIndex + 1);
        } else {
            this.setKeyboardVisibility(false);
        }

        this.refreshNextButton();
    }

    private void onDoneTapped() {


        if (this.mCVV.isEmpty()) {
            Helper.showSnackBar(this, "Invalid CVV");
            return;
        }

        if (this.mExpiry.isEmpty()) {
            Helper.showSnackBar(this, "Invalid expiry");
            return;
        }

        if (Card.isValidLuhnNumber(Card.normalizeCardNumber(this.mCardNumber))) {
            Intent intent = new Intent();
            intent.putExtra("card_cvv", this.mCVV);
            intent.putExtra("card_holder_name", this.mCardHolderName);
            intent.putExtra("card_expiry", this.mExpiry);
            intent.putExtra("card_number", this.mCardNumber);
            this.setResult(-1, intent);
            this.finish();
        } else {
            Helper.showSnackBar(this, "Invalid card number");
        }

    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.hardKeyboardHidden == 1) {
            LinearLayout parent = this.findViewById(R.id.parent);
            LayoutParams layoutParams = (LayoutParams) parent.getLayoutParams();
            layoutParams.addRule(13, 0);
            parent.setLayoutParams(layoutParams);
        }

    }

    private void setKeyboardVisibility(boolean visible) {
        EditText editText = this.findViewById(R.id.card_number_field);
        if (!visible) {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        } else {
            this.getWindow().setSoftInputMode(5);
        }

    }
}

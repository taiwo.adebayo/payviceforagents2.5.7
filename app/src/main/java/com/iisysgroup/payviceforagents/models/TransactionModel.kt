package com.iisysgroup.payviceforagents.models

import com.iisysgroup.poslib.host.Host
import java.io.Serializable

/**
 * Created by Agbede on 2/28/2018.
 */
data class TransactionModel(val terminalID : String, val authID : String, val stan : String, val rrn : String, val cardholderName : String, val pan : String, val expiryDate : String, val amount : Long, val additionalAmount : Long, val transactionType : Host.TransactionType, val responseCode : String, val isApproved : Boolean, val transactionStatus : String, val transactionStatusReason : String, val accountType : String, val merchantId : String, val OFIC : String, val ISODateTime : String) : Serializable
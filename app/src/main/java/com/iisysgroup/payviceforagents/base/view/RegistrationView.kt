package com.iisysgroup.payviceforagents.base.view

interface RegistrationView {

    fun onNameValidationError()

    fun onEmailValidationError()

    fun onPhoneValidationError()

    fun onPasswordValidationError()

    fun onPinValidationError()

    fun onShowProgress()

    fun onDismissProgress()

    fun onDisplayReferral(code: String)

    fun onRegistrationSuccess()

    fun setRegistrationError(error: Throwable?)

}
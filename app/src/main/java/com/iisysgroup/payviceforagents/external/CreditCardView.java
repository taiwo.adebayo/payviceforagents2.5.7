package com.iisysgroup.payviceforagents.external;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */

public class CreditCardView extends FrameLayout {
    private static final int TEXTVIEW_CARD_HOLDER_ID;
    private static final int TEXTVIEW_CARD_EXPIRY_ID;
    private static final int TEXTVIEW_CARD_NUMBER_ID;
    private static final int TEXTVIEW_CARD_CVV_ID;
    private static final int FRONT_CARD_ID;
    private static final int BACK_CARD_ID;
    private static final int FRONT_CARD_OUTLINE_ID;
    private static final int BACK_CARD_OUTLINE_ID;

    static {
        TEXTVIEW_CARD_HOLDER_ID = com.cooltechworks.creditcarddesign.R.id.front_card_holder_name;
        TEXTVIEW_CARD_EXPIRY_ID = com.cooltechworks.creditcarddesign.R.id.front_card_expiry;
        TEXTVIEW_CARD_NUMBER_ID = com.cooltechworks.creditcarddesign.R.id.front_card_number;
        TEXTVIEW_CARD_CVV_ID = com.cooltechworks.creditcarddesign.R.id.back_card_cvv;
        FRONT_CARD_ID = com.cooltechworks.creditcarddesign.R.id.front_card_container;
        BACK_CARD_ID = com.cooltechworks.creditcarddesign.R.id.back_card_container;
        FRONT_CARD_OUTLINE_ID = com.cooltechworks.creditcarddesign.R.id.front_card_outline;
        BACK_CARD_OUTLINE_ID = com.cooltechworks.creditcarddesign.R.id.back_card_outline;
    }

    private int mCurrentDrawable;
    private String mRawCardNumber;
    private CreditCardView.ICustomCardSelector mSelectorLogic;
    private String mCardHolderName;
    private String mCVV;
    private String mExpiry;

    public CreditCardView(Context context) {
        super(context);
        this.init();
    }

    public CreditCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(attrs);
    }

    public CreditCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(attrs);
    }

    public String getCardHolderName() {
        return this.mCardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        cardHolderName = cardHolderName == null ? "" : cardHolderName;
        if (cardHolderName.length() > 16) {
            cardHolderName = cardHolderName.substring(0, 16);
        }

        this.mCardHolderName = cardHolderName;
        ((TextView) this.findViewById(TEXTVIEW_CARD_HOLDER_ID)).setText(cardHolderName);
    }

    public String getCVV() {
        return this.mCVV;
    }

    public void setCVV(String cvv) {
        if (cvv == null) {
            cvv = "";
        }

        this.mCVV = cvv;
        ((TextView) this.findViewById(TEXTVIEW_CARD_CVV_ID)).setText(cvv);
    }

    public void setCVV(int cvvInt) {
        if (cvvInt == 0) {
            this.setCVV("");
        } else {
            String cvv = String.valueOf(cvvInt);
            this.setCVV(cvv);
        }

    }

    public String getExpiry() {
        return this.mExpiry;
    }

    private void init() {
        this.mCurrentDrawable = com.cooltechworks.creditcarddesign.R.drawable.card_color_round_rect_default;
        this.mRawCardNumber = "";
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(com.cooltechworks.creditcarddesign.R.layout.view_creditcard, this, true);
    }

    private void init(AttributeSet attrs) {
        this.init();
        TypedArray a = this.getContext().obtainStyledAttributes(attrs, com.cooltechworks.creditcarddesign.R.styleable.creditcard, 0, 0);
        String cardHolderName = a.getString(com.cooltechworks.creditcarddesign.R.styleable.creditcard_card_holder_name);
        String expiry = a.getString(com.cooltechworks.creditcarddesign.R.styleable.creditcard_card_expiration);
        String cardNumber = a.getString(com.cooltechworks.creditcarddesign.R.styleable.creditcard_card_number);
        int cvv = a.getInt(com.cooltechworks.creditcarddesign.R.styleable.creditcard_cvv, 0);
        int cardSide = a.getInt(com.cooltechworks.creditcarddesign.R.styleable.creditcard_card_side, 1);
        this.setCardNumber(cardNumber);
        this.setCVV(cvv);
        this.setCardExpiry(expiry);
        this.setCardHolderName(cardHolderName);
        if (cardSide == 0) {
            this.showBackImmediate();
        }

        this.paintCard();
        a.recycle();
    }

    private void flip(boolean ltr, boolean isImmediate) {
        View layoutContainer = this.findViewById(com.cooltechworks.creditcarddesign.R.id.card_outline_container);
        View frontView = this.findViewById(FRONT_CARD_OUTLINE_ID);
        View backView = this.findViewById(BACK_CARD_OUTLINE_ID);
        View frontContentView = this.findViewById(FRONT_CARD_ID);
        View backContentView = this.findViewById(BACK_CARD_ID);
        View layoutContentContainer = this.findViewById(com.cooltechworks.creditcarddesign.R.id.card_container);
        if (isImmediate) {
            frontContentView.setVisibility(ltr ? VISIBLE : GONE);
            backContentView.setVisibility(ltr ? GONE : VISIBLE);
        } else {
            short duration = 600;
            FlipAnimator flipAnimator = new FlipAnimator(frontView, backView, frontView.getWidth() / 2, backView.getHeight() / 2);
            flipAnimator.setInterpolator(new OvershootInterpolator(0.5F));
            flipAnimator.setDuration((long) duration);
            if (ltr) {
                flipAnimator.reverse();
            }

            flipAnimator.setTranslateDirection(3);
            flipAnimator.setRotationDirection(2);
            layoutContainer.startAnimation(flipAnimator);
            FlipAnimator flipAnimator1 = new FlipAnimator(frontContentView, backContentView, frontContentView.getWidth() / 2, backContentView.getHeight() / 2);
            flipAnimator1.setInterpolator(new OvershootInterpolator(0.5F));
            flipAnimator1.setDuration((long) duration);
            if (ltr) {
                flipAnimator1.reverse();
            }

            flipAnimator1.setTranslateDirection(3);
            flipAnimator1.setRotationDirection(2);
            layoutContentContainer.startAnimation(flipAnimator1);
        }

    }

    public void showFront() {
        this.flip(true, false);
    }

    public void showFrontImmediate() {
        this.flip(true, true);
    }

    public void showBack() {
        this.flip(false, false);
    }

    public void showBackImmediate() {
        this.flip(false, true);
    }

    public void setCardExpiry(String dateYear) {
        dateYear = dateYear == null ? "" : CreditCardUtils.handleExpiration(dateYear);
        this.mExpiry = dateYear;
        ((TextView) this.findViewById(TEXTVIEW_CARD_EXPIRY_ID)).setText(dateYear);
    }

    public void paintCard() {
        CardSelector card = this.selectCard();
        View cardContainer = this.findViewById(com.cooltechworks.creditcarddesign.R.id.card_outline_container);
        View chipContainer = this.findViewById(com.cooltechworks.creditcarddesign.R.id.chip_container);
        View chipInner = this.findViewById(com.cooltechworks.creditcarddesign.R.id.chip_inner_view);
        View cardBack = this.findViewById(BACK_CARD_OUTLINE_ID);
        View cardFront = this.findViewById(FRONT_CARD_OUTLINE_ID);
        chipContainer.setBackgroundResource(card.getResChipOuterId());
        chipInner.setBackgroundResource(card.getResChipInnerId());
        ImageView frontLogoImageView = cardContainer.findViewById(com.cooltechworks.creditcarddesign.R.id.logo_img);
        frontLogoImageView.setImageResource(card.getResLogoId());
        ImageView centerImageView = cardContainer.findViewById(com.cooltechworks.creditcarddesign.R.id.logo_center_img);
        centerImageView.setImageResource(card.getResCenterImageId());
        ImageView backLogoImageView = this.findViewById(BACK_CARD_ID).findViewById(com.cooltechworks.creditcarddesign.R.id.logo_img);
        backLogoImageView.setImageResource(card.getResLogoId());
        cardBack.setBackgroundResource(card.getResCardId());
        cardFront.setBackgroundResource(card.getResCardId());
    }

    public void revealCardAnimation() {
        CardSelector card = this.selectCard();
        View cardFront = this.findViewById(FRONT_CARD_OUTLINE_ID);
        View cardContainer = this.findViewById(com.cooltechworks.creditcarddesign.R.id.card_outline_container);
        this.paintCard();
        this.animateChange(cardContainer, cardFront, card.getResCardId());
    }

    public CardSelector selectCard() {
        return this.mSelectorLogic != null ? this.mSelectorLogic.getCardSelector(this.mRawCardNumber) : CardSelector.selectCard(this.mRawCardNumber);
    }

    public void animateChange(View cardContainer, View v, int drawableId) {
        this.showAnimation(cardContainer, v, drawableId);
    }

    public void showAnimation(final View cardContainer, View v, final int drawableId) {
        v.setBackgroundResource(drawableId);
        if (this.mCurrentDrawable != drawableId) {
            short duration = 1000;
            int cx = v.getLeft();
            int cy = v.getTop();
            int radius = Math.max(v.getWidth(), v.getHeight()) * 4;
            if (Build.VERSION.SDK_INT < 21) {
                SupportAnimator anim = ViewAnimationUtils.createCircularReveal(v, cx, cy, 0.0F, (float) radius);
                anim.setInterpolator(new AccelerateDecelerateInterpolator());
                anim.setDuration(duration);
                (new Handler()).postDelayed(new Runnable() {
                    public void run() {
                        cardContainer.setBackgroundResource(drawableId);
                    }
                }, (long) duration);
                v.setVisibility(VISIBLE);
                anim.start();
                this.mCurrentDrawable = drawableId;
            } else {
                Animator anim1 = android.view.ViewAnimationUtils.createCircularReveal(v, cx, cy, 0.0F, (float) radius);
                v.setVisibility(VISIBLE);
                anim1.setDuration((long) duration);
                anim1.start();
                anim1.addListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        cardContainer.setBackgroundResource(drawableId);
                    }
                });
                this.mCurrentDrawable = drawableId;
            }

        }
    }

    public void setSelectorLogic(CreditCardView.ICustomCardSelector mSelectorLogic) {
        this.mSelectorLogic = mSelectorLogic;
    }

    public String getCardNumber() {
        return this.mRawCardNumber;
    }

    public void setCardNumber(String rawCardNumber) {
        this.mRawCardNumber = rawCardNumber == null ? "" : rawCardNumber;
        String newCardNumber = this.mRawCardNumber;

        for (int cardNumber = this.mRawCardNumber.length(); cardNumber < 16; ++cardNumber) {
            newCardNumber = newCardNumber + 'X';
        }

        String var4 = CreditCardUtils.handleCardNumber(newCardNumber, "  ");
        ((TextView) this.findViewById(TEXTVIEW_CARD_NUMBER_ID)).setText(var4);
        if (this.mRawCardNumber.length() == 3) {
            this.revealCardAnimation();
        } else {
            this.paintCard();
        }

    }

    interface ICustomCardSelector {
        CardSelector getCardSelector(String var1);
    }
}
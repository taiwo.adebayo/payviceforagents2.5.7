package com.iisysgroup.payviceforagents.base.presenter

interface LoginPresenter {

    fun login(userID: String, password: String)
}
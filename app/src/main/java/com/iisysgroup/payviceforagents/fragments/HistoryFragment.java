package com.iisysgroup.payviceforagents.fragments;

        import android.os.Bundle;
        import android.os.Handler;
        import android.support.annotation.Nullable;
        import android.support.design.widget.Snackbar;
        import android.support.v4.app.Fragment;
        import android.support.v4.widget.SwipeRefreshLayout;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;

        import com.iisysgroup.payviceforagents.R;
        import com.iisysgroup.payviceforagents.entities.VasResult;
        import com.iisysgroup.payviceforagents.payviceservices.Requests;
        import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
        import com.iisysgroup.payviceforagents.util.Helper;
        import com.iisysgroup.payviceforagents.util.HistoryAdapter;

        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;

/**
 * Created by Bamitale@Itex on 5/24/2016.
 */
public class HistoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    HistoryAdapter historyAdapter;

    List<HistoryAdapter.History> historyList = new ArrayList<>();
    Handler handler = new Handler();

    View errorLayout, emptyLayout;

    String userId = SecureStorage.retrieve(Helper.USER_ID, "");
    String terminalId = SecureStorage.retrieve(Helper.TERMINAL_ID, "");


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_transaction_history, container, false);

        populateView(view);

        return view;
    }

    protected void populateView(View view) {
        recyclerView = view.findViewById(android.R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        historyAdapter = new HistoryAdapter(getActivity(), historyList);
        recyclerView.setAdapter(historyAdapter);

        swipeRefreshLayout = view.findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(this);


        errorLayout = view.findViewById(R.id.error_layout);
        emptyLayout = view.findViewById(R.id.empty_layout);

        String historySerial = Helper.getPreference(getActivity(), Helper.HISTORY_SERIAL, "");
//
//        if (!historySerial.trim().isEmpty()) {
//            historyList.addAll(HistoryAdapter.processRecords(historySerial));
//            historyAdapter.notifyDataSetChanged();
//        }


        downloadRecords();
    }

    @Override
    public void onRefresh() {
        downloadRecords();
    }

    void downloadRecords() {

        if (!swipeRefreshLayout.isRefreshing() && historyList.isEmpty()) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        }

        errorLayout.setVisibility(View.INVISIBLE);
        emptyLayout.setVisibility(View.INVISIBLE);


        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<HistoryAdapter.History> data = getTransactionHistories();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        if (data != null) {
                            if (data.isEmpty()) {
                                recyclerView.setVisibility(View.GONE);
                                emptyLayout.setVisibility(View.VISIBLE);
                                return;
                            }

                            if (data.size() != historyList.size()) {
                                historyList.clear();
                                historyList.addAll(data);
                                historyAdapter.notifyDataSetChanged();
                                Helper.savePreference(getContext(), Helper.DOWNLOAD_BALANCE, false);
                            }

                        } else {
                            Helper.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Snackbar snackbar = Snackbar.make(errorLayout,
                                            "Could not retrieve transaction history", Snackbar.LENGTH_INDEFINITE);
                                    snackbar.setAction("Refresh", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            downloadRecords();
                                        }
                                    });
                                    snackbar.show();
                                }
                            });
                            if (historyList.isEmpty()) {
                                swipeRefreshLayout.setVisibility(View.INVISIBLE);
                                errorLayout.setVisibility(View.VISIBLE);
                            }

                        }

                    }
                });
            }
        }).start();
    }

    public List<HistoryAdapter.History> getTransactionHistories() {
        try {
            String url = getString(R.string.tams_url);

            String action = getString(R.string.tams_webapi_action);
            String control = "RECORDS";

            Map<String, String> params = new HashMap<>();
            params.put("action", action);
            params.put("userid", userId);
            params.put("termid", terminalId);
            params.put("control", control);

            VasResult tamsResult = Requests.processRequest(url, params);

            if (tamsResult.result != VasResult.Result.APPROVED)
                throw new IllegalAccessException(tamsResult.message);

            if (!tamsResult.message.isEmpty())
                Helper.savePreference(getActivity(), Helper.HISTORY_SERIAL, tamsResult.message);


            return HistoryAdapter.processRecords(tamsResult.message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}


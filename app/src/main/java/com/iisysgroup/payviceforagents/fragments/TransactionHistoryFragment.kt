package com.iisysgroup.payviceforagents.fragments

import android.app.DatePickerDialog
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.Spinner
import android.widget.TextView

import com.google.gson.Gson
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.models.HistoryParam
import com.iisysgroup.payviceforagents.models.Product
import com.iisysgroup.payviceforagents.models.Transaction
import com.iisysgroup.payviceforagents.models.TransactionHistory
import com.iisysgroup.payviceforagents.models.UserTransactions
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
import com.iisysgroup.payviceforagents.util.PaginationScrollListiner
import com.iisysgroup.payviceforagents.util.TransactionHistoryApi
import com.iisysgroup.payviceforagents.util.TransactionHistotyAdapter
import kotlinx.android.synthetic.main.content_transaction_history.*
import kotlinx.android.synthetic.main.transaction_error_layout.*

import org.json.JSONException
import org.json.JSONObject

import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Date
import java.util.HashMap
import java.util.LinkedHashMap
import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.okButton
import org.jetbrains.anko.support.v4.alert
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Itex-PC on 05/11/2018.
 */

class TransactionHistoryFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    internal var swipeRefreshLayout: SwipeRefreshLayout? = null
    internal var recyclerView: RecyclerView? = null
    internal var historyAdapter: TransactionHistotyAdapter ? = null

    private var currentDay: Int = 0
    private var currentYear: Int = 0
    private var currentMonth: Int = 0
    private var dateFormat: SimpleDateFormat? = null
    private var isLoading: Boolean = false


    private var walletId: String? = null
    private var defaultWalletId: String? = null


    internal var historyList: MutableList<Transaction> = ArrayList()
    internal var handler = Handler()

    internal var errorLayout: View?=null
    internal var emptyLayout: View?=null
    internal var view: View?=null

    internal var layoutManager: LinearLayoutManager ?=null
    internal var gson = Gson()
    internal var userId = SecureStorage.retrieve(Helper.USER_ID, "")
    internal var terminalId = SecureStorage.retrieve(Helper.TERMINAL_ID, "")
    internal var password = SecureStorage.retrieve(Helper.STORED_PASSWORD, "")

    private val limit = 15
    private var currentPage = 1
    internal var viewWallet: String? = null
    internal var product: String? = null
    internal var productName: String =""
    internal var startDates: String= ""
    internal var endDates: String =""
    private var historyParam: HistoryParam? = null
    private var isLastPage: Boolean = false
    private var progrssBar: ProgressBar? = null

    private var dy1: Int = 0
    private var isRefresh = false
    private var lastData: String? = null
    private var isRetry: Boolean = false




    private fun successfulFailedResponse(){
        try {
            swipeRefreshLayout!!.isRefreshing = false
            isRetry=false
            progrssbar.visibility = View.GONE
            errorLayout!!.visibility= View.GONE
            emptyLayout!!.visibility =View.GONE
            isLoading=false
        }
        catch (e:Throwable){
            Log.e("Error",e.localizedMessage)
        }

    }
    private fun getTransactionHistories() {

            val connectivityManager = context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val status = connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnectedOrConnecting


            if (status) {
                isLoading=true

                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder().addInterceptor(interceptor)
                        .connectTimeout(120, TimeUnit.SECONDS)
                        .readTimeout(120, TimeUnit.SECONDS)
                        .writeTimeout(120, TimeUnit.SECONDS).build()

                val retrofit = Retrofit.Builder()
                        .baseUrl("http://vas.itexapp.com")
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()

                val transactionHistoryApi = retrofit.create(TransactionHistoryApi::class.java)

                if (product != null) {

                    historyParam = HistoryParam(terminalId, userId, password, viewWallet, product, productName, startDates, endDates, limit, currentPage)


                } else {
                    historyParam = HistoryParam(terminalId, userId, password, limit, currentPage)
                }


                val transactionHistoryCall = transactionHistoryApi.fetchHistoty(historyParam)


                transactionHistoryCall!!.enqueue(object : Callback<TransactionHistory> {


                    override fun onResponse(call: Call<TransactionHistory>?, response: Response<TransactionHistory>?) {
                        Log.i("RetrofitResponse", "Here....")

                        val data = response!!.body()


                        handler.post(Runnable {


                            successfulFailedResponse()

                            if (data != null && data.error === false && data.userTransactions?.transactions?.size!! > 0) {

                                Log.i("onScrolled download", "dy3: $dy1")


                                //If the user refreshed the page clear the list and make is refresh false
                                if (isRefresh) {

                                    historyList.clear()
                                    isRefresh = false
                                }

                                val userTransactionStr = Gson().toJson(data.userTransactions)

                                SecureStorage.store(Helper.USER_TRANSACTIONS, userTransactionStr)
                              try{
                                progrssbar.visibility = View.GONE
                              }catch (e:Throwable){

                              }
                                historyList.addAll(data.userTransactions!!.transactions)
                                historyAdapter!!.notifyDataSetChanged()

                            }
                            else{
                                try {
                                    lastData = Helper.getPreference(context, Helper.USER_TRANSACTIONS, " ")
                                    val lastUserTrans = Gson().fromJson(lastData, UserTransactions::class.java)
                                    if (historyList.isEmpty() && lastUserTrans.transactions.size > 0) {
                                        swipeRefreshLayout!!.visibility = View.INVISIBLE
                                        errorLayout!!.visibility = View.VISIBLE
                                    } else if (historyList.isEmpty()) {
                                        swipeRefreshLayout!!.visibility = View.INVISIBLE
                                        emptyLayout!!.visibility = View.VISIBLE
                                    }
                                }catch (e: Throwable){
                                    swipeRefreshLayout!!.visibility = View.INVISIBLE
                                    alert {

                                        title = "Error"
                                        message= "Gateway Time-out"

                                        okButton {
                                            swipeRefreshLayout!!.visibility = View.VISIBLE
                                            downloadRecords() }
                                    }.show()
                                }
                            }
                        })


                    }

                    override fun onFailure(call: Call<TransactionHistory>, t: Throwable) {
                        Log.i("Response", t.message.toString())
                        successfulFailedResponse()

                        Log.i("ResponGHJHNHse", t.message.toString())
                        swipeRefreshLayout!!.isRefreshing=false
                        errorLayout!!.visibility=View.VISIBLE

                        if (t.message.toString().contains("Failed to connect to /") || t.message.toString().contains("Unable to resolve host" ) ) {

                            desc!!.text="Please Check your internet connection and try again "
                        } else {

                            desc!!.text= t?.message.toString()


                        }



                    }
                })
            } else {
                Helper.runOnUiThread(Runnable {
                    successfulFailedResponse()
                    if (historyList.isEmpty()) {


                        val snackbar = Snackbar.make(error_layout,
                                "Could not retrieve transaction history", Snackbar.LENGTH_INDEFINITE)
                        snackbar.setAction("Refresh") {

                            downloadRecords()
                        }
                        snackbar.show()
                    }
                    else{
                        if (historyList.isEmpty()) {

                            val snackbar = Snackbar.make(error_layout,
                                    "Could not retrieve transaction history", Snackbar.LENGTH_INDEFINITE)
                            snackbar.setAction("Retry") {
                                isRetry=true
                                downloadRecords()
                            }
                            snackbar.show()
                        }
                    }
                })


            }


        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        //        Log.i("oncDisplay, ", "Oncrete ,: " );
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view = inflater.inflate(R.layout.content_transaction_history, container, false)

        swipeRefreshLayout = view!!.findViewById<View>(R.id.swipe_layout) as SwipeRefreshLayout
        progrssBar = view!!.findViewById<View>(R.id.progrssbar) as ProgressBar




        populateView(view!!)


        return view
    }

    protected fun populateView(view: View) {
        recyclerView = view.findViewById(android.R.id.list)
        layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView!!.layoutManager = layoutManager

        historyAdapter = TransactionHistotyAdapter(activity, historyList)
        recyclerView!!.adapter = historyAdapter

        swipeRefreshLayout!!.setOnRefreshListener(this@TransactionHistoryFragment)


        errorLayout = view.findViewById(R.id.error_layout)
        emptyLayout = view.findViewById(R.id.empty_layout)


        val historySerial = Helper.getPreference(activity, Helper.HISTORY_SERIAL, "")



        downloadRecords()


        swipeRefreshLayout!!.setOnRefreshListener(this)


        recyclerView!!.addOnScrollListener( object : PaginationScrollListiner(linearLayoutManager = layoutManager!!){

            override fun isLastPage(): Boolean {

                var jsonString :String  = SecureStorage.retrieve(Helper.USER_TRANSACTIONS, "")

                var latestTransactions :UserTransactions= Gson().fromJson(jsonString, UserTransactions::class.java)

                if((latestTransactions.transactionSummarry.lastPage ==currentPage)){

                    isLastPage =true
                    Helper.runOnUiThread(Runnable {
                        val snackbar = Snackbar.make(recyclerView!!,
                                "You have reached the end of this the page ", Snackbar.LENGTH_SHORT)

                        snackbar.show()
                    })
                }else{
                    isLastPage =false
                }


                return isLastPage!!
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {

                if (!isLoading()) {

                    progrssbar.visibility = View.VISIBLE
                    currentPage += 1
                    getTransactionHistories()


                }
                else
                    return
            }

        })

    }

    override fun onRefresh() {

        refresh()

    }

    private fun refresh(){

//        Log.i ("isLoading outside ", isLoading.toString())
//
//        Log.i ("isLoading outside ", isLoading.toString())
        if(!isLoading){
            Log.i ("isLoading inside ", isLoading.toString())
            currentPage = 1
            isRefresh = true
            downloadRecords()
        }else{
            Log.i ("isLoading obeyed ", isLoading.toString())

            swipeRefreshLayout!!.isRefreshing=false
            return
        }
    }

    internal fun downloadRecords() {

        swipeRefreshLayout!!.visibility = View.VISIBLE
        if(!isRetry){
            swipeRefreshLayout!!.isRefreshing=true
        }

        getTransactionHistories()

    }


    override fun onCreateOptionsMenu(menu: Menu?, menuInflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, menuInflater)
        menuInflater!!.inflate(R.menu.filter, menu)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {

            R.id.filter_by -> popUp()
        }


        return super.onOptionsItemSelected(item)

    }


    private fun popUp() {
        val main = HashMap<String, Any>()

        val builderSingle = AlertDialog.Builder(context!!)

        val userTrans = Helper.getPreference(context, Helper.USER_TRANSACTIONS, "")



        val products = ArrayList<String>()

        val productt = gson.fromJson(userTrans, UserTransactions::class.java)

        val allData = LinkedHashMap<String, String>()

        Log.i("userTransactionStr", "popUp: " + productt.transactionSummarry.getproduct())
        val jsonObject = productt.transactionSummarry.getproduct()
        val prodObject = jsonObject.asJsonObject.toString()
        Log.i("userTransactionStr obj", "popUp: " + jsonObject.asJsonObject.toString())

        try {
            val jsonObject1 = JSONObject(jsonObject.asJsonObject.toString())
            val all = jsonObject1.keys()

            while (all.hasNext()) {

                //Gets the curremt object name
                val current = all.next()


                //Uses the object key to get the content
                val eachProduct = JSONObject(jsonObject1.get(current).toString())


                val product = gson.toJson(eachProduct)
                Log.i("userTransactionStr obj", "product: $product")

                val allProducts = gson.fromJson(product, Product::class.java)

                Log.i("userTransactionStr obj", "name: " + allProducts.getproduct().name)
                products.add(allProducts.getproduct().name)
                Log.i("userTransactionStr obj", "key: " + allProducts.getproduct().key)
                allData[allProducts.getproduct().key] = allProducts.getproduct().name

            }


        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.i("userTransactionStr obj", "key: $allData")


        val inflater = this.layoutInflater


        val view = inflater.inflate(R.layout.dialog_filter, null)

        val walletIdSpinner = view.findViewById<View>(R.id.wallet_id_spinner) as Spinner
        val productSpinner = view.findViewById<View>(R.id.product_spinner) as Spinner


        val allSpinner = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, products)
        productSpinner.adapter = allSpinner


        productSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val itemSelected = productSpinner.selectedItem.toString()
                Log.i("userTransactionStr", "onItemSelected: $itemSelected")

                for ((key1, value) in allData) {
                    if (value === itemSelected) {
                        productName = itemSelected
                        product = key1
                    }
                }


                Log.i("userTransactionStr", "productName: $productName Product $product")


            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }


        val wallets = ArrayList<String>()


        val userTransactions = gson.fromJson(userTrans, UserTransactions::class.java)
        walletId = userTransactions.walletID
        wallets.add(walletId!! + "   Your Wallet")
        if (!userTransactions.subAgents.isEmpty()) {
            for (subAgent in userTransactions.subAgents) {
                wallets.add(subAgent.walletID + "   " + subAgent.name)
            }

        }


        val walletSpinner = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, wallets)

        walletIdSpinner.adapter = walletSpinner




        walletIdSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                Log.i("Spinner", "onItemSelected: " + walletIdSpinner.selectedItem.toString())




                defaultWalletId = walletIdSpinner.selectedItem.toString().split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0].trim { it <= ' ' }
                Log.i("Spinner..", "onItemSelected: " + defaultWalletId!!)

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }


        val startDateTextView = view.findViewById<View>(R.id.start_date) as TextView
        val endDateTextView = view.findViewById<View>(R.id.end_date) as TextView

        val toadaysDate = Calendar.getInstance()
        currentDay = toadaysDate.get(Calendar.DAY_OF_MONTH)
        currentMonth = toadaysDate.get(Calendar.MONTH)
        currentYear = toadaysDate.get(Calendar.YEAR)


        dateFormat = SimpleDateFormat("yyyy-MM-dd")

        val todaysDateInMills = toadaysDate.timeInMillis
        val Timeat31ago = 86400000L * 31

        val Date31DaysAgo = todaysDateInMills - Timeat31ago

        endDates = dateFormat!!.format(toadaysDate.time)

        startDates = dateFormat!!.format(Date(Date31DaysAgo))


        startDateTextView.text = startDates

        endDateTextView.text = endDates









        endDateTextView.setOnClickListener {
            val startDateDialogue = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val currentMonth = month + 1
                currentDay = dayOfMonth


                val currentMonthst = isgreaterthan9(currentMonth)
                val currentDayst = isgreaterthan9(currentDay)



                endDates = year.toString() + "-" + currentMonthst + "-" + currentDayst

                endDateTextView.text = endDates
            }
            DatePickerDialog(context!!, startDateDialogue, currentYear, currentMonth, currentDay).show()
        }


        startDateTextView.setOnClickListener {
            val popupCalender = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                toadaysDate.set(currentMonth, currentMonth, currentDay)

                currentYear = year
                currentMonth = month + 1
                currentDay = dayOfMonth


                val currentMonthst = isgreaterthan9(currentMonth)
                val currentDayst = isgreaterthan9(currentDay)

                startDates = currentYear.toString() + "-" + currentMonthst + "-" + currentDayst



                startDateTextView.text = startDates
            }


            DatePickerDialog(context!!, popupCalender, currentYear, currentMonth - 1, currentDay).show()

            //                 endDateTextView.setText(startDate);
        }


        builderSingle.setView(view)


        builderSingle.setPositiveButton(getString(R.string.apply_filter)) { dialog, which ->
            historyList.clear()
            historyAdapter!!.notifyDataSetChanged()

            val startDate = endDateTextView.text.toString().trim { it <= ' ' }
            val endDate = startDateTextView.text.toString().trim { it <= ' ' }

            emptyLayout!!.visibility =View.GONE
            errorLayout!!.visibility =View.GONE

            progrssBar!!.visibility =View.GONE

           Log.i(" isLoading......",  isLoading.toString())

            if(!isLoading){
                Log.i ("isLoading inside ", isLoading.toString())
                currentPage = 1
                isRefresh = true
                swipeRefreshLayout!!.isRefreshing =true
                downloadRecords()
            }
// else{
//                Log.i ("isLoading obeyed ", isLoading.toString())
//
////                swipeRefreshLayout!!.isRefreshing=false
////                return
////            }
////            if (isLoading ){
//
//                swipeRefreshLayout!!.isRefreshing=true
//
//                val snackbar = Snackbar.make(error_layout,
//                        "You cannot Filter while Transaction History is loading!", Snackbar.LENGTH_SHORT)
//
//                snackbar.show()
//              return@setPositiveButton
//            }


        }




        builderSingle.show()


    }


    private fun isgreaterthan9(value: Int): String {
        val outcome: String
        if (value > 9) {
            outcome = "" + value
        } else {
            outcome = "0$value"
        }
        return outcome
    }

}

package com.iisysgroup.payviceforagents.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.util.SingleImageTitleObject;

import java.util.List;

public class ServiceListFragment extends Fragment {

    List<SingleImageTitleObject> services;
    SingleImageTitleObject.SingleImageTitleAdapter adapter;
    GridView gridView;
    private OnFragmentInteractionListener mListener;

    public ServiceListFragment() {
        // Required empty public constructor
    }

    public static ServiceListFragment getServiceListFragment(List<SingleImageTitleObject> services) {
        ServiceListFragment fragment = new ServiceListFragment();
        fragment.services = services;

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // getActivity().setTitle("Select A Service");
        View view = inflater.inflate(R.layout.fragment_service_list, container, false);

        gridView = (GridView) view.findViewById(android.R.id.list);

        adapter = new SingleImageTitleObject.SingleImageTitleAdapter(services, getActivity(), R.layout.network_grid_item);

        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null) {
                    mListener.onServiceListFragmentInteraction(services.get(position).title);
                }
            }
        });
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

//    @Override
//    public void onListItemClick(ListView l, View v, int position, long id) {
//        if (mListener != null) {
//            mListener.onServiceListFragmentInteraction(services.get(position).title);
//        }
//    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onServiceListFragmentInteraction(String service);
    }

}

package com.iisysgroup.payviceforagents.activities;


import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.commonsware.cwac.saferoom.SafeHelperFactory;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Prefs;
import com.itex.richard.payviceconnect.wrapper.PayviceServices;


/**
 * Created by Bamitale @Itex on 2/11/2016.
 */
public class MyApplication extends Application {
    public static final String TAG = "app_pit_ogl";
    public static final String DEBUG_CLIENT_SECRET = "z+xzMgCB8cUu1XRlzj06/TiFgT9p2wuA6q5wiZc5HZo=";
    public static final String DEBUG_CLIENT_ID = "IKIAB9CAC83B8CB8D064799DB34A58D2C8A7026A203B";
    public static final String DEBUG_CLIENT_ACCESS_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiaXN3LWNvbGxlY3Rpb25zIiwiaXN3LXBheW1lbnRnYXRld2F5IiwicGFzc3BvcnQiLCJ2YXVsdCJdLCJwcm9kdWN0aW9uX3BheW1lbnRfY29kZSI6IjA0MjYwNDEzMDM1MSIsInJlcXVlc3Rvcl9pZCI6IjAwMTg2MTEwMzU2Iiwic2NvcGUiOlsicHJvZmlsZSJdLCJleHAiOjE0NzIyNjM5MDIsImp0aSI6IjRkZTVmZjZkLWJlZjItNDZkNy1iMjcwLWU4ZmJkYzNmNmRmYiIsImNsaWVudF9pZCI6IklLSUFCOUNBQzgzQjhDQjhEMDY0Nzk5REIzNEE1OEQyQzhBNzAyNkEyMDNCIiwicGF5bWVudF9jb2RlIjoiMDUxNDIwNDE1OTQ5OCJ9.AVggBXuIaQAglTs4LnTE3A88ngRu2rU-awNguuYtIZXIF4dlgnZtJf47kDb5cqsvKPXLizXyvCTzoEjB1T9s50gZrmRqK1bbMrJtZsXYKp2aodeJ7io3FAB_DclokF5BX1jvciduleS9ZCoXmRyN4vK_gPpUAmfOPO93tVKbxc5wQU4HfWTrtMHZUcbwwhpBazrvDmF_wWj3a3AqrchmT6AVk4a8azjv9Z3Wfe7FsxZ2q_wKFq4VgZoDGEf1AmB-lHQKjoGO4venUDlOysdHZ2ZrthFyUD8mx-mzEJPEz4OnlNdVDiNC7idYS6xmNFNzm19nR3FljuKRY6RO270k1A";

    public static final String CLIENT_SECRET = "R+oOTWexaXZwnNHohOZboODyCcbmaS6/NHLj/oUU5lk=";
    public static final String CLIENT_ID = "IKIAAFECA0A52E9DFE2F4C71D71BD0DC45DEBA554F85";
    public static final String CLIENT_ACCESS_TOKEN = "eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiaXN3LWNvbGxlY3Rpb25zIiwiaXN3LXBheW1lbnRnYXRld2F5IiwicGFzc3BvcnQiLCJ2YXVsdCJdLCJtZXJjaGFudF9jb2RlIjoiTVgxODkiLCJyZXF1ZXN0b3JfaWQiOiIxMjM0NDY1Mjc0OCIsInNjb3BlIjpbInByb2ZpbGUiXSwiZXhwIjoxNDczOTg0NzE2LCJqdGkiOiJjNzAwOTY1Zi03NTRjLTRhMGEtODJhYS02NzlmZmNkOGQ1ZmMiLCJwYXlhYmxlX2lkIjoiMzQ3IiwiY2xpZW50X2lkIjoiSUtJQUFGRUNBMEE1MkU5REZFMkY0QzcxRDcxQkQwREM0NURFQkE1NTRGODUiLCJwYXltZW50X2NvZGUiOiIwNDI2MDQxMzAzNTEifQ.WpZu5oShnP-ef3RqurE2B_TD6_ZpX6Kmk_gUdXLgN3KZehEwUYZSAyopzjOSb2C5wjlE4I5bewCkVr8JRjrQvNkbpuZZymw26NPxd5o4LWJdE-OSgko_Q-ZWu7eqgnUOd2emzYwjCWJzq5CfWnJDPixZGcPUOFDl8f2jzg7GbeLG0VZq38B0iTrYB-G6CsfgiJS9OIFhvLfZ8xkLc8XRzRTEizwhs23fWS6tbiSFI6KzHwfnbf3w3ObW36JQz-asND5hbszBvpQku6sXnp4eHGN0nsGU-fQM0Qp36adCUFAb79CbTB01YIeLkxsPbfMflQAbpCJcNUWerhmsico05w";
    public static final int GET_CARD_REQUEST_CODE = 1001;


    private Prefs prefs;
    private PayviceServices vasRequestHandler;
    private static  String  sessionKey;
//    private AppDB db;


    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        try {
            SecureStorage.init(this)
                    .setEncryptionMethod(SecureStorage.Builder.EncryptionMethod.ENCRYPTED)
                    .setPassword(getString(R.string.default_terminal_id))
                    .setStoreName(TAG)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        prefs = new Prefs(this);

        vasRequestHandler = PayviceServices.getInstance(this);

//        Passport.overrideApiBase("https://sandbox.interswitchng.com/passport");
//        Payment.overrideApiBase(Payment.SANDBOX_API_BASE);

//        Passport.overrideApiBase(Passport.SANDBOX_API_BASE);
//        Payment.overrideApiBase(Payment.SANDBOX_API_BASE);

        char[] passPhrase = {'!', 'I', 't', 'e', 'x', '_', '&'};
        SafeHelperFactory helperFactory = new SafeHelperFactory(passPhrase);

//        db = Room.databaseBuilder(this, AppDB.class, "app_payvice.db")
//                .openHelperFactory(helperFactory)
//                .build();
    }

    public Prefs getPrefs() {
        return prefs;
    }

    public void setPrefs(Prefs prefs) {
        this.prefs = prefs;
    }

    public PayviceServices getVasRequestHandler() {
        return vasRequestHandler;
    }

//    public AppDB getDb() {
//        return db;
//    }

    public static void setSessionKey(String key){
        sessionKey= key;
    }
    public static String  getSessionKey(){
        return sessionKey;
    }
//
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}

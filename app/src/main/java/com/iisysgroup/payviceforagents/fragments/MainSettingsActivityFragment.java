package com.iisysgroup.payviceforagents.fragments;

import android.os.Bundle;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.util.SingleImageTitleObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bamitale@Itex on 5/11/2016.
 */
public class MainSettingsActivityFragment extends LoginSettingsActivityFragment {

    List<SingleImageTitleObject> objectList = new ArrayList<>();
    SingleImageTitleObject.SingleImageTitleAdapter adapter;

    {
        objectList.add(new SingleImageTitleObject("Change Transaction Pin", R.drawable.ic_lock_outline_24dp));
        objectList.add(new SingleImageTitleObject("Change Password", R.drawable.ic_lock_outline_24dp));
        objectList.add(new SingleImageTitleObject("Reset Transaction Pin", R.drawable.ic_lock_black_24dp));
        objectList.add(new SingleImageTitleObject("Privacy Policy", R.drawable.ic_security));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adapter = new SingleImageTitleObject.SingleImageTitleAdapter(objectList, getActivity());
        setListAdapter(adapter);
    }


}

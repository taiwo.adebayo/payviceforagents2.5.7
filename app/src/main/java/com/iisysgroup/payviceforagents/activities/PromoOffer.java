package com.iisysgroup.payviceforagents.activities;

import android.os.Bundle;
import android.view.View;

import com.iisysgroup.payviceforagents.R;

public class PromoOffer extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_offer);

        findViewById(R.id.referPromoLayout).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.referPromoLayout) {
            showReferralOptionDialog();
        }
    }
}

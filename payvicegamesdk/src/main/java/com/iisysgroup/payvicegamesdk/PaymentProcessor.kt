package com.iisysgroup.payvicegamesdk

import android.app.Activity
import android.app.ProgressDialog
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.iisysgroup.payvicegamesdk.utils.CardUtils

import com.interswitchng.sdk.model.RequestOptions
import com.interswitchng.sdk.payment.android.PaymentSDK
import com.interswitchng.sdk.payment.android.inapp.PayWithCard
import com.interswitchng.sdk.payment.model.Card
import com.interswitchng.sdk.payment.model.PurchaseRequest
import com.interswitchng.sdk.payment.model.PurchaseResponse
import com.interswitchng.sdk.util.RandomString

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by Bamitale@Itex on 25/01/2018.
 */
class PaymentProcessor(internal val context: Activity,
                       private val requestData: FundRequestData) {

    private val responseLiveData = MutableLiveData<TamsResponse>()
    internal val options = RequestOptions.builder()
            .setClientId(context.getString(R.string.client_id))
            .setClientSecret(context.getString(R.string.client_s))
            .setAccessToken(context.getString(R.string.client_at))
            .build()


    internal val progressDialog by lazy {
        ProgressDialog.show(context, "Wallet Funding",
                "Processing", true, false)
    }


    val response: LiveData<TamsResponse>
        get() = responseLiveData


    fun startUITransaction(amount: Double) {
        progressDialog.setMessage("Verifying user...")
        progressDialog.show()

        verifyPin(requestData).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result, error ->
                    error?.let(this::setError)

                    result.let {
                        if (result) {
                            progressDialog.dismiss()
                            showCardUI(context, amount, requestData.userId, requestData.walletId)
                        } else {
                            setError(RuntimeException("Invalid pin"))
                        }
                    }
                }
    }

    fun startNonUITransaction(amount: Double, card: Cards) {
        progressDialog.setMessage("Verifying user...")
        progressDialog.show()

        verifyPin(requestData).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result, error ->
                    error?.let(this::setError)

                    result.let {
                        if (result) {
                            progressDialog.setMessage("Processing...")
                            startNonUIPayment(amount, card)
                        } else {
                            setError(RuntimeException("Invalid pin"))
                        }
                    }
                }
    }

    private fun verifyPin(requestData: FundRequestData): Single<Boolean> {
        return Single.fromCallable {
            Requests.verifyUserPin(requestData.userId,
                    requestData.walletId, requestData.encryptedPin)
        }
    }

    private fun showCardUI(context: Activity, amount: Double, userId: String, walletID: String) {


        val payCallback = PayCallback(this)
        val payWithCard = PayWithCard(context, walletID, "$userId wallet funding",
                amount.toString(), "NGN", options, payCallback)

        payWithCard.start()

    }

    private fun startNonUIPayment(amount: Double, card: Cards) {

        val purchaseRequest = PurchaseRequest() // Setup purchaseRequest parameters
        purchaseRequest.customerId = requestData.userId // Optional email, mobile no, BVN etc to uniquely identify the customer.

        purchaseRequest.amount  = amount.toString() // Amount in Naira
        purchaseRequest.currency = "NGN" // ISO Currency code
        purchaseRequest.pan = CardUtils.normalizeCardNumber(card.pan) //Card No or Token
        purchaseRequest.pinData = card.pin // Optional Card PIN for card payment
        purchaseRequest.expiryDate = "${card.expiryYearString}${card.expiryMonthString}" // Card or Token expiry date in YYMM format
        purchaseRequest.requestorId = requestData.walletId
        purchaseRequest.cvv2 = card.cvv
        purchaseRequest.transactionRef = RandomString.numeric(12) // Generate a unique transaction reference.

        val paymentSDK = PaymentSDK(context, options)
        paymentSDK.purchase(purchaseRequest, UIPayCallback(this,purchaseRequest))
    }

    fun completeTransaction(purchaseResponse: PurchaseResponse) {
        progressDialog.setMessage("Completing transaction...")
        progressDialog.show()
        Single.fromCallable {
            Requests.sendTamsFundNotification(requestData, purchaseResponse)
        }.subscribeOn(Schedulers.io())
         .observeOn(AndroidSchedulers.mainThread()).doOnError {
            setResponse(TamsResponse(it.localizedMessage))
        }.subscribe { it ->
            setResponse(it)
        }
    }


    private fun setResponse(response: TamsResponse) {
        progressDialog.dismiss()
        responseLiveData.postValue(response)
    }

    internal fun setError(error: Throwable) {
        setResponse(TamsResponse(error.localizedMessage))
    }


}


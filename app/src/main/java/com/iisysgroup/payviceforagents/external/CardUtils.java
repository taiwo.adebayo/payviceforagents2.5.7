package com.iisysgroup.payviceforagents.external;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.persistence.entitiy.Card;
import com.iisysgroup.payviceforagents.securestorage.KeyCrypto;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Bamitale @Itex on 2/26/2016.
 */
public class CardUtils {
    public static Map<String, Integer> MiiMap = new HashMap<>();
    public static Map<String, String> MiiNameMap = new HashMap<>();

    static {
        CardUtils.addMiiItem("50", R.drawable.verve_logo);
        CardUtils.addMiiItem("51", R.drawable.master_card_logo);
        CardUtils.addMiiItem("52", R.drawable.master_card_logo);
        CardUtils.addMiiItem("53", R.drawable.master_card_logo);
        CardUtils.addMiiItem("54", R.drawable.master_card_logo);
        CardUtils.addMiiItem("55", R.drawable.master_card_logo);
        CardUtils.addMiiItem("4", R.drawable.visa_logo);

        CardUtils.addMiiNameItem("50", "Verve");
        CardUtils.addMiiNameItem("51", "Mastercard");
        CardUtils.addMiiNameItem("52", "Mastercard");
        CardUtils.addMiiNameItem("53", "Mastercard");
        CardUtils.addMiiNameItem("54", "Mastercard");
        CardUtils.addMiiNameItem("55", "Mastercard");
        CardUtils.addMiiNameItem("4", "Visa");

    }


    public static final int getCardLogo(String pan) {
        if (pan.length() < 2)
            return -1;
        else {
            if (pan.charAt(0) == '4')
                return MiiMap.get("4");
            else {
                String mii = pan.substring(0, 2);
                if (MiiMap.containsKey(mii))
                    return MiiMap.get(mii);
                else
                    return -1;
            }
        }
    }

    public static final String getCardMiiName(String bin) {
        if (bin.length() < 2)
            return "";
        else {
            if (bin.charAt(0) == '4')
                return MiiNameMap.get("4");
            else {
                String mii = bin.substring(0, 2);
                if (MiiNameMap.containsKey(mii))
                    return MiiNameMap.get(mii);
                else
                    return "";
            }
        }
    }

    public static void addMiiItem(String bin, int imageDrawable) {
        MiiMap.put(bin, imageDrawable);
    }

    public static void addMiiNameItem(String mii, String name) {
        MiiNameMap.put(mii, name);
    }


    public static String generateOnlineCard(String terminalID, Card card, String salt) throws Exception {


        String pan = card.PAN;
        String monthEx = card.expiryMonth;
        String yearEx = card.expiryYear;
        String cvv = card.CVV;


        String pan1 = pan.substring(0, 8);
        String pan2 = pan.substring(8, pan.length());

        String lvPan1 = turnToLV(pan1);
        String lvPan2 = turnToLV(pan2);
        String lvMon = turnToLV(monthEx);
        String lvYr = turnToLV(yearEx);
        String lvCvv = turnToLV(cvv);
        String lvTerm = turnToLV(terminalID);
        String lvSalt = turnToLV(salt);

        String lvName = "";

        if (card.cardHolder == null)
            card.cardHolder = "";

        lvName = turnToLV(card.cardHolder);

        //               Year  TerminalID Pan(0,8)  key    cvv     pan(8,len)  month  name

        String lvSemi = lvYr + lvTerm + lvPan1 + lvSalt + lvCvv + lvPan2 + lvMon; // + lvName;


        String lvFinal = turnToLV(lvSemi);

        KeyCrypto keyCrypto = getKeyCrypto(salt);

        lvFinal = KeyCrypto.hex(keyCrypto.encryptData(lvFinal));
        return lvFinal;
    }


    public static final KeyCrypto getKeyCrypto(String clrKey) throws Exception {
        byte dKey[] = Arrays.copyOf(hash512(clrKey), KeyCrypto.AES_MIN_KEY_LENGTH);

        return new KeyCrypto(dKey, "RC4", "RC4");
    }

    public static String turnToLV(String string) {
        if (string == null)
            throw new IllegalArgumentException("input cannot be null");

        int len = string.length();

        String lv = pad(len + "", 3, '0', true);

        string = lv + string;

        return string;
    }

    public static final String pad(String string, int maxLength, char padding, boolean addPaddingToLeft) {
        if (string == null)
            throw new IllegalArgumentException("input cannot be null");

        if (string.length() >= maxLength)
            return string;

        String pads = "";

        int requiredLength = maxLength - string.length();

        for (int i = 0; i < requiredLength; i++) {
            pads += padding;
        }

        if (addPaddingToLeft)
            string = pads + string.trim();
        else
            string = string.trim() + pads;

        return string;
    }


    public static final Card getCardData(String cardData, String key) throws Exception {
        if (cardData == null)
            throw new IllegalArgumentException("input cannot be null");

        KeyCrypto keyCrypto = getKeyCrypto(key);

        String decData = new String(keyCrypto.decryptData(cardData)).trim();

        String data = decodeLV(decData, 3);

        List<String> items = new ArrayList<>();
        while (!data.isEmpty()) {
            String temp = decodeLV(data, 3);
            items.add(temp);
            temp = data.substring(0, 3) + temp;
            data = data.substring(data.indexOf(temp) + temp.length(), data.length());
        }

        Card card = new Card();
        card.PAN = items.get(2) + items.get(5);
        card.CVV = items.get(4);
        card.expiryYear = items.get(0);
        card.expiryMonth = items.get(6);

        if (items.size() == 8)
            card.cardHolder = items.get(7);

        return card;
    }

    public static final String decodeLV(String string, int lv_length) {
        if (string == null)
            throw new IllegalArgumentException("input cannot be null");

        int len = Integer.parseInt(string.substring(0, lv_length));

        string = string.substring(lv_length, len + lv_length);

        return string;
    }

    public static byte[] hash512(String input) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");

            return messageDigest.digest(input.getBytes("UTF-8"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public static String processPan(String pan) {
        String acqCode = pan.substring(0, 6);
        String endCode = pan.substring(pan.length() - 4, pan.length());

        int elen = pan.length() - acqCode.length() - endCode.length();

        String etext = pad("*", elen, '*', true);

        String newPan = acqCode + etext + endCode;

        return newPan;
    }


}

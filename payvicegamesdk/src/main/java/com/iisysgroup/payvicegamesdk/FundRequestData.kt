package com.iisysgroup.payvicegamesdk

data class FundRequestData(val userId: String,
                           val walletId: String,
                           val userKey: String,
                           val encryptedPin: String)
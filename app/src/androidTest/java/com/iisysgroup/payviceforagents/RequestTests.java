package com.iisysgroup.payviceforagents;

import android.app.Application;
import android.test.ApplicationTestCase;

import com.iisysgroup.payviceforagents.base.interactor.MultichoiceInteractor;
import com.iisysgroup.payviceforagents.entities.PlanValue;
import com.iisysgroup.payviceforagents.payviceservices.MultichoiceRequests;
import com.iisysgroup.payviceforagents.util.Helper;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Bamitale@Itex on 28/02/2018.
 */

public class RequestTests extends ApplicationTestCase<Application> {

    public RequestTests() {
        super(Application.class);
    }

    public void testValidMultichoicePayment() throws Exception {
        MultichoiceRequests.PaymentResult paymentResult = MultichoiceRequests.payMultichoiceSubscription("10208738160",
                "bimetalic001@gmail.com", "89364449", String.valueOf(104115), 0, "");
        assertEquals(false, paymentResult.getError());
    }

    public void testInvalidMultichoicePayment() throws Exception {
        MultichoiceRequests.PaymentResult paymentResult = MultichoiceRequests.payMultichoiceSubscription("10208738160",
                "bimetalic001@gmail.com", "89364449", String.valueOf(10411500), 0, "");

        System.out.print("Payment ServiceResult: " + paymentResult);
        assertEquals(true, paymentResult.getError());
    }

    public void testInvalidMultichoicePayment2() throws Exception {
        MultichoiceRequests.PaymentResult paymentResult = MultichoiceRequests.payMultichoiceSubscription("10208738160",
                "bimetalic001@gmail.com", "89364448", String.valueOf(104115), 0, "");

        System.out.print("Payment ServiceResult: " + paymentResult);
        assertEquals(true, paymentResult.getError());
    }

    public void testInvalidMultichoicePayment3() throws Exception {
        MultichoiceRequests.PaymentResult paymentResult = MultichoiceRequests.payMultichoiceSubscription("1020873816034",
                "bimetalic001@gmail.com", "89364449", String.valueOf(104115), 0, "");

        System.out.print("Payment ServiceResult: " + paymentResult);
        assertEquals(true, paymentResult.getError());
    }

    public void testSmartCardValidationRequest() throws Exception {
        MultichoiceRequests.IUCVerificationResult result = MultichoiceRequests.validateSmartCardNumber(MultichoiceInteractor.MultichoiceProduct.DSTV, "10208738160");
        assertEquals(true, result.isValid());
    }

    public void testSmartCardInvalidValidationRequest() throws Exception {
        MultichoiceRequests.IUCVerificationResult result = MultichoiceRequests.validateSmartCardNumber(MultichoiceInteractor.MultichoiceProduct.DSTV, "1020873816012");
        assertEquals(false, result.isValid());
    }

    public void testSmartCardInvalidValidationRequest2() throws Exception {
        MultichoiceRequests.IUCVerificationResult result = MultichoiceRequests.validateSmartCardNumber(MultichoiceInteractor.MultichoiceProduct.DSTV, "");
        assertEquals(false, result.isValid());
    }

    public void testMultichoiceLookUpRequest() throws Exception {
        assertEquals(false, MultichoiceRequests.getAvailableMultichoicePlans(MultichoiceInteractor.MultichoiceProduct.GOTV, " ").isEmpty());
    }

    public void testParseMultichoiceLookupResponse() throws Exception {
        String response = "{\n" +
                "    \"error\": false,\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"name\": \"Active Package\",\n" +
                "            \"product_code\": \"AP\",\n" +
                "            \"amount\": \"4370.00\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"DStv Asian Bouquet E36\",\n" +
                "            \"product_code\": \"ASIAE36\",\n" +
                "            \"amount\": \"4800.00\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"DStv French Touch Add-on Bouquet E36\",\n" +
                "            \"product_code\": \"FRN7E36\",\n" +
                "            \"amount\": \"1400.00\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"DStv Great Wall\",\n" +
                "            \"product_code\": \"GWALLE36\",\n" +
                "            \"amount\": \"1000.00\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"DStv Compact Bouquet E36\",\n" +
                "            \"product_code\": \"COMPE36\",\n" +
                "            \"amount\": \"6300.00\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"DStv Compact Plus Bouquet E36\",\n" +
                "            \"product_code\": \"COMPLE36\",\n" +
                "            \"amount\": \"9900.00\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"DStv Family Bouquet E36\",\n" +
                "            \"product_code\": \"COFAME36\",\n" +
                "            \"amount\": \"3800.00\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"DStv Premium W/Afr E36\",\n" +
                "            \"product_code\": \"PRWE36\",\n" +
                "            \"amount\": \"14700.00\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        List<PlanValue> planValues = Helper.parseMultichoiceLookupResponse(new JSONObject(response));
        assertEquals(false, planValues.isEmpty());
    }

}

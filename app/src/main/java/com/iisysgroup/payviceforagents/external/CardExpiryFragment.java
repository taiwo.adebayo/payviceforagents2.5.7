package com.iisysgroup.payviceforagents.external;

import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.iisysgroup.payviceforagents.R;

import java.util.Calendar;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */

public class CardExpiryFragment extends CreditCardFragment {
    EditText cardExpiryView;

    public CardExpiryFragment() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle state) {
        View v = inflater.inflate(R.layout.lyt_card_expiry, group, false);
        this.cardExpiryView = (EditText) v.findViewById(com.cooltechworks.creditcarddesign.R.id.card_expiry);
        String expiry = "";
        if (this.getArguments() != null && this.getArguments().containsKey("card_expiry")) {
            expiry = this.getArguments().getString("card_expiry");
        }

        if (expiry == null) {
            expiry = "";
        }

        this.cardExpiryView.setText(expiry);
        this.cardExpiryView.addTextChangedListener(this);
        return v;
    }

    public void afterTextChanged(Editable s) {
        String text = s.toString().replace("/", "");
        String month = "";
        String year = "";
        int previousLength;
        int cursorPosition;
        if (text.length() >= 2) {
            month = text.substring(0, 2);
            previousLength = Integer.parseInt(month);
            if (previousLength <= 0 || previousLength >= 13) {
                this.cardExpiryView.setError("Invalid month");
                return;
            }

            year = text.substring(2);
            if (text.length() >= 4) {
                cursorPosition = Integer.parseInt(year);
                Calendar modifiedLength = Calendar.getInstance();
                int currentYear = modifiedLength.get(Calendar.YEAR);
                int currentMonth = modifiedLength.get(Calendar.MONTH) + 1;
                int millenium = currentYear / 1000 * 1000;
                if (cursorPosition + millenium < currentYear) {
                    this.cardExpiryView.setError("Card expired");
                    return;
                }

                if (cursorPosition + millenium == currentYear && previousLength < currentMonth) {
                    this.cardExpiryView.setError("Card expired");
                    return;
                }
            }
        } else {
            month = text;
        }

        previousLength = this.cardExpiryView.getText().length();
        cursorPosition = this.cardExpiryView.getSelectionEnd();
        text = CreditCardUtils.handleExpiration(month, year);
        this.cardExpiryView.removeTextChangedListener(this);
        this.cardExpiryView.setText(text);
        this.cardExpiryView.setSelection(text.length());
        this.cardExpiryView.addTextChangedListener(this);
        int modifiedLength1 = text.length();
        if (modifiedLength1 <= previousLength && cursorPosition < modifiedLength1) {
            this.cardExpiryView.setSelection(cursorPosition);
        }

        this.onEdit(text);
        if (text.length() == 5) {
            this.onComplete();
        }

    }

    public void focus() {
        if (this.isAdded()) {
            this.cardExpiryView.selectAll();
        }

    }
}

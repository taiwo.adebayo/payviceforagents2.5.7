package com.iisysgroup.payvicegamesdk

import android.app.Dialog
import android.content.Context
import android.graphics.Point
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import com.interswitchng.sdk.model.RequestOptions
import com.interswitchng.sdk.payment.IswCallback
import com.interswitchng.sdk.payment.android.AuthorizeWebView
import com.interswitchng.sdk.payment.android.PaymentSDK
import com.interswitchng.sdk.payment.model.AuthorizePurchaseRequest
import com.interswitchng.sdk.payment.model.AuthorizePurchaseResponse
import com.interswitchng.sdk.payment.model.PurchaseRequest
import com.interswitchng.sdk.payment.model.PurchaseResponse
import com.interswitchng.sdk.util.StringUtils
import kotlin.concurrent.thread

class UIPayCallback(paymentProcessor: PaymentProcessor,
                    private val iswPurchaseRequest: PurchaseRequest): PayCallback(paymentProcessor) {

    private val context = paymentProcessor.context
    private val options = paymentProcessor.options


    override fun onSuccess(purchaseResponse: PurchaseResponse) {
        if (purchaseResponse is AuthorizePurchaseResponse) {
            super.onSuccess(purchaseResponse)
            return
        }

        if (StringUtils.hasText(purchaseResponse.responseCode)) {
            if (PaymentSDK.SAFE_TOKEN_RESPONSE_CODE == purchaseResponse.responseCode) {
                // OTP required, ask user for OTP and authorize transaction

                    val alertDialog= AlertDialog.Builder(context)
                            .setTitle("Enter OTP")
                            .setView(R.layout.otp_input_layout)
                            .setCancelable(false).create()
                    alertDialog.show()

                    val otpEdit: EditText = alertDialog.findViewById<EditText>(R.id.otpEdit)!!
                    alertDialog.findViewById<Button>(R.id.proceed_button)?.setOnClickListener(View.OnClickListener {
                        if (!TextUtils.isEmpty(otpEdit.text.toString())) {
                            val otp = otpEdit.text.toString()
                            alertDialog.dismiss()
                            doSafePurchaseTokenOtpAuthorization(iswPurchaseRequest, purchaseResponse,options, otp)
                        } else {
                            otpEdit.error = "Please enter OTP token"
                            otpEdit.requestFocus()
                        }
                    })

                    alertDialog.findViewById<Button>(R.id.cancel_button)!!.setOnClickListener(View.OnClickListener {
                        val otp = "-1-1-1-1-1-1"
                        alertDialog.dismiss()
                        doSafePurchaseTokenOtpAuthorization(iswPurchaseRequest, purchaseResponse,options, otp)
                    })

            } else if (PaymentSDK.CARDINAL_RESPONSE_CODE == purchaseResponse.responseCode) {
                // redirect user to cardinal authorization page
                doCardinalOtpAuthorization(purchaseResponse, options)
            }
        } else {
            super.onSuccess(purchaseResponse)
        }

    }


    private fun doSafePurchaseTokenOtpAuthorization(purchaseRequest: PurchaseRequest,
                                                     response: PurchaseResponse, options: RequestOptions, otp: String) {
        val otpRequest = AuthorizePurchaseRequest()
        // Setup purchaseRequest parameters using the selected Payment Method
        otpRequest.paymentId = response.paymentId // Set the payment identifier for the purchaseRequest

        otpRequest.authData = purchaseRequest.authData// Set the purchaseRequest Auth Data

        otpRequest.otp = otp // Accept OTP from user

        // Set the unique transaction reference.
        otpRequest.transactionRef = response.transactionRef
        val paymentSDK = PaymentSDK(context, options)

        thread {
            paymentSDK.authorizePurchase(otpRequest,
                    object : IswCallback<AuthorizePurchaseResponse>() {
                        override fun onError(e: Exception) {
                            this@UIPayCallback.onError(e)
                        }

                        override fun onSuccess(authorizePurchaseResponse: AuthorizePurchaseResponse) {
                            //Handle and notify user of successful transaction
                            this@UIPayCallback.onSuccess(authorizePurchaseResponse)
                        }
                    })
        }
    }


    private fun doCardinalOtpAuthorization(response: PurchaseResponse, options: RequestOptions) {

        val cardinalDialog = Dialog(context)
        cardinalDialog.setCancelable(true)

        cardinalDialog.setOnCancelListener {
            onError(RuntimeException("Transaction cancelled by user"))
        }

        // Create WebView to process the Authorize purchase purchaseRequest
        val webView = object : AuthorizeWebView(context, response) {
            override fun onPageDone() {
                val request = AuthorizePurchaseRequest()
                request.authData = request.authData // Set the purchaseRequest Auth Data.
                request.paymentId = response.paymentId // Set the payment identifier for the purchaseRequest.
                request.transactionId = response.transactionId // Set payment identifier for the purchaseRequest.
                request.eciFlag = response.eciFlag   // Set the Electronic Commerce Indicator (ECI).
                PaymentSDK(context, options)
                        .authorizePurchase(request,
                                object : IswCallback<AuthorizePurchaseResponse>() {
                                    override fun onError(e: Exception) {
                                        e.printStackTrace()
                                        this@UIPayCallback.onError(e)
                                    }

                                    override fun onSuccess(authorizePurchaseResponse: AuthorizePurchaseResponse) {
                                        //Handle and notify user of successful transaction
                                        cardinalDialog.dismiss()
                                        this@UIPayCallback.onSuccess(authorizePurchaseResponse)
                                    }
                                })
            }

            override fun onPageError(e: Exception) {
                // Handle and notify user of error
                onError(e)
            }
        }


        // Other webview customizations goes here e.g.
        webView.requestFocus(View.FOCUS_DOWN)
        webView.settings.javaScriptEnabled = true
        webView.isVerticalScrollBarEnabled = true


        cardinalDialog.setContentView(webView)

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y
        val window = cardinalDialog.window
        window!!.setLayout(width, height)

        cardinalDialog.show()
    }


}
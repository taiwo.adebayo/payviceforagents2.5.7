package com.iisysgroup.payviceforagents.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.base.interactor.MultichoiceInteractor;
import com.iisysgroup.payviceforagents.callbacks.OnPlanInputFragmentInteractionListener;
import com.iisysgroup.payviceforagents.callbacks.SelectedItemCallback;
import com.iisysgroup.payviceforagents.entities.PlanValue;
import com.iisysgroup.payviceforagents.entities.Service;
import com.iisysgroup.payviceforagents.payviceservices.MultichoiceRequests;
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.VasServices;

import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;


public class BillPlanInputFragment extends BaseBillFragment {

    @BindView(R.id.pinLayout)
    LinearLayout pinLayout;
    @BindView(R.id.selectAmountLayout)
    RelativeLayout selectPinAmount;
    @BindView(R.id.selectableAmountText)
    TextView pinAmountText;
    @BindView(R.id.pinCountText)
    EditText pinCountText;

    @BindView(R.id.recipientTIL)
    TextInputLayout recipientTIL;
    @BindView(R.id.beneficiaryEdit)
    EditText serviceNumberEdit;


    private OnPlanInputFragmentInteractionListener mListener;

    public BillPlanInputFragment() {
        // Required empty public constructor
    }

    public static BillPlanInputFragment newInstance(String service) {
        BillPlanInputFragment fragment = new BillPlanInputFragment();
        Bundle args = new Bundle();
        args.putString(SERVICE, service);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bill_plan_input, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recipientTIL.setHint(Helper.getBeneficiaryTextHint(service.name));
    }

    public void onButtonPressed(Service.Product product, Beneficiary beneficiary, PlanValue planValue, int period) {
        if (mListener != null) {
            mListener.onPlanInputFragmentInteraction(product, beneficiary, planValue, period);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPlanInputFragmentInteractionListener) {
            mListener = (OnPlanInputFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    boolean isValidInputs() {

        if (product == null) {
            serviceText.setError("Please select product");
            Helper.showErrorAnim(serviceText);
            Helper.showSnackBar(serviceText, "Please select product");
            return false;
        }

        if (serviceNumberEdit.getText().toString().trim().isEmpty()) {
            doServiceNumberEditError();
            return false;
        }

        if (planValue == null) {
            pinAmountText.setError("Please select plan");
            Helper.showErrorAnim(pinAmountText);
            Helper.showSnackBar(pinAmountText, "Please select plan");
            return false;
        }

        if (pinCountText.getText().toString().trim().isEmpty()) {
            Helper.showErrorAnim(pinCountText);
            pinCountText.setError("Please enter number of months");
            pinCountText.requestFocus();
            return false;
        }


        return true;
    }

    private void doServiceNumberEditError() {
        serviceNumberEdit.setError(VasServices.VAS_SERVICE_INPUT_TEXT_DESC.get(service.name));
        serviceNumberEdit.requestFocus();
        Helper.showErrorAnim(serviceNumberEdit);
    }

    @OnClick(R.id.proceedBtn)
    void proceed() {
        if (isValidInputs()) {
            if (isMultichoiceService()) {
                validateMultichoiceSmartcardNumber();
                return;
            }
            String beneficiary = serviceNumberEdit.getText().toString();
            onButtonPressed(product, new Beneficiary(beneficiary, beneficiary, "")
                    , planValue, Integer.parseInt(pinCountText.getText().toString()));
        }
    }


    @OnClick(R.id.selectAmountLayout)
    void selectPlanAmount() {
        if (isMultichoiceService()) {
            if (TextUtils.isEmpty(serviceNumberEdit.getText())) {
                doServiceNumberEditError();
                return;
            }
        }
        selectPlan();
    }


    @Override
    protected void selectPlan() {
        final PlanListDialog planListDialog = new PlanListDialog(getActivity(), service.type, product,
                new SelectedItemCallback<PlanValue>() {
                    @Override
                    public void onItemSelected(PlanValue item) {
                        planValue = item;
                        pinAmountText.setText(String.format("%d - %s", item.value, item.description));
                    }
                });

        if (isMultichoiceService()) {
            planListDialog.setSmartCardNumber(serviceNumberEdit.getText().toString());
        }
        planListDialog.show();
    }


    boolean isMultichoiceService() {
        return product.requestCode.equals(VasServices.DSTV) || product.requestCode.equals(VasServices.GOTV);
    }

    void validateMultichoiceSmartcardNumber() {
        final ProgressDialog progressDialog = ProgressDialog.show(getContext(),
                null, "Validating smart card", true, false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final MultichoiceRequests.IUCVerificationResult result = MultichoiceRequests.validateSmartCardNumber(
                            MultichoiceInteractor.MultichoiceProduct.valueOf(product.requestCode),
                            serviceNumberEdit.getText().toString());

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (progressDialog != null) {
                                progressDialog.dismiss();
                            }
                            if (result.isValid()) {
                                Beneficiary beneficiary = new Beneficiary(serviceNumberEdit.getText().toString(),
                                        result.getFullname() + " - " + result.getUnit(), "");
                                onButtonPressed(product, beneficiary, planValue,
                                        Integer.parseInt(pinCountText.getText().toString()));
                            } else {
                                serviceNumberEdit.setError("Invalid smart card number");
                                serviceNumberEdit.requestFocus();
                                Helper.showErrorAnim(serviceNumberEdit);
                            }
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


}

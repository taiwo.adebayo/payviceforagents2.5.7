package com.iisysgroup.payviceforagents

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.Contacts
import android.support.annotation.UiThread
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.Toast
import com.bumptech.glide.RequestBuilder
import com.google.gson.Gson
import com.iisgroup.bluetoothprinterlib.Utils.Util.Language.it
import com.iisysgroup.newland.NewlandDevice
import com.iisysgroup.newland.NewlandDeviceManager
import com.iisysgroup.payviceforagents.PayviceServices.context
import com.iisysgroup.payviceforagents.activities.SettingsActivity
import com.iisysgroup.payviceforagents.db.VasTerminalService
import com.iisysgroup.payviceforagents.models.GetTidModel
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.SharedPreferenceUtils
import com.iisysgroup.payviceforagents.util.TerminalDataHandler
import com.iisysgroup.poslib.deviceinterface.DeviceState
import com.iisysgroup.poslib.deviceinterface.EmvCardReader
import com.iisysgroup.poslib.deviceinterface.bluetooth.BTSelectorDialog
import com.iisysgroup.poslib.deviceinterface.bluetooth.BluetoothMposDevice.ATTACHED_BLUETOOTH_DEVICE
import com.iisysgroup.poslib.deviceinterface.bluetooth.BluetoothMposDevice.ATTACHED_BLUETOOTH_DEVICE_NAME
import com.iisysgroup.poslib.host.HostInteractor
import com.iisysgroup.poslib.host.dao.PosLibDatabase
import com.iisysgroup.poslib.host.entities.ConfigData
import com.iisysgroup.poslib.host.entities.ConnectionData
import com.iisysgroup.poslib.host.entities.KeyHolder
import com.iisysgroup.poslib.host.entities.VasTerminalData
import com.newland.mtypex.bluetooth.BlueToothV100ConnParams
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_term_magm.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.*
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.async


class TermMagmActivity : AppCompatActivity() {

    val preferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(this)
    }
     var deviceManager = NewlandDeviceManager.getInstance()


    object TerminalUtils {

        fun isTerminalPrepped(context: Context, application : App) : Boolean {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

            val isValidSettings = when {
                !sharedPreferences.contains(context.getString(R.string.key_terminal_id)) ||
                        !sharedPreferences.contains(context.getString(R.string.key_ip_address)) ||
                        !sharedPreferences.contains(context.getString(R.string.key_ip_address)) ||
                         sharedPreferences.getString(context.getString(R.string.key_terminal_id), "").length !== 8 ||
                        !sharedPreferences.contains(context.getString(R.string.key_pref_port_type)) ||
                        !sharedPreferences.contains(context.getString(R.string.key_host_type)) -> false
                else -> true
            }

            if (!isValidSettings){
                return false
            } else {
                (application).db.configDataDao.get() ?: return false
            }

            return true

        }

        fun isTerminalIdSet(context: Context) : Boolean{
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            if(sharedPreferences.getString(context.getString(R.string.key_terminal_id), "").length !== 8){
                return false
            }else{
                return true
            }
        }
    }

    var bluetoothDevice: BluetoothDevice? = null

    fun loadAttachedBluetoothDevice() {
        if (preferences.contains(ATTACHED_BLUETOOTH_DEVICE)) {
            val def = "---------"
            val btAddress = preferences.getString(ATTACHED_BLUETOOTH_DEVICE, def)

            val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

            if (btAddress != def && BluetoothAdapter.checkBluetoothAddress(btAddress)) {
                //System.out.println(btAddress);
                bluetoothDevice = bluetoothAdapter.getRemoteDevice(btAddress)
                deviceManager.init(NewlandDevice(this), NewlandDeviceManager.ME3X_DRIVER,
                        BlueToothV100ConnParams(bluetoothDevice!!.getAddress()), deviceManager.MyDeviceEventListener())
                try {
                    deviceManager.connect()
                }catch (e : Exception){
                    connectRetry()
                }

            }
        }
    }

    fun connectBluetooth(){
        val btSelectorDialog = BTSelectorDialog()
        btSelectorDialog.setAllowedDeviceNames(listOf("Newland", "C-ME30S"))

        btSelectorDialog.bluetoothDevice
                .subscribeOn(Schedulers.io())
                .subscribe({ b ->
                    bluetoothDevice = b
                    saveAttachedBluetoothDevice()
                }) { throwable ->
                    bluetoothDevice = null
                }

        btSelectorDialog.show(this.getFragmentManager(), btSelectorDialog.javaClass.simpleName)
    }

    fun saveAttachedBluetoothDevice() {
        if (bluetoothDevice != null) {
            preferences.edit()
                    .putString(ATTACHED_BLUETOOTH_DEVICE, bluetoothDevice!!.getAddress())
                    .putString(ATTACHED_BLUETOOTH_DEVICE_NAME, bluetoothDevice!!.getName())
                    .apply()

            deviceManager.init(NewlandDevice(this), NewlandDeviceManager.ME3X_DRIVER,
                    BlueToothV100ConnParams(bluetoothDevice!!.getAddress()), deviceManager.MyDeviceEventListener())
            try {
                deviceManager.connect()
            }catch (e : Exception){
                connectRetry()
            }
        }

    }

    override fun finish() {
        progressDialog.dismiss()
        super.finish()
    }

    private fun SharedPreferences.isSSL() : Boolean {
        return when (this.getString(getString(R.string.key_host_type), "")){
            "" -> false
            "SSL" -> true
            else -> true
        }
    }

    private val sharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(this)
    }

    private val connectionData by lazy {
        val terminal_id = sharedPreferences.getString(getString(R.string.key_terminal_id), null)
        val ip_address = sharedPreferences.getString(getString(R.string.key_ip_address), null)
        val ip_port = Integer.parseInt(sharedPreferences.getString(getString(R.string.key_pref_port), null))
        val isSSL = sharedPreferences.isSSL()

        ConnectionData(terminal_id, ip_address, ip_port, isSSL)
       // ConnectionData("2033GP23", "196.6.103.73", 5043, isSSL)
    }

    val hostInteractor: HostInteractor by lazy {
        (application as App).hostInteractor
    }

    fun displayError(e: Exception){
        toast("Error \n "+ e.message)
//        progressDialog.dismiss()
//        progressDialog.setTitle("Error")
//        progressDialog.setMessage(e.message)
//        progressDialog.show()

    }

    val alert by lazy{
        AlertDialog.Builder(this)
                .setTitle(null)
                .setMessage(null)
                .create()
    }

    val db by lazy {
        (application as App).db
    }

    val progressDialog by lazy {
        ProgressDialog(this).apply { setCancelable(false) }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_term_magm)

//        setSupportActionBar(toolbar)
//
       // supportActionBar?.let {
            //sit.setDisplayHomeAsUpEnabled(true)
       // }

        try {
            loadAttachedBluetoothDevice()
        }
        catch (e : Exception){

        }

        if(bluetoothDevice == null){
            try{
                connectBluetooth()
            }catch (e : Exception){

            }

        }

        fun injectTerminalParameters(currencyCode: String?) {
            TerminalDataHandler.currencyCode = currencyCode
            TerminalDataHandler.setTerminalProperties(this)
        }

        configureTerminalBtn.setOnClickListener {
           // if(TerminalUtils.isTerminalIdSet(this)){
            if (deviceManager.device != null) {
                Log.d("okh", deviceManager.device.deviceInfo.sn + " serial")
                configureTerminal()
                injectTerminalParameters("556")
            }
            else{
                        toast("Please first pair with a POS Device through Bluetooth")
               }
//                try{
////                    if (deviceManager.device.deviceInfo.sn !=null) {
////                        configureTerminal()
////                        injectTerminalParameters("556")
////                    } else{
////                        toast("Please first connect with a POS Device")
////                    }
//                }catch (e : Exception){
//
//            }
        }

        downloadParameterBtn.setOnClickListener {
            //downloadParameters()
        }

        notifacationUser.setOnClickListener{
            setNotifyReceiver(this@TermMagmActivity)
        }

        if (!isValidConnectionSettings())
        {

        }

    }

    private fun isValidConnectionSettings() = when {
        !sharedPreferences.contains(getString(R.string.key_terminal_id)) ||
                !sharedPreferences.contains(getString(R.string.key_ip_address)) ||
                !sharedPreferences.contains(getString(R.string.key_ip_address)) ||
                !sharedPreferences.contains(getString(R.string.key_pref_port_type)) ||
                !sharedPreferences.contains(getString(R.string.key_host_type)) -> false
        else -> true
    }


    private fun downloadParameters()
    {
        GlobalScope.launch(Dispatchers.Default){
            val keyHolder = db.keyHolderDao.get()

            if (keyHolder == null)
            {
                GlobalScope.launch(Dispatchers.Main){

//                    val alertdialog = AlertDialog.Builder(baseContext)
//                    alertdialog.setTitle("Error")
//                    alertdialog.setMessage("Invalid keys. Press OK to reconfigure terminal").
//                            setCancelable(false).
//                            setPositiveButton("Ok"){ dialog, which ->
//                                if(TerminalUtils.isTerminalIdSet(this@TermMagmActivity)){
//                                    configureTerminal()
//                                }else{
//                                    val alertdialog = AlertDialog.Builder(baseContext)
//                                    alertdialog.setTitle("Error")
//                                    alertdialog.setMessage("Please Set Terminal ID").
//                                            setPositiveButton("Set"){ dialog, which ->
//                                                startActivity(Intent(this@TermMagmActivity, SettingsActivity::class.java))
//                                            }.
//                                            setNegativeButton("Cancel"){ dialog, which ->
//                                                dialog.cancel()
//                                            }.show()
//                                }
//                            }.show()

                }
            }
            else {
                GlobalScope.launch(Dispatchers.Main){
                    progressDialog.setMessage("Now downloading parameters")
                    progressDialog.show()
                }


                hostInteractor.getConfigData(connectionData, keyHolder)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {configData: ConfigData?, throwable: Throwable? ->
                            throwable?.let {

                                GlobalScope.launch(Dispatchers.Main){

                                    val alertdialog = AlertDialog.Builder(baseContext)
                                    alertdialog.setTitle("Error")
                                    alertdialog.setMessage("Invalid keys. Press OK to reconfigure terminal").
                                            setPositiveButton("Set"){ dialog, which ->
                                                if(TerminalUtils.isTerminalIdSet(this@TermMagmActivity)){
                                                    try{
                                                        configureTerminal()
                                                    }catch (e : Exception){

                                                    }

                                                }else {
                                                    val alertdialog = AlertDialog.Builder(baseContext)
                                                    alertdialog.setTitle("Error")
                                                    alertdialog.setMessage("Please Set Terminal ID").
                                                            setPositiveButton("Set"){ dialog, which ->
                                                                startActivity(Intent(this@TermMagmActivity, SettingsActivity::class.java))
                                                            }.
                                                            setNegativeButton("Cancel"){ dialog, which ->
                                                                dialog.cancel()
                                                            }.show()
                                                }
                                            }.show()

                                }
                                return@let
                            }

                            configData?.let {
                                doAsync {
                                    db.configDataDao.save(it)
                                    Log.d("OkH", it.data.toString())
                                    SharedPreferenceUtils.setIsTerminalPrepped(this@TermMagmActivity, true)
                                }
                                GlobalScope.launch(Dispatchers.Main){

                                    progressDialog.dismiss()
                                    SecureStorage.store("mainkey", keyHolder.masterKey)
                                    SecureStorage.store("pinkey", keyHolder.pinKey)
                                    Log.d("keys", keyHolder.masterKey + " "+ keyHolder.pinKey)
                                    val alertdialog = AlertDialog.Builder(baseContext)
                                    alertdialog.setTitle("Success")
                                    toast("Successfully Configured")
                                    alertdialog.setMessage("Device configured successfully").
                                            setPositiveButton("Ok"){ dialog, which ->
                                                dialog.dismiss()
                                            }.show()
                                }

                            }
                        }
            }
        }
    }

    private fun getVasTerminalService() : Single<VasTerminalData>{
        return VasTerminalService.Factory.getService().getVasTerminalDetails()
    }

  private fun connectRetry(){

      val alertdialog = AlertDialog.Builder(baseContext)
      alertdialog.setTitle("Error")
      alertdialog.setMessage("Failed to connect to the Device \n Please make sure the sevice is switched on").
              setPositiveButton("Retry"){ dialog, which ->
                  try {
                      deviceManager.connect()
                  }catch (e : Exception){
                      connectRetry()
                  }
              }.
              setNegativeButton("Cancel"){ dialog, which ->
                  this@TermMagmActivity.finish()
              }.show()

    }


    @SuppressLint("CheckResult")
    private fun configureTerminal() {
               val progressDialog = ProgressDialog.show(this, "Configuring Terminal",
                "Please wait", true, false)
        progressDialog.show()

//        val alertdialog = AlertDialog.Builder(baseContext)
//        alertdialog.setTitle("Configuring Terminal")
//        alertdialog.setMessage("Invalid keys. Press OK to reconfigure terminal")
//        alertdialog.show()
//       val progressDialog = ProgressDialog.show(this, "Configuring Terminal",
//                "Please wait", true, false)
//        progressDialog.show()

            Toast.makeText(this, "Configuring Terminal", Toast.LENGTH_SHORT).show()
        //NewlandDevice(this)
       // progressDialog.setMessage("Getting Terminal Id")
       // progressDialog.show()
        Single.fromCallable {
            var client = OkHttpClient()
            val mediaType = MediaType.parse("application/json")
            val body = RequestBody.create(mediaType, "{\"oem\" : \"Newland\",\"model\" : \"ME30\",\"sn\" : \"${deviceManager.device.deviceInfo.sn}\",\"version\" : \"1.0.2\"}");
            Log.i("okh",body.toString())
            var request = Request.Builder()
                   // .url("https://197.253.19.75/tams/eftpos/op/pos.php")
                    .url("http://197.253.19.75/tams/eftpos/op/pos.php")
                    .addHeader("Content-Type", "application/json")
                    .post(body)
                    .build();
              var res = client.newCall(request).execute()
              var resString = res.body()!!.string()
              Gson().fromJson(resString, GetTidModel.MainModel::class.java)
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.i("okh", it.toString())
                    Log.i("okh", it.getTid()+"")
                    Log.i("okh seriel", deviceManager.device.deviceInfo.sn)
                    if(it.getError()!!){
                      //  progressDialog.dismiss()

                        Toast.makeText(this, "TID was not found for the device with Seriel Number: ${deviceManager.device.deviceInfo.sn} \\n Please call customer service", Toast.LENGTH_SHORT).show()

                    }else{
                        var editor = sharedPreferences.edit();
                        editor.putString(getString(R.string.key_terminal_id), it.getTid())
                        editor.apply();
                        //Toast.makeText(context, "Getting keys", Toast.LENGTH_SHORT).show()
                     //   progressDialog.setMessage("Getting keys")
                        getVasTerminalService()
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe {
                                    terminalData, error ->

                                    Log.d("okh", terminalData.pinKey+terminalData.countryCode+ terminalData.tid)

                                 //   Log.d("okh", error.message)
                                 //   terminalData?.let {
                                        GlobalScope.launch(Dispatchers.Default){
                                            (application as App).db.vasTerminalDataDao.save(terminalData!!)
                                        }
                                    Log.d("okh", connectionData.ipAddress + connectionData.terminalID)
                                    SecureStorage.store("terminalID", connectionData.terminalID)
                                        val liveKeyHolder = hostInteractor.getKeyHolder(connectionData)
                                        liveKeyHolder.subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe { keyHolder: KeyHolder?, throwable: Throwable? ->
                                                    //                                                  //  keyHolder?.let {
//                                                       // Toast.makeText(context, "Configuring terminal", Toast.LENGTH_SHORT).show()
//                                                       // progressDialog.setMessage("Configuring term
//
//                                                       inal")
//
//                                                        val isTest = when(sharedPreferences.getString(getString(R.string.key_platform), "").toLowerCase()){
//                                                            "test" -> true
//                                                            else -> false
//                                                        }
//
//                                                        if (isTest)
//                                                            keyHolder!!.isTestPlatform = true
                                                    Log.d("okh", keyHolder.toString())
                                                     async {
                                                            db.keyHolderDao.save(keyHolder!!)
                                                            SecureStorage.store("mainkey", keyHolder!!.masterKey)
                                                            SecureStorage.store("pinkey", keyHolder!!.pinKey)
                                                            Log.d("keys", keyHolder!!.masterKey + " "+ keyHolder.pinKey)
                                                        }

                                                        hostInteractor.getConfigData(connectionData, keyHolder!!)
                                                                .observeOn(AndroidSchedulers.mainThread())
                                                                .subscribeOn(Schedulers.io())
                                                                .subscribe { configData , throwable ->
//                                                                   // configData?.let {
                                                                    Log.d("okh", configData.toString())
                                                                        doAsync {
                                                                            db.configDataDao.save(configData)
                                                                            SharedPreferenceUtils.setIsTerminalPrepped(this@TermMagmActivity, true)

                                                                        UI {
                                                                            SecureStorage.store("merchantid", configData.getConfigData("03015").toString())
                                                                        }
                                                                        }

                                                                    progressDialog.setTitle("Success")
                                                                    progressDialog.setMessage("Device configured successfully")
                                                                    //progressDialog.setIndeterminate(false);
                                                                    progressDialog.setCancelable(true)
                                                                    progressDialog.show()
                                                                    progressDialog.dismiss()
                                                                    toast("Successfully Configured")
//                                                                     //   Toast.makeText(context, "Device configured successfully", Toast.LENGTH_SHORT).show()
////                                                                        val alertdialog = AlertDialog.Builder(baseContext)
////                                                                        alertdialog.setTitle("Success")
////                                                                        alertdialog.setMessage("TDevice configured successfully").show()
//
                                                                    }
//
                                                                    throwable?.let {
                                                                        displayError(error as Exception)
                                                                }
//                                                                }
//
//
                                                }
//                                                   // throwable?.let {
//                                                        toast("Error")
//                                                        displayError(error as Exception)
//                                                  //  }
//                                                }

                                  //  }

                                  //  error?.let {
                                    //    progressDialog.dismiss()
                                   // Toast.makeText(context, error!!.message.toString(), Toast.LENGTH_SHORT).show()
//                                        val alertdialog = AlertDialog.Builder(baseContext)
//                                        alertdialog.setTitle("Error")
//                                        alertdialog.setMessage(error!!.message.toString()).show()
                                  //  }
                                }
                    }
                },{
                  //  it.printStackTrace()
                  //  progressDialog.dismiss()
//                    val alertdialog = AlertDialog.Builder(baseContext)
//                    alertdialog.setMessage("Unknown Error occured").
//                            setPositiveButton("Ok"){ dialog, which ->
//                                progressDialog.dismiss()
//                                dialog.cancel()
//                            }.show()
                })

        }
    }

    private  fun setNotifyReceiver(context: Context){
        var view = LayoutInflater.from(context).inflate(R.layout.content_enter_user, null)
        var notificationUser = view.findViewById<EditText>(R.id.user)
        notificationUser.setText(SharedPreferenceUtils.getNotificationUser(context))


        val alertdialog = AlertDialog.Builder(context)
        alertdialog.setTitle("Enter Notification Username").
                setPositiveButton("Set"){ dialog, which ->
                    if(notificationUser.text.isEmpty()){
                        notificationUser.error = "Please enter Username"
                    }else{
                        SharedPreferenceUtils.saveNotificationUser(context, notificationUser.text.toString())
                        dialog.dismiss();
                    }
                }.
                setNegativeButton("Cancel"){ dialog, which ->
                    dialog.dismiss()
                }.show()

    }



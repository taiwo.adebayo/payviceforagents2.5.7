package com.iisysgroup.payviceforagents.baseimpl.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.iisysgroup.payviceforagents.base.presenter.ResultPresenter
import com.iisysgroup.payviceforagents.entities.StatusObject

class ResultActivityViewModel : ViewModel(), ResultPresenter {

    private val statusObject = MutableLiveData<StatusObject>()
    val statusLiveData: LiveData<StatusObject>
        get() = statusObject

    override fun setStatusObject(statusObject: StatusObject) {
        this.statusObject.value = statusObject
    }
}
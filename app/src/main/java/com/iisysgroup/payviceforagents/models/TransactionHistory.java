package com.iisysgroup.payviceforagents.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Itex-PC on 31/10/2018.
 */

public class TransactionHistory {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("errors")
    @Expose
    private List<Object> errors = null;
    @SerializedName("userTransactions")
    @Expose
    private UserTransactions userTransactions;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }

    public UserTransactions getUserTransactions() {
        return userTransactions;
    }

    public void setUserTransactions(UserTransactions userTransactions) {
        this.userTransactions = userTransactions;
    }

}

//public class TransactionSummary {
//
//    @SerializedName("count")
//    @Expose
//    private Integer count;
//    @SerializedName("amount")
//    @Expose
//    private String amount;
//    @SerializedName("creditCount")
//    @Expose
//    private Integer creditCount;
//    @SerializedName("creditAmount")
//    @Expose
//    private String creditAmount;
//    @SerializedName("debitCount")
//    @Expose
//    private Integer debitCount;
//    @SerializedName("debitAmount")
//    @Expose
//    private String debitAmount;
//    @SerializedName("currentPage")
//    @Expose
//    private Integer currentPage;
//    @SerializedName("lastPage")
//    @Expose
//    private Integer lastPage;
////    @SerializedName("products")
////    @Expose
////    private Products products;
//
//    public Integer getCount() {
//        return count;
//    }
//
//    public void setCount(Integer count) {
//        this.count = count;
//    }
//
//    public String getAmount() {
//        return amount;
//    }
//
//    public void setAmount(String amount) {
//        this.amount = amount;
//    }
//
//    public Integer getCreditCount() {
//        return creditCount;
//    }
//
//    public void setCreditCount(Integer creditCount) {
//        this.creditCount = creditCount;
//    }
//
//    public String getCreditAmount() {
//        return creditAmount;
//    }
//
//    public void setCreditAmount(String creditAmount) {
//        this.creditAmount = creditAmount;
//    }
//
//    public Integer getDebitCount() {
//        return debitCount;
//    }
//
//    public void setDebitCount(Integer debitCount) {
//        this.debitCount = debitCount;
//    }
//
//    public String getDebitAmount() {
//        return debitAmount;
//    }
//
//    public void setDebitAmount(String debitAmount) {
//        this.debitAmount = debitAmount;
//    }
//
//    public Integer getCurrentPage() {
//        return currentPage;
//    }
//
//    public void setCurrentPage(Integer currentPage) {
//        this.currentPage = currentPage;
//    }
//
//    public Integer getLastPage() {
//        return lastPage;
//    }
//
//    public void setLastPage(Integer lastPage) {
//        this.lastPage = lastPage;
//    }
//
////    public Products getProducts() {
////        return products;
////    }
////
////    public void setProducts(Products products) {
////        this.products = products;
////    }
//
//}
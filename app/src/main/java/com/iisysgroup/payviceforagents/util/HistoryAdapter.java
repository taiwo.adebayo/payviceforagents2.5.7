package com.iisysgroup.payviceforagents.util;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.activities.RepeatTransactionActivity;
import com.iisysgroup.payviceforagents.activities.TransactionHistoryDetailActivity;
import com.iisysgroup.payviceforagents.external.CardUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Bamitale @Itex on 1/15/2016.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

    List<History> historyList;
    Context context;

    public HistoryAdapter(Context context, List<History> historyList) {
        this.context = context;
        this.historyList = historyList;
    }

    public static List<History> processRecords(String recordString) {

        System.out.println(recordString);
        String[] records = recordString.split("\\*");
        List<History> list = new ArrayList<>();
//
        History history = null;
        for (String record : records) {
            String[] cols = record.split("\\|");

            if (cols.length >= 8) {
                //[0] - Beneficiary  [1] - amount  [2] - Request Code [3] - Terminal ID [4] - Username [5] - Transaction Reference
                //[6] - Transaction type  [7] - Date  [8] - Remark/Summary_

                history = new History();
                history.beneficiary = cols[0];
                history.amount = cols[1].replace('N', '\u20A6');


                history.product = cols[2];
                history.transactionReference = cols[5];
                history.transactionType = cols[6];
                history.date = cols[7];

                if (cols.length > 8) history.remark = cols[8];

                list.add(history);
            }

        }
        return list;
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.new_history_item_r, parent, false);
        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryViewHolder holder, int position) {
        holder.bind(context, historyList.get(position));
    }

    public static class History implements Serializable {
        //[0] - Beneficiary  [1] - amount  [2] - Request Code [3] - Terminal ID [4] - Username [5] - Transaction Reference
        //[6] - Transaction type  [7] - Date
        public String transactionReference, amount, beneficiary, product, date, transactionType, remark;


        public boolean isPurchase() {
            String tranType = transactionType.toLowerCase().trim();
            return !(tranType.contains("fund") || tranType.contains("transfer") || tranType.contains("promo"));
        }

        public boolean isRecharge() {
            String productCode = product.toLowerCase().trim();

            return (productCode.contains("etisalat") ||
                    productCode.contains("mtn") || productCode.contains("glo") || productCode.contains("airtel"));
        }
    }

    class HistoryViewHolder extends RecyclerView.ViewHolder {
        View repayView, contentView;
        TextView tranTypeTitleView, amountView, referenceView,
                beneficiaryView, productView, dateView;

        public HistoryViewHolder(View itemView) {
            super(itemView);
            contentView = itemView;
            repayView = itemView.findViewById(R.id.reload_view);
            tranTypeTitleView = itemView.findViewById(R.id.history_tran_type_title);
            amountView = itemView.findViewById(R.id.history_amount_textview);
            beneficiaryView = itemView.findViewById(R.id.history_beneficiary_textview);
            productView = itemView.findViewById(R.id.history_product_textview);
            dateView = itemView.findViewById(R.id.history_date_textview);
            referenceView = itemView.findViewById(R.id.history_reference_textview);
        }

        public void bind(final Context context, final History history) {
            if (history.transactionType.toLowerCase().trim().contains("fund")) {
                tranTypeTitleView.setText("Fund");
                amountView.setTextColor(R.drawable.card_color_round_rect_green);
                history.product = "Fund";
                history.beneficiary = CardUtils.processPan(history.beneficiary);
            } else if (history.transactionType.toLowerCase().trim().contains("promo")) {
                tranTypeTitleView.setText("Promo");
                amountView.setTextColor(R.drawable.card_color_round_rect_green);
                history.product = "Promo";
            } else if (history.transactionType.toLowerCase().trim().contains("transfer")) {
                tranTypeTitleView.setText("Transfer");
                history.product = "Wallet Transfer";
                amountView.setTextColor(R.drawable.card_color_round_rect_blue);
            } else {
                tranTypeTitleView.setText("Purchase");
                amountView.setTextColor(R.drawable.card_color_round_rect_orange);
            }

            beneficiaryView.setText(history.beneficiary);
            amountView.setText(history.amount);
            String [] date = history.date.split(" ");
            if (date!=null && date[0]!=null){
                dateView.setText(date[0]);
            }

            referenceView.setText(history.transactionReference);
            productView.setText(history.product);

            if (history.isPurchase()) {
                repayView.setVisibility(View.VISIBLE);
                repayView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, RepeatTransactionActivity.class);
                        intent.putExtra(RepeatTransactionActivity.EXTRA_HISTORY, history);
                        context.startActivity(intent);
                    }
                });
            } else {
                repayView.setVisibility(View.INVISIBLE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, TransactionHistoryDetailActivity.class);
                        intent.putExtra(TransactionHistoryDetailActivity.EXTRA_HISTORY, history);
                        context.startActivity(intent);
                    }
                });
            }



        }
    }


}

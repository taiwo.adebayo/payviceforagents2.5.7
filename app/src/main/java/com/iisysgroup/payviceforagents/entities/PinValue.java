package com.iisysgroup.payviceforagents.entities;

import java.io.Serializable;

/**
 * Created by Bamitale@Itex on 5/5/2016.
 */
public class PinValue implements Serializable {
    public int value, available_quantity;


    @Override
    public String toString() {
        return "\u20A6 " + value;
    }
}

package com.iisysgroup.payviceforagents.balance.model

data class CommissionTransferResponse(val result : String, val status : String, val message : String, val balance : String, val commission : String)

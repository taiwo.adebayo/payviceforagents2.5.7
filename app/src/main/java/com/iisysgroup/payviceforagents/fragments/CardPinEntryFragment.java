package com.iisysgroup.payviceforagents.fragments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.util.Helper;


public class CardPinEntryFragment extends DialogFragment implements View.OnClickListener {

    TextView pinEdit;
    Button button;
    String currentString = "";
    Vibrator vibrator;

    int numberPadKeys[] = {R.id.btn_0, R.id.btn_1, R.id.btn_2, R.id.btn_3, R.id.btn_4, R.id.btn_5
            , R.id.btn_6, R.id.btn_7, R.id.btn_8, R.id.btn_9, R.id.btn_Del};


    private MutableLiveData<String> pinLiveData = new MutableLiveData<>();


    public CardPinEntryFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("Enter Card Pin");

        View view = inflater.inflate(R.layout.fragment_cvv_check, container, false);

        for (int key : numberPadKeys) {
            View keyView = view.findViewById(key);
            keyView.setOnClickListener(this);
            keyView.setHapticFeedbackEnabled(true);
        }

        pinEdit = view.findViewById(R.id.editPin);
        button = view.findViewById(R.id.proceedBtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    onButtonPressed(pinEdit.getText().toString());
                }
            }
        });

        return view;
    }

    public void onButtonPressed(String input) {
        pinLiveData.setValue(input);
        Helper.hideSoftKeyboard(getActivity());
    }


    boolean isValid() {

        if (pinEdit.getText().toString().length() < 4) {
            Snackbar.make(pinEdit, "Enter a valid pin", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        // v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        vibrator.vibrate(100);

        if (v.getId() == R.id.btn_Del)
            processDelete();
        else {
            FrameLayout fl = (FrameLayout) v;

            TextView textView = (TextView) fl.getChildAt(0);

            processInput(textView.getText().toString());
        }
    }


    protected void processDelete() {
        currentString = pinEdit.getText().toString().trim();

        if (!TextUtils.isEmpty(currentString)) {
            currentString = currentString.substring(0, currentString.length() - 1);
        }

        pinEdit.setText(currentString);
    }


    protected void processInput(String input) {
        if (input != null && !input.isEmpty()) {
            currentString = pinEdit.getText().toString();

            currentString += input;
            pinEdit.setText(currentString);
        }
    }

    public LiveData<String> getPin() {
        return pinLiveData;
    }

}

package com.iisysgroup.payviceforagents.activities

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.iisgroup.bluetoothprinterlib.Manaer.PrintfManager
import com.iisysgroup.payviceforagents.App
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.baseimpl.viewmodel.ResultActivityViewModel
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.StatusObject
import com.iisysgroup.payviceforagents.fragments.StatusFragment
import com.iisysgroup.payviceforagents.models.TransactionModel
import com.iisysgroup.payviceforagents.printer.BluetoothActivity
import com.iisysgroup.payviceforagents.printer.BluetoothPrinter
import com.iisysgroup.payviceforagents.printer.BluetoothPrinter.*
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
import kotlinx.android.synthetic.main.activity_print.*
import kotlinx.android.synthetic.main.activity_result.*
import org.jetbrains.anko.doAsync
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import com.iisgroup.bluetoothprinterlib.Utils.Util.Language.it
import com.iisysgroup.payviceforagents.printer.PrinterService.decodeBitmap
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*


class ResultVasActivity : AppCompatActivity() {

    private lateinit var printerManager: PrintfManager
    private lateinit var bluetoothPrinter: BluetoothPrinter
    private lateinit var result : StatusObject
    private var currentService : String = ""
    private var currentAmount : String = ""
    private var currentPhone : String = ""
    private var currentProduct : String = ""
    private var currentReason : String = ""
    private var currentApproval : String = ""
    private var currentRef : String = ""
    private var currentType : String = ""
    private var currentMeter : String = ""
    private var currentName : String = ""
    private var paymentMethod : String = ""
    private var currentPin : String = ""
    private var currentInfo : String = ""
    lateinit var currentTariff : String
    lateinit var currentInvoiceCode : String
    lateinit var currentAddress : String
    lateinit var currentArrears : String
    lateinit var currentTransId : String
    lateinit var currentUnitValue : String
    lateinit var currentCashier : String
    lateinit var currentVat : String
    private var body_ : String = ""
    private var header : String = ""
    private var body_a : String = ""
    private var body_b : String = ""
    private var body_pin : String = ""
    private var body_c : String = ""
    private var body_d : String = ""
    private var body_e : String = ""
    private var body_6 : String = ""
    private var ref : String = ""
    private var itexString : String = ""
    private var itexString2 : String = ""
    private var itexAddress : String = ""
    private var itexPowered : String = ""
    private var itexWebsite : String = ""
    private var itexPhone : String = ""
    private var itexSettlement : String = ""
    private var itexNumbers : String = ""
    private var itexEmail : String = ""
    private var footer : String = ""
    private var footer2 : String = ""
    private var spaceString : String = ""
    private var spacesadd : String = ""
    private var beneficiary : String = ""
    private var convenienceFee : String = ""
    private var currentAccount : String = ""
    private var emptyString : String = ""
    private var starline : String = ""
    private var newline : String = ""
    private var icon : Bitmap ?= null
    private var imageByte : ByteArray ?= null

    var spaces : String = ""
    var tranType = "CARD: "

    private var transaction : TransactionModel ?= null


    private val viewModel by lazy {
        ViewModelProviders.of(this)[ResultActivityViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val merchantname = SecureStorage.retrieve("merchantname", "")
        val merchantid = SecureStorage.retrieve("merchantid", "")
        val terminalID = SecureStorage.retrieve("terminalID", "")
        val walletId = SecureStorage.retrieve(Helper.TERMINAL_ID, "")

            itexString = "Itex Integrated Services" +space(24)
        itexString2 = "www.iisysgroup.com" +space(18)
        itexAddress = "163A, Sinari daranijo street, VI, Lagos" +space(39)
        itexPowered = "POWERED BY ITEX "
        itexWebsite = "www.iisysgroup.com"
        itexPhone = "0700-2255-4839"
        itexSettlement = "Contact Itex For Settlement on"
        itexNumbers = "0906 277 4420, 0907 031 4511, 0907 031 4443"
        itexEmail = "vassupport@iisysgroup.com"
        paymentMethod = "CASH"

        starline = spaceadd(14) + "**************" + spaceadd(14)
        newline = "   " + space(3)

        footer =   newline+
                   spaceadd(itexPowered.length) + itexPowered.toUpperCase() + spaceadd(itexPowered.length )

        footer2 =            spaceadd(itexWebsite.length) + itexWebsite + spaceadd(itexWebsite.length ) +
                   spaceadd(itexPhone.length) + itexPhone + spaceadd(itexPhone.length )

        emptyString =    newline
        
        bluetoothPrinter = BluetoothPrinter(this@ResultVasActivity)
        currentService = SecureStorage.retrieve("currentservice", "Value Added Service")
        currentAmount =  SecureStorage.retrieve("currentAmount", "0")
        currentPhone =  SecureStorage.retrieve("currentPhone", "0")
        currentProduct =  SecureStorage.retrieve("currentProduct", "")
        currentReason =  SecureStorage.retrieve("currentReason", "")
        currentApproval =  SecureStorage.retrieve("currentApproval", "")
        currentTariff =  SecureStorage.retrieve("currentTarriff", "")
        currentInvoiceCode =  SecureStorage.retrieve("currentInvoice", "")
        currentCashier =  SecureStorage.retrieve("currentCashier", "")
        currentAddress =  SecureStorage.retrieve("currentAddress", "")
        currentArrears =  SecureStorage.retrieve("currentArrears", "")
        currentTransId =  SecureStorage.retrieve("currentTransId", "")
        currentVat =  SecureStorage.retrieve("currentVat", "")
        currentUnitValue =  SecureStorage.retrieve("currentUnitValue", "")
        currentRef =  SecureStorage.retrieve("currentRef", "")
        currentType = SecureStorage.retrieve("currentType", "")
        currentMeter = SecureStorage.retrieve("currentMeter", "")
        currentName = SecureStorage.retrieve("currentName", "")
        currentPin = SecureStorage.retrieve("currentPin", "")
        currentInfo = SecureStorage.retrieve("currentInfo", "")
        beneficiary = SecureStorage.retrieve("accountname", "")
        convenienceFee = SecureStorage.retrieve("conveniencefee", "")
        currentAccount = SecureStorage.retrieve("currentAccount", "")

        val formatter = NumberFormat.getCurrencyInstance(Locale.ENGLISH);
        currentAmount = formatter.format(currentAmount.toDouble())
        currentAmount = "N "+ currentAmount.substring(1, currentAmount.length)

        Log.d ("okh", "currentService " + currentService )
        Log.d ("okh", "currentAmount " + currentAmount )
        Log.d ("okh", "currentPhone " + currentPhone )
        Log.d ("okh", "currentProduct " + currentProduct )
        Log.d ("okh", "currentReason " + currentReason )
        Log.d ("okh", "currentApproval " + currentApproval )
        Log.d ("okh", "currentRef " + currentRef )
        Log.d ("okh", "currentType " + currentType )
        Log.d ("okh", "currentMeter " + currentMeter )
        Log.d ("okh", "currentName " + currentName )
        Log.d ("okh", "currentPin " + currentPin )
        Log.d ("okh", "currentInfo " + currentInfo )

        val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().time)


        //balancing up the space to be even if the length of the string is odd
        currentProduct = adjust(currentProduct)
        currentService = adjust(currentService)
        currentApproval = adjust(currentApproval)
        currentAmount = adjust(currentAmount)

        currentMeter = currentMeter.trim()
        currentReason = currentReason.trim()
        currentName = currentName.trim()
        currentRef = currentRef.trim()

            if (currentApproval.equals("Approved", true)){
                imgStatus.setImageResource(R.drawable.transaction_approved)
                vasresult.text = currentReason
            }
            else {
                imgStatus.setImageResource(R.drawable.transaction_declined)
                vasresult.text = currentReason
            }

        header = spaceadd(7) + "PAYVICE " + spaceadd(7) +
                itexAddress+
                newline+
                spaceadd(currentService.length) + currentService.toUpperCase() + spaceadd(currentService.length )

        if (!currentService.equals(getString(R.string.Withdrawal), true) && !currentService.equals(getString(R.string.Transfer), true)) {
            header +=  spaceadd(currentProduct.length) + currentProduct.toUpperCase() + spaceadd(currentProduct.length)
        }

        currentService = currentService.trim()
        currentApproval = currentApproval.trim()
        currentProduct = currentProduct.trim()

        if (currentApproval.equals(getString(R.string.Approved), true)){
           ref =  "REF: " + currentRef +  space(currentRef.length+5)
        }

        if (currentService.equals(getString(R.string.Airtime), true) ) {

           body_a =    spaceadd(currentApproval.length) + currentApproval.toUpperCase() + spaceadd(currentApproval.length)  +                                    newline +   "WID: "+ walletId + space(walletId.length+5)+ ref

            if (currentProduct.contains("Pin", true)) {
                body_pin = "PIN: " + currentPin + space(currentPin.length + 5) +
                        "USAGE: " + currentInfo + space(currentInfo.length + 7)
            }
           body_b =         "RECIPIENT: "+ currentPhone + space(currentPhone.length+11)+
                            "DATE: " + date + space(date.length+6) +
                            "PAYMENT METHOD: " + paymentMethod + space(paymentMethod.length+16) + newline + starline +

                            spaceadd(currentAmount.length)+ currentAmount + spaceadd(currentAmount.length)

           body_c =   starline +currentReason.toUpperCase() + space(currentReason.length)

        }

        if (currentService.equals(getString(R.string.Multichoice), true) ) {

                body_a = spaceadd(currentApproval.length) + currentApproval.toUpperCase() + spaceadd(currentApproval.length) +
                        newline + "WID: " + walletId + space(walletId.length + 5) +  ref

                body_b = "RECIPIENT: " + currentPhone + space(currentPhone.length + 11) +
                        "PAYMENT METHOD: " + paymentMethod + space(paymentMethod.length+16) +
                        "DATE: " + date + space(date.length + 6) +
                        "NAME: " + currentName + space(currentName.length + 6) +
                        newline +   starline +
                        spaceadd(currentAmount.length)+ currentAmount + spaceadd(currentAmount.length)

                body_c =  starline + currentReason.toUpperCase() + space(currentReason.length)

                body_d = newline + itexSettlement + space(itexSettlement.length)

                body_e = itexNumbers + space(itexNumbers.length) +
                        itexEmail + space(itexEmail.length)

        }

        if (currentService.equals(getString(R.string.Smile), true) ) {

            body_a = spaceadd(currentApproval.length) + currentApproval.toUpperCase() + spaceadd(currentApproval.length) +
                    newline + "WID: " + walletId + space(walletId.length + 5) +  ref

            body_b = "RECIPIENT: " + currentPhone + space(currentPhone.length + 11) +
                    "PAYMENT METHOD: " + paymentMethod + space(paymentMethod.length+16) +
                    "DATE: " + date + space(date.length + 6) +
                    "NAME: " + currentName + space(currentName.length + 6) +
                    newline +   starline +
                    spaceadd(currentAmount.length)+ currentAmount + spaceadd(currentAmount.length)

            body_c =  starline + currentReason.toUpperCase() + space(currentReason.length)

            body_d = newline + itexSettlement + space(itexSettlement.length)

            body_e = itexNumbers + space(itexNumbers.length) +
                    itexEmail + space(itexEmail.length)

        }

        if ( currentService.equals(getString(R.string.Disco), true)){

            body_a =   spaceadd(currentApproval.length) + currentApproval.toUpperCase() + spaceadd(currentApproval.length)+
                    newline + "WID: "+ walletId + space(walletId.length+5)+
                    "PAYMENT METHOD: " + paymentMethod + space(paymentMethod.length+16) +
                    "PAYMENT PLAN: " +currentType + space(currentType.length+6)+
                    "ADDRESS" +currentAddress + space(currentAddress.length+6)
                    "TARIFF: " + currentTariff + space(currentTariff.length+6)+
                    "Arrears DED: " + currentArrears + space(currentArrears.length+7)+
                    "CASHIER: " + currentCashier + space(currentCashier.length+6)

            body_b =         "METER NO: " +currentMeter + space(currentMeter.length+7)+
                    "NAME: " + currentName + space(currentName.length+6)+
                    "UNITS: "+ currentUnitValue + space(currentUnitValue.length+6)+
                    "VAT: "+ currentVat + space(currentVat.length+6)
                    "DATE: " + date + space(date.length+6)+ newline + starline +
                    spaceadd(currentAmount.length)+ currentAmount + spaceadd(currentAmount.length)

            body_c =    starline + currentReason.toUpperCase() + space(currentReason.length)

        }

        if (currentService.equals(getString(R.string.Transfer), true)){
           body_ = spaceadd(currentApproval.length) + currentApproval.toUpperCase() + spaceadd(currentApproval.length)+
                    newline + "WID: "+ walletId + space(walletId.length+5)+
                    "PAYMENT METHOD: " + paymentMethod + space(paymentMethod.length+16)

            body_a = "NAME: "+currentName + space(currentName.length+6)+
                     "ACCOUNT: "+currentAccount + space(currentAccount.length+9)

            body_b = "DATE: " + date + space(date.length+6)+ newline + starline +
                    spaceadd(currentAmount.length)+ currentAmount + spaceadd(currentAmount.length)

            body_c =    starline + currentReason.toUpperCase() + space(currentReason.length)
        }

        if (currentService.equals(getString(R.string.Withdrawal), true)){

                try{
                    transaction = intent.getSerializableExtra("transaction") as TransactionModel

                    if (transaction != null) {
                        paymentMethod = "CARD"
                        if (transaction!!.isApproved) {
                            imgStatus.setImageResource(R.drawable.transaction_approved)
                        } else {
                            imgStatus.setImageResource(R.drawable.transaction_declined)
                        }

                        vasresult.text = transaction!!.transactionStatus
                    }
                }

                catch (e : Exception){
                    Log.e("okh", e.message)
                }

                body_ =   spaceadd(transaction!!.transactionStatus.length)+ transaction!!.transactionStatus + spaceadd(transaction!!.transactionStatus.length) +
                        "RRN: "+ transaction!!.rrn + space(transaction!!.rrn.length+5) +
                        "AMT: "+ currentAmount + space(currentAmount.toString().length+5)+
                        "TID: "+ terminalID + space(terminalID.length+5)+
                        "PAYMENT METHOD: " + paymentMethod + space(paymentMethod.length+16)

                body_a = "ACCT: "+transaction!!.accountType + space(transaction!!.accountType.length+6) +
                         "WID: "+ walletId + space(walletId.length+5) +
                        "MID: "+transaction!!.merchantId + space(transaction!!.merchantId.length+5) +
                        "VID: "+transaction!!.terminalID + space(transaction!!.terminalID.length+5)+
                        tranType+  transaction!!.cardholderName.trim() + space(transaction!!.cardholderName.trim().length+6)


                body_b =  "AUTH ID: "+transaction!!.authID + space(transaction!!.authID.length+9)+
                        "PAN: "+transaction!!.pan + space(transaction!!.pan.length+5) +
                        "EXP: "+transaction!!.expiryDate + space(transaction!!.expiryDate.length+5) +
                        "STAN: "+transaction!!.stan + space(transaction!!.stan.length+6) +
                        "DATE: " + date + space(date.length+6)

                body_c =  newline + starline +
                        spaceadd(currentAmount.length)+ currentAmount + spaceadd(currentAmount.length)+  starline + currentReason.toUpperCase() + space(currentReason.length)

        }
    }

    fun space(length : Int) : String{
        spaces = ""
        //val total = length + 6

        if (32 > length ) {
         val spacelength = 32 - length
            for (i in 1..spacelength) {
                spaces += " "

            }
        }
        else if (length > 64){
             val spacelength = 96 - length
            for (i in 1..spacelength){
                spaces += " "
            }
        }
        else {
            val spacelength = 64 - length
            for (i in 1..spacelength) {
                spaces += " "
            }
        }
        return  spaces
    }

    fun spaceadd(length : Int) : String {
        spacesadd = ""

        if (32 > length) {
            val spacelength = (32 - length) / 2
            for (i in 1..spacelength) {
                spacesadd += " "
            }
        }
        else{
             val spacelength = (64 - length) / 2
               for (i in 1..spacelength) {
                     spacesadd += " "
               }
        }
        return spacesadd
    }

    fun adjust(content : String) : String {
        var newContent = ""

        if (!content.isEmpty()){
        val even = content.length % 2
        if (even == 1) {
            var newContent = content.trim()
            newContent += " "
            return newContent
        } else {
            return content
        }
    }
        else {
            return content
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (D) Log.d("okh", "onActivityResult $resultCode requestCode $requestCode")

        try{
            when (requestCode) {
                REQUEST_CONNECT_DEVICE -> {
                    bluetoothPrinter.connectPrinterStatus(false)
                    // When DeviceListActivity returns with a device to connectPrinter
                    if (resultCode == Activity.RESULT_OK) {
                        // Get the device MAC address
                        val address = data?.extras!!
                                .getString(BluetoothActivity.EXTRA_DEVICE_ADDRESS)
                        // Get the BLuetoothDevice object
                        try {
                            //send device address (UID)
                            bluetoothPrinter.connectPrinter(address)


                        } catch (e: Exception) {
                            // TODO 14: handle exception
                            Log.e("okh", "Bluetooth Exception")
                        }

                    } else {
                        bluetoothPrinter.disConnecteBluetooth()
//                        bluetoothPrinter.showSnackBar("Error Connecting to Device")

                    }
                }

                REQUEST_ENABLE_BT -> {
                    // When the request to enable Bluetooth returns
                    if (resultCode == Activity.RESULT_OK) {
                        // Bluetooth is now enabled, so set up a chat session
                        bluetoothPrinter.showSnackBar("Bluetooth Enabled.")
                        if (!bluetoothPrinter.isPrinterConnected) {
                            bluetoothPrinter.connectPrinterStatus(true)
                            bluetoothPrinter.bluetoothConnect()
                        }
                    } else {
                        // User did not enable Bluetooth or an error occured
                        bluetoothPrinter.showSnackBar("Bluetooth not connected")
                        bluetoothPrinter.bluetoothEnable()
                    }
                }
            }
        }catch (e : Exception){

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_result_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val itemId = item?.itemId
        if (itemId==R.id.action_print) {

            try{

                printContent(header)
                if (currentService.equals(getString(R.string.Disco), true) ){
                    printContent(body_a)
                    printContent(body_b)
                    printContent(body_c)
                }

               else if (currentService.equals(getString(R.string.Airtime), true)){
                    printContent(body_a)
                    printContent(body_pin)
                    printContent(body_b)
                    printContent(body_c)
      
                }

                else if (currentService.equals(getString(R.string.Multichoice), true)){
                    printContent(body_a)
                    printContent(body_b)
                    printContent(body_c)
                    printContent(body_d)
                    printContent(body_e)

                }
                else if (currentService.equals(getString(R.string.Smile), true)){
                    printContent(body_a)
                    printContent(body_b)
                    printContent(body_c)
                    printContent(body_d)
                    printContent(body_e)

                }
                else if (currentService.equals(getString(R.string.Withdrawal),     true)) {
                    printContent(body_)
                    printContent(body_a)
                    printContent(body_b)
                    printContent(body_c)
                }
                else if (currentService.equals(getString(R.string.Transfer), true)) {
                    printContent(body_)
                    printContent(body_a)
                    printContent(body_b)
                    printContent(body_c)

                }

                printContent(spaceString)
                printContent(footer)
                printContent(footer2)
                printContent(newline)
                printContent(newline)
                    if (bluetoothPrinter.isBluetoothConnected()) {
                        bluetoothPrinter.mPrinterService.stop()
                    }
            }catch (e : Exception){

            }

        }
        return super.onOptionsItemSelected(item)
    }

    fun printContent(body_ : String){
        if (bluetoothPrinter.isBluetoothConnected()) {
            bluetoothPrinter.printContent(body_)

        } else {
            //start connection request if not connected
            bluetoothPrinter.startBluetooth()
        }
    }

    fun printImage(bitmapByte : ByteArray){
        if (bluetoothPrinter.isBluetoothConnected()) {
            bluetoothPrinter.printImage(bitmapByte)

        } else {
            //start connection request if not connected
            bluetoothPrinter.startBluetooth()
        }
    }

    override fun onStop() {
        super.onStop()

        if (bluetoothPrinter.isBluetoothConnected()) {
            bluetoothPrinter.mPrinterService.stop()
        }
    }

    override fun onBackPressed() {
        finish()
        super.onBackPressed()
    }

    companion object {
        const val EXTRA_RESULT = "result"
    }

}

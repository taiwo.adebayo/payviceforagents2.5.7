package com.iisysgroup.payviceforagents.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.callbacks.OnPlanInputFragmentInteractionListener;
import com.iisysgroup.payviceforagents.callbacks.OnValueInputFragmentInteractionListener;
import com.iisysgroup.payviceforagents.entities.Service;
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.VasServices;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;


public class BillPlanValueInputFragment extends BaseBillFragment {

    Service.Product planProduct, valueProduct;
    @BindView(R.id.productOptionGrp)
    RadioGroup radiotopup;
    @BindView(R.id.radiotopuppin)
    RadioButton radPlanButton;
    @BindView(R.id.radiotopupvirtual)
    RadioButton radValueButton;
    @BindView(R.id.amountEntryLayout)
    LinearLayout vtuLayout;
    @BindView(R.id.pinLayout)
    LinearLayout pinLayout;
    @BindView(R.id.selectAmountLayout)
    RelativeLayout selectPinAmount;
    @BindView(R.id.pinCountText)
    EditText pinCountText;
    @BindView(R.id.recipientTIL)
    TextInputLayout recipientTIL;
    @BindView(R.id.beneficiaryEdit)
    EditText serviceNumberEdit;
    private OnValueInputFragmentInteractionListener valueInputListener;
    private OnPlanInputFragmentInteractionListener planInputListener;


    public BillPlanValueInputFragment() {
        // Required empty public constructor
    }

    public static BillPlanValueInputFragment newInstance(String service) {
        BillPlanValueInputFragment fragment = new BillPlanValueInputFragment();
        Bundle args = new Bundle();
        args.putString(SERVICE, service);
        fragment.setArguments(args);
        return fragment;
    }

    @OnCheckedChanged(R.id.radiotopupvirtual)
    void onValueSelected() {
        if (isValue()) {
            product = valueProduct;
            serviceText.setText(product.name);
            vtuLayout.setVisibility(View.VISIBLE);
            pinLayout.setVisibility(View.GONE);
        }

    }

    @OnCheckedChanged(R.id.radiotopuppin)
    void onPlanSelected() {
        if (isPlan()) {
            product = planProduct;
            serviceText.setText(product.name);
            pinLayout.setVisibility(View.VISIBLE);
            vtuLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bill_plan_value_input, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        subTitleText.setText(service.name);
        selectServiceLayout.setEnabled(false);

        for (Service.Product product : service.products) {
            if (product.proxyCode == null) {
                valueProduct = product;
            } else {
                planProduct = product;
            }
        }

        radPlanButton.setText(planProduct.name.substring(service.name.length()));
        radValueButton.setText(valueProduct.name.substring(service.name.length()));

        radValueButton.setChecked(true);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnValueInputFragmentInteractionListener && context instanceof OnPlanInputFragmentInteractionListener) {
            valueInputListener = (OnValueInputFragmentInteractionListener) context;
            planInputListener = (OnPlanInputFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnValueInputFragmentInteractionListener and OnPlanInputFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        valueInputListener = null;
        planInputListener = null;
    }

    boolean isValue() {
        return radValueButton.isChecked();
    }

    boolean isPlan() {
        return radPlanButton.isChecked();
    }


    @OnClick(R.id.selectAmountLayout)
    void selectPlanAmount() {
        selectPlan();
    }


    boolean isValidValueInputs() {

        if (product == null) {
            serviceText.setError("Please select product");
            Helper.showErrorAnim(serviceText);
            Helper.showSnackBar(serviceText, "Please select product");
            return false;
        }
        if (serviceNumberEdit.getText().toString().trim().isEmpty()) {
            serviceNumberEdit.setError(VasServices.VAS_SERVICE_INPUT_TEXT_DESC.get(service.name));
            serviceNumberEdit.requestFocus();
            Helper.showErrorAnim(serviceNumberEdit);
            return false;
        }


        if (amountEdit.getText().toString().isEmpty()) {
            amountEdit.setError("Enter amount");
            amountEdit.requestFocus();
            Helper.showErrorAnim(amountEdit);
            return false;
        }


        if (Integer.parseInt(amountEdit.getText().toString().replaceAll("₦", "").replaceAll(",", "").trim()) < 50) {
            amountEdit.setError("Amount cannot be less than 50");
            amountEdit.requestFocus();
            Helper.showErrorAnim(amountEdit);
            return false;
        }

        return true;
    }

    boolean isValidPlanInputs() {

        if (product == null) {
            serviceText.setError("Please select product");
            Helper.showErrorAnim(serviceText);
            Helper.showSnackBar(serviceText, "Please select product");
            return false;
        }

        if (planValue == null) {
            pinAmountText.setError("Please select plan");
            Helper.showErrorAnim(pinAmountText);
            Helper.showSnackBar(pinAmountText, "Please select plan");
            return false;
        }

        if (pinCountText.getText().toString().trim().isEmpty()) {
            Helper.showErrorAnim(pinCountText);
            pinCountText.setError("Please enter number of months");
            pinCountText.requestFocus();
            return false;
        }

        if (serviceNumberEdit.getText().toString().trim().isEmpty()) {
            serviceNumberEdit.setError(VasServices.VAS_SERVICE_INPUT_TEXT_DESC.get(service.name));
            serviceNumberEdit.requestFocus();
            Helper.showErrorAnim(serviceNumberEdit);
            return false;
        }

        return true;
    }

    @OnClick(R.id.proceedBtn)
    void proceed() {
        Beneficiary beneficiary = new Beneficiary(serviceNumberEdit.getText().toString(),
                serviceNumberEdit.getText().toString(), "");
        if (isValue()) {
            if (isValidValueInputs()) {
                valueInputListener.onValueInputFragmentInteraction(product, amountEdit.getText().toString().trim(),beneficiary,"","","","");
            }
        } else if (isPlan()) {
            if (isValidPlanInputs()) {
                planInputListener.onPlanInputFragmentInteraction(product,
                        beneficiary, planValue, Integer.parseInt(pinCountText.getText().toString()));
            }
        }


    }


}

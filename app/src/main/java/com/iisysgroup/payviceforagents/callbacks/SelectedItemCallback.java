package com.iisysgroup.payviceforagents.callbacks;

/**
 * Created by Bamitale@Itex on 08/05/2017.
 */

public interface SelectedItemCallback<T> {
    void onItemSelected(T item);
}

package com.iisysgroup.payviceforagents.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.models.HistoryParam;
import com.iisysgroup.payviceforagents.models.Transaction;
import com.iisysgroup.payviceforagents.models.TransactionHistory;
import com.iisysgroup.payviceforagents.models.UserTransactions;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.HistoryAdapter_D;
import com.iisysgroup.payviceforagents.util.TransactionHistoryApi;
import com.interswitchng.sdk.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Bamitale@Itex on 5/24/2016.
 */
public class HistoryFragment_D extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    HistoryAdapter_D historyAdapter;

    List<Transaction> historyList = new ArrayList<>();
    Handler handler = new Handler();

    View errorLayout, emptyLayout;

    String userId = SecureStorage.retrieve(Helper.USER_ID, "");
    String terminalId = SecureStorage.retrieve(Helper.TERMINAL_ID, "");
    String password = SecureStorage.retrieve(Helper.STORED_PASSWORD,"");
    private int limit, currentPage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_transaction_history, container, false);

        populateView(view);

        return view;
    }

    protected void populateView(View view) {
        recyclerView = view.findViewById(android.R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        historyAdapter = new HistoryAdapter_D(getActivity(), historyList);
        recyclerView.setAdapter(historyAdapter);

        swipeRefreshLayout = view.findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(this);


        errorLayout = view.findViewById(R.id.error_layout);
        emptyLayout = view.findViewById(R.id.empty_layout);

        String historySerial = Helper.getPreference(getActivity(), Helper.HISTORY_SERIAL, "");


//        if (!historySerial.trim().isEmpty()) {
//            historyList.addAll(HistoryAdapter_D.processRecords(historySerial));
//            historyAdapter.notifyDataSetChanged();
//        }


        downloadRecords();
    }

    @Override
    public void onRefresh() {
        downloadRecords();
    }

    void downloadRecords() {

        if (!swipeRefreshLayout.isRefreshing() && historyList.isEmpty()) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        }

        errorLayout.setVisibility(View.INVISIBLE);
        emptyLayout.setVisibility(View.INVISIBLE);


        getTransactionHistories();

    }

    final List<Transaction> transf = new ArrayList<>();

    public List<User> getTransactionHistories() {

        limit=15; currentPage =1;

        Log.i("RetrofitResponse","Here");

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor)
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://vas.itexapp.com")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Log.i("RetrofitResponse","Here");

        TransactionHistoryApi transactionHistoryApi = retrofit.create(TransactionHistoryApi.class);

//        Call<TransactionHistory> transactionHistoryCall = transactionHistoryApi.fetchHistoty( new HistoryParam(terminalId,userId,password,limit,currentPage), terminalId);

        Call<TransactionHistory> transactionHistoryCall = transactionHistoryApi.fetchHistoty(new HistoryParam(terminalId,userId,password,limit,currentPage));
        Log.i("RetrofitResponse","Here");
        transactionHistoryCall.enqueue(new Callback<TransactionHistory>() {
            @Override
            public void onResponse(Call<TransactionHistory> call, Response<TransactionHistory> response) {
                Log.i("RetrofitResponse","Here");

              try {
                  final UserTransactions data = response.body().getUserTransactions();

                  handler.post(new Runnable() {
                      @Override
                      public void run() {
                          swipeRefreshLayout.setRefreshing(false);
                          swipeRefreshLayout.setVisibility(View.VISIBLE);
                          if (data != null) {
                              if (data.getError() == true) {
                                  recyclerView.setVisibility(View.GONE);
                                  emptyLayout.setVisibility(View.VISIBLE);
                                  return;
                              }

                              if (data.getTransactions().size() != historyList.size()) {
                                  historyList.clear();
                                  historyList.addAll(data.getTransactions());
                                  historyAdapter.notifyDataSetChanged();

                                  //Needed to populate the filters on the filters menu of transaction history
                                  Helper.savePreference(getActivity(), Helper.USER_TRANSACTIONS, new Gson().toJson(data, UserTransactions.class));
//                                Helper.savePreference(getContext(), Helper.DOWNLOAD_BALANCE, false);
                              }
//
                          } else {
                              Helper.runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      Snackbar snackbar = Snackbar.make(errorLayout,
                                              "Could not retrieve transaction history", Snackbar.LENGTH_INDEFINITE);
                                      snackbar.setAction("Refresh", new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                              downloadRecords();
                                          }
                                      });
                                      snackbar.show();
                                  }
                              });
                              if (historyList.isEmpty()) {
                                  swipeRefreshLayout.setVisibility(View.INVISIBLE);
                                  errorLayout.setVisibility(View.VISIBLE);
                              }

                          }
                      }

                  });

              }catch (Exception e){

              }

//                     for(Transaction trand : historyResponse.getUserTransactions().getTransactions()) {
//
//
//                         Log.i("RertofitTransaction", "onResponse: " + trand.getAuditDescription());
//
//                     }
//
//            }



            }

            @Override
            public void onFailure(Call<TransactionHistory> call, Throwable t) {
               // Log.i("Response", t.getMessage().toString());

            }
        });

        return null;
    }


}







package com.iisysgroup.payviceforagents.payviceservices

import android.content.Context
import com.iisgroup.bluetoothprinterlib.Utils.Util.Language.it
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.entities.VasResult
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
//import com.iisysgroup.payviceforagents.util.db
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * Created by Bamitale@Itex on 27/02/2018.
 */


object Requests {

    const val BASE_PFM_URL = "http://www.pfm.payvice.com/api/mobile/"

    @JvmStatic
    fun processRequest(url: String, params: Map<String, String>): VasResult {
        val transactionManager = VasTransactionManager(url, params)
        return transactionManager.processTransaction()
    }

    @JvmStatic
    fun doLogOut(context: Context) {
        Helper.clearData()
        Helper.savePreference(context, Helper.BALANCE, "")
        Helper.savePreference(context, Helper.HISTORY_SERIAL, "")
        SecureStorage.store(Helper.LOGGED_IN, false)

//        Thread(Runnable {
//            try {
//                val db = context.db
//                db.cardDao().rxGetAll().subscribeOn(Schedulers.io()).subscribe {
//                    it?.let {
//                        db.cardDao().deleteAll(it)
//                    }
//                }
//
//                logOut(context)
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }).start()
    }

    @Throws(Exception::class)
    private fun logOut(context: Context) {
        val url = context.getString(R.string.tams_url)

        val params = HashMap<String, String>()
        val action = context.resources.getString(R.string.tams_logout_action)
        val terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID, "203301ZA")
        val userId = SecureStorage.retrieve(Helper.USER_ID, "")

        params["action"] = action
        params["termid"] = terminalID
        params["userid"] = userId


        val tamsResult = processRequest(url, params)

    }

    @JvmStatic
    fun initUser(context: Context, userId: String): VasResult {
        val terminalID = context.getString(R.string.tams_default_terminal_id)

        return initUser(context, terminalID, userId)
    }

    @JvmStatic
    fun initUser(context: Context, terminalID: String, userID: String): VasResult {
        val params = HashMap<String, String>()

        val action = context.getString(R.string.tams_login_action)
        val initControl = context.getString(R.string.tams_init_action)
        val url = context.resources.getString(R.string.tams_url)


        //for init set CONTROL = INIT
        params["action"] = action
        params["termid"] = terminalID
        params["userid"] = userID
        params["control"] = initControl


        return processRequest(url, params)
    }

    @JvmStatic
    fun login(context: Context, userId: String, password: String, walletId: String): VasResult {
        val params = HashMap<String, String>()

        val action = context.getString(R.string.tams_login_action)
        val url = context.resources.getString(R.string.tams_url)

        params["action"] = action
        params["termid"] = walletId
        params["userid"] = userId
        params["password"] = password

        return processRequest(url, params)
    }

    @JvmStatic
    fun verifyFundingPin(context: Context, userID: String, encPin: String, terminalID: String): Boolean {

        val action = context.getString(R.string.tams_webapi_action)
        val url = context.getString(R.string.tams_url)


        val control = "DOMIGS"

        val params = HashMap<String, String>()
        params["action"] = action
        params["userid"] = userID
        params["termid"] = terminalID
        params["control"] = control
        params["op"] = "INIT"
        params["pin"] = encPin

        val tamsResult = processRequest(url, params)

        return tamsResult.result == VasResult.Result.APPROVED

    }

    @JvmStatic
    fun sendTransferRequest(context: Context, beneficiaryWalletID: String, amount: String, userPin: String): VasResult {

        val params = TreeMap<String, String>()

        val action = context.getString(R.string.tams_webapi_action)
        val control = "Transfer"
        val termID = SecureStorage.retrieve(Helper.TERMINAL_ID, "")
        val userid = SecureStorage.retrieve(Helper.USER_ID, "")


        params["action"] = action
        params["control"] = control
        params["termid"] = termID
        params["recipient_terminal_id"] = beneficiaryWalletID
        params["amount"] = amount
        params["pin"] = userPin
        params["userid"] = userid


        val url = context.getString(R.string.tams_url)
        return processRequest(url, params)

    }

    @JvmStatic
    fun sendTamsProxyRequest(context: Context, requestCode: String): String {

        val url = context.getString(R.string.tams_url)
        val action = context.getString(R.string.tams_webapi_action)

        val userId = SecureStorage.retrieve(Helper.USER_ID, "")
        val terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID,
                context.getString(R.string.tams_default_terminal_id))

        val params = HashMap<String, String>()
        params["action"] = action
        params["amount"] = ""
        params["control"] = ""
        params["pass"] = SecureStorage.retrieve(Helper.STORED_PASSWORD, "")
        params["phoneno"] = ""
        params["termid"] = terminalID
        params["userid"] = userId
        params["op"] = "PROXY"
        params["network"] = requestCode

        val result = processRequest(url, params)
        return if (result.result == VasResult.Result.APPROVED && result.message.contains("::"))
            result.message
        else
            ""
    }

    @JvmStatic
    fun getTamsPlans(context: Context, proxyCode: String) =
            Helper.parseTamsPlanValueProxyResponse(sendTamsProxyRequest(context, proxyCode))

    @JvmStatic
    fun getTamsPins(context: Context, proxyCode: String) = Helper.parseTamsPinValueProxyResponse(sendTamsProxyRequest(context, proxyCode))


}
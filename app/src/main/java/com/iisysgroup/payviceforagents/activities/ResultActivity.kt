package com.iisysgroup.payviceforagents.activities
import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.iisgroup.bluetoothprinterlib.Manaer.PrintfManager
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.baseimpl.viewmodel.ResultActivityViewModel
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.StatusObject
import com.iisysgroup.payviceforagents.fragments.StatusFragment
import com.iisysgroup.payviceforagents.models.TransactionModel
import com.iisysgroup.payviceforagents.printer.BluetoothActivity
import com.iisysgroup.payviceforagents.printer.BluetoothPrinter
import com.iisysgroup.payviceforagents.printer.BluetoothPrinter.*
import com.iisysgroup.payviceforagents.securestorage.SecureStorage


class ResultActivity : AppCompatActivity() {

    private lateinit var printerManager: PrintfManager
    private lateinit var resultObject: ServiceResult
    private lateinit var bluetoothPrinter: BluetoothPrinter
    private lateinit var result : StatusObject
    private lateinit var currentService : String
    private lateinit var currentAmount : String
    private lateinit var currentPhone : String
    private lateinit var printString : String
    private lateinit var itexString : String
    private lateinit var spaceString : String

    private var serviceResult : ServiceResult ?= null
    private var transaction : TransactionModel ?= null

    private val viewModel by lazy {
        ViewModelProviders.of(this)[ResultActivityViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        bluetoothPrinter = BluetoothPrinter(this@ResultActivity)
        currentService = SecureStorage.retrieve("currentservice", "Value Added Service")
        currentAmount =  SecureStorage.retrieve("currentAmount", "0")
        currentPhone =  SecureStorage.retrieve("currentPhone", "0")

        try{
            serviceResult = intent.getSerializableExtra(EXTRA_RESULT) as ServiceResult
            transaction = intent.getSerializableExtra("transaction") as TransactionModel
            viewModel.setStatusObject(serviceResult!!.toStatusObject())
            if (serviceResult != null){
                result = serviceResult!!.toStatusObject()
                var statusReason : String
                if (result.statusMessage.contains("Declined")){
                    statusReason  = result.statusReason + "                     "
                }
                else{
                    statusReason = result.statusReason + "     "
                }

                if (currentPhone.length == 10){
                    currentPhone = "$statusReason$currentPhone                    "
                }
                else if (currentPhone.length == 11){
                    currentPhone = "$statusReason$currentPhone                     "
                }
            }
        }catch (e : Exception){

        }


//        Log.d("okh", serviceResult.toStatusObject().statusAmount+ serviceResult.toStatusObject().statusMessage+serviceResult.toStatusObject().statusReason+ serviceResult.toStatusObject().statusRecipient+ serviceResult.toStatusObject().statusService+ serviceResult.toStatusObject().statusIsApproved())


        var serviceString = "$currentService $currentAmount naira"
        itexString = "Itex Integrated Systems          $serviceString"

        val remain = 62 - itexString.length

        var space : String = ""
        for (i in 1..remain){
            space += " "
        }
        itexString += space
        spaceString = "                                  "

        var space2 : String = ""
        val remain2 = 31 - "$currentService $currentAmount naira".length
        for (i in 1..remain2){
            space2 += " "
        }

        serviceString += space2
        if (currentService.contains("GOTV")){
            spaceString = "                                  a"
        }

        if (currentService.equals("transfer", true)) {

            if (transaction != null) {
                printString = "             " + "PAYVICE" + "            " +
                        result.statusMessage + "            " +
                        transaction!!.rrn + "            " +
                        transaction!!.authID + "            " +
                        transaction!!.transactionStatus + "            " +
                        transaction!!.transactionStatusReason + "            " +
                        transaction!!.accountType + "            " +
                        transaction!!.accountType + "            " +
                        transaction!!.merchantId + "            " +
                        transaction!!.terminalID + "            " +
                        transaction!!.cardholderName + "            " +
                        transaction!!.pan + "            " +
                        transaction!!.expiryDate + "            " +
                        transaction!!.stan + "            " +
                        transaction!!.ISODateTime + "            " +
                        transaction!!.rrn
            }
        }else{
            printString = "             " + "PAYVICE" + "            " +
                    result.statusMessage + "            " +
                    currentPhone
        }


        //  initPrinter()
    }

    private fun initPrinter() {
        printerManager = PrintfManager.getInstance(this)
//        printerManager.addBluetoothChangLister { name, address ->
//            toast("Printer $name($address) attached")
//        }
        printerManager.defaultConnection()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (D) Log.d("okh", "onActivityResult $resultCode requestCode $requestCode")

        try{
            when (requestCode) {
                REQUEST_CONNECT_DEVICE -> {
                    bluetoothPrinter.connectPrinterStatus(false)
                    // When DeviceListActivity returns with a device to connectPrinter
                    if (resultCode == Activity.RESULT_OK) {
                        // Get the device MAC address
                        val address = data?.extras!!
                                .getString(BluetoothActivity.EXTRA_DEVICE_ADDRESS)
                        // Get the BLuetoothDevice object
                        try {
                            //send device address (UID)
                            bluetoothPrinter.connectPrinter(address)


                        } catch (e: Exception) {
                            // TODO 14: handle exception
                            Log.e("okh", "Bluetooth Exception")
                        }

                    } else {
                        bluetoothPrinter.disConnecteBluetooth()
//                        bluetoothPrinter.showSnackBar("Error Connecting to Device")

                    }
                }

                REQUEST_ENABLE_BT -> {
                    // When the request to enable Bluetooth returns
                    if (resultCode == Activity.RESULT_OK) {
                        // Bluetooth is now enabled, so set up a chat session
                        bluetoothPrinter.showSnackBar("Bluetooth Enabled.")
                        if (!bluetoothPrinter.isPrinterConnected) {
                            bluetoothPrinter.connectPrinterStatus(true)
                            bluetoothPrinter.bluetoothConnect()
                        }
                    } else {
                        // User did not enable Bluetooth or an error occured
                        bluetoothPrinter.showSnackBar("Bluetooth not connected")
                        bluetoothPrinter.bluetoothEnable()
                    }
                }
            }
        }catch (e : Exception){

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_result_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val itemId = item?.itemId
        if (itemId==R.id.action_print) {

            try{
                printContent(printString)
                printContent(itexString)
                printContent(spaceString)
                    if (bluetoothPrinter.isBluetoothConnected()) {
                        bluetoothPrinter.mPrinterService.stop()
                    }
            }catch (e : Exception){

            }

        }
        return super.onOptionsItemSelected(item)
    }

    fun printContent(printString : String){
        if (bluetoothPrinter.isBluetoothConnected()) {

            bluetoothPrinter.printContent(printString)

        } else {
            //start connection request if not connected
            bluetoothPrinter.startBluetooth()
        }
    }

    override fun onStop() {
        super.onStop()

        if (bluetoothPrinter.isBluetoothConnected()) {
            bluetoothPrinter.mPrinterService.stop()
        }
    }


//    private fun doPrint() {
//        try {
//            thread {
//                printHeader()
//                resultObject.printResult(printerManager)
//                printFooter()
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//            alert {
//                title = "Error"
//                message = "An error occurred while printing. - ${e.localizedMessage}"
//                okButton { }
//            }.show()
//        }
//    }
//
//    private fun printHeader() {
//        val bitmap = BitmapFactory.decodeResource(resources, resultObject.getPrinterLogo()
//                ?: R.mipmap.ic_launcher)
//        printerManager.printf_bitmap(bitmap)
//        printerManager.printLargeText(5, getString(R.string.app_name))
//        printerManager.printfWrap()
//    }
//
//    private fun printFooter() {
//        printerManager.printfWrap(2)
//        printerManager.printPlusLine_50()
//        printerManager.printfWrap()
//        printerManager.printText("Powered by Itex Integrated Services Limited.")
//        printerManager.printfWrap()
//        printerManager.printTabSpace(6)
//        printerManager.printText("Helpline: 09070314511")
//        printerManager.printfWrap(4)
//    }
//

    class ResultStatusFragment : StatusFragment() {

        override fun initViews(view: View?) {

            try{
                val viewModel by lazy {
                    ViewModelProviders.of(activity!!)[ResultActivityViewModel::class.java]
                }

                statusObject = viewModel.statusLiveData.value
            }catch (e : Exception){

            }

            super.initViews(view)
        }

    }

    companion object {
        const val EXTRA_RESULT = "result"
    }
}

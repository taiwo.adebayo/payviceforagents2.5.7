package com.iisysgroup.payviceforagents.util;

import android.app.Activity;
import android.content.res.AssetManager;

import com.iisysgroup.payviceforagents.entities.StatusObject;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;

import java.io.IOException;
import java.io.InputStreamReader;

public class EmailHandler {

    public static void sendNotificationMail(final Activity activity, StatusObject statusObject) {

        try {
            InputStreamReader reader = new InputStreamReader(activity.getAssets().open("index.html", AssetManager.ACCESS_BUFFER));
            char buffer[] = new char[1024];
            int readlength = 0;
            StringBuilder sb = new StringBuilder();
            while ((readlength = reader.read(buffer)) != -1) {
                sb.append(buffer, 0, readlength);
            }

            String title = "Transaction Receipt for " + SecureStorage.retrieve(Helper.USERNAME, "");
            String username = SecureStorage.retrieve(Helper.USERNAME, "");
            String message = sb.toString().trim()
                    .replace("$amount", statusObject.getStatusAmount())
                    .replace("$recipient", statusObject.statusRecipient)
                    .replace("$product", statusObject.statusService)
                    .replace("$details", statusObject.statusReason)
                    .replace("$subject", title)
                    .replace("$status", statusObject.getStatusMessage())
                    .replace("$username", username)
                    .replace("$username", username);

            final String emailAddress = SecureStorage.retrieve(Helper.USER_EMAIL, "");
//        Log.i("Payvice", "Recipient Email: " +  emailAddress);
            final EmailNotifier.EmailObject emailObject = new EmailNotifier.EmailObject();
            emailObject.message = message;
            emailObject.subject = title;


            Helper.ThreadService.execute(new Runnable() {
                @Override
                public void run() {
                    new EmailNotifier(activity)
                            .sendEmail(emailObject, emailAddress);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

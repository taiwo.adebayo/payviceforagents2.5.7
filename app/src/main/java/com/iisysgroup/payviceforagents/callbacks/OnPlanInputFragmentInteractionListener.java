package com.iisysgroup.payviceforagents.callbacks;

import com.iisysgroup.payviceforagents.entities.PlanValue;
import com.iisysgroup.payviceforagents.entities.Service;
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary;

/**
 * Created by Bamitale@Itex on 09/05/2017.
 */
public interface OnPlanInputFragmentInteractionListener {
    void onPlanInputFragmentInteraction(Service.Product product, Beneficiary beneficiary, PlanValue planValue, int period);
}

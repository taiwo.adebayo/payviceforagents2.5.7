package com.iisysgroup.payviceforagents.baseimpl.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.iisysgroup.payviceforagents.base.view.LoginView

class LoginViewModel : ViewModel(), LoginView {
    override fun showMessage(message: String) {
        messageLiveData.value = message
    }

    private val progressLiveData = MutableLiveData<Boolean>()
    val onProgressUpdate: LiveData<Boolean>
        get() = progressLiveData

    private val loginLiveData = MutableLiveData<Void>()
    val onLoginSuccess: LiveData<Void>
        get() = loginLiveData

    private val messageLiveData = MutableLiveData<String>()
    val onLoginMessage : LiveData<String>
        get() = messageLiveData

    private val errorLiveData = MutableLiveData<String>()
    val onLoginError: LiveData<String>
        get() = errorLiveData

    private val deviceChangedLiveData = MutableLiveData<Void>()
    val onDeviceChanged: LiveData<Void>
        get() = deviceChangedLiveData

    private val updatePinLiveData = MutableLiveData<Void>()
    val onRequestPinUpdate: LiveData<Void>
        get() = updatePinLiveData

    private val userValidationLiveData = MutableLiveData<Void>()
    val onUserIdValidationError: LiveData<Void>
        get() = userValidationLiveData

    private val passwordValidationLiveData = MutableLiveData<Void>()
    val onPasswordValidationError: LiveData<Void>
        get() = passwordValidationLiveData

    override fun showProgress() {
        progressLiveData.value = true
    }

    override fun dismissProgress() {
        progressLiveData.value = false
    }

    override fun setLoginError(throwable: Throwable) {
        errorLiveData.value = throwable.localizedMessage
    }

    override fun setLoginSuccessful() {
        loginLiveData.value = null
    }

    override fun setDeviceChangedError() {
        deviceChangedLiveData.value = null
    }

    override fun setShouldUpdatePin() {
        updatePinLiveData.value = null
    }

    override fun setInvalidUser() {
        userValidationLiveData.value = null
    }

    override fun setInvalidPassword() {
        passwordValidationLiveData.value = null
    }
}
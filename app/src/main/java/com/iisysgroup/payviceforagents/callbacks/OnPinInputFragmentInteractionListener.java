package com.iisysgroup.payviceforagents.callbacks;

import com.iisysgroup.payviceforagents.entities.PinValue;
import com.iisysgroup.payviceforagents.entities.Service;

/**
 * Created by Bamitale@Itex on 09/05/2017.
 */
public interface OnPinInputFragmentInteractionListener {
    // TODO: Update argument type and name
    void onPinInputFragmentInteraction(Service.Product product, PinValue pinValue, int quantity);
}

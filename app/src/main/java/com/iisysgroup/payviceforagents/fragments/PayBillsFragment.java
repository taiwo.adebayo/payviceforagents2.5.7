package com.iisysgroup.payviceforagents.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.util.ImageTitleObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PayBillsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class PayBillsFragment extends ListFragment {

    List<ImageTitleObject> services;
    ImageTitleObject.ImageTitleListAdapter adapter;
    private OnFragmentInteractionListener mListener;


    public PayBillsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        getActivity().setTitle("Pay Bills");

        services = new ArrayList<>();
        services.add(new ImageTitleObject(R.drawable.ic_airplanemode_active_black_48dp, "Airline Tickets", "Purchase airline tickets"));
        services.add(new ImageTitleObject(R.drawable.ic_tv_black_48dp, "Cable TV", "Pay for cable tv subscriptions"));
        services.add(new ImageTitleObject(R.drawable.ic_shopping_cart_black_48dp, "eCommerce", "Pay for products from popular e-commerce sites"));
        services.add(new ImageTitleObject(R.drawable.ic_insert_emoticon_black_48dp, "eDonations", "Make online donations"));
        services.add(new ImageTitleObject(R.drawable.ic_gavel_black_48dp, "eGovernment", "Pay tax, car insurance bill etc"));
        services.add(new ImageTitleObject(R.drawable.ic_cloud_download_black_48dp, "Internet Services", "Pay for internet service subscription from major ISPs"));
        services.add(new ImageTitleObject(R.drawable.ic_wb_incandescent_black_48dp, "Utility", "Pay electricity, water bill etc. here"));
        services.add(new ImageTitleObject(R.drawable.ic_credit_card_black_48dp, "Others", "Make payments for other products and services"));

        adapter = new ImageTitleObject.ImageTitleListAdapter(services, getActivity());

        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (mListener != null) {
            mListener.onPayBillsFragmentInteraction(position);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onPayBillsFragmentInteraction(int position);
    }
}

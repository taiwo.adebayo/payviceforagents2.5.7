package com.iisysgroup.payviceforagents.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.fcm.PayviceFirebaseMessagingService;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Agbede on 3/26/2018.
 */

public class NotificationsUtil {

    public void showNotification(Context context, String email) {
        android.support.v4.app.NotificationCompat.Builder builder = new NotificationCompat.Builder(context, PayviceFirebaseMessagingService.APP_DEFAULT_CHANNEL)
                .setColor(ContextCompat.getColor(context, R.color.accent))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentTitle("Payvice")
                .setContentText(email + " logged in successfully")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .setBigContentTitle("Payvice").bigText(email + " logged in successfully"))
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setAutoCancel(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            builder.setPriority(Notification.PRIORITY_HIGH);
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(300, builder.build());
    }
}

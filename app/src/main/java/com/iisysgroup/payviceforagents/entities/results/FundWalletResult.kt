package com.iisysgroup.payviceforagents.entities.results

import com.iisgroup.bluetoothprinterlib.Manaer.PrintfManager
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.StatusObject
import com.iisysgroup.payviceforagents.util.toCurrencyString
import com.iisysgroup.payvicegamesdk.TamsResponse
import java.io.Serializable

class FundWalletResult(val amount: Int, val response: TamsResponse) : ServiceResult, Serializable {

    override fun toStatusObject() : StatusObject {

        val statusObject = StatusObject(response.result == TamsResponse.Status.APPROVED)
        statusObject.statusReason = response.message
        statusObject.setStatusAmountInKobo(amount.toLong())
        return statusObject
    }

    override fun printResult(printer: PrintfManager) {
        printer.printTabSpace(7)
        printer.printText("Wallet Funding")
        printer.printfWrap(2)

        val status = if (response.result == TamsResponse.Status.APPROVED) {
            StatusObject.STATUS_APPROVED
        } else {
            StatusObject.STATUS_DECLINED
        }


        printer.printTabSpace(6)
        printer.printText(status.toUpperCase())
        printer.printfWrap()

        /*printer.printTwoColumn("Card", response)
        printer.printTwoColumn("Date", response.date)
        printer.printTwoColumn("Reference", response.reference)*/

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap()

        printer.printTabSpace(10)
        printer.printText("N ${amount.toCurrencyString()}")
        printer.printfWrap()

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap(2)

        printer.printText(response.message)
        printer.printfWrap()
    }

    override fun getPrinterLogo(): Int? {
        return null
    }
}
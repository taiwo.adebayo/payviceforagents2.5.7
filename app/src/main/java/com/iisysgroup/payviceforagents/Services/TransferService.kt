package com.iisysgroup.payviceforagents.Services

import com.google.gson.GsonBuilder
import com.iisysgroup.payviceforagents.models.*
import com.iisysgroup.payviceforagents.models.WithdrawalWalletResponse.WithdrawalWalletCreditModel
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import com.wizarpos.emvsample.models.transfer.TransferSuccessModel
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface TransferService {

    //API endpoint for transfer
//    @POST("/tams/tams/transfer-engine.php")
//    fun transfer(@Body transferModel : TransactionDetails, @Header("Content-Type") contentType : String = "application/json", @Header("ITEX-Signature") signature : String, @Header("ITEX-Nonce") nonce : String) : Deferred<TransactionResponse>

    @POST("/vas/vice-banking/transfer/payment")
    fun transfer(@Body transferModel: TransferDetails, @Header("Content-Type") contentType : String = "application/json", @Header("ITEX-Signature") signature : String, @Header("ITEX-Nonce") nonce : String) : Call<TransferSuccessModel>


    @POST("/vas/vice-banking/transfer/lookup")
    fun lookUpAccountNumberTransfer(@Body lookUpRequestDetails : AccountLookUpDetailTransfer) : Call<LookupSuccessModel>

//    //API endpoint for withdraw
//    @POST("/tams/tams/transfer-engine.php")
//    fun withdraw(@Body transferModel : TransactionDetails, @Header("Content-Type") contentType : String = "application/json", @Header("ITEX-Signature") signature : String, @Header("ITEX-Nonce") nonce : String) : Deferred<TransactionResponse>

    @POST("/vas/vice-banking/withdrawal/payment")
    fun withdraw(@Body withdrawalModel : WithdrawalDetails, @Header("Content-Type") contentType : String = "application/json", @Header("ITEX-Signature") signature : String, @Header("ITEX-Nonce") nonce : String) : Call<WithdrawalWalletCreditModel>

    //API endpoint for deposit - cash
//    @POST("/tams/tams/transfer-engine.php")
//    fun deposit(@Body transferModel : TransactionDetails, @Header("Content-Type") contentType : String = "application/json", @Header("ITEX-Signature") signature : String, @Header("ITEX-Nonce") nonce : String) : Deferred<TransactionResponse>

    @POST("/vas/vice-banking/withdrawal/lookup")
    fun lookUpAccountNumberWithdrawal(@Body lookUpRequestDetails : AccountLookUpDetailWithdrawal) : Call<WithdrawalLookupSuccessModel>

    fun withdraws(@Body withdrawalModel : WithdrawalDetails, @Header("Content-Type") contentType : String = "application/json", @Header("ITEX-Signature") signature : String, @Header("ITEX-Nonce") nonce : String) : Deferred<WithdrawalWalletCreditModel>


    companion object Factory {
        //public val BASE_URL = "http://basehuge.itexapp.com:8090"
        public val BASE_URL = "http://vas.itexapp.com"
        fun create(): TransferService {
            val clientBuilder = OkHttpClient.Builder()

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            clientBuilder.connectTimeout(30, TimeUnit.SECONDS)
            clientBuilder.readTimeout(60, TimeUnit.SECONDS)
            clientBuilder.writeTimeout(60, TimeUnit.SECONDS)

            clientBuilder.addInterceptor(logging)

            val service = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(clientBuilder.build())
                    .build().create(TransferService::class.java)

//            val gson = GsonBuilder().setLenient().create()
//            val retrofit = Retrofit.Builder().client(clientBuilder.build()).addConverterFactory(GsonConverterFactory.create(gson)).addCallAdapterFactory(CoroutineCallAdapterFactory()).baseUrl(BASE_URL).build()
//            val service = retrofit.create(TransferService::class.java)

            return service
        }
    }
}
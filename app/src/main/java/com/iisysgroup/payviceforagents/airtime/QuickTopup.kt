@file:Suppress("IMPLICIT_CAST_TO_ANY")

package com.iisysgroup.payviceforagents.airtime

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.activities.PaymentOptionActivity
import com.iisysgroup.payviceforagents.activities.UserPinEntryActivity
import com.iisysgroup.payviceforagents.baseimpl.interactor.AirtimeInteractorImpl
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.*
import com.iisysgroup.payviceforagents.util.StringUtils.getClientRef
import com.iisysgroup.payvicegamesdk.TamsResponse
import com.itex.richard.payviceconnect.model.AirtimeModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_quick_topup.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.toast

class QuickTopup : AppCompatActivity() {

    private val mProgressDialog by lazy {
        indeterminateProgressDialog(title = "Processing", message = "We are now working on your request")
    }

    private val terminalId by lazy {
        SecureStorage.retrieve(Helper.TERMINAL_ID, "")
    }

    private val userName by lazy {
        SecureStorage.retrieve(Helper.USER_ID, "")
    }

    private val userPassword by lazy {
        SecureStorage.retrieve(Helper.STORED_PASSWORD, "")
    }

    private val interactor by lazy {
        AirtimeInteractorImpl(applicationContext)
    }

    private var airtimeAmount = 0
    private lateinit var phone : String

    private lateinit var encryptedUserPin : String
    private lateinit var encryptedPin : String

    private lateinit var airtel : ArrayList<String>
    private lateinit var glo : ArrayList<String>
    private lateinit var mtn : ArrayList<String>
    private lateinit var etisalat : ArrayList<String>

    private val vtuService by lazy {
        when (networkSpinner.selectedItemPosition){

            0 -> {"empty"}
            1 -> {"AIRTELVTU"}
            2 -> {"ETISALATVTU"}
            3 -> {"GLOVTU"}
            4 -> {"MTNVTU"}
            else -> {"empty"}
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quick_topup)
        mtn = arrayListOf<String>("0703", "0706", "0803", "0806", "0810", "0813", "0814", "0816", "0903")
        glo = arrayListOf<String>("0705", "0805" ,"0807", "0905", "0815")
        etisalat = arrayListOf<String>("0809", "0817", "0818", "0909", "0908")
        airtel = arrayListOf<String>("0701","0708","0802","0808","0812","0902")

        supportActionBar?.title = "Quick top-up"


        buyAirtimeButton.setOnClickListener {
            if (isValidated()){
                phone = phone_number.text.toString()
                airtimeAmount = amount.text.toString().toInt()
                pay()
            }
        }



        phone_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s?.length == 4){
                    val firstFour = s?.substring(0, 4)



                    if (airtel.contains(firstFour)){
                        networkSpinner.setSelection(1)
                    } else if (etisalat.contains(firstFour)){
                        networkSpinner.setSelection(2)
                    } else if (glo.contains(firstFour)){
                        networkSpinner.setSelection(3)
                    } else if (mtn.contains(firstFour)){
                        networkSpinner.setSelection(4)
                    } else {
                        networkSpinner.setSelection(0)
                    }


                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }



    private fun pay() {
        showPinEntry()
    }

    private fun isValidated(): Boolean {
        if (networkSpinner.selectedItemPosition == 0){
            toast("Select a valid network")
            return false
        }
        else if (phone_number.text.toString().length != 11){
            phone_number.error = "Enter a valid phone number"
            return false
        }

        else if (amount.text.toString().toInt() < 50){
            amount.error = "The least amount is 50 Naira."
            return false
        }

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UserPinEntryActivity.PIN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                encryptedUserPin = data!!.getStringExtra(UserPinEntryActivity.PIN_RESPONSE_DATA)
                encryptedPin = SecureStorage.retrieve(Helper.PIN, "");
                showPaymentOption(PaymentOption.Mode.PAY, airtimeAmount, phone)
            }
        }
        if (requestCode == PaymentOptionActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {

                val paymentOption = data?.getSerializableExtra(PaymentOptionActivity.RESULT_OPTION) as PaymentOption
                val card = data.getSerializableExtra(PaymentOptionActivity.RESULT_CARD) as? Card
                continuePayment(paymentOption, card)
            }
        }
    }

    private fun continuePayment(paymentOption: PaymentOption, card: Card?) {
        when (paymentOption) {
            PaymentOption.WALLET -> {
                mProgressDialog.show()
//                val request = AirtimeRequestDetails(terminalId, userName, airtimeAmount.toString(), phone, vtuService, encryptedUserPin, null)
                interactor.payForAirtime(phone, airtimeAmount, vtuService, encryptedUserPin).subscribe { result, error ->
                    result?.let {
                        Log.d("airtime", it.toString())
                    }
                }
                val clientReference = getClientRef(this@QuickTopup, "")
                    Log.d("quick topup", "terminal id" + terminalId + "username" + userName + "airtime amount" + airtimeAmount.toString() + "phone" + phone + "vtuservice" + vtuService + "encrypteduserpin" + encryptedUserPin)
                    val request = AirtimeModel.AirtimeRequestDetails(terminalId, userName, airtimeAmount.toString(), phone, vtuService, encryptedUserPin, " ", clientReference)

//                Log.d("quick topup", "terminal id"+ terminalId + "username"+ userName+ "airtime amount"+airtimeAmount.toString() +"phone" + phone + "vtuservice" + vtuService + "encrypteduserpin" + encryptedUserPin)

                    Log.d("quick topup", "terminal id" + terminalId + "username" + userName + "airtime amount" + airtimeAmount.toString() + "phone" + phone + "vtuservice" + vtuService + "encrypteduserpin" + encryptedUserPin)

                    payviceServices.PurchaseAirtime(request)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                mProgressDialog.dismiss()
                                alert {
                                    title = "Airtime Response"
                                    message = it.message
                                    okButton { }
                                }.show()

                            },
                                    {
                                        mProgressDialog.dismiss()
                                        alert {
                                            title = "Airtime Response"
                                            message = "Airtime purchase was not successful"
                                            okButton { }
                                        }.show()
                                    })
                }
                else -> {
                    //                val cardProcessor = DebitCardProcessor(this, encryptedUserPin)
                    val cardProcessor = DebitCardProcessor(this, encryptedUserPin)

                    cardProcessor.processTransaction(airtimeAmount, card).subscribe { result ->

                        result?.let {
                            mProgressDialog.dismiss()
                            if (it.result == TamsResponse.Status.APPROVED) {

//                            val request = AirtimeRequestDetails(terminalId, userName, airtimeAmount.toString(), phone, vtuService, encryptedUserPin, "")
                                val clientReference = StringUtils.getClientRef(this@QuickTopup, "")
                                val request = AirtimeModel.AirtimeRequestDetails(terminalId, userName, airtimeAmount.toString(), phone, vtuService, encryptedUserPin, "", clientReference)

                                payviceServices.PurchaseAirtime(request)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({
                                            Log.d("OkH", it.toString())
                                        },
                                                {
                                                })


                            } else {
                                mProgressDialog.dismiss()
                                alert {
                                    title = "Error"
                                    message = it.message
                                }.show()
                            }
                        }
                    }
                }
            }
        }
}

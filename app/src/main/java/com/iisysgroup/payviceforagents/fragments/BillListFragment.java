package com.iisysgroup.payviceforagents.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.callbacks.OnRecyclerViewItemClickListener;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.ImageTitleObject;
import com.iisysgroup.payviceforagents.util.VasServices;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BillListFragment extends Fragment implements OnRecyclerViewItemClickListener {

    @BindView(R.id.subTitleText)
    TextView subTitleText;

    @BindView(R.id.productOptionText)
    TextView productOptionText;

    @BindView(R.id.airtimeRecyclerView)
    RecyclerView airtimeRecyclerView;

    @BindView(R.id.bankingRecyclerView)
    RecyclerView bankingRecyclerView;

    @BindView(R.id.cableTvRecyclerView)
    RecyclerView cableTvRecyclerView;

    @BindView(R.id.internetServicesRecyclerView)
    RecyclerView internetServicesRecyclerView;

    @BindView(R.id.utilitiesRecyclerView)
    RecyclerView utilitiesRecyclerView;

    @BindView(R.id.miscRecyclerView)
    RecyclerView miscRecyclerView;

    private OnFragmentInteractionListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_bill_list, null);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        resetValues();
        LinearLayoutManager airtimeLLM = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        airtimeRecyclerView.setLayoutManager(airtimeLLM);

        airtimeRecyclerView.setAdapter(new ImageTitleObject.SimpleImageObjectAdapter(
                Arrays.asList(new ImageTitleObject(R.drawable.etisalat, VasServices.ETISALAT),
                        new ImageTitleObject(R.drawable.airtel, VasServices.AIRTEL),
                        new ImageTitleObject(R.drawable.glo, VasServices.GLO),
                        new ImageTitleObject(R.drawable.mtn, VasServices.MTN))
                , getActivity(), this));
        airtimeLLM.scrollToPositionWithOffset(0, -50);

        LinearLayoutManager bankingLLM = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        bankingRecyclerView.setLayoutManager(bankingLLM);

        bankingRecyclerView.setAdapter(new ImageTitleObject.SimpleImageObjectAdapter(
                Arrays.asList(new ImageTitleObject(R.drawable.pay_cash_in, VasServices.TRANSFER),
                        new ImageTitleObject(R.drawable.pay_cash_out, VasServices.WITHDRAWAL))
                , getActivity(), this));
        bankingLLM.scrollToPositionWithOffset(0, -50); //Specify a scroll offset

        cableTvRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));

        cableTvRecyclerView.setAdapter(new ImageTitleObject.SimpleImageObjectAdapter(
                Arrays.asList(new ImageTitleObject(R.drawable.gotv, VasServices.GOTV),
                        new ImageTitleObject(R.drawable.dstv, VasServices.DSTV),
                        new ImageTitleObject(R.drawable.startime, VasServices.STARTIMES))
                , getActivity(), this));

        internetServicesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));
        internetServicesRecyclerView.setAdapter(new ImageTitleObject.SimpleImageObjectAdapter(
                Arrays.asList(new ImageTitleObject(R.drawable.smile, VasServices.SMILE))
                , getActivity(), this));


        utilitiesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        utilitiesRecyclerView.setAdapter(new ImageTitleObject.SimpleImageObjectAdapter(
                Arrays.asList(
                        new ImageTitleObject(R.drawable.ikedc, VasServices.IKEJA_ELECTRIC),
                        new ImageTitleObject(R.drawable.ekedc, VasServices.EKO_ELECTRIC),
                        new ImageTitleObject(R.drawable.ibedc, VasServices.IBADAN_ELECTRIC),
                        new ImageTitleObject(R.drawable.eedc, VasServices.ENUGU_ELECTRIC),
                        new ImageTitleObject(R.drawable.aedc, VasServices.ABUJA_ELECTRIC),
                        new ImageTitleObject(R.drawable.phdc, VasServices.PORTHARCOURT_ELECTRIC)
//                        new ImageTitleObject(R.drawable.lcc, VasServices.LEKKI_LCC)
                )
                , getActivity(), this));

        miscRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        miscRecyclerView.setAdapter(new ImageTitleObject.SimpleImageObjectAdapter(
                Arrays.asList(
//                        new ImageTitleObject(R.drawable.waec, VasServices.WAEC),
                        new ImageTitleObject(R.drawable.ic_genesis, VasServices.GENESIS))
                , getActivity(), this));

    }


    public void onButtonPressed(String service) {
        if (mListener != null) {
            mListener.onBillListFragmentInteraction(service);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(RecyclerView recyclerView, int position) {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();

        if (adapter instanceof ImageTitleObject.SimpleImageObjectAdapter) {
            String service = ((ImageTitleObject.SimpleImageObjectAdapter) adapter).getItem(position).title;
            Log.d("service onItemClick." ,service);
            onButtonPressed(service);
        }
    }

    void resetValues(){
        SecureStorage.store("currentservice", "Value Added Service");
        SecureStorage.store("currentAmount", "0");
        SecureStorage.store("currentPhone", "0");
        SecureStorage.store("currentProduct", "");
        SecureStorage.store("currentReason", "");
        SecureStorage.store("currentApproval", "");
        SecureStorage.store("currentRef", "");
        SecureStorage.store("currentType", "");
        SecureStorage.store("currentMeter", "");
        SecureStorage.store("currentName", "");
        SecureStorage.store("currentPin", "");
        SecureStorage.store("currentInfo", "");
        SecureStorage.store("accountname", "");
        SecureStorage.store("conveniencefee", "");
        SecureStorage.store("currentAccount", "");
    }

    public interface OnFragmentInteractionListener {
        void onBillListFragmentInteraction(String service);
    }
}

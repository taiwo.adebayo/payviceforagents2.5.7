package com.iisysgroup.payviceforagents.payviceservices;

import com.iisysgroup.payviceforagents.models.LoginResponseModel;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Agbede on 3/26/2018.
 */

public interface LoginService {

    @GET("login")
    Single<LoginResponseModel> logUserIn(@Query("action") String action, @Query("termid")
            String terminalId, @Query("userid") String userId,
                                         @Query("password") String encryptedPassword);

}


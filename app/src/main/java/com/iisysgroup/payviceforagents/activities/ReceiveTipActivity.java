package com.iisysgroup.payviceforagents.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ReceiveTipActivity extends AppCompatActivity {
    static final String payviceQrFileName = "payvice_tip.png";
    ImageView qrImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_tip);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        qrImageView = findViewById(R.id.qrImage);


        new Thread(new Runnable() {
            @Override
            public void run() {
                showQrImage();
            }
        }).start();

    }

    private void showQrImage() {
        if (!fileExist()) {
            createQRImageFile();
        }

        loadQRImageFile();
    }

    private boolean fileExist() {
        File file = new File(getFilesDir(), payviceQrFileName);
        System.out.println("fileExist Path File: " + file.getAbsolutePath());
        return file.exists();
    }

    private void createQRImageFile() {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        final String content = SecureStorage.retrieve(Helper.TERMINAL_ID, "");
        FileOutputStream fos = null;
        try {
            BitMatrix bitMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, 400, 400);
            final Bitmap qrBitmap = Helper.toBitmap(bitMatrix);
            fos = this.openFileOutput(payviceQrFileName, Context.MODE_PRIVATE);

            qrBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);

        } catch (WriterException | IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void loadQRImageFile() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        File file = new File(getFilesDir(), payviceQrFileName);
        System.out.println("Path File: " + file);
        final Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                qrImageView.setImageBitmap(bitmap);
            }
        });
    }
}

package com.iisysgroup.payviceforagents.payviceservices;

import android.content.Context;
import android.util.Log;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.PinValue;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.RestWrapper;
import com.iisysgroup.payviceforagents.util.VasTransactionData;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.iisysgroup.payviceforagents.util.Parsers.getTamsResult;

/**
 * Created by Bamitale @Itex on 2/8/2016.
 */
public class VasTransactionManager {

    Context context;
    VasTransactionData vasTransactionData;
    String url, action, terminalID, userId;
    Map<String, String> params;


    public VasTransactionManager(Context context, VasTransactionData vasTransactionData, String additionalPaymentData) {
        this(context, vasTransactionData, null, 0, additionalPaymentData);
    }


    /**
     * @param context
     * @param vasTransactionData
     * @param additionalPaymentData can be null in the absence of additional data
     */
    public VasTransactionManager(Context context, VasTransactionData vasTransactionData,
                                 PinValue pinValue, long quantity, String additionalPaymentData) {
        this.context = context;
        this.vasTransactionData = vasTransactionData;

        url = context.getString(R.string.tams_url);
        action = context.getString(R.string.tams_webapi_action);
        terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID, context.getString(R.string.tams_default_terminal_id));
        userId = SecureStorage.retrieve(Helper.USER_ID, "");


        params = new HashMap<>();
        params.put("action", action);
        params.put("amount", vasTransactionData.getAmount());
        params.put("control", "");
        params.put("pin", Helper.preparePin(vasTransactionData.getPin()));
//        params.put("phoneno", vasTransactionData.getBeneficiary());

        String requestCode = vasTransactionData.getProduct().requestCode;

        params.put("network", requestCode);
        params.put("termid", terminalID);
        params.put("userid", userId);
        params.put("op", "EXCHANGE");

        if (pinValue != null && quantity > 0) {
            params.put("value", pinValue.value + "");
            params.put("no", quantity + "");
        }

        if (additionalPaymentData != null && !additionalPaymentData.isEmpty())
            params.put("PaymentData", additionalPaymentData);

    }

    /**
     * @param url
     * @param params
     */
    public VasTransactionManager(String url, Map<String, String> params) {
        this.url = url;
        this.params = params;
    }


    public VasResult processTransaction() {
        RestWrapper restWrapper = new RestWrapper(url);
        VasResult tamsResult = new VasResult();

        restWrapper.setParams(params);
        String responseString = null;

        try {
            responseString = restWrapper.processRequest(RestWrapper.Request.GET);

//            Log.i("responseString", responseString);

            if (!responseString.isEmpty()) {
                tamsResult = getTamsResult(responseString);
                Log.d("tams", "processTransaction: " + tamsResult.message+tamsResult.key);

                if (tamsResult.message.contains("YES")){
                    Log.d("tams", "processTransaction: pin");
                }
//                tamsResult = getTamsResult_d(responseString);

            }
            else
                tamsResult.message = "Server communication error. Please try again later.";

        } catch (IOException e) {
        }

        return tamsResult;
    }


}

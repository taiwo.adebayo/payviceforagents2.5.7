package com.iisysgroup.payviceforagents.baseimpl.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.google.gson.Gson
import com.iisysgroup.payviceforagents.base.presenter.AirtimePresenter
import com.iisysgroup.payviceforagents.baseimpl.interactor.AirtimeInteractorImpl
import com.iisysgroup.payviceforagents.entities.Service
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.results.AirtimeDataResult
import com.iisysgroup.payviceforagents.entities.results.AirtimePinResult
import com.iisysgroup.payviceforagents.entities.results.AirtimeVtuResult
import com.iisysgroup.payviceforagents.models.TransactionModel
import com.iisysgroup.payviceforagents.models.pinData
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.amountToSafeInt
import com.iisysgroup.payviceforagents.util.processTransaction
import com.iisysgroup.payvicegamesdk.TamsResponse
import com.itex.richard.payviceconnect.model.DataModel

class AirtimeActivityViewModel(
        private val applicationContext: Application) : AndroidViewModel(applicationContext), AirtimePresenter {

    private val interactor by lazy {
        AirtimeInteractorImpl(applicationContext)
    }


    private val product = MutableLiveData<Service.Product>()
    private val error = MutableLiveData<Throwable>()
    private val progressDialog = MutableLiveData<Pair<Boolean, String>>()
    private val transaction  : TransactionModel ?= null
    private val paymentResponse = MutableLiveData<ServiceResult>()
    val paymentResponseLiveData: LiveData<ServiceResult>
        get() = paymentResponse


    private val planList = MutableLiveData<List<DataModel.DataResponseElements>>()

    private val selectedPlan = MutableLiveData<DataModel.DataResponseElements>()
    val selectedPlanLiveData: LiveData<DataModel.DataResponseElements>
        get() = selectedPlan


    override fun setProduct(product: Service.Product) {

        if (this.product.value != product) {
            this.planList.value = null
        }
        this.product.value = product
    }

    override fun loadPlans(): LiveData<List<DataModel.DataResponseElements>> {
        if (planList.value == null) {
            showProgress(true)
            interactor.getDataPlans(product.value!!.proxyCode)
                    .subscribe { it, t ->
                        showProgress(false)

                        t?.let(this::setError)

                        it?.let {
                            if (it.error) {
                                setError(RuntimeException(it.message))
                            } else {
                                planList.value = it.data
                            }
                        }
                    }
        }


        return planList
    }

    override fun setSelectedPlan(position: Int) {
        planList.value?.let {
            selectedPlan.value = it[position]
        }
    }


    override fun purchaseVtu(amount: Int, phoneNumber: String, authPin: String) {
        showProgress(true)
        interactor.payForAirtime(phoneNumber, amount,
                product.value!!.requestCode, authPin).subscribe { result, error ->
            showProgress(false)
            error?.let(this::setError)
            result?.let {
                val beneficiary = Beneficiary(phoneNumber, phoneNumber, product.value!!.name)
                val data = ServiceResult.Data(beneficiary, amount)
                Log.d("okh", beneficiary.assignedProduct + " "+ data + " " + it)
                SecureStorage.store("currentservice", "Airtime")
                SecureStorage.store("currentAmount", data.amount.toString())
                SecureStorage.store("currentPhone", data.beneficiary.data)
                SecureStorage.store("currentProduct", beneficiary.assignedProduct)
                SecureStorage.store("currentReason", it.message)
                SecureStorage.store("currentRef", it.ref)
                if (it.error){
                    SecureStorage.store("currentApproval", "Declined")
                }
                else{
                    SecureStorage.store("currentApproval", "Approved")
                }

                paymentResponse.value = AirtimeVtuResult(data, it)
            }

        }
    }

    override fun purchasePin(amount: Int, phoneNumber: String, authPin: String) {
        showProgress(true)
        Log.d("value!!.requestCode",  product.value!!.requestCode)
        interactor.payForAirtimePin(phoneNumber, amount,
                product.value!!.requestCode, authPin).subscribe { result, error ->
            showProgress(false)
            error?.let(this::setError)

            result?.let {
                val beneficiary = Beneficiary(phoneNumber, phoneNumber, product.value!!.name)
                val data = ServiceResult.Data(beneficiary, amount)
                Log.d("it>>",  Gson().toJson(it))
                SecureStorage.store("currentservice", "Airtime")
                SecureStorage.store("currentAmount", amount)
                SecureStorage.store("currentPhone", data.beneficiary.data)
                SecureStorage.store("currentProduct", beneficiary.assignedProduct)
                SecureStorage.store("currentReason", it.message)
                SecureStorage.store("currentRef", it.ref)
                SecureStorage.store("currentPin", it.pin_data.pin)
                SecureStorage.store("currentInfo", it.pin_data.dial)
                if (it.error){
                    SecureStorage.store("currentApproval", "Declined")
                }
                else{
                    SecureStorage.store("currentApproval", "Approved")
                }
                paymentResponse.value = AirtimePinResult(data, it)
            }

        }
    }

    override fun purchaseVtWithCard(amount: Int, phoneNumber: String, card: Card?, processor: DebitCardProcessor) {
        processor.processTransaction(amount, card).subscribe { response ->
            response?.let {
                if (it.result != TamsResponse.Status.APPROVED) {
                    setError(RuntimeException(it.message))
                } else {
                    purchaseVtu(amount, phoneNumber, processor.userPin)
                }
            }
        }
    }

    override fun purchasePinWithCard(amount: Int, phoneNumber: String, card: Card?, processor: DebitCardProcessor) {
        processor.processTransaction(amount, card).subscribe { response ->
            response?.let {
                if (it.result != TamsResponse.Status.APPROVED) {
                    setError(RuntimeException(it.message))
                } else {
                    purchaseVtu(amount, phoneNumber, processor.userPin)
                }
            }
        }
    }

    override fun purchaseData(phoneNumber: String, authPin: String) {
        showProgress(true)
        interactor.payForData(phoneNumber, selectedPlan.value!!, authPin).subscribe { result, error ->
            showProgress(false)
            error?.let(this::setError)

            result?.let {

                val beneficiary = Beneficiary(phoneNumber, phoneNumber, product.value?.name ?: "")
                val amount = selectedPlan.value?.amount?.amountToSafeInt() ?: 0
                val data = ServiceResult.Data(beneficiary, amount)
                val plan = selectedPlan.value!!.description
                SecureStorage.store("currentservice", "Airtime")
                SecureStorage.store("currentAmount", amount)
                SecureStorage.store("currentPhone", data.beneficiary.data)
                SecureStorage.store("currentProduct", beneficiary.assignedProduct)

                SecureStorage.store("currentReason", it.message)
                SecureStorage.store("currentRef", it.ref)
                if (it.error){
                    SecureStorage.store("currentApproval", "Declined")
                }
                else{
                    SecureStorage.store("currentApproval", "Approved")
                }
                paymentResponse.value = AirtimeDataResult(data, plan, it)
            }
        }
    }

    override fun purchaseDataWithCard(phoneNumber: String, card: Card?, processor: DebitCardProcessor) {
        val amount = selectedPlan.value?.amount?.toInt() ?: 0
        processor.processTransaction(amount, card).subscribe { response ->
            response?.let {
                if (it.result != TamsResponse.Status.APPROVED) {
                    setError(RuntimeException(it.message))
                } else {
                    purchaseData(phoneNumber, processor.userPin)
                }
            }
        }
    }

    override fun purchasePinToSms(phoneNumber: String, authPin: String) {
        interactor.payForPin(phoneNumber, selectedPlan.value!!, authPin).subscribe { result, error ->

            error?.let(this::setError)

            result?.let {
                val beneficiary = Beneficiary(phoneNumber, phoneNumber, product.value?.name ?: "")
                val amount = selectedPlan.value?.amount?.amountToSafeInt() ?: 0
                val data = ServiceResult.Data(beneficiary, amount)
                val plan = selectedPlan.value!!.description

                paymentResponse.value = AirtimeDataResult(data, plan, it)
            }
        }
    }

//    {"amount":false,"date":"","error":false,"message":"","ref":""}
//{"amount":false,"date":"","error":false,"message":"","ref":""}


    override fun purchasePinToSmsWithCard(phoneNumber: String, card: Card?, processor: DebitCardProcessor) {
        val amount = selectedPlan.value?.amount?.toInt() ?: 0
        processor.processTransaction(amount, card).subscribe { response ->

            response?.let {
                if (it.result != TamsResponse.Status.APPROVED) {
                    setError(RuntimeException(it.message))
                } else {
                    purchasePinToSms(phoneNumber, processor.userPin)
                }
            }
        }
    }

    override fun setError(throwable: Throwable) {
        throwable.printStackTrace()
        error.value = throwable
    }

    override fun showProgress(show: Boolean, message: String) {
        progressDialog.value = Pair(show, message)
    }


    override fun onSetProduct(): LiveData<Service.Product> {
        return product
    }

    override fun onError(): LiveData<Throwable> {
        return error
    }

    override fun onProgressUpdate(): LiveData<Pair<Boolean, String>> {
        return progressDialog
    }
}
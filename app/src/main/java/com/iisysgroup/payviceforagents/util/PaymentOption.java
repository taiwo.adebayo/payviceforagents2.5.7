package com.iisysgroup.payviceforagents.util;

public enum PaymentOption {
    WALLET, DEBIT_CARD, LINKED_CARD, BANK_ACCOUNT;

    public enum Mode {
        PAY, FUND, REPEAT
    }
}

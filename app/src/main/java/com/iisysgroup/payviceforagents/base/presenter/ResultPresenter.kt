package com.iisysgroup.payviceforagents.base.presenter

import com.iisysgroup.payviceforagents.entities.StatusObject

interface ResultPresenter {
    fun setStatusObject(statusObject: StatusObject)
}
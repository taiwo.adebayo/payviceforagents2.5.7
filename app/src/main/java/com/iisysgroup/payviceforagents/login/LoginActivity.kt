package com.iisysgroup.payviceforagents.login

import android.Manifest
import android.app.ProgressDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.appinvite.FirebaseAppInvite
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.iisysgroup.payviceforagents.activities.MainActivity
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.activities.BillPaymentActivity
import com.iisysgroup.payviceforagents.activities.LoginSettingsActivity
import com.iisysgroup.payviceforagents.activities.RegisterActivity
import com.iisysgroup.payviceforagents.airtime.AirtimeActivity
import com.iisysgroup.payviceforagents.base.presenter.LoginPresenter
import com.iisysgroup.payviceforagents.baseimpl.interactor.LoginInteractorImpl
import com.iisysgroup.payviceforagents.baseimpl.presenter.LoginPresenterImpl
import com.iisysgroup.payviceforagents.baseimpl.viewmodel.LoginViewModel
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
import com.iisysgroup.payviceforagents.util.StringUtils.getClientRef
import com.iisysgroup.payviceforagents.util.VasServices
import com.jakewharton.rxbinding2.view.clicks
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.layout_fab_submenu.*

/**
 * A login screen that offers login via email/password.
 */

class LoginActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(LoginViewModel::class.java)
    }

    private val presenter: LoginPresenter by lazy {
        LoginPresenterImpl(LoginInteractorImpl(applicationContext), viewModel)
    }

    var progressDialog: ProgressDialog? = null

    private var fabExpanded = false

    private val isPermitted: Boolean
        get() {
            val status = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE)

            return status == PackageManager.PERMISSION_GRANTED
        }


    //closes FAB submenus
    private fun closeSubMenusFab() {
        layoutFabMtn.setVisibility(View.INVISIBLE)
        layoutFabGlo.setVisibility(View.INVISIBLE)
        layoutFabAirtel.setVisibility(View.INVISIBLE)
        layoutFabNinemobile.setVisibility(View.INVISIBLE)
        quickAirtime.setImageResource(R.drawable.ic_airtime)
        fabExpanded = false
        scrollView.alpha = 1.00f
    }

    //Opens FAB submenus
    private fun openSubMenusFab() {
        layoutFabMtn.setVisibility(View.VISIBLE)
        layoutFabGlo.setVisibility(View.VISIBLE)
        layoutFabAirtel.setVisibility(View.VISIBLE)
        layoutFabNinemobile.setVisibility(View.VISIBLE)
        //Change settings icon to 'X' icon
        quickAirtime.setImageResource(R.drawable.ic_close_black_24dp)
        fabExpanded = true
        scrollView.alpha = 0.1f

        topUpView.setOnClickListener {
            closeSubMenusFab()
        }

        layoutFabMtn.setOnClickListener {
            closeSubMenusFab()
            SecureStorage.store(Helper.LOGGED_IN, true)

            val intentToAirtimeActivity = Intent(applicationContext, AirtimeActivity::class.java)
            intentToAirtimeActivity.putExtra(BillPaymentActivity.SERVICE, VasServices.MTN)
            startActivity(intentToAirtimeActivity)
        }
        layoutFabGlo.setOnClickListener {
            closeSubMenusFab()
            SecureStorage.store(Helper.LOGGED_IN, true)
            val intentToAirtimeActivity = Intent(applicationContext, AirtimeActivity::class.java)
            intentToAirtimeActivity.putExtra(BillPaymentActivity.SERVICE, VasServices.GLO)
            startActivity(intentToAirtimeActivity)
        }
        layoutFabAirtel.setOnClickListener {
            closeSubMenusFab()
            SecureStorage.store(Helper.LOGGED_IN, true)
            val intentToAirtimeActivity = Intent(applicationContext, AirtimeActivity::class.java)
            intentToAirtimeActivity.putExtra(BillPaymentActivity.SERVICE, VasServices.AIRTEL)
            startActivity(intentToAirtimeActivity)
        }
        layoutFabNinemobile.setOnClickListener {
            closeSubMenusFab()
            SecureStorage.store(Helper.LOGGED_IN, true)
            val intentToAirtimeActivity = Intent(applicationContext, AirtimeActivity::class.java)
            intentToAirtimeActivity.putExtra(BillPaymentActivity.SERVICE, VasServices.ETISALAT)
            startActivity(intentToAirtimeActivity)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        //If user has signed in before, show quick airtime topup
        if (SecureStorage.retrieve(Helper.HAS_USER_SIGNED_IN_BEFORE, false)){
            quickAirtime.visibility = View.VISIBLE
        } else {
            quickAirtime.visibility = View.GONE
        }

        quickAirtime.setOnClickListener{
            if (fabExpanded){
                val startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade)
                scrollView.startAnimation(startAnimation);
                closeSubMenusFab();
            } else {
                openSubMenusFab();
//                val startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out)
//                scrollView.startAnimation(startAnimation);
            }
    }

        //If the user wants to do quick top-up
//        quickAirtime.setOnClickListener {
//            SecureStorage.store(Helper.LOGGED_IN, true)
//            //val intentToAirtimeActivity = Intent(applicationContext, QuickTopup::class.java)
//            val listAirtime = arrayOf("9Mobile", "Airtel", "Glo", "Mtn")
//            var network = ""
//            val mBuilder = AlertDialog.Builder(this@LoginActivity)
//            mBuilder.setTitle("Select Network")
//            mBuilder.setSingleChoiceItems(listAirtime, -1){  dialog: DialogInterface?, i: Int ->
//                if (i==0){
//                    network = VasServices.ETISALAT
//                }
//                if (i==1){
//                    network = VasServices.AIRTEL
//                }
//                if (i==2){
//                    network = VasServices.GLO
//                }
//                if (i==3){
//                    network = VasServices.MTN
//                }
//
//                val theService = VasServices.SERVICES[network]
//                if (theService != null) {
//                if (theService.type == Service.Type.AIRTIME) {
//                    intent = Intent(this, AirtimeActivity::class.java)
//                }
//            }
//                val intentToAirtimeActivity = Intent(applicationContext, AirtimeActivity::class.java)
//                intentToAirtimeActivity.putExtra(BillPaymentActivity.SERVICE, network)
//                startActivity(intentToAirtimeActivity)
//            }
//            mBuilder.setNeutralButton("Cancel"){   dialog, which ->
//                dialog.cancel()
//            }
//
//            val mDialog = mBuilder.create()
//            mDialog.show()
//
//        }

//        initializeFacebookDeepLink()
        setUpObservers()

        // Set up the login form.
        userEdit.clearFocus()
        passwordEdit.clearFocus()
        val encrypted = SecureStorage.retrieve(Helper.PIN, "")
        Log.d ("encryptedPinLogin", encrypted+" ");

        stayLoggedCheckView.isChecked = SecureStorage.retrieve(Helper.STAY_LOGGED_IN, false)
        if (stayLoggedCheckView.isChecked) {
            userEdit.setText(SecureStorage.retrieve(Helper.USER_ID, ""))
        }

        passwordEdit.setOnEditorActionListener(TextView.OnEditorActionListener { textView, id, keyEvent ->
            //5650 is the imeID for password
            if (id == 5650 || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

    }

//    private fun initializeFacebookDeepLink() {
//        // Check for App Invite invitations and launch deep-link activity if possible.
//        // Requires that an Activity is registered in AndroidManifest.xml to handle
//        // deep-link URLs.
//        FirebaseDynamicLinks.getInstance().getDynamicLink(intent)
//                .addOnSuccessListener(this, OnSuccessListener { data ->
//                    if (data == null) {
//                        Log.d(Helper.TAG, "getInvitation: no data")
//                        return@OnSuccessListener
//                    }
//
//                    // Get the deep link
//                    val deepLink = data.link
//
//                    // Extract invite
//                    val invite = FirebaseAppInvite.getInvitation(data)
//                    if (invite != null) {
//                        val invitationId = invite.invitationId
//                    }
//
//                    // Handle the deep link
//                    // [START_EXCLUDE])
//                    if (deepLink != null) {
//                        val intent = Intent(Intent.ACTION_VIEW).apply {
//                            `package` = packageName
//                            setData(deepLink)
//                        }
//
//                        startActivity(intent)
//                    }
//                    // [END_EXCLUDE]
//                })
//                .addOnFailureListener(this) { e -> Log.w(Helper.TAG, "getDynamicLink:onFailure", e) }
//    }

    private fun setUpObservers() {

        signInButton.clicks().subscribe {
            if (Helper.hasInternetConnectivity(this)) {
                attemptLogin()
            }
        }

        registerButton.clicks().subscribe {
//            checkForPermissions()
            startActivity(Intent(applicationContext, RegisterActivity::class.java))

//            attemptLogin()
        }

        recoverPassButton.clicks().subscribe {
            SecureStorage.store(Helper.LOGGED_IN, false)
            finish()
            val recoverPasswordIntent = Intent(applicationContext, LoginSettingsActivity::class.java)
            recoverPasswordIntent.putExtra(LoginSettingsActivity.OPERATION, LoginSettingsActivity.RECOVER_PASSWORD)
            startActivity(recoverPasswordIntent)
        }


        viewModel.onProgressUpdate.observe(this, Observer {
            it?.let(this::showProgress)
        })

        viewModel.onLoginError.observe(this, Observer {
            Helper.showInfoDialog(this, "Oops, something is wrong", "Please try again after a while")

        })


        viewModel.onLoginMessage.observe(this, Observer {messageFromServer ->
            val outcome:String=  messageFromServer!!.replace(" suspend ", " suspended ")
            Helper.showInfoDialog(this, "Oops, something is wrong", outcome)

//            toast(outcome   )
        })


        viewModel.onRequestPinUpdate.observe(this, Observer {
            Helper.savePreference(this, Helper.PASSWORD_IS_RESET, true)

            Helper.savePreference(this, Helper.ID_TO_RESET, userEdit.text.toString())
        })

        viewModel.onLoginSuccess.observe(this, Observer {
            if (stayLoggedCheckView.isChecked) {
                SecureStorage.store(Helper.USER_ID, userEdit.text.toString())
            }

            getClientRef(this@LoginActivity, "")
            //pardon the long name :)
            SecureStorage.store(Helper.HAS_USER_SIGNED_IN_BEFORE, true)
            SecureStorage.store(Helper.STAY_LOGGED_IN, stayLoggedCheckView.isChecked)
            SecureStorage.store(Helper.LOGGED_IN, true)
            SecureStorage.store(Helper.USERNAME,"")

            Log.d("detailslogin", SecureStorage.retrieve(Helper.USER_ID, "") + " ID " + SecureStorage.retrieve(Helper.PIN, "") + " PIN "+ SecureStorage.retrieve(Helper.STORED_PASSWORD, "") + " PAssword "+ SecureStorage.retrieve(Helper.USERNAME,"") + " username "+ SecureStorage.retrieve(Helper.USER_EMAIL, " email ") + " "+ SecureStorage.retrieve(Helper.USER_PHONE, " "+ " phone "))


            finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        })

        viewModel.onUserIdValidationError.observe(this, Observer {
            userEdit.error = getString(R.string.prompt_login_id)
            Helper.showErrorAnim(userEdit)
            userEdit.requestFocus()
        })

        viewModel.onPasswordValidationError.observe(this, Observer {
            passwordEdit.error = getString(R.string.enter_password)
            Helper.showErrorAnim(passwordEdit)
            passwordEdit.requestFocus()
        })

        /*viewModel.onDeviceChanged.observe(this, Observer {

            Helper.showInfoDialogWithAction(this, "New Device", "New device detected. " +
                    "Press Ok to change linked device.") { dialog, which ->
                linkDeviceButton.performClick()
            }
        })*/
    }

    override fun onResume() {
        super.onResume()
        //checkForPermissions()
        Helper.saveTimeOutTime(this)
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Helper.showSnackBar(this, "Could not connect to Google Play Services.")
    }

    private fun attemptLogin() {
        val userId = userEdit.text.toString()
        val password = passwordEdit.text.toString()

//        toast(userId)
//        toast(password)


//        if (NumberUtils.isNumeric(userId)){
            SecureStorage.store(Helper.USER_PHONE, userId)

            if (userId.length != 11){
                passwordEdit.error = "Enter a valid password"
            }
//        }

        presenter.login(userId, password)
    }

    private fun checkForPermissions() {
        if (!isPermitted) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
                Helper.showInfoDialogWithAction(this, "Permission Required",
                        getString(R.string.app_name) + " needs to read your device id " +
                                "to help secure your user account against unauthorized access"
                ) { dialog, which -> showAllowPermissionSettings() }
            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_PHONE_STATE),
                        MY_READ_PHONE_STATE_PERMISSION_REQUEST_CODE)
            }
        } else
            toggleDisplay(true)

    }

    private fun showAllowPermissionSettings() {
        Helper.gotoActivitySettingScreen(this)
    }

    private fun toggleDisplay(permitted: Boolean) {
            registerButton.isEnabled = permitted
//        signInButton.isEnabled = permitted
        recoverPassButton.isEnabled = permitted

        if (!permitted) {
            Snackbar.make(userEdit.rootView,
                    getString(R.string.app_name) + " need to read your device id " +
                            "to help secure your user account against unauthorized access",
                    Snackbar.LENGTH_LONG).setAction("Ok") { showAllowPermissionSettings() }.show()
        }

        else{
            startActivity(Intent(applicationContext, RegisterActivity::class.java))
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_READ_PHONE_STATE_PERMISSION_REQUEST_CODE ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    toggleDisplay(true)
                } else {
                    toggleDisplay(false)
                }
        }
    }

    override fun onPause() {
        super.onPause()
        Helper.saveTimeOutTime(this)

    }

    override fun onBackPressed() {
        moveTaskToBack(true)
        SecureStorage.store(Helper.LOGGED_IN, false)
    }


    override fun finish() {
        progressDialog?.dismiss()
        super.finish()
    }
    private fun showProgress(show: Boolean) {
        progressDialog?.dismiss()

        if (show) {
            try{
                progressDialog = ProgressDialog.show(this, "Logging In",
                        "Please wait", true, false)
            }catch (e : Exception){

            }

        }
    }

    companion object {
        private const val MY_READ_PHONE_STATE_PERMISSION_REQUEST_CODE = 1009
    }

}





package com.iisysgroup.payviceforagents.paymentprocessors

import io.reactivex.Flowable


interface BaseProcessor<T> {
    fun getResponse(): Flowable<T>
}
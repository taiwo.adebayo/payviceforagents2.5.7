package com.iisysgroup.payviceforagents.baseimpl.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.iisysgroup.payviceforagents.base.interactor.MultichoiceInteractor
import com.iisysgroup.payviceforagents.base.presenter.MultichoicePresenter
import com.iisysgroup.payviceforagents.baseimpl.interactor.MultichoiceInteractorImpl
import com.iisysgroup.payviceforagents.entities.Service
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.results.MultichoiceResult
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.VasServices
import com.iisysgroup.payviceforagents.util.processTransaction
import com.iisysgroup.payvicegamesdk.TamsResponse
import com.itex.richard.payviceconnect.model.DstvModel

class MultichoiceViewModel(appContext: Application) : AndroidViewModel(appContext), MultichoicePresenter {

    private var tempList: List<DstvModel.Data>? = null

    private val interactor by lazy {
        MultichoiceInteractorImpl(appContext)
    }

    private val product = MutableLiveData<MultichoiceInteractor.MultichoiceProduct>()
    val productLiveData: LiveData<MultichoiceInteractor.MultichoiceProduct>
        get() = product


    private val error = MutableLiveData<Throwable>()
    val errorWatcher: LiveData<Throwable>
        get() = error

    private val paymentResponse = MutableLiveData<ServiceResult>()
    val paymentResponseLiveData: LiveData<ServiceResult>
        get() = paymentResponse


    private val plans = MutableLiveData<List<DstvModel.Data>>()


    private val selectedPlan = MutableLiveData<DstvModel.Data>()
    val selectPlanLiveData: LiveData<DstvModel.Data>
        get() = selectedPlan

    private val smartCardName = MutableLiveData<String>()
    val iucNameLiveData: LiveData<String>
        get () = smartCardName

    private val progressDialog = MutableLiveData<Pair<Boolean, String>>()
    val progressDialogLiveData: LiveData<Pair<Boolean, String>>
        get () = progressDialog

    private val validated = MutableLiveData<Boolean>()
    val isIucValidated: LiveData<Boolean>
        get() = validated


    override fun setService(service: Service) {
        when (service.name) {

            VasServices.DSTV -> setProduct(MultichoiceInteractor.MultichoiceProduct.DSTV)
            VasServices.GOTV -> setProduct(MultichoiceInteractor.MultichoiceProduct.GOTV)
        }

        validated.value = false
    }

    override fun setProduct(product: MultichoiceInteractor.MultichoiceProduct) {
        this.product.value = product
    }

    override fun setSelectedPlan(position: Int) {
        selectedPlan.value = plans.value?.get(position)
    }

    override fun validateIuc(iuc: String) {
        showProgressDialog(true, "Validating card")
        interactor.validateIucAndGetPlans(iuc, product.value!!).subscribe { result, error ->

            showProgressDialog(false)

            error?.let(this::setError)

            result?.let {
                if (it.error) {
                    setError(RuntimeException("Invalid device number"))
                } else {
                    this.tempList = it.data
                    SecureStorage.store("currentName", it.fullname)
                    smartCardName.value = "${it.fullname}\nType: ${it.unit ?: ""}"
                }
            }
        }
    }

    override fun subscribe(iuc: String, authPin: String) {
        showProgressDialog(true, "Completing payment... Please wait")
        interactor.subscribe(iuc, selectedPlan.value!!, product.value!!, authPin).subscribe { result, error ->
            showProgressDialog(false)
            error?.let(this::setError)
            result?.let {
                val beneficiary = Beneficiary(iuc, smartCardName.value ?: "", product.value?.name
                        ?: "")
                val amount = selectedPlan.value?.amount?.toDouble()?.toInt() ?: 0
                val plan = selectedPlan.value?.name ?: ""
                val data = ServiceResult.Data(beneficiary, amount)

                SecureStorage.store("currentservice", "Multichoice")
                SecureStorage.store("currentAmount", data.amount.toString())
                SecureStorage.store("currentPhone", data.beneficiary.data)
                SecureStorage.store("currentProduct", beneficiary.assignedProduct)

                SecureStorage.store("currentReason", it.message)
                SecureStorage.store("currentRef", it.ref)

                if (it.error){
                    SecureStorage.store("currentApproval", "Declined")
                }
                else{
                    SecureStorage.store("currentApproval", "Approved")
                }

                paymentResponse.value = MultichoiceResult(data, plan, it)
            }
        }
    }

    override fun subscribeWithCard(iucNumber: String, card: Card?, processor: DebitCardProcessor) {
        val amount = selectedPlan.value?.amount?.toInt() ?: 0
        processor.processTransaction(amount, card).subscribe { response ->
            response?.let {
                if (it.result != TamsResponse.Status.APPROVED) {
                    setError(RuntimeException(it.message))
                } else {
                    subscribe(iucNumber, processor.userPin)
                }
            }
        }
    }

    override fun setPlans(plans: List<DstvModel.Data>) {
        this.plans.value = plans
    }


    override fun setSmartCardIsValidated(isValidated: Boolean) {
        validated.value = isValidated

        if (isValidated) {
            tempList?.let(this::setPlans)
        } else {
            tempList = null
        }
    }

    override fun setError(throwable: Throwable) {
        error.value = throwable
    }

    private fun showProgressDialog(show: Boolean, message: String = "") {
        progressDialog.postValue(Pair(show, message))
    }

    override fun getPlans(): LiveData<List<DstvModel.Data>> = plans
}
package com.iisysgroup.payviceforagents.paymentprocessors

import android.app.Activity
import android.arch.lifecycle.LiveDataReactiveStreams
import android.support.v7.app.AppCompatActivity
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
import com.iisysgroup.payvicegamesdk.*
import com.interswitchng.sdk.model.RequestOptions
import io.reactivex.Flowable


typealias PCard = com.iisysgroup.payvicegamesdk.Cards

class DebitCardProcessor(val activity: Activity, val userPin: String) : BaseProcessor<TamsResponse> {

    internal val options = RequestOptions.builder()
            .setClientId(activity.getString(R.string.client_id))
            .setClientSecret(activity.getString(R.string.client_s))
            .setAccessToken(activity.getString(R.string.client_at))
            .build()

    private val fundRequestData by lazy {
        val userId = SecureStorage.retrieve(Helper.USER_ID, "")
        val walletId = SecureStorage.retrieve(Helper.TERMINAL_ID, "")
        val key = SecureStorage.retrieve(Helper.USER_KEY, "").split("|")

        FundRequestData(userId, walletId, key[1].substring(0, 8), userPin)
    }




    private val processor by lazy {
        PaymentProcessor(activity, fundRequestData)
    }

    fun payWithDebit(amount: Int) {

        processor.startUITransaction(amount.toDouble())
    }

    fun payWithLinkedCard(amount: Int, card: Card) {
        val pCard = PCard(card.PAN, card.expiryMonth.toInt(),
                card.expiryYear.toInt(), card.CVV, card.cardHolder ?: "", card.pin)

        processor.startNonUITransaction(amount.toDouble(), pCard)
    }


    override fun getResponse(): Flowable<TamsResponse> {
        return Flowable.fromPublisher(LiveDataReactiveStreams.toPublisher(activity as AppCompatActivity, processor.response))
    }
}
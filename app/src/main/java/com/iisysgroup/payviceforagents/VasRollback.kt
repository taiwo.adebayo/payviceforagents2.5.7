package com.iisysgroup.payviceforagents

import android.content.Context
import android.util.Log
import com.iisysgroup.poslib.ISO.common.IsoAdapter
import com.iisysgroup.poslib.ISO.common.IsoTransactionExecutor
import com.iisysgroup.poslib.host.entities.ConnectionData
import com.iisysgroup.poslib.host.entities.VasTerminalData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class VasRollback {

    fun rollback(context : Context,request : String) : String{
        var message = ""
      GlobalScope.launch(Dispatchers.Default){
          try{
              var app = context.applicationContext as App
              var varesr : VasTerminalData ?= null
                      GlobalScope.launch(Dispatchers.Default){
                  app.db.vasTerminalDataDao.get()
              }
              val connectionData = ConnectionData(varesr!!.tid, "196.6.103.73", 5043, true)
              //val connectionData = ConnectionData(varesr.tid, "197.253.19.75", 5001, true)
              Log.i("okh", "Rollback started")
              val response = IsoTransactionExecutor.execute(context, IsoAdapter.prepareByteStream(request.toByteArray(charset("UTF-8"))), connectionData);
              Log.i("okh", "rollback response " + response)
              message = response;
          }catch (e : Exception){
              e.printStackTrace()
              message =  e.message + ""
          }
      }
        return message
    }



}
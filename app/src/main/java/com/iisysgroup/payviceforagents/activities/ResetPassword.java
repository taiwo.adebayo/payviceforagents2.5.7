package com.iisysgroup.payviceforagents.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.login.LoginActivity;

public class ResetPassword extends AppCompatActivity {

    WebView passwordResetWeb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        passwordResetWeb = findViewById(R.id.passwordResetWeb);

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
        Log.d("deeplink", "onCreate: "+ appLinkData);
        passwordResetWeb.setWebViewClient(new myWebClient());
        passwordResetWeb.getSettings().setJavaScriptEnabled(true);
        passwordResetWeb.loadUrl(appLinkData+"");
        handleIntent(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reset_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.finish) {
            finish();
            Intent intent = new Intent(this,LoginActivity.class);
//            intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
//            startActivity(new Intent(this, MainActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        {
            if ((keyCode == KeyEvent.KEYCODE_BACK) && passwordResetWeb.canGoBack()) {
                passwordResetWeb.goBack();
                return true;
            }

            return super.onKeyDown(keyCode, event);
        }
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }



    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null) {
            String recipeId = appLinkData.getLastPathSegment();
            if (recipeId.equalsIgnoreCase("connect")){
                Uri appData = Uri.parse("content://com.recipe_app/recipe/").buildUpon()
                        .appendPath(recipeId).build();
                Log.d("deeplink", "handleIntent: " + appData.getHost() + " " + appData);
                finish();
                startActivity(new Intent(this, MainActivity.class));
            }

            //showRecipe(appData);
        }
    }

}

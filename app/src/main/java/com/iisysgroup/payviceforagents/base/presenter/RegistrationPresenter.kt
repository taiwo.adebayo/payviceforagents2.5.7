package com.iisysgroup.payviceforagents.base.presenter

import com.iisysgroup.payviceforagents.entities.RegistrationData

interface RegistrationPresenter {

    fun validateData(data: RegistrationData)
    fun setReferralCode(code: String)
}
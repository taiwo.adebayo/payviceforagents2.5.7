package com.iisysgroup.payviceforagents.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.callbacks.OnPinInputFragmentInteractionListener;
import com.iisysgroup.payviceforagents.callbacks.OnPlanInputFragmentInteractionListener;
import com.iisysgroup.payviceforagents.callbacks.OnValueInputFragmentInteractionListener;
import com.iisysgroup.payviceforagents.entities.PinValue;
import com.iisysgroup.payviceforagents.entities.PlanValue;
import com.iisysgroup.payviceforagents.entities.Service;
import com.iisysgroup.payviceforagents.entities.StatusObject;
import com.iisysgroup.payviceforagents.external.CreditCardUtils;
import com.iisysgroup.payviceforagents.external.MyCardEditActivity;
import com.iisysgroup.payviceforagents.fragments.AuthorizePinFragment;
import com.iisysgroup.payviceforagents.fragments.CvvCheckFragment;
import com.iisysgroup.payviceforagents.fragments.PaymentOptionFragment;
import com.iisysgroup.payviceforagents.fragments.SelectCardFragment;
import com.iisysgroup.payviceforagents.fragments.StatusFragment;
import com.iisysgroup.payviceforagents.models.TransactionModel;
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor;
import com.iisysgroup.payviceforagents.payviceservices.VasTransactionManager;
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary;
import com.iisysgroup.payviceforagents.persistence.entitiy.Card;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.EmailHandler;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.PaymentOption;
import com.iisysgroup.payviceforagents.util.VasServices;
import com.iisysgroup.payvicegamesdk.TamsResponse;
import com.iisysgroup.poslib.host.Host;
import com.itex.richard.payviceconnect.model.AbujaModel;
import com.itex.richard.payviceconnect.model.EkoModel;
import com.itex.richard.payviceconnect.model.EnuguModel;
import com.itex.richard.payviceconnect.model.IbadanModel;
import com.itex.richard.payviceconnect.model.IkejaModel;
import com.itex.richard.payviceconnect.model.PortharcourtModel;
import com.itex.richard.payviceconnect.wrapper.PayviceServices;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class DefaultVasActivity extends BaseActivity
        implements PaymentOptionFragment.OnFragmentInteractionListener,
        SelectCardFragment.OnFragmentInteractionListener,
        CvvCheckFragment.OnFragmentInteractionListener,
        AuthorizePinFragment.OnFragmentInteractionListener,
        OnValueInputFragmentInteractionListener,
        OnPinInputFragmentInteractionListener,
        OnPlanInputFragmentInteractionListener {

    //Transaction Parameters
    protected String amount, userPin;
    protected Service.Product product;
    protected int quantity, period;
    protected PinValue pinValue;
    protected PlanValue planValue;
    Beneficiary beneficiary;
    String meterNumber;
    String productCode;
    String phone;
    String meterName;
    String clientReference;
    Activity context;
    Toolbar toolbar;
    VasTransactionManager transactionManager;
    ProgressDialog progressDialog;
    StatusObject statusObject = new StatusObject(false);
    String wallet = SecureStorage.retrieve(Helper.TERMINAL_ID, "");
    String terminal = SecureStorage.retrieve(Helper.TERMINAL_ID, "");
    String userName = SecureStorage.retrieve(Helper.USER_ID, "");
    String password = SecureStorage.retrieve(Helper.PLAIN_PASSWORD, "");
    String channel = "ANDROID";
    String deviceID =SecureStorage.retrieve(Helper.TERMINAL_ID, "");

    Fragment curentFragment;
       PayviceServices payviceServices = PayviceServices.getInstance(this);



    String userAccountId = SecureStorage.retrieve(Helper.TERMINAL_ID, "");
    String description = "Wallet funding for user: " + userId + " - " + userAccountId;

    int convenienceFee = 0;
    boolean is_vas_card_payment = false, is_bill_payment = false;

    String userID = SecureStorage.retrieve(Helper.USER_ID, "");
    String terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID, "");
    static String service;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("");
        progressDialog.setTitle(null);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        clientReference =MyApplication.getSessionKey();
//        service = BillPaymentActivity.service;

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.default_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    void loadUI() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if (curentFragment.getClass().getSimpleName().trim().equals("StatusFragment")) {
                finish();
            }

            else {
                if (fm.getBackStackEntryCount() > 1) {
                    fm.popBackStack();
                } else
                    NavUtils.navigateUpFromSameTask(this);
            }
        }

        if (id == R.id.action_cancel) {
            finish();
        }
        return true;
    }

    public void loadFragment(Fragment fragment) {
        ft = fm.beginTransaction();
        curentFragment = fragment;
        ft.replace(R.id.container, fragment, fragment.getClass().getSimpleName());

        ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        Log.i("fragment DefaultVas",getClass().getSimpleName());

         if (curentFragment.getClass().getSimpleName().trim().equals("StatusFragment"))
            finish();
//        BillValueInputFragment
        else {
            if (fm.getBackStackEntryCount() > 1) {
                fm.popBackStack();
            } else
                NavUtils.navigateUpFromSameTask(this);
        }
    }


    @Override
    public void onPinInputFragmentInteraction(Service.Product product, PinValue pinValue, int quantity) {
        this.pinValue = pinValue;
        this.product = product;
        this.quantity = quantity;

        int temp = quantity * pinValue.value;
        this.amount = temp + "";


        beneficiary = new Beneficiary("", "None", "");
        planValue = null;

        loadFragment(new AuthorizePinFragment());
    }

    @Override
    public void onPlanInputFragmentInteraction(Service.Product product, Beneficiary beneficiary, PlanValue planValue, int period) {
        this.product = product;
        this.planValue = planValue;
        this.period = period;

        int temp = period * planValue.value;
        this.amount = temp + "";

        this.beneficiary = beneficiary;

        pinValue = null;

        loadFragment(new AuthorizePinFragment());
    }

    @Override
    public void onValueInputFragmentInteraction(Service.Product product, String amount, Beneficiary beneficiary,String phone,String productCode, String meterNumber, String meterName) {
        this.product = product;
        this.amount = amount.replaceAll("₦", "").replaceAll(",", "").trim();
        this.beneficiary = beneficiary;
        this.meterNumber = meterNumber;
        if(productCode != null)
        this.productCode = productCode;
        this.phone = phone;
        this.meterName = meterName;


        pinValue = null;
        planValue = null;

        loadFragment(new AuthorizePinFragment());
    }

    @Override
    public void onAuthorizePinFragmentInteraction(String pin) {
        this.userPin = Helper.preparePin(pin);
        loadFragment(PaymentOptionFragment.getInstance(beneficiary, Integer.parseInt(amount), convenienceFee));
    }


    @Override
    public void onPaymentOptionFragmentInteraction(PaymentOption paymentOption) {
        switch (paymentOption) {
            case BANK_ACCOUNT:
                // break;
                Helper.showDefaultMessage(this);
                break;

            case WALLET:
                is_vas_card_payment = false;
                pay(service);
                break;

            case LINKED_CARD:
                is_vas_card_payment = true;
                loadFragment(new SelectCardFragment());
                break;
            case DEBIT_CARD:
                is_vas_card_payment = true;
                startActivityForResult(new Intent(getApplicationContext(), MyCardEditActivity.class),
                        MyApplication.GET_CARD_REQUEST_CODE);
                break;
        }
    }


    @Override
    public void onLinkedFragmentInteraction(final Card card) {
        loadFragment(CvvCheckFragment.newInstance(card));
    }

    @Override
    public void onCardPinInteraction(final Card card, String cardPin) {
        card.pin = cardPin;
        if (Helper.hasInternetConnectivity(this)) {
            new AlertDialog.Builder(this)
                    .setTitle("Process Transaction")
                    .setMessage("Proceed with transaction?")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            fundWithCard(card);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
        }
    }


    void fundWithCard(Card card) {
        statusObject = new StatusObject(false);
        statusObject.setStatusAmount(Double.parseDouble(amount));

        DebitCardProcessor cardProcessor =
                new DebitCardProcessor(DefaultVasActivity.this, userPin);

        Flowable<TamsResponse> fundObservable = cardProcessor.getResponse()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        LiveDataReactiveStreams.fromPublisher(fundObservable).observe(this, new Observer<TamsResponse>() {
            @Override
            public void onChanged(@Nullable TamsResponse tamsResponse) {
                if (tamsResponse.getResult() == TamsResponse.Status.APPROVED) {
                    if (!is_vas_card_payment) {
                        statusObject.setStatus(true);
                        statusObject.statusReason = "Wallet has been funded";
                        is_vas_card_payment = false;
                        notifyTransactionResult();
                    } else {
                        pay(product.name);
                    }
                } else {
                    statusObject.statusReason = tamsResponse.getMessage();
                    notifyTransactionResult();
                }
            }
        });

        cardProcessor.payWithLinkedCard((int) Double.parseDouble(amount), card);
    }

    void notifyTransactionResult() {
        showStatusResult();
        //EmailHandler.sendNotificationMail(DefaultVasActivity.this, statusObject);
    }


    void pay(String service) {
        Log.d("Feature...............", service);

        switch (service){

            case VasServices.IKEJA_ELECTRIC :
              authorizeIkejaPayment();
                      break ;
            case VasServices.IBADAN_ELECTRIC :
                this.amount = (String.valueOf(Integer.parseInt(amount)*100));
                authorizeIbadanPayment();
                break ;
            case VasServices.ENUGU_ELECTRIC :
                this.amount = String.valueOf (Integer.parseInt(amount)*100);
                authorizeEnuguPayment();
                break ;
            case VasServices.EKO_ELECTRIC :
                this.amount = String.valueOf (Integer.parseInt(amount)*100);
                authorizeEkoPayment();
                break ;
            case VasServices.ABUJA_ELECTRIC :
                this.amount = String.valueOf (Integer.parseInt(amount)*100);
                authorizeAbujaPayment();
                break ;
            case VasServices.PORTHARCOURT_ELECTRIC :
                this.amount = String.valueOf (Integer.parseInt(amount)*100);
                authorizePortharcourtPayment();
                break ;
            default:
                Toast.makeText(this, "This feature is not yet available",Toast.LENGTH_SHORT).show();
                break ;

        }

    }

    private void authorizeIkejaPayment() {

        Observable<IkejaModel.IkejaPayResponse> IkejaPayResponse =null;
        String service = "";

        SecureStorage.store("currentMeter", meterNumber);
        Log.d("product.name........", product.name);
        if (product.name.equals(VasServices.IKEJA_ELECTRICITY_PREPAID)){
            service = "token";
            SecureStorage.store("currentType", "PREPAID");
            IkejaModel.IkejaPayDetailsPrepaid ikejaPayDetails = new IkejaModel.IkejaPayDetailsPrepaid(wallet, wallet, userName, password,meterNumber, amount, phone, userPin, "getcus", "vend", service, MyApplication.getSessionKey(),Service.Type.ANDROID.toString());
            IkejaPayResponse = payviceServices.IkejaPayBillPrepaid(ikejaPayDetails);
        }

        else if(product.name.equals(VasServices.IKEJA_ELECTRICITY_POSTPAID)) {
            service = "postpaid";
            SecureStorage.store("currentType", "POSTPAID");
            IkejaModel.IkejaPayDetailsPostPaid ikejaPayDetailsPostPaid = new IkejaModel.IkejaPayDetailsPostPaid(wallet, wallet, userName, password, beneficiary.getData(), amount, "070", userPin, "getcus", "pay", service, MyApplication.getSessionKey(),Service.Type.ANDROID.toString());
            IkejaPayResponse = payviceServices.IkejaPayBillPostPaid(ikejaPayDetailsPostPaid);
        }


        showProgressDialog("Making Purchase...",false);

//        FunctionsKt.getPayviceServices(getApplicationContext())

        final String finalService = service;
        IkejaPayResponse.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<IkejaModel.IkejaPayResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(IkejaModel.IkejaPayResponse ikejaPayResponse) {
                        dismissProgressDialog();

//        statusObject.setStatus(false);

//
                        if (beneficiary != null)
                            statusObject.statusRecipient = beneficiary.getDisplayInfo();
                        else
                            statusObject.statusRecipient = "Self";
//
                        statusObject.statusService = product.name;
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//

                        if(!ikejaPayResponse.getError()) {
                            statusObject.statusReason = ikejaPayResponse.getMessage() + "\n \n Name :" + ikejaPayResponse.getPayer() +"\n \n Amount : " + "₦" + amount+  "\n \n Date :" + ikejaPayResponse.getDate() +"\n \n  Address : " + ikejaPayResponse.getAddress() + "\n \n Transaction Id : " + ikejaPayResponse.getTransactionID();
                            if(finalService == "token")
                                statusObject.statusReason = ikejaPayResponse.getMessage() + "\n \n Name :" + ikejaPayResponse.getPayer() + "\n \nAmount : " + "₦" +amount +  "\n \n Token  : " + ikejaPayResponse.getToken() + "\n \n Date :" + ikejaPayResponse.getDate() + "\n \n  Address : " + ikejaPayResponse.getAddress() + "\n \nTransaction Id : " + ikejaPayResponse.getTransactionID();

                        }else
                            statusObject.statusReason = ikejaPayResponse.getMessage() ;
                        statusObject.setStatus(!ikejaPayResponse.getError());

                        SecureStorage.store("currentservice", "Disco");
                        SecureStorage.store("currentAmount", amount);
                        SecureStorage.store("currentPhone", ikejaPayResponse.getToken());
                        SecureStorage.store("currentProduct", "Ikeja");

                        SecureStorage.store("currentReason", ikejaPayResponse.getMessage());
                        if (ikejaPayResponse.getError()){
                            SecureStorage.store("currentApproval", "Declined");
                        }
                        else{
                            SecureStorage.store("currentApproval", "Approved");
                        }

                        TransactionModel transaction = null;
                        Intent intent = new Intent(getBaseContext(), ResultVasActivity.class);
                        intent.putExtra("transaction", transaction);
                        startActivity(intent);

                        notifyTransactionResult();
//
//            }
//        }).start();

                    }

                    @Override
                    public void onError(Throwable e) {
//                   if  (IllegalStateException e) {
                        statusObject.statusReason = e.getMessage();
                        dismissProgressDialog();
//              e.printStackTrace();
//                }
//                else{
//                    e.printStackTrace();
//                    statusObject.statusReason = "An error occurred. Please try again later";
//                }


                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    public void authorizeEnuguPayment(){

        Observable<EnuguModel.PayResponse>ekoPayResponse =null;
        String type = "prepaid";
        SecureStorage.store("currentMeter", meterNumber);
        SecureStorage.store("currentType", "PREPAID");
        if (product.name == VasServices.ENUGU_ELECTRICITY_POSTPAID){
            type = "postpaid";
            SecureStorage.store("currentType", "POSTPAID");
        }

        showProgressDialog("Making Purchase...",false);

//

        EnuguModel.PayRequest enuguPayDetails = new EnuguModel.PayRequest(wallet,userName,password,type,Service.Type.ANDROID.toString(),meterNumber,amount,phone,productCode,meterName,VasServices.CASH,userPin,clientReference);
        ekoPayResponse = payviceServices.EnuguElectricPayment(enuguPayDetails);


        final String finalType = type;
        ekoPayResponse.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<EnuguModel.PayResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(EnuguModel.PayResponse enuguPayResponse) {
                        dismissProgressDialog();

//        statusObject.setStatus(false);

//
                        if (beneficiary != null)
                            statusObject.statusRecipient = beneficiary.getDisplayInfo();
                        else
                            statusObject.statusRecipient = "Self";
//
                        statusObject.statusService = product.name;
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//
//                    VasResult result = transactionManager.processTransaction();
//                    String message = result.message, balance = result.balance;
//                        statusObject.statusReason = enuguPayResponse.getMessage() + "\n \n Name :" + enuguPayResponse.getName()+"\n \nAccount : " + enuguPayResponse.getAccount() + "\n \nAccountType : " + enuguPayResponse.getType() +"\n \nToken : " + enuguPayResponse.getToken() +"\n \nValue : " + enuguPayResponse.getValue() + "\n \nUnit Value : " + enuguPayResponse.getUnit_value() +  "\n \nToken : " + enuguPayResponse.getToken()+"\n \n Address : " + enuguPayResponse.getAddress()  +"\n \n Transaction Id : " + enuguPayResponse.getTransactionID() ;
                        if(!enuguPayResponse.getError()) {
                            statusObject.statusReason = enuguPayResponse.getMessage() + "\n \n Name :" + enuguPayResponse.getName() + "\n \nAccount : " + enuguPayResponse.getAccount() + "\n \nAccountType : " + enuguPayResponse.getType() + "\n \nAmount : "+ "₦"  + String.valueOf((Double.parseDouble(amount)/100))  + "\n \n Address : " + enuguPayResponse.getAddress() + "\n \n Transaction Id : " + enuguPayResponse.getTransactionID();

                            if(finalType == VasServices.PREPAID)
                                statusObject.statusReason = enuguPayResponse.getMessage() + "\n \n Name :" + enuguPayResponse.getName() + "\n \nAccount : " + enuguPayResponse.getAccount() + "\n \nAccountType : " + enuguPayResponse.getType() +  "\n \nAmount : " +"₦"  + Double.parseDouble(amount)/100  + "\n \nToken : " + enuguPayResponse.getToken() + "\n \n Address : " + enuguPayResponse.getAddress() + "\n \n Transaction Id : " + enuguPayResponse.getTransactionID();


                        }else {
                            Log.i("Got an error", enuguPayResponse.getDescription());
                            statusObject.statusReason = enuguPayResponse.getDescription();
                        }
                        statusObject.setStatus(!enuguPayResponse.getError());

                        SecureStorage.store("currentservice", "Disco");
                        SecureStorage.store("currentAmount", Double.parseDouble(amount)/100+"");
                        SecureStorage.store("currentPhone", enuguPayResponse.getToken());
                        SecureStorage.store("currentProduct", "EEDC");
                        SecureStorage.store("currentArrears", enuguPayResponse.getArrears());
                        SecureStorage.store("currentTarriff", enuguPayResponse.getTariff());
                        SecureStorage.store("currentAddress", enuguPayResponse.getAddress());
                        SecureStorage.store("currentInvoice",enuguPayResponse.getInvoiceNumber());
                        SecureStorage.store("currentTransId", enuguPayResponse.getReference());
                        SecureStorage.store("currentVat", enuguPayResponse.getVat());
                        SecureStorage.store("currentCashier", enuguPayResponse.getCasher());
                        SecureStorage.store("currentUnitValue", enuguPayResponse.getUnit_value());

                        SecureStorage.store("currentReason", enuguPayResponse.getMessage());
                        if (enuguPayResponse.getError()){
                            SecureStorage.store("currentApproval", "Declined");
                        }
                        else{
                            SecureStorage.store("currentApproval", "Approved");
                        }

                        TransactionModel transaction = null;
//                        TransactionModel transaction = new TransactionModel(userId, "", "", "", "", "", "", Long.valueOf(amount), 0, Host.TransactionType.CASH_IN, "",  true, "Approved", "", "", "", "", "");

                        Intent intent = new Intent(getBaseContext(), ResultVasActivity.class);
                        intent.putExtra("transaction", transaction);
                        startActivity(intent);

                        notifyTransactionResult();
//
//            }
//        }).start();

                    }

                    @Override
                    public void onError(Throwable e) {
//                   if  (IllegalStateException e) {
                        statusObject.statusReason = e.getMessage();
                        dismissProgressDialog();
//              e.printStackTrace();
//                }
//                else{
//                    e.printStackTrace();
//                    statusObject.statusReason = "An error occurred. Please try again later";
//                }


                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    public  void authorizeEkoPayment(){

        Observable<EkoModel.EkoPayResponse>ekoPayResponse =null;
        String service = "";
        SecureStorage.store("currentMeter", meterNumber);
        SecureStorage.store("currentType", "PREPAID");
        Log.d("product.name........", product.name);
        service = VasServices.PREPAID;
        if(product.name == VasServices.EKO_ELECTRICITY_POSTPAID) {
            service = VasServices.POSTPAID;
            SecureStorage.store("currentType", "POSTPAID");
        }

        EkoModel.EkoPayDetails ekoPayDetailsPostPaid = new EkoModel.EkoPayDetails(meterNumber,service,amount,terminalID,phone,userID,VasServices.CASH,password,userPin,Service.Type.ANDROID.toString(),clientReference);
        ekoPayResponse = payviceServices.EkoPayBill(ekoPayDetailsPostPaid);
        showProgressDialog("Making Purchase...",false);


        final String finalService = service;
        ekoPayResponse.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<EkoModel.EkoPayResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(EkoModel.EkoPayResponse ekoPayResponse) {
                        dismissProgressDialog();

//        statusObject.setStatus(false);

//
                        if (beneficiary != null)
                            statusObject.statusRecipient = beneficiary.getDisplayInfo();
                        else
                            statusObject.statusRecipient = "Self";
//
                        statusObject.statusService = product.name;
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//
//                    VasResult result = transactionManager.processTransaction();
//                    String message = result.message, balance = result.balance;
//
                        if(!ekoPayResponse.getError()) {
                            statusObject.statusReason = ekoPayResponse.getMessage() + "\n \n Name :" + ekoPayResponse.getPayer()  + "\n \n AccountType :" + ekoPayResponse.getAccount_type()  +"\n \nAmount : "+ "₦"  + String.valueOf((Double.parseDouble(amount)/100))+ "\n \n Address  :" + ekoPayResponse.getAddress()  + "\n \n Date :" + ekoPayResponse.getDate()  +"\n \nTransaction Id " + ekoPayResponse.getTransactionID();

                            if (finalService == VasServices.PREPAID)
                                statusObject.statusReason = ekoPayResponse.getMessage() + "\n \n Name :" + ekoPayResponse.getPayer()  + "\n \n AccountType :" + ekoPayResponse.getAccount_type()  + "\n \nAmount : "+ "₦"  + String.valueOf((Double.parseDouble(amount)/100))+"\n \n Token :" + ekoPayResponse.getToken() + "\n \n Date :" + ekoPayResponse.getDate() + "\n \n Address  :" + ekoPayResponse.getAddress()  +  "\n \nTransaction Id " + ekoPayResponse.getTransactionID();


                        }else

                            statusObject.statusReason = ekoPayResponse.getMessage() ;

                        statusObject.setStatus(!ekoPayResponse.getError());

                        SecureStorage.store("currentservice", "Disco");
                        SecureStorage.store("currentAmount", Double.parseDouble(amount)/100);
                        SecureStorage.store("currentPhone", ekoPayResponse.getToken());
                        SecureStorage.store("currentProduct", "EKO");

                        SecureStorage.store("currentReason", ekoPayResponse.getMessage());
                        if (ekoPayResponse.getError()){
                            SecureStorage.store("currentApproval", "Declined");
                        }
                        else{
                            SecureStorage.store("currentApproval", "Approved");
                        }

                        TransactionModel transaction = null;

                        Intent intent = new Intent(getBaseContext(), ResultVasActivity.class);
                        intent.putExtra("transaction", transaction);
                        startActivity(intent);

                        notifyTransactionResult();
//
//            }
//        }).start();

                    }

                    @Override
                    public void onError(Throwable e) {
//                   if  (IllegalStateException e) {
                        statusObject.statusReason = e.getMessage();
                        dismissProgressDialog();
//              e.printStackTrace();
//                }
//                else{
//                    e.printStackTrace();
//                    statusObject.statusReason = "An error occurred. Please try again later";
//                }
                        notifyTransactionResult();

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    private void authorizeAbujaPayment() {

        Observable<AbujaModel.PurchaseResponse>abujaPayResponse =null;
        String meterType = "0";
        String requestType ="1";
        SecureStorage.store("currentType", "PREPAID");
        if (product.name == VasServices.ABUJA_ELECTRICITY_POSTPAID){
            meterType = "2";
            SecureStorage.store("currentType", "POSTPAID");
        }


        showProgressDialog("Making Purchase...",false);

//
        SecureStorage.store("currentMeter", meterNumber);
        AbujaModel.PurchaseDetails abujaDetails = new  AbujaModel.PurchaseDetails(wallet,userName,requestType,meterType,meterNumber,Service.Type.ANDROID.toString(),phone,amount,productCode,userPin,VasServices.CASH);
        abujaPayResponse = payviceServices.abujaPayBill(abujaDetails);


        final String finalMeterType = meterType;
        abujaPayResponse.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<AbujaModel.PurchaseResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(AbujaModel.PurchaseResponse abujaPayResponse) {
                        dismissProgressDialog();

//                  statusObject.setStatus(false);

//
                        if (beneficiary != null)
                            statusObject.statusRecipient = beneficiary.getDisplayInfo();
                        else
                            statusObject.statusRecipient = "Self";
//
                        statusObject.statusService = product.name;
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//
//                    VasResult result = transactionManager.processTransaction();
//                    String message = result.message, balance = result.balance;
//                        statusObject.statusReason = enuguPayResponse.getMessage() + "\n \n Name :" + enuguPayResponse.getName()+"\n \nAccount : " + enuguPayResponse.getAccount() + "\n \nAccountType : " + enuguPayResponse.getType() +"\n \nToken : " + enuguPayResponse.getToken() +"\n \nValue : " + enuguPayResponse.getValue() + "\n \nUnit Value : " + enuguPayResponse.getUnit_value() +  "\n \nToken : " + enuguPayResponse.getToken()+"\n \n Address : " + enuguPayResponse.getAddress()  +"\n \n Transaction Id : " + enuguPayResponse.getTransactionID() ;
                        if(!abujaPayResponse.getError()) {
                            statusObject.statusReason = abujaPayResponse.getDescription() + "\n \n Name :" + abujaPayResponse.getMessage() + "\n \nAccount : " + abujaPayResponse.getAccount() + "\n \nAccountType : " + abujaPayResponse.getType() + "\n \nValue : " + abujaPayResponse.getValue() + "\n \nAmount : " + "₦" +abujaPayResponse.getAmount()  + "\n \n Transaction Id : " + abujaPayResponse.getTransactionID();
                            if (finalMeterType == "0")
                                statusObject.statusReason = abujaPayResponse.getDescription() + "\n \n Name :" + abujaPayResponse.getMessage() + "\n \nAccount : " + abujaPayResponse.getAccount() + "\n \nAccountType : " + abujaPayResponse.getType() + "\n \nValue : " + abujaPayResponse.getValue() + "\n \nAmount : " +  "₦" + abujaPayResponse.getAmount() + "\n \nToken : " + abujaPayResponse.getToken() + "\n \n Transaction Id : " + abujaPayResponse.getTransactionID();

                        }else
                            statusObject.statusReason = abujaPayResponse.getDescription() ;

                        statusObject.setStatus(!abujaPayResponse.getError());

                        SecureStorage.store("currentservice", "Disco");
                        SecureStorage.store("currentAmount", Double.parseDouble(amount)/100);
                        SecureStorage.store("currentPhone", abujaPayResponse.getToken());
                        SecureStorage.store("currentVat", abujaPayResponse.getVat());
                        SecureStorage.store("currentValue", abujaPayResponse.getValue());
                        SecureStorage.store("currentUnitValue", abujaPayResponse.getUnit_value());
                        SecureStorage.store("currentProduct", "Abuja");

                        SecureStorage.store("currentReason", abujaPayResponse.getMessage());
                        if (abujaPayResponse.getError()){
                            SecureStorage.store("currentApproval", "Declined");
                        }
                        else{
                            SecureStorage.store("currentApproval", "Approved");
                        }
                        TransactionModel transaction = null;
                        Intent intent = new Intent(getBaseContext(), ResultVasActivity.class);
                        intent.putExtra("transaction", transaction);
                        startActivity(intent);

                        notifyTransactionResult();

//        }).start();

                    }

                    @Override
                    public void onError(Throwable e) {
//                   if  (IllegalStateException e) {
                        statusObject.statusReason = e.getMessage();
                        dismissProgressDialog();
//              e.printStackTrace();
//                }
//                else{
//                    e.printStackTrace();
//                    statusObject.statusReason = "An error occurred. Please try again later";
//                }


                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void  authorizePortharcourtPayment(){

        Observable<PortharcourtModel.purchaseResponse> portharcourtPayResponse =null;
        String type = VasServices.PREPAID;
        SecureStorage.store("currentType", "PREPAID");
        if (product.name == VasServices.PORTHARCOURT_ELECTRICITY_POSTPAID){
            type = VasServices.POSTPAID;
            SecureStorage.store("currentType", "POSTPAID");
        }


        showProgressDialog("Making Purchase...",false);

//

        SecureStorage.store("currentMeter", meterNumber);
        PortharcourtModel.purchaseDetails phcPayDetails = new  PortharcourtModel.purchaseDetails(terminalID,wallet,userName,password,userPin,type,Service.Type.ANDROID.toString(),meterNumber,amount,phone,productCode,meterName,VasServices.CASH,clientReference);
        portharcourtPayResponse = payviceServices.portharcourtPayBill(phcPayDetails);


        final String finalType = type;
        portharcourtPayResponse.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<PortharcourtModel.purchaseResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(PortharcourtModel.purchaseResponse PHCPayResponse) {
                        dismissProgressDialog();

                        Log.d("Portharcourt Response :", new Gson().toJson(PHCPayResponse,PortharcourtModel.purchaseResponse.class));
                        Log.d("Portharcourt Response :", new Gson().toJson(PHCPayResponse));

//        statusObject.setStatus(false);

//
                        if (beneficiary != null)
                            statusObject.statusRecipient = beneficiary.getDisplayInfo();
                        else
                            statusObject.statusRecipient = "Self";
//
                        statusObject.statusService = product.name;
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//
//                    VasResult result = transactionManager.processTransaction();
//                    String message = result.message, balance = result.balance;
//                        statusObject.statusReason = enuguPayResponse.getMessage() + "\n \n Name :" + enuguPayResponse.getName()+"\n \nAccount : " + enuguPayResponse.getAccount() + "\n \nAccountType : " + enuguPayResponse.getType() +"\n \nToken : " + enuguPayResponse.getToken() +"\n \nValue : " + enuguPayResponse.getValue() + "\n \nUnit Value : " + enuguPayResponse.getUnit_value() +  "\n \nToken : " + enuguPayResponse.getToken()+"\n \n Address : " + enuguPayResponse.getAddress()  +"\n \n Transaction Id : " + enuguPayResponse.getTransactionID() ;
                        if(!PHCPayResponse.getError()) {
                            statusObject.statusReason = PHCPayResponse.getDescription() + "\n \n Name :" + PHCPayResponse.getName() + "\n \nAccount : " + PHCPayResponse.getAccount() + "\n \nAccountType : " + PHCPayResponse.getType() + "\n \nValue : " + PHCPayResponse.getValue() + "\n \nAmount : " + "₦" +PHCPayResponse.getAmount() + "\n \n Date :" + PHCPayResponse.getPaymentDate()  +"\n \n Transaction Id : " + PHCPayResponse.getTransactionID();

                            if (finalType == VasServices.PREPAID)
                                statusObject.statusReason = PHCPayResponse.getDescription() + "\n \n Name :" + PHCPayResponse.getName() + "\n \nAccount : " + PHCPayResponse.getAccount() + "\n \nAccountType : " + PHCPayResponse.getType() + "\n \nValue : " + PHCPayResponse.getValue() + "\n \nAmount : " + "₦" +PHCPayResponse.getAmount() + "\n \n Date :" + PHCPayResponse.getPaymentDate() + "\n \nToken : " + PHCPayResponse.getToken() + "\n \n Transaction Id : " + PHCPayResponse.getTransactionID();

//                            statusObject.statusReason = PHCPayResponse.getDescription() + "\n \n Name :" + PHCPayResponse.getMessage()+"\n \nAccount : " + PHCPayResponse.getAccount() + "\n \nAccountType : " + PHCPayResponse.getType() +"\n \nToken : " + PHCPayResponse.getToken() +"\n \nValue : " + PHCPayResponse.getValue() +  "\n \nAmount : " + PHCPayResponse.getAmount() +"\n \n Transaction Id : " + PHCPayResponse.getTransactionID() ;
                        }else
                            statusObject.statusReason = PHCPayResponse.getDescription() ;

                        statusObject.setStatus(!PHCPayResponse.getError());

                        SecureStorage.store("currentservice", "Disco");
                        SecureStorage.store("currentAmount", Double.parseDouble(amount)/100);
                        SecureStorage.store("currentPhone", PHCPayResponse.getToken());
                        SecureStorage.store("currentProduct", "Port Harcourt");

                        SecureStorage.store("currentReason", PHCPayResponse.getMessage());
                        if (PHCPayResponse.getError()){
                            SecureStorage.store("currentApproval", "Declined");
                        }
                        else{
                            SecureStorage.store("currentApproval", "Approved");
                        }

                        TransactionModel transaction = null;
                        Intent intent = new Intent(getBaseContext(), ResultVasActivity.class);
                        intent.putExtra("transaction", transaction);
                        startActivity(intent);

                        notifyTransactionResult();

//        }).start();

                    }

                    @Override
                    public void onError(Throwable e) {
//                   if  (IllegalStateException e) {
                        statusObject.statusReason = e.getMessage();
                        dismissProgressDialog();
                        Log.d("Portharcourt Response :", new Gson().toJson(statusObject.statusReason));

//              e.printStackTrace();
//                }
//                else{
//                    e.printStackTrace();
//                    statusObject.statusReason = "An error occurred. Please try again later";
//                }


                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    private void authorizeIbadanPayment() {

        Observable<IbadanModel.IbPayResponse>ibadanPayResponse =null;
        String type = VasServices.PREPAID;
        SecureStorage.store("currentType", "PREPAID");
        if (product.name == VasServices.IBADAN_ELECTRICITY_POSTPAID){
            type = VasServices.POSTPAID;
            SecureStorage.store("currentType", "POSTPAID");
        }

        showProgressDialog("Making Purchase..",false);

//
        SecureStorage.store("currentMeter", meterNumber);
        IbadanModel.IbPayRequest ibadanPayDetails = new  IbadanModel.IbPayRequest(wallet,userName,password,userPin,type,Service.Type.ANDROID.toString(),meterNumber,amount,phone,productCode,meterName,VasServices.CASH,clientReference);
        ibadanPayResponse = payviceServices.ibadanPayBill(ibadanPayDetails);

        final String finalType = type;
        ibadanPayResponse.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<IbadanModel.IbPayResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(IbadanModel.IbPayResponse ibadanPayResponse) {
                        dismissProgressDialog();

//        statusObject.setStatus(false);

////
//                        if (beneficiary != null)
//                            statusObject.statusRecipient = beneficiary.getDisplayInfo();
//                        else
//                            statusObject.statusRecipient = "Self";
//
//                        statusObject.statusService = product.name;
//

                       if(!ibadanPayResponse.getError()) {
                           statusObject.statusReason = "\n \n Name :" + ibadanPayResponse.getName() + "\n \nAccount : " + ibadanPayResponse.getAccount() + "\n \nAccountType : " + ibadanPayResponse.getType()  + "\n \nUnit Value : " + ibadanPayResponse.getTokenDetailsKWH() + "\n \nAmount : " + "₦" + String.valueOf(Integer.parseInt(amount)/100)+ "\n \n Transaction Id : " + ibadanPayResponse.getTransactionID();

                             if (finalType == VasServices.PREPAID)
                                 statusObject.statusReason =  "\n \n Name :" + ibadanPayResponse.getName() + "\n \nAccount : " + ibadanPayResponse.getAccount() + "\n \nAccountType : " + ibadanPayResponse.getType() + "\n \nToken : " + ibadanPayResponse.getToken()  + "\n \nUnit Value : " + ibadanPayResponse.getTokenDetailsKWH() + "\n \nAmount : " +"₦" + ibadanPayResponse.getTokenDetailsAmount() + "\n \n Transaction Id : " + ibadanPayResponse.getTransactionID();

                       }else
                           statusObject.statusReason = ibadanPayResponse.getMessage() ;

                        statusObject.setStatus(!ibadanPayResponse.getError());

                        SecureStorage.store("currentservice", "Disco");
                        SecureStorage.store("currentAmount", Double.parseDouble(amount)/100);
                        SecureStorage.store("currentPhone", ibadanPayResponse.getToken());
                        SecureStorage.store("currentProduct", "Ibadan");

                        SecureStorage.store("currentReason", ibadanPayResponse.getMessage());
                        if (ibadanPayResponse.getError()){
                            SecureStorage.store("currentApproval", "Declined");
                        }
                        else{
                            SecureStorage.store("currentApproval", "Approved");
                        }

                        TransactionModel transaction = null;
                        Intent intent = new Intent(getBaseContext(), ResultVasActivity.class);
                        intent.putExtra("transaction", transaction);
                        startActivity(intent);

                        notifyTransactionResult();

//        }).start();

                    }

                    @Override
                    public void onError(Throwable e) {
//                   if  (IllegalStateException e) {
                        statusObject.statusReason = e.getMessage();
                        dismissProgressDialog();
//              e.printStackTrace();
//                }
//                else{
//                    e.printStackTrace();
//                    statusObject.statusReason = "An error occurred. Please try again later";
//                }


                    }

                    @Override
                    public void onComplete() {

                    }
                });




    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MyApplication.GET_CARD_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY) != null) {

                String cardHolderName = data.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME);
                String cardNumber = data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER);
                String expiry = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY);
                String cvv = data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV);

                Card currentCard = new Card();
                currentCard.cardHolder = cardHolderName;
                currentCard.PAN = cardNumber;
                currentCard.CVV = cvv;
                currentCard.expiryMonth = expiry.split("\\/")[0];
                currentCard.expiryYear = expiry.split("\\/")[1];

                // Your processing goes here.
                loadFragment(CvvCheckFragment.newInstance(currentCard));
            }
        }

        if (requestCode == SelectCardFragment.LINK_CARD_REQUEST_CODE) {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }


    Map<String, String> getVasPaymentParams() {

        String action = context.getString(R.string.tams_webapi_action);

        Map<String, String> params = new HashMap<>();
        params.put("action", action);
        params.put("amount", amount);
        params.put("control", "");
        params.put("pin", userPin);
        params.put("phoneno", beneficiary.getData());

        String requestCode = product.requestCode;

        params.put("network", requestCode);
        params.put("termid", terminalID);
        params.put("userid", userId);
        params.put("op", "EXCHANGE");

        if (pinValue != null && quantity > 0) {
            params.put("value", pinValue.value + "");
            params.put("no", quantity + "");
        }

        if (planValue != null && period > 0) {
            params.put("value", planValue.value + "");
            params.put("no", period + "");
        }

        params.put("convenience", convenienceFee + "");

        return params;
    }
    //Notifiers

    void showProgressDialog(final String title, final String message, final boolean cancelable) {
        Helper.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boolean isCancelable = cancelable;

                progressDialog.setTitle(title);
                progressDialog.setMessage(message);
                progressDialog.setCancelable(isCancelable);
                progressDialog.show();
            }
        });

    }

    void showProgressDialog(final String message, final boolean cancelable) {
        Helper.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boolean isCancelable = cancelable;
                progressDialog.setMessage(message);
                progressDialog.setCancelable(isCancelable);
                progressDialog.show();
            }
        });
    }

    void dismissProgressDialog() {
        Helper.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        });
    }


    void showStatusResult() {
        dismissProgressDialog();
        Helper.runOnUiThread(new Runnable() {
            @Override
            public void run() {

               // loadFragment(StatusFragment.getInstance(statusObject));
            }
        });
    }

}
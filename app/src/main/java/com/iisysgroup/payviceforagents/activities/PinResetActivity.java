package com.iisysgroup.payviceforagents.activities;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;

import java.util.HashMap;
import java.util.Map;

public class PinResetActivity extends BaseActivity {

    EditText pinEdit, pinEdit2, pinEdit3, pinEdit4, confirmPinEdit;
    TextInputLayout pinTIL, confirmPinTIL;

    ProgressDialog progressDialog;
    Handler handler = new Handler();
    DialogInterface.OnClickListener action = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Helper.savePreference(getApplication(), Helper.ID_TO_RESET, "");
            Helper.savePreference(getApplication(), Helper.PASSWORD_IS_RESET, false);
            finish();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_reset);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pinEdit = findViewById(R.id.pinEdit);
        pinEdit2 = findViewById(R.id.pinEdit2);
        pinEdit3 = findViewById(R.id.pinEdit3);
        pinEdit4 = findViewById(R.id.pinEdit4);
        confirmPinEdit = findViewById(R.id.confirmPinEdit);

        pinTIL = findViewById(R.id.pinTIL);
        confirmPinTIL = findViewById(R.id.confirmPinTIL);

        findViewById(R.id.pin_reset_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Helper.hasInternetConnectivity(getApplication()))
                    doPinReset();

            }
        });
    }

    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_log_out) {
            new AlertDialog.Builder(this)
                    .setTitle("Log Out")
                    .setMessage("Proceed with log out?")
                    .setNegativeButton(android.R.string.cancel, null)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            logUserOut();
                        }
                    }).show();

        }
        return super.onOptionsItemSelected(item);
    }

    boolean isValidated() {

        String pin = pinEdit.getText().toString();

        if (pin.length() < 4) {
            pinTIL.setErrorEnabled(true);
            pinTIL.setError("pin should not be less than 4 characters");
            Helper.showErrorAnim(pinEdit);
            pinEdit.requestFocus();
            return false;
        }

        if (!pin.equals(confirmPinEdit.getText().toString())) {
            confirmPinTIL.setErrorEnabled(true);
            confirmPinTIL.setError("pin doesn't match");
            Helper.showErrorAnim(confirmPinEdit);
            confirmPinEdit.requestFocus();
            return false;
        }

        return true;
    }

    void resetPin() throws Exception {

        final String url = getResources().getString(R.string.tams_url),
                PIN_ACTION = "TAMS_PIN_UPDATE", CONTROL = "PIN_RESET";

        String userId = SecureStorage.retrieve(Helper.USER_ID, "");
        String terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID, "");

        String pin = pinEdit.getText().toString();
        pin = Helper.preparePin(pin);

        Map<String, String> params = new HashMap<>();

        params.put("action", PIN_ACTION);
        params.put("userid", userId);
        params.put("termid", terminalID);
        params.put("control", CONTROL);
        params.put("pin", pin);
        SecureStorage.store(Helper.PIN, pin);
        Log.d("encryptedpinreset", "resetPin: "+ pin);

        VasResult tamsResult = Requests.processRequest(url, params);

        if (tamsResult.result == VasResult.Result.APPROVED) {
            Helper.showInfoDialogWithAction(this,
                    "Pin Reset Successful", "Your pin has been successfully reset.", action);
            //finish();
            Helper.savePreference(getApplication(), Helper.ID_TO_RESET, "");
            Helper.savePreference(getApplication(), Helper.PASSWORD_IS_RESET, false);
            startActivity(new Intent(this, MainActivity.class));

        } else
            throw new IllegalAccessException(tamsResult.message);

    }

    void doPinReset() {
        if (isValidated()) {
            progressDialog = ProgressDialog.show(PinResetActivity.this, "Pin Reset", "Please wait...", true, false);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        resetPin();
                    } catch (IllegalAccessException e) {
                        Helper.showInfoDialog(PinResetActivity.this, "Pin Reset failed",
                                e.getMessage());
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Helper.showInfoDialog(PinResetActivity.this, "Pin Reset failed",
                                "Please check your details and try again.");

                    } finally {
                        dismissProgressDialog();
                    }
                }
            }).start();
        }
    }

    void dismissProgressDialog() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        });
    }


}

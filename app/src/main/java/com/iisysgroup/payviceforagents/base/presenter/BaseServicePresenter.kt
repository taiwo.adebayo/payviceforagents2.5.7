package com.iisysgroup.payviceforagents.base.presenter

import android.arch.lifecycle.LiveData
import com.iisysgroup.payviceforagents.entities.Service

interface BaseServicePresenter {
    fun setProduct(product: Service.Product)
    fun onSetProduct(): LiveData<Service.Product>

    fun setError(throwable: Throwable)
    fun onError(): LiveData<Throwable>

    fun showProgress(show: Boolean, message: String = "Processing...")
    fun onProgressUpdate(): LiveData<Pair<Boolean, String>>


}
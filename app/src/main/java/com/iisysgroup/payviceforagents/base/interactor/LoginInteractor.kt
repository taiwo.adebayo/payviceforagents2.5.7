package com.iisysgroup.payviceforagents.base.interactor

import com.iisysgroup.payviceforagents.entities.VasResult
import com.iisysgroup.payviceforagents.models.PayviceForMerchants
import com.iisysgroup.payviceforagents.models.PayviceForMerchantsSummary
import io.reactivex.Single

interface LoginInteractor {
    fun getUserInfo(userId: String): Single<VasResult>
    fun login(userID: String, password: String, walletId: String): Single<VasResult>
    fun getDeviceId(): String
    fun storeLoginDetails(userId: String, encryptedPassword: String, key: String, loginResult: VasResult)
    fun storePfmData(data: PayviceForMerchants, summary: PayviceForMerchantsSummary)
}
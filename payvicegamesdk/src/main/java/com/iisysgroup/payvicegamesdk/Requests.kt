package com.iisysgroup.payvicegamesdk

import android.app.ProgressDialog
import android.content.Context
import android.util.Log
import com.iisysgroup.payvicegamesdk.utils.CardUtils
import com.iisysgroup.payvicegamesdk.utils.Helper
import com.iisysgroup.payvicegamesdk.utils.KeyCrypto
import com.interswitchng.sdk.payment.model.PurchaseResponse
import java.util.*


object Requests {

    private const val TAMS_WEB_API_ACTION = "TAMS_WEBAPI"
    private const val TAMS_LOGIN_ACTION = "TAMS_LOGIN"
    private const val TAMS_INIT_CONTROL = "INIT"
    private const val TAMS_DEFAULT_TID = "203301ZA"
    private const val TAMS_URL = "http://197.253.19.75/ctms/eftpos/devinterface/transactionadvice.php"

    private val tamsRequestProcessor = TamsRequestProcessor()



    fun verifyUserPin(userId: String, walletID: String, encryptedPin: String): Boolean {
        val control = "DOMIGS"

        val params = HashMap<String, String>()
        params["action"] = TAMS_WEB_API_ACTION
        params["userid"] = userId
        params["termid"] = walletID
        params["control"] = control
        params["op"] = "INIT"
        params["pin"] = encryptedPin

        val tamsResponse = tamsRequestProcessor.processRequest(TAMS_URL, params)

        return tamsResponse.result == TamsResponse.Status.APPROVED
    }

    fun verifyUserId(userId: String, walletID: String = TAMS_DEFAULT_TID): TamsResponse {

        val params = HashMap<String, String>()
        params["action"] = TAMS_LOGIN_ACTION
        params["userid"] = userId
        params["termid"] = walletID
        params["control"] = TAMS_INIT_CONTROL


        return tamsRequestProcessor.processRequest(TAMS_URL, params)
    }

    fun sendTamsFundNotification(requestData: FundRequestData, response: PurchaseResponse): TamsResponse {
        val amount =  response.amount
        val serializedPaymentData = getSerializedData(response)
        val checkSum = getCheckSum(requestData.userKey, response)
        val encryptedCardDetails = getEncryptedCardInfo(requestData.userKey, response)

       return doTamsNotification(requestData, amount, encryptedCardDetails, serializedPaymentData, checkSum)
    }

    private fun getSerializedData(response: PurchaseResponse): String {
        val transactionReference = response.transactionRef + "_" + System.currentTimeMillis()

        return response.amount +
                "|" + transactionReference +
                "|" + response.transactionIdentifier +
                "|" + response.panLast4Digits +
                "|" + response.cardType +
                "|" + response.otpTransactionIdentifier +
                "|" + response.token +
                "|" + response.message
    }


    private fun getCheckSum(userKey: String, purchaseResponse: PurchaseResponse): String {
        val transactionReference = purchaseResponse.transactionRef + "_" + System.currentTimeMillis()



        Log.d("OkHTransactionReference", transactionReference)
        return KeyCrypto.hex(CardUtils.getKeyCrypto(userKey).encryptData(transactionReference))
    }

    private fun getEncryptedCardInfo(userKey: String, purchaseResponse: PurchaseResponse): String {
        val pan =  purchaseResponse.token
        val expiryYear = purchaseResponse.tokenExpiryDate.substring(IntRange(0,1))
        val expiryMonth = purchaseResponse.tokenExpiryDate.subSequence(2,3)
        val cardDetails =  "$pan|$expiryMonth|$expiryYear"

        Log.d("OkH", cardDetails)


        return KeyCrypto.hex(CardUtils.getKeyCrypto(userKey).encryptData(cardDetails))
    }

    private fun doTamsNotification(requestData: FundRequestData, amount: String, encryptedCardDetails: String,
                                   serializedPaymentData: String, checkSum: String): TamsResponse {

            val action = TAMS_WEB_API_ACTION
            val control = "DOMIGS"

            Log.d("OkHCheckSum", checkSum)

            val params = HashMap<String, String>()
            params["action"] = action
            params["userid"] = requestData.userId
            params["termid"] = requestData.walletId
            params["control"] = control
            params["value"] = encryptedCardDetails
            params["paydata"] = serializedPaymentData
            params["transaction_id"] = checkSum
            params["amount"] = amount
            params["pin"] = requestData.encryptedPin



        return tamsRequestProcessor.processRequest(TAMS_URL, params)
    }


}
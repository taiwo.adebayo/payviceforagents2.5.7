package com.iisysgroup.payviceforagents.entities;

import java.io.Serializable;

/**
 * Created by Bamitale@Itex on 5/5/2016.
 */
public class PlanValue implements Serializable {
    public int value;
    public String description;
    public String planCode;


    @Override
    public String toString() {
        return description + " - \u20A6 " + value;
    }
}

package com.iisysgroup.payviceforagents.fragments;


import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.iisysgroup.payviceforagents.activities.MainActivity;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.HistoryAdapter_D;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends HistoryFragment_D {

    final MainFragment context = this;
    @BindView(R.id.walletBalanceText)
    TextView walletBalanceText;
    @BindView(R.id.wallet_id)
    TextView walletId;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    Handler handler = new Handler();
    private OnFragmentInteractionListener mListener;
    private ImageView imageView;

    public MainFragment() {
        // Required empty public constructor
    }

    @OnClick(R.id.referBtn)
    void showReferral() {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).showReferralOptionDialog();
        }
    }

    @OnClick(R.id.bonusBtn)
    void showPromo() {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).showPromoDialog();
        }
    }

    @OnClick(R.id.rateBtn)
    void rateApp() {
        String appName = getString(R.string.app_name);
        Helper.getAlertDialogBuilder(getActivity(), "Rate " + appName,
                "Thanks for using " + appName + ". Would you mind taking a minute to rate it?")
                .setNegativeButton("No Thanks", null)
                .setPositiveButton("Rate Now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            startActivity(myAppLinkToMarket);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getActivity(), " unable to find market app", Toast.LENGTH_LONG).show();
                        }
                    }
                }).show();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_main_new, container, false);
        ButterKnife.bind(this, view);



        progressBar.setIndeterminate(true);
        walletId.setText(SecureStorage.retrieve(Helper.TERMINAL_ID, ""));
        walletBalanceText.setText(Helper.getPreference(getContext(), Helper.BALANCE, ""));
        walletBalanceText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateScreens();
            }
        });


        recyclerView = view.findViewById(android.R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        historyAdapter = new HistoryAdapter_D(getActivity(), historyList);
        recyclerView.setAdapter(historyAdapter);

        swipeRefreshLayout = view.findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(this);



        errorLayout = view.findViewById(R.id.error_layout);
        emptyLayout = view.findViewById(R.id.empty_layout);



        String historySerial = Helper.getPreference(getActivity(), Helper.HISTORY_SERIAL, "");

        Log.i("Info", "onCreateView: " + historySerial );

//        if (!historySerial.trim().isEmpty()) {
//            historyList.addAll(HistoryAdapter.processRecords(historySerial));
//            historyAdapter.notifyDataSetChanged();
//        }

        populateScreens();

        return view;
    }

    public void onStart() {
        super.onStart();

        if (Helper.getPreference(getContext(), Helper.DOWNLOAD_BALANCE, false)) {
            populateScreens();
        }

    }

    @Override
    public void onRefresh() {
        populateScreens();
    }

    void getBalance() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.VISIBLE);
                        }
                    });
                    String url = getString(R.string.tams_url);


                    String action = getString(R.string.tams_webapi_action);
                    String control = "BALANCE";
                    Map<String, String> params = new HashMap<>();
                    params.put("action", action);
                    params.put("userid", userId);
                    params.put("termid", terminalId);
                    params.put("control", control);

                    VasResult tamsResult = Requests.processRequest(url, params);

                    if (tamsResult.result != VasResult.Result.APPROVED)
                        throw new IllegalStateException(tamsResult.message);


                    final String balance = "\u20A6 " + tamsResult.balance.replace("N", "").trim();

                    Helper.savePreference(getContext(), Helper.BALANCE, balance);
                    Helper.savePreference(getActivity(), Helper.DOWNLOAD_BALANCE, false);
                    Helper.savePreference(getActivity(), Helper.COMMISSION_BALANCE, tamsResult.balance);

                    Helper.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (walletBalanceText != null)
                                walletBalanceText.setText(balance);
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();

                    Helper.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (walletBalanceText != null && walletBalanceText.isShown()) {
                                try {
                                    Snackbar snackbar = Snackbar.make(walletBalanceText,
                                            "Could not retrieve balance", Snackbar.LENGTH_LONG);
                                    snackbar.setAction("Refresh", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            try {
                                                populateScreens();
                                            } catch (Exception e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    });
                                    snackbar.show();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    });
                } finally {
                    Helper.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                }
            }
        }).start();

    }

    void populateScreens() {
        getBalance();
        downloadRecords();
        //getName();

    }


    public void onButtonPressed(int action) {
        if (mListener != null) {
            mListener.onFragmentInteraction(action);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(int action);
    }


}

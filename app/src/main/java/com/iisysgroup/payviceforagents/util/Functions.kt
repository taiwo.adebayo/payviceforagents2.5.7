package com.iisysgroup.payviceforagents.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.provider.ContactsContract
import com.iisysgroup.payviceforagents.App
import com.iisysgroup.payviceforagents.activities.*
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
//import com.iisysgroup.payviceforagents.persistence.AppDB
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payvicegamesdk.TamsResponse
import com.itex.richard.payviceconnect.model.SmileModel
import com.itex.richard.payviceconnect.wrapper.PayviceServices
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import java.text.DecimalFormat


inline fun <T> getSubscriber(crossinline action: (T) -> Unit): (T) -> Unit {
    return {
        action.invoke(it)
    }
}

inline fun <T, Throwable> getSubscriber(crossinline action: (T, Throwable) -> Unit): (T, Throwable) -> Unit {
    return { t, y ->
        action.invoke(t, y)
    }
}


val Context.payviceServices: PayviceServices
    get() = (this.applicationContext as App).getVasRequestHandler()!!

//val Context.db: AppDB
//    get() = (this.applicationContext as MyApplication).db


const val SMILE_APPROVED_STATUS = 1
fun SmileModel.SmileSuccessResponse.isApproved() = this.status == SMILE_APPROVED_STATUS

fun <T : Any> Single<T>.runSafelyOnBackground() =
        this.subscribeOn(io.reactivex.schedulers.Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

fun <T : Any> Observable<T>.runSafelyOnBackground() =
        this.subscribeOn(io.reactivex.schedulers.Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

fun <T : Any> Flowable<T>.runSafelyOnBackground() =
        this.subscribeOn(io.reactivex.schedulers.Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

fun DebitCardProcessor.processTransaction(amount: Int, card: Card?): Flowable<TamsResponse> {
    card?.let {
        this.payWithLinkedCard(amount, it)
    } ?: kotlin.run {
        this.payWithDebit(amount)
    }

    return this.getResponse()
}


fun Activity.showPinEntry() {
    startActivityForResult(Intent(this, UserPinEntryActivity::class.java),
            UserPinEntryActivity.PIN_REQUEST_CODE)
}

fun Activity.showPaymentOption(mode: PaymentOption.Mode, amount: Int, beneficiary: String) {
    val paymentOptionIntent = Intent(this, PaymentOptionActivity::class.java).apply {
        putExtra(PaymentOptionActivity.EXTRA_MODE, mode)
        putExtra(PaymentOptionActivity.EXTRA_AMOUNT, amount)
        putExtra(PaymentOptionActivity.EXTRA_BENEFICIARY, beneficiary)
    }

    startActivityForResult(paymentOptionIntent, PaymentOptionActivity.REQUEST_CODE)
}

fun Activity.showError(throwable: Throwable?) {
    throwable?.let {
        val message = if (it.localizedMessage.isNullOrEmpty()) {
            it.cause?.localizedMessage ?: "Error occurred. Please try again"
        } else {
            it.localizedMessage
        }
        Helper.showInfoDialog(this, "Error", message)
    }
}


fun Activity.showResultScreen(result: ServiceResult) {
    finish()
    startActivity(Intent(this, ResultVasActivity::class.java).apply {
        putExtra(ResultVasActivity.EXTRA_RESULT, result)
    })
}

const val GET_CONTACT_REQUEST_CODE = 100
fun Activity.getPhoneContacts() {
    val intent = Intent(Intent.ACTION_PICK).apply {
        type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
    }

    startActivityForResult(Intent.createChooser(intent, "Select Contact"), GET_CONTACT_REQUEST_CODE)
}

fun String.normalizePhoneNumber(): String {
    if (this.length > 4 && this.startsWith("+")) {
        val ext = this.substring(0, 4)
        val inNo = this.replace(ext, "0").replace(" ", "")

        return inNo
    }

    return this
}

fun Int.toCurrencyString() = DecimalFormat.getInstance().format(this)

fun String.amountToSafeInt() = toDouble().toInt()


package com.iisysgroup.payviceforagents.external;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.cooltechworks.creditcarddesign.CreditCardUtils;
import com.iisysgroup.payviceforagents.R;


public class CardNumberFragment extends CreditCardFragment {
    EditText mCardNumberView;

    public CardNumberFragment() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle state) {
        View v = inflater.inflate(R.layout.lyt_card_number, group, false);
        this.mCardNumberView = (EditText) v.findViewById(R.id.card_number_field);
        mCardNumberView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(23)});
        this.mCardNumberView.addTextChangedListener(this);
        String number = "";
        if (this.getArguments() != null && this.getArguments().containsKey("card_number")) {
            number = this.getArguments().getString("card_number");
        }

        if (number == null) {
            number = "";
        }

        this.mCardNumberView.setText(number);
        return v;
    }

    public void afterTextChanged(Editable s) {
        int cursorPosition = this.mCardNumberView.getSelectionEnd();
        int previousLength = this.mCardNumberView.getText().length();
        String cardNumber = CreditCardUtils.handleCardNumber(s.toString());
        int modifiedLength = cardNumber.length();
        this.mCardNumberView.removeTextChangedListener(this);
        this.mCardNumberView.setText(cardNumber);

        if (cardNumber.length() > 5 && cardNumber.substring(0, 5).trim().equals("5061")) {
            System.out.println(true);
            this.mCardNumberView.setSelection(cardNumber.length() > 23 ? 23 : cardNumber.length());
        } else
            this.mCardNumberView.setSelection(cardNumber.length() > 19 ? 19 : cardNumber.length());

        this.mCardNumberView.addTextChangedListener(this);
        if (modifiedLength <= previousLength && cursorPosition < modifiedLength) {
            this.mCardNumberView.setSelection(cursorPosition);
        }

        this.onEdit(cardNumber);
        if (cardNumber.length() > 5 && cardNumber.substring(0, 5).trim().equals("5061")) {
            System.out.println(true);
            if (cardNumber.replace(" ", "").length() == 19) {
                this.onComplete();
            }
        } else {
            if (cardNumber.replace(" ", "").length() == 16) {
                this.onComplete();
            }
        }


    }

    public void focus() {
        if (this.isAdded()) {
            this.mCardNumberView.selectAll();
        }

    }
}
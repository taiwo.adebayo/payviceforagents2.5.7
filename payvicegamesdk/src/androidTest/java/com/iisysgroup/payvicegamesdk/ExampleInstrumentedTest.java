package com.iisysgroup.payvicegamesdk;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.iisysgroup.payvicegamesdk.test", appContext.getPackageName());
    }

//    @Test
//    public void testValidUserVerification(){
//        String email =  "bimetalic001@gmail.com";
//        PaymentProcessor paymentProcessor = new PaymentProcessor();
//        TamsResponse response = paymentProcessor.verifyEmail(email, Constants.defaultTerminalID);
//        System.out.println(response);
//        assertEquals(TamsResponse.Status.APPROVED, response.getResult());
//    }
//
//    @Test
//    public void testInvalidValidUserVerification(){
//        String email =  "d@gmail.com";
//        PaymentProcessor paymentProcessor = new PaymentProcessor();
//        TamsResponse response = paymentProcessor.verifyEmail(email, Constants.defaultTerminalID);
//        assertEquals(response.getResult(), TamsResponse.Status.DECLINED);
//    }


//    @Test
//    public void testVerifyWrongPin(){
//        String email =  "danbamitale@gmail.com";
//        PaymentProcessor paymentProcessor = new PaymentProcessor();
//        boolean response = paymentProcessor.verifyUserPin(email, "12204418", "1234");
//        assertEquals(response, false);
//    }
//
//    @Test
//    public void testVerifyCorrectPin(){
//        String email =  "bimetalic001@gmail.com";
//        PaymentProcessor paymentProcessor = new PaymentProcessor();
//        boolean response = paymentProcessor.verifyUserPin(email, "89364449", "ba1fe5823eca2e00762c20f11fc5656f9fd4462db0b89b52ed1604dc62c25de2");
//        assertEquals(response, true);
//    }
}

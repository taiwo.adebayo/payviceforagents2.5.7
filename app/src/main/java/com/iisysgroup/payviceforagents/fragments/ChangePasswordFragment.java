package com.iisysgroup.payviceforagents.fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends DialogFragment {

    TextInputLayout passwordTIL, newPasswordTIL, confirmPasswordTIL, pinTIL;
    EditText passwordEdit, newPasswordEdit, confirmPasswordEdit;
    EditText pinEdit;

    ProgressDialog progressDialog;
    Handler handler = new Handler();

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_change_password, container, false);
        getActivity().setTitle("Change Password");

        Helper.decryptUserKey(SecureStorage.retrieve(Helper.TERMINAL_ID, ""),
                SecureStorage.retrieve(Helper.USER_KEY, ""));

        passwordEdit = v.findViewById(R.id.passwordEdit);
        newPasswordEdit = v.findViewById(R.id.newPasswordEdit);
        confirmPasswordEdit = v.findViewById(R.id.confirmPasswordEdit);

        passwordTIL = v.findViewById(R.id.passwordTIL);
        newPasswordTIL = v.findViewById(R.id.newPasswordTIL);
        confirmPasswordTIL = v.findViewById(R.id.confirmPasswordTIL);

        pinEdit = v.findViewById(R.id.pinEdit);
        pinTIL = v.findViewById(R.id.pinTIL);

        v.findViewById(R.id.update_password_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Helper.hasInternetConnectivity(getActivity()))
                            doPasswordUpdate();
                    }
                });

        return v;
    }

    boolean isValidated() {
        if (passwordEdit.getText().toString().isEmpty()) {
            passwordTIL.setErrorEnabled(true);
            passwordTIL.setError(getString(R.string.enter_current_password));
            Helper.showErrorAnim(passwordEdit);
            passwordEdit.requestFocus();
            return false;
        }

        if (newPasswordEdit.getText().toString().isEmpty()) {
            newPasswordTIL.setErrorEnabled(true);
            newPasswordTIL.setError(getString(R.string.enter_new_password));
            Helper.showErrorAnim(newPasswordEdit);
            newPasswordEdit.requestFocus();
            return false;
        }

        if (newPasswordEdit.getText().length() < 8) {
            newPasswordTIL.setErrorEnabled(true);
            newPasswordTIL.setError("password should not be less than 8 characters");
            Helper.showErrorAnim(newPasswordEdit);
            newPasswordEdit.requestFocus();
            return false;
        }


        if (!newPasswordEdit.getText().toString().equals(confirmPasswordEdit.getText().toString())) {
            confirmPasswordTIL.setErrorEnabled(true);
            confirmPasswordTIL.setError("password doesn't match");
            Helper.showErrorAnim(confirmPasswordEdit);
            confirmPasswordEdit.requestFocus();
            return false;
        }

        if (pinEdit.getText().toString().length() != 4) {
            pinTIL.setErrorEnabled(true);
            pinTIL.setError("Enter your pin");
            Helper.showErrorAnim(pinEdit);
            pinEdit.requestFocus();
            return false;
        }

        return true;
    }

    void doPasswordUpdate() {
        if (isValidated()) {
            progressDialog = ProgressDialog.show(getActivity(), "Updating Password", "Please wait...", true, false);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        updatePassword();
                    } catch (IllegalAccessException e) {
                        Helper.showInfoDialog(getActivity(), "Update Failed",
                                e.getMessage());
                        e.printStackTrace();
                    } catch (SocketTimeoutException e) {
                        Helper.showInfoDialog(getActivity(), "Update Failed",
                                "Connection timed out");
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Helper.showInfoDialog(getActivity(), "Update Failed",
                                "Please check your details and try again.");

                    } finally {
                        dismissProgressDialog();
                    }
                }
            }).start();
        }
    }

    void updatePassword() throws Exception {

        final String url = getResources().getString(R.string.tams_url),
                PIN_ACTION = "TAMS_PIN_UPDATE";

        String userId = SecureStorage.retrieve(Helper.USER_ID, "");
        String terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID, "");

        String password = passwordEdit.getText().toString();
        String newPassword = newPasswordEdit.getText().toString();

        /*
            The user pin is linked to the old password, so to change password, you must also change
            the pin.
         */

        String oldEPassword = Helper.preparePassword(password);
        String oldPin = Helper.preparePin(pinEdit.getText().toString());

        String ePassword = Helper.preparePassword(newPassword);

        if (!Requests.verifyFundingPin(getActivity(), userId, oldPin, terminalID)) {
            throw new IllegalAccessException("Incorrect Pin");
        }

        Map<String, String> params = new HashMap<>();

        params.put("action", PIN_ACTION);
        params.put("userid", userId);
        params.put("termid", terminalID);
        params.put("password", oldEPassword);
        params.put("newpassword", ePassword);

        VasResult tamsResult = Requests.processRequest(url, params);

        if (tamsResult.result == VasResult.Result.APPROVED) {
            //Password change is approved, do pin change too
            SecureStorage.store(Helper.STORED_PASSWORD, ePassword);


            params.put("action", PIN_ACTION);
            params.put("userid", userId);
            params.put("termid", terminalID);
            params.put("control", "PIN_RESET");
            params.put("pin", Helper.preparePin(pinEdit.getText().toString()));

            tamsResult = Requests.processRequest(url, params);
            if (tamsResult.result != VasResult.Result.APPROVED) {
                throw new IllegalAccessException(tamsResult.message);
            }

            Helper.showInfoDialogWithAction(getActivity(),
                    "Update Successful", "Password changed", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });
        } else
            throw new IllegalAccessException(tamsResult.message);

    }

    void dismissProgressDialog() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        });
    }

}

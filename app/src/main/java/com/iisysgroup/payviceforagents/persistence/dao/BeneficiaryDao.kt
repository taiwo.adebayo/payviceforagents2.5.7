//package com.iisysgroup.payviceforagents.persistence.dao
//
//import android.arch.persistence.room.*
//import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary
//import io.reactivex.Flowable
//
//
//@Dao
//interface BeneficiaryDao {
//
//    @Insert(onConflict = OnConflictStrategy.IGNORE)
//    fun insert(vararg beneficiary: Beneficiary)
//
//    @Delete
//    fun delete(beneficiary: Beneficiary)
//
//
//    @Query("SELECT * FROM beneficiary")
//    fun getAll(): Flowable<List<Beneficiary>>
//
//
//    @Query("SELECT * FROM beneficiary WHERE assigned_product LIKE :product")
//    fun getAll(product: String): Flowable<List<Beneficiary>>
//}
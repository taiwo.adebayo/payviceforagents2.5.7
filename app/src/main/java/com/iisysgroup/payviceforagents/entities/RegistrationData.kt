package com.iisysgroup.payviceforagents.entities

data class RegistrationData(val name: String,
                            val email: String,
                            val phone: String,
                            val referralCode: String = "",
                            val password: String,
                            val pin: String) {

    val userID: String
        get() = if (phone.isNotBlank()) phone else email
}
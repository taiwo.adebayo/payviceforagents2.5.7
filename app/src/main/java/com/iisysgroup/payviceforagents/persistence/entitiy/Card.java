package com.iisysgroup.payviceforagents.persistence.entitiy;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.iisysgroup.payviceforagents.external.CardUtils;


import java.io.Serializable;

/**
 * Created by Bamitale @Itex on 2/12/2016.
 */

@Entity
public class Card implements Serializable {

    @ColumnInfo(name = "card_holder")
    public String cardHolder = "";

    @PrimaryKey
    @ColumnInfo(name = "card_pan")
    public String PAN;

    @ColumnInfo(name = "card_expiry_month")
    public String expiryMonth;

    @ColumnInfo(name = "card_expiry_year")
    public String expiryYear;

    @ColumnInfo(name = "card_verification_code")
    public String CVV;

    @Ignore
    public String pin;

    public static String normalizeCardNumber(String number) {
        return number == null ? null : number.trim().replaceAll("\\s+|-", "");
    }

    public static boolean isValidLuhnNumber(String number) {
        boolean isOdd = true;
        int sum = 0;

        for (int index = number.length() - 1; index >= 0; --index) {
            char c = number.charAt(index);
            if (!Character.isDigit(c)) {
                return false;
            }

            int digitInteger = Integer.parseInt("" + c);
            isOdd = !isOdd;
            if (isOdd) {
                digitInteger *= 2;
            }

            if (digitInteger > 9) {
                digitInteger -= 9;
            }

            sum += digitInteger;
        }

        return sum % 10 == 0;
    }

    public String getMaskedPan() {
        if (PAN == null)
            return "";
        else
            return CardUtils.processPan(PAN);
    }

    @Override
    public String toString() {
        return cardHolder + " " + getMaskedPan() + " " + expiryMonth + " " + expiryYear + " " + CVV;
    }


}

package com.iisysgroup.payviceforagents.entities.results

import com.iisgroup.bluetoothprinterlib.Manaer.PrintfManager
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.StatusObject
import com.iisysgroup.payviceforagents.util.toCurrencyString
import com.itex.richard.payviceconnect.model.DataModel
import java.io.Serializable

class AirtimeDataResult(private val data: ServiceResult.Data,
                        private val plan: String,
                        private val dataResponse: DataModel.DataSubscriptionResponse) : ServiceResult, Serializable {

    override fun toStatusObject(): StatusObject {
        val statusObject = StatusObject(!dataResponse.error)
        statusObject.statusReason = dataResponse.message
        statusObject.statusService = data.beneficiary.assignedProduct
        return statusObject
    }

    override fun printResult(printer: PrintfManager) {
        printer.printTabSpace(7)
        printer.printText("Data Subscription")
        printer.printfWrap(2)

        val status = if (!dataResponse.error) {
            StatusObject.STATUS_APPROVED
        } else {
            StatusObject.STATUS_DECLINED
        }

        printer.printTabSpace(6)
        printer.printText(status.toUpperCase())
        printer.printfWrap()

        printer.printTwoColumn("Network", data.beneficiary.assignedProduct)
        printer.printTwoColumn("Phone Number", data.beneficiary.displayInfo)
        printer.printTwoColumn("Purchase Plan", plan)
        printer.printTwoColumn("Date", dataResponse.date)
        printer.printTwoColumn("Reference", dataResponse.ref)

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap()

        printer.printTabSpace(10)
        printer.printText("N ${data.amount.toCurrencyString()}")
        printer.printfWrap()

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap(2)

        printer.printText(dataResponse.message)
        printer.printfWrap()
    }

    override fun getPrinterLogo(): Int? {
        return null
    }
}
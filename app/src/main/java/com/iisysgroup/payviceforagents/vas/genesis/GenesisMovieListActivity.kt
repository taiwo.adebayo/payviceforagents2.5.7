package com.iisysgroup.payviceforagents.vas.genesis

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnCheckedChanged
import butterknife.OnClick
import com.google.gson.Gson
import com.iisysgroup.payviceforagents.GlideApp
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.activities.PaymentOptionActivity
import com.iisysgroup.payviceforagents.activities.UserPinEntryActivity
import com.iisysgroup.payviceforagents.vas.genesis.adapters.MovieListAdapter
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.*
import com.iisysgroup.payvicegamesdk.TamsResponse
import com.itex.richard.payviceconnect.model.Genesis
import com.itex.richard.payviceconnect.wrapper.PayviceServices
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_genesis_movile_list.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class GenesisMovieListActivity : AppCompatActivity()  {
    private lateinit var encryptedUserPin: String
    private lateinit var mov : Genesis.Movies

    // id of the selected cinema from cinema house list
    private lateinit var cinemaId : String;

    private var ticketType : String? = null

    private val mProgressDialog by lazy {
        indeterminateProgressDialog(message = "Please wait while we purchase your ticket.", title = "Processing") {
            setCancelable(false)
        }
    }

    private val mWaitDialog by lazy {
        indeterminateProgressDialog(message = "Please wait..", title = "Fetching Movies") {
            setCancelable(false)
        }
    }

    private val payviceServices by lazy {
        PayviceServices.getInstance(this)
    }

    private val terminalId by lazy {
        SecureStorage.retrieve(Helper.TERMINAL_ID, "")
    }

    private val userId by lazy {
        SecureStorage.retrieve(Helper.USER_ID, "")
    }

    @BindView(R.id.d_radio_regular)
    lateinit var rdRegular : RadioButton
    @BindView(R.id.d_radio_premium)
    lateinit var rdPremium : RadioButton
    @BindView(R.id.d_radio_combo)
    lateinit var rdCombo : RadioButton
    @BindView(R.id.d_radio_vip)
    lateinit var rdVip : RadioButton
    @BindView(R.id._3d_with_glasses_radio)
    lateinit var rd3DwithGlasses : RadioButton
    @BindView(R.id._3d_without_glasses_radio)
    lateinit var rd3DwithoutGlasses : RadioButton
    @BindView(R.id.d_btn_buy)
    lateinit var btnBuy : Button
    lateinit var dialog : Dialog

    var amount : Double? = null

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            android.R.id.home -> {
                startActivity(Intent(this@GenesisMovieListActivity, GenesisMovieActivity::class.java))
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_genesis_movile_list)



        supportActionBar?.title = "Movies"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        cinemaId = intent.getStringExtra("cima");
        Log.d("genesis",cinemaId)

        mWaitDialog.show()
        payviceServices.GenesisGetMovies(Genesis.GetMoviesRequest(cinemaId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<Genesis.GenesisResponses>{
                    override fun onComplete() {
                        mWaitDialog.dismiss();
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: Genesis.GenesisResponses) {
                        setUpRecyclerView(t.movies!!)
                        mWaitDialog.dismiss()
                    }

                    override fun onError(e: Throwable) {
                        Log.d("genesis",e.message);
                        mWaitDialog.dismiss()
                        alert{
                            title = "Error"
                            message = "An error has occurred, please try again."
                            isCancelable =false

                            okButton {  finish()}
                        }.show()

//
                    }

                })
    }

    private fun setUpRecyclerView(movies : List<Genesis.Movies>){
        val adapter = MovieListAdapter(movies, this, object : MovieListAdapter.OnMovieSelectedListener{
            override fun startPayment(movie: Genesis.Movies) {
                mov = movie
                showMovie(movie)
            }

        })
        recyclerView.adapter = adapter
        val mLinearLayoutManagerVertical = GridLayoutManager(this,2) // (Context context, int spanCount)
        recyclerView.layoutManager = mLinearLayoutManagerVertical
        recyclerView.itemAnimator = DefaultItemAnimator()
    }

    private fun showMovie(movie : Genesis.Movies)
    {
        dialog = Dialog(this)
        dialog.setContentView(R.layout.movie_details)
        ButterKnife.bind(this, dialog);
        var otherMovies = dialog.findViewById<LinearLayout>(R.id.other_movies)
        var _3DMovies  = dialog.findViewById<LinearLayout>(R.id._3d_movies)
        var poster = dialog.findViewById<ImageView>(R.id.d_movieimg)
        var txtMovieName  = dialog.findViewById<TextView>(R.id.d_name)
        var txttime  = dialog.findViewById<TextView>(R.id.d_movieTime)
        var txtGenre  = dialog.findViewById<TextView>(R.id.d_genre)
        var txtDuration  = dialog.findViewById<TextView>(R.id.d_duration)
        var txtSynosis  = dialog.findViewById<TextView>(R.id.d_synopsis)
        var txtRegular  = dialog.findViewById<TextView>(R.id.d_regularPrice)
        var txtPremium  = dialog.findViewById<TextView>(R.id.d_premiumPrice)
        var txtCombo  = dialog.findViewById<TextView>(R.id.d_comboPrice)
        var txtVip  = dialog.findViewById<TextView>(R.id.d_vipPrice)

        var with3d = dialog.findViewById<TextView>(R.id._3d_with_glasses_price)
        var without3d = dialog.findViewById<TextView>(R.id._3d_without_glasses_price)


//        btnBuy.isEnabled = false

        GlideApp.with(this)
                .load(movie.poster)
                .placeholder(R.drawable.ic_genesis)
                .into(poster)

        txtMovieName.text = movie.title
        txttime.text = formatDate(movie.start_date + " " + movie.start_time)
        txtGenre.text = movie.genre
        txtDuration.text = movie.duration +"min"
        var synopsis = movie.synopsis;
        if(movie.synopsis.length > 110)
            synopsis = movie.synopsis.substring(0,110);
        txtSynosis.text = synopsis + "..."
        if( movie.amount.threeDWithGlasses== null || movie.amount.threeDWithoutGlasses == null ){

            otherMovies.visibility = View.VISIBLE
            _3DMovies.visibility = View.GONE
            txtRegular.text = setPrice(movie.amount.regular)
            txtPremium.text = setPrice(movie.amount.premium)
            txtCombo.text = setPrice(movie.amount.combo)
            txtVip.text = setPrice(movie.amount.vip)
        }else {
            otherMovies.visibility = View.GONE
            _3DMovies.visibility = View.VISIBLE

            with3d.text = setPrice(movie.amount.threeDWithGlasses)
            without3d.text = setPrice(movie.amount.threeDWithoutGlasses)

        }

        var btnClose = dialog.findViewById<Button>(R.id.d_btn_close)
        btnClose.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                dialog.dismiss()
            }
        })

        dialog.show()
    }


    @OnCheckedChanged(R.id.d_radio_regular)
    fun regularPriceChecked()
    {
        ticketType = "regular"
        Log.d("radio","here");
        if(rdRegular.isChecked)
        {
            amount = mov.amount.regular;
            rdPremium.setChecked(false)
            rdCombo.setChecked(false)
            rdVip.isChecked = false;
            title = "regular"
        }
    }

    @OnCheckedChanged(R.id.d_radio_premium)
    fun premiumPriceChecked()
    {
        if(rdPremium.isChecked)
        {
            ticketType = "premium"
            amount = mov.amount.premium;
            rdRegular.isChecked = false;
            rdCombo.isChecked = false;
            rdVip.isChecked = false;
            title = "premium"
        }
    }

    @OnCheckedChanged(R.id.d_radio_combo)
    fun comboPriceChecked()
    {
        if(rdCombo.isChecked)
        {
            ticketType = "combo"
            amount = mov.amount.combo
            rdRegular.isChecked = false;
            rdPremium.isChecked = false;
            rdVip.isChecked = false;
            title = "combo"
        }
    }

    @OnCheckedChanged(R.id.d_radio_vip)
    fun vipPriceChecked()
    {
        if(rdVip.isChecked)
        {
            ticketType = "vip"
            amount = mov.amount.vip
            rdRegular.isChecked = false
            rdPremium.isChecked = false
            rdCombo.isChecked = false
            title = "vip"
        }
    }

    @OnCheckedChanged(R.id._3d_with_glasses_radio)
    fun with3DGlassesPriceChecked()
    {
        if(rd3DwithGlasses.isChecked)
        {
            amount = mov.amount.threeDWithGlasses
            rd3DwithoutGlasses.isChecked = false;
            ticketType = "threeDWithGlasses"

        }
    }

    @OnCheckedChanged(R.id._3d_without_glasses_radio)
    fun without3DGlassesPriceChecked()
    {
        if(rd3DwithoutGlasses.isChecked)
        {
            amount = mov.amount.threeDWithoutGlasses
            rd3DwithGlasses.isChecked = false;
            ticketType = "threeDWithoutGlasses"
        }
    }
    @OnClick(R.id.d_btn_buy)
    fun buyClicked()
    {
        Log.d("buybutton","clicked")
        if(enableBuyButton()) {
            showPinEntry()
            dialog.dismiss()
        }
        else
            toast("Price not set").show()
    }

    private fun enableBuyButton(): Boolean
    {
        return amount != null
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UserPinEntryActivity.PIN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                encryptedUserPin = data!!.getStringExtra(UserPinEntryActivity.PIN_RESPONSE_DATA)
                showPaymentOption(PaymentOption.Mode.PAY, amount!!.roundToInt(), "")
            }
        }

        if (requestCode == PaymentOptionActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val paymentOption = data?.getSerializableExtra(PaymentOptionActivity.RESULT_OPTION) as PaymentOption
                val card = data.getSerializableExtra(PaymentOptionActivity.RESULT_CARD) as? Card
               // continuePayment(paymentOption, card)
            }
        }
    }

//    private fun continuePayment(paymentOption: PaymentOption, card: Card?) {
//        when (paymentOption) {
//            PaymentOption.WALLET -> {
//                mProgressDialog.show()
//                val clientReference = StringUtils.getClientRef(this@GenesisMovieListActivity, "")
//                Log.d("mov.movie_id",mov.movie_id)
//                Log.d("userId",userId)
//                Log.d("terminalId",terminalId)
//                Log.d("encryptedUserPin",encryptedUserPin)
//                Log.d("amount!!", amount!!.toString())
//                Log.d("ticketType",ticketType!!)
//                var gg = Genesis.BuyTicketRequest(mov.movie_id,userId,terminalId,encryptedUserPin, amount!!,ticketType!!);
//                Log.d("genesis", Gson().toJson(gg))
//                payviceServices.GenesisBuyTickets(Genesis.BuyTicketRequest(mov.movie_id,userId,terminalId,encryptedUserPin, amount!!,ticketType!!))
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribe(object : Observer<Genesis.GenesisResponses>{
//                            override fun onComplete() {
//
//                            }
//
//                            override fun onSubscribe(d: Disposable) {
//                            }
//
//                            override fun onNext(t: Genesis.GenesisResponses) {
//                               // mProgressDialog.dismiss()
////                                if (!t.error) {
////                                    val alertdialog = AlertDialog.Builder(baseContext)
////
////                                    alertdialog.setTitle("Response")
////                                            .setMessage("Your ticket was successfully bought. Here are the details. Keep the ticket ID very safe. \nTicket ID - ${t.ticket_id}\nScreen - ${t.screen}\nMovie Title - ${t.title}\nMovie title - ${mov.title}\nDate - ${mov.start_date}\nTime - ${mov.start_time}")
////                                            .show();
////                                } else {
////                                    val alertdialog = AlertDialog.Builder(baseContext)
////
////                                    alertdialog.setTitle("Response")
////                                            .setMessage(t.message!!)
////                                            .show();
////
////                                    Log.i("OkH", t.toString())
////                                }
//                            }
//                            override fun onError(e: Throwable) {
//                                mProgressDialog.dismiss()
//                                Log.d("genesis error",Gson().toJson(e.printStackTrace()));
//                                alert {
//                                    title = "Response"
//                                    message = "An error has occurred, please try again."
//                                }.show()
//                            }
//
//                        })
//            }
//            else -> {
//                val cardProcessor = DebitCardProcessor(this, encryptedUserPin)
//                cardProcessor.processTransaction(amount!!.roundToInt(), card).subscribe { result ->
//                    result?.let {
//                        if (it.result == TamsResponse.Status.APPROVED) {
//                            mProgressDialog.show()
//                            val clientReference = StringUtils.getClientRef(this@GenesisMovieListActivity, "")
//
//                            payviceServices.GenesisBuyTicketsCard(Genesis.BuyTicketRequest(mov.movie_id,userId,terminalId,encryptedUserPin, amount!!,ticketType!!))
//                                    .subscribeOn(Schedulers.io())
//                                    .observeOn(AndroidSchedulers.mainThread())
//                                    .subscribe(object : Observer<Genesis.GenesisResponses>{
//                                        override fun onComplete() {
//                                        }
//
//                                        override fun onSubscribe(d: Disposable) {
//                                        }
//
//                                        override fun onNext(t: Genesis.GenesisResponses) {
//                                            mProgressDialog.dismiss()
//                                            alert {
//                                                title = "Genesis Movie Ticket"
//                                                message = "Your ticket was successfully bought. Here are the details. Keep the ticket ID very safe. \nTicket ID - ${t.ticket_id}\nScreen - ${t.screen}\nMovie Title - ${t.title}\nDate - ${mov.start_date}\nTime - ${mov.start_time}"
//                                                okButton {
//
//                                                }
//                                            }.show()
//
//                                            Log.i("OkH", t.toString())
//
//                                        }
//
//                                        override fun onError(e: Throwable) {
//                                            mProgressDialog.dismiss()
//                                            e.printStackTrace()
//                                        }
//
//                                    })
//                        } else {
//                            alert{
//                                title = "Error"
//                                message = it.message
//                            }.show()
//                        }
//                    }
//                }
//            }
//
//        }
//    }

    private fun formatDate(date : String) : String
    {
        val d = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date)
        val cal = Calendar.getInstance()
        cal.setTime(d)
        return SimpleDateFormat("EEE, d MMM 'at' hh:mm aaa").format(cal.getTime())
    }

    private fun setPrice(amount : Double?) : String
    {
        var price = "₦ ";
        if (amount != null)
            price += amount.toString()
        else price = "Price not available"

        return price;
    }

}

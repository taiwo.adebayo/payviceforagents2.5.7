package com.iisysgroup.payviceforagents.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.fragments.LinkedCardsFragment;

public class LinkedCardActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linked_card);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fm = getSupportFragmentManager();
        loadFragment(new LinkedCardsFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if (fm.getBackStackEntryCount() > 1) {
                fm.popBackStack();
            } else
                NavUtils.navigateUpFromSameTask(this);
        }

        if (id == R.id.action_cancel) {
            finish();
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();
        } else
            NavUtils.navigateUpFromSameTask(this);

    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    public void enableFab(boolean show) {
//        fab.setEnabled(show);
//
//        if(show)
//            fab.setVisibility(View.VISIBLE);
//        else
//            fab.setVisibility(View.INVISIBLE);
    }
}

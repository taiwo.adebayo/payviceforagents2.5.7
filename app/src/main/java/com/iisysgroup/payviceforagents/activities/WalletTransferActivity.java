package com.iisysgroup.payviceforagents.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.fragments.AuthorizePinFragment;
import com.iisysgroup.payviceforagents.fragments.StatusFragment;
import com.iisysgroup.payviceforagents.fragments.WalletTransferActivityFragment;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary;
import com.iisysgroup.payviceforagents.util.EmailHandler;
import com.iisysgroup.payviceforagents.util.Helper;

public class WalletTransferActivity extends DefaultVasActivity
        implements WalletTransferActivityFragment.OnFragmentInteractionListener,
        AuthorizePinFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_transfer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadFragment(new WalletTransferActivityFragment());
    }


    void transferToOtherWallet() {

        new AlertDialog.Builder(this)
                .setTitle("Wallet Transfer")
                .setMessage("Proceed with transaction?")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doWalletTransfer();
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }


    void doWalletTransfer() {
        showProgressDialog("Wallet Transfer", "Processing.... Please wait", false);
        statusObject.setStatus(false);
        statusObject.setStatusAmount(Double.parseDouble(amount));

        statusObject.statusRecipient = beneficiary.getDisplayInfo();
        statusObject.statusService = "Wallet Transfer";

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    VasResult result = Requests.sendTransferRequest(context, beneficiary.getData(), amount, userPin);
                    String message = result.message, balance = result.balance;

                    statusObject.statusReason = message;
                    if (result.result == VasResult.Result.APPROVED) {
                        statusObject.setStatus(true);
                        EmailHandler.sendNotificationMail(WalletTransferActivity.this, statusObject);
                    }

                } catch (IllegalStateException e) {
                    statusObject.statusReason = e.getMessage();
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    statusObject.statusReason = "An error occurred. Please try again later";
                } finally {
                    dismissProgressDialog();
                    Helper.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadFragment(StatusFragment.getInstance(statusObject));
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public void onAuthorizePinFragmentInteraction(String pin) {
        this.userPin = Helper.preparePin(pin);

        if (Helper.hasInternetConnectivity(getApplicationContext()))
            transferToOtherWallet();
    }

    @Override
    public void onWalletTransferFragmentInteraction(Beneficiary beneficiary, String amount) {
        this.beneficiary = beneficiary;
        this.amount = amount;
        loadFragment(new AuthorizePinFragment());
    }
}

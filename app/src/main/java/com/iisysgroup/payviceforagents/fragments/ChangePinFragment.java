package com.iisysgroup.payviceforagents.fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePinFragment extends DialogFragment {

    @BindView(R.id.pinTIL)
    TextInputLayout pinTIL;
    @BindView(R.id.newPinTIL)
    TextInputLayout newPinTIL;
    @BindView(R.id.confirmPinTIL)
    TextInputLayout confirmPinTIL;
    @BindView(R.id.pinEdit)
    EditText pinEdit;
    @BindView(R.id.newPinEdit)
    EditText newPinEdit;
    @BindView(R.id.confirmPinEdit)
    EditText confirmPinEdit;
    ProgressDialog progressDialog;
    Handler handler = new Handler();

    public ChangePinFragment() {
        // Required empty public constructor
    }

    @OnClick(R.id.update_pin_button)
    void onPinUpdate() {
        if (Helper.hasInternetConnectivity(getActivity()))
            doPinUpdate();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_change_pin, container, false);
        ButterKnife.bind(this, v);

        getActivity().setTitle("Change Pin");


        return v;
    }

    boolean isValidated() {
        if (pinEdit.getText().toString().isEmpty()) {
            pinTIL.setErrorEnabled(true);
            pinTIL.setError("Input is required");
            pinEdit.requestFocus();
            Helper.showErrorAnim(pinEdit);
            return false;
        }

        if (newPinEdit.getText().toString().isEmpty()) {
            newPinTIL.setErrorEnabled(true);
            newPinTIL.setError("Input is required");
            newPinEdit.requestFocus();
            Helper.showErrorAnim(newPinEdit);
            return false;
        }

        if (newPinEdit.getText().length() < 4) {
            newPinTIL.setErrorEnabled(true);
            newPinTIL.setError("pin should not be less than 4 characters");
            newPinEdit.requestFocus();
            Helper.showErrorAnim(newPinEdit);
            return false;
        }

        if (!newPinEdit.getText().toString().equals(confirmPinEdit.getText().toString())) {
            confirmPinTIL.setErrorEnabled(true);
            confirmPinTIL.setError("pin doesn't match");
            Helper.showErrorAnim(confirmPinEdit);
            confirmPinEdit.requestFocus();
            return false;
        }

        return true;
    }

    void doPinUpdate() {
        if (isValidated()) {
            progressDialog = ProgressDialog.show(getActivity(), "Updating Pin", "Please wait...", true, false);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        updatePin();
                    } catch (IllegalAccessException e) {
                        Helper.showInfoDialog(getActivity(), "Update Failed",
                                e.getMessage());
                        e.printStackTrace();
                    } catch (SocketTimeoutException e) {
                        Helper.showInfoDialog(getActivity(), "Update Failed",
                                "Connection timed out");
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Helper.showInfoDialog(getActivity(), "Update Failed",
                                "Please check your details and try again.");

                    } finally {
                        dismissProgressDialog();
                    }
                }
            }).start();
        }
    }

    void updatePin() throws Exception {

        final String url = getResources().getString(R.string.tams_url),
                PIN_ACTION = "TAMS_PIN_UPDATE";

        String userId = SecureStorage.retrieve(Helper.USER_ID, "");
        String terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID, "");

        String pin = pinEdit.getText().toString();
        String newPin = newPinEdit.getText().toString();

        String oldEPin = Helper.preparePin(pin);

        String ePin = Helper.preparePin(newPin);

        Map<String, String> params = new HashMap<>();

        params.put("action", PIN_ACTION);
        params.put("userid", userId);
        params.put("termid", terminalID);
        params.put("pin", oldEPin);
        params.put("newpin", ePin);

        VasResult tamsResult = Requests.processRequest(url, params);

        if (tamsResult.result == VasResult.Result.APPROVED) {
            Helper.showInfoDialogWithAction(getActivity(),
                    "Update Successful", tamsResult.message, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });

        } else
            throw new IllegalAccessException(tamsResult.message);

    }

    void dismissProgressDialog() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        });
    }

}

package com.iisysgroup.payviceforagents.external;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */
public interface IActionListener {
    void onActionComplete(CreditCardFragment var1);

    void onEdit(CreditCardFragment var1, String var2);
}

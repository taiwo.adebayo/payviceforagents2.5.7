package com.iisysgroup.payviceforagents.activities

import android.app.Activity
import android.app.ProgressDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import bolts.AppLinks
import com.facebook.FacebookSdk
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.base.presenter.RegistrationPresenter
import com.iisysgroup.payviceforagents.baseimpl.interactor.RegistrationInteractorImpl
import com.iisysgroup.payviceforagents.baseimpl.presenter.RegistrationPresenterImpl
import com.iisysgroup.payviceforagents.baseimpl.viewmodel.RegistrationActivityViewModel
import com.iisysgroup.payviceforagents.entities.RegistrationData
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
import com.jakewharton.rxbinding2.widget.RxCompoundButton
import kotlinx.android.synthetic.main.activity_register.*
import java.util.logging.Logger

class RegisterActivity : AppCompatActivity() {

    internal var progressDialog: ProgressDialog? = null

    private lateinit var viewModel: RegistrationActivityViewModel
    private lateinit var presenter: RegistrationPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        viewModel = ViewModelProviders.of(this).get(RegistrationActivityViewModel::class.java)
        presenter = RegistrationPresenterImpl(RegistrationInteractorImpl(applicationContext), viewModel)



//        Dexter.withActivity(this).withPermission(android.Manifest.permission.READ_PHONE_STATE)
//                .withListener(object : PermissionListener {
//                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
//
//                    }
//
//                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
//                        Snackbar.make(view, "Permission to read phone state required",
//                                Snackbar.LENGTH_SHORT).setAction(android.R.string.ok) {
//                            val intent = Intent()
//                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
//                            val uri = Uri.fromParts("package", packageName, null)
//                            intent.data = uri
//                            startActivityForResult(intent, 100)
//                        }.show()
//                    }
//
//                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
//                        token.continuePermissionRequest()
//                    }
//                }).onSameThread().check()


        FacebookSdk.sdkInitialize(this)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val facebookDeepLinkData: Uri?

        if (intent != null) {
            facebookDeepLinkData = AppLinks.getTargetUrlFromInboundIntent(this, intent)
            if (facebookDeepLinkData != null) {
                Log.i(Helper.TAG, "App Link Target URL: $facebookDeepLinkData")
                val referralCode = facebookDeepLinkData.getQueryParameter("ref")

                presenter.setReferralCode(referralCode)
            } else if (intent.data != null) {
                val appLinkData = intent.data
                val referralCode = appLinkData.getQueryParameter("ref")

                presenter.setReferralCode(referralCode)
            }
        }

        RxCompoundButton.checkedChanges(referralCodeCheckBox).subscribe {
            if (it) {
                showReferrerDialog()
            } else {
                resetReferralCode()
            }
        }


        viewModel.onRegistrationError.observe(this, Observer { throwable ->
            var message = throwable!!.localizedMessage
            Helper.showInfoDialog(this@RegisterActivity, "Registration Failed",
                    message)
            Logger.getLogger(RegisterActivity::class.java.name).warning(message)
        })



        viewModel.onProgressUpdate.observe(this, Observer {
            it?.let {
                progressDialog?.dismiss()

                if (it) {
                    progressDialog = ProgressDialog.show(this,
                            "Registering", "Please wait...", true, false)
                }
            }
        })

        viewModel.onSetReferral.observe(this, Observer {
            it?.let {
                referralCodeCheckBox.text = it
                referralCodeCheckBox.isChecked = true
                SecureStorage.store(Helper.REFERRAL_CODE, it)
            }
        })

        viewModel.onValidationError.observe(this, Observer {
            it?.let {
                when (it) {
                    RegistrationActivityViewModel.ValidationType.USERNAME -> showError(usernameTIL, getString(R.string.enter_name_prompt))
                    RegistrationActivityViewModel.ValidationType.EMAIL -> showError(emailTIL, getString(R.string.enter_email_prompt))
                    RegistrationActivityViewModel.ValidationType.PHONE -> showError(phoneTIL, getString(R.string.enter_phone_prompt))
                    RegistrationActivityViewModel.ValidationType.PASSWORD -> showError(passwordTIL, getString(R.string.enter_password))
                    RegistrationActivityViewModel.ValidationType.PIN -> showError(pinTIL, getString(R.string.enter_pin_prompt))
                }
            }
        })

        viewModel.onRegistrationSuccessful.observe(this, Observer {
            it.let {
                finish()
                startActivity(Intent(this, VerificationActivity::class.java))
            }
        })


        registerButton.setOnClickListener {

            if (phoneEdit.text.length != 11){
                phoneEdit.error = "Please Confirm Phone Number to 11-digits"
                Helper.showErrorAnim(phoneEdit)
            }
            else {
                val data = RegistrationData(
                        name = usernameEdit.text.toString(),
                        email = emailEdit.text.toString(),
                        phone = phoneEdit.text.toString(),
                        password = passwordEdit.text.toString(),
                        pin = pinEdit.text.toString(),
                        referralCode = if (referralCodeCheckBox.isChecked) referralCodeCheckBox.text.toString() else "")

                presenter.validateData(data)
            }
        }

    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ReferrerActivity.REF_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val referralCode = data!!.getStringExtra(ReferrerActivity.RESULT)
                presenter.setReferralCode(referralCode)
            } else {
                resetReferralCode()
            }

        }
    }

    private fun showError(view: TextInputLayout, error: String = "Invalid input") {
        view.isErrorEnabled = true
        view.error = error
        view.requestFocus()
        Helper.showErrorAnim(view)
    }

    private fun resetReferralCode() {
        referralCodeCheckBox.isChecked = false
        SecureStorage.delete(Helper.REFERRAL_CODE)
        referralCodeCheckBox.setText(R.string.have_referral)
    }


    private fun showReferrerDialog() {
        startActivityForResult(Intent(this, ReferrerActivity::class.java), ReferrerActivity.REF_CODE)
    }

}

package com.iisysgroup.payviceforagents

import com.iisysgroup.payvicegamesdk.PaymentProcessor
import com.interswitchng.sdk.payment.IswCallback
import com.interswitchng.sdk.payment.model.PurchaseResponse

open class PayCallback (private val paymentProcessor: PaymentProcessor) : IswCallback<PurchaseResponse>() {

    override fun onError(e: Exception) {

        //todo handle this exception

        //paymentProcessor.setError(e)
    }

    override fun onSuccess(purchaseResponse: PurchaseResponse) {
        paymentProcessor.completeTransaction(purchaseResponse)
    }
}
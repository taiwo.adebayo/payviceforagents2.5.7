package com.iisysgroup.payviceforagents.util

data class DefaultUIModel(val transactionTitle : String, val amount : Long, val additionalAmount : Long = 0)
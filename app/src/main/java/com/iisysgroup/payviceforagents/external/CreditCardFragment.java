package com.iisysgroup.payviceforagents.external;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */

import android.support.v4.app.Fragment;
import android.text.TextWatcher;

public abstract class CreditCardFragment extends Fragment implements TextWatcher, IFocus {
    protected IActionListener mActionListener;

    public CreditCardFragment() {
    }

    public void setActionListener(IActionListener listener) {
        this.mActionListener = listener;
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    public void onEdit(String edit) {
        if (this.mActionListener != null) {
            this.mActionListener.onEdit(this, edit);
        }

    }

    public void onComplete() {
        if (this.mActionListener != null) {
            this.mActionListener.onActionComplete(this);
        }

    }
}

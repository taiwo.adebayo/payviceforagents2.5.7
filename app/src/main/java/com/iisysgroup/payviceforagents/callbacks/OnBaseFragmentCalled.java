package com.iisysgroup.payviceforagents.callbacks;

import com.iisysgroup.payviceforagents.util.HistoryAdapter_D;

/**
 * Created by Itex-PC on 09/11/2018.
 */

public interface OnBaseFragmentCalled{
    void allTransactionHistory(HistoryAdapter_D.History history);
}

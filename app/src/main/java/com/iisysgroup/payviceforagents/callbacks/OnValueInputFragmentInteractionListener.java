package com.iisysgroup.payviceforagents.callbacks;

import com.iisysgroup.payviceforagents.entities.Service;
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary;

/**
 * Created by Bamitale@Itex on 09/05/2017.
 */
public interface OnValueInputFragmentInteractionListener {
    void onValueInputFragmentInteraction(Service.Product product, String amount, Beneficiary beneficiary,String phone,String productCode, String meterNumber,String meterName);
}

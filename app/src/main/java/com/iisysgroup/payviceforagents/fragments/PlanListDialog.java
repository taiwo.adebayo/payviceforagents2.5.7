package com.iisysgroup.payviceforagents.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.base.interactor.MultichoiceInteractor;
import com.iisysgroup.payviceforagents.callbacks.SelectedItemCallback;
import com.iisysgroup.payviceforagents.entities.PlanValue;
import com.iisysgroup.payviceforagents.entities.Service;
import com.iisysgroup.payviceforagents.payviceservices.MultichoiceRequests;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.VasServices;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Bamitale@Itex on 08/05/2017.
 */

public class PlanListDialog extends BottomSheetDialog implements SwipeRefreshLayout.OnRefreshListener {

    View viewUnavailable;
    List<PlanValue> planValues = new ArrayList<>();
    Handler handler = new Handler();
    ArrayAdapter<PlanValue> listAdapter;
    Service.Type serviceType;
    Service.Product product;
    SelectedItemCallback<PlanValue> selectedItemCallback;
    private TextView titleText;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeLayout;
    private ListView listView;

    private String smartCardNumber = "";

    public PlanListDialog(@NonNull Context context) {
        super(context);
    }

    public PlanListDialog(@NonNull Context context, @StyleRes int theme) {
        super(context, theme);
    }

    protected PlanListDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }


    public PlanListDialog(Context context, Service.Type serviceType, Service.Product product, SelectedItemCallback<PlanValue> selectedItemCallback) {
        super(context);
        this.serviceType = serviceType;
        this.product = product;
        this.selectedItemCallback = selectedItemCallback;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_sheet_list_layout_2);

        titleText = findViewById(R.id.titleText);
        String title = "";
        switch (serviceType) {
            case PLAN:
            case PLAN_VALUE:
                title = "Select Plan";
                break;
            case PIN:
            case PIN_VALUE:
                title = "Select Pin";
                break;
        }
        titleText.setText(title);
        progressBar = findViewById(R.id.progressBar);
        swipeLayout = findViewById(R.id.swipe_layout);
        listView = findViewById(android.R.id.list);
        viewUnavailable = findViewById(R.id.view_unavailable);

        listAdapter = new ArrayAdapter<>(getContext(),
                R.layout.network_option_list_item, R.id.primaryTitleText, planValues);

        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PlanValue planValue = planValues.get(position);
                selectedItemCallback.onItemSelected(planValue);
                dismiss();
            }
        });

        onRefresh();
    }


    void downloadProxy() {
        final List<PlanValue> tempList;
        switch (product.requestCode) {
            case VasServices.DSTV:
                tempList = getMultichoicePlans(MultichoiceInteractor.MultichoiceProduct.DSTV, smartCardNumber);
                break;
            case VasServices.GOTV:
                tempList = getMultichoicePlans(MultichoiceInteractor.MultichoiceProduct.GOTV, smartCardNumber);
                break;
            default:
                tempList = Requests.getTamsPlans(getContext(), product.proxyCode);
        }

        Helper.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!tempList.isEmpty()) {
                    planValues.clear();
                    planValues.addAll(tempList);
                    listAdapter.notifyDataSetChanged();
                } else {
                    onError();
                }
                Helper.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeLayout.setRefreshing(false);
                    }
                });
            }
        });

    }

    List<PlanValue> getMultichoicePlans(MultichoiceInteractor.MultichoiceProduct service, String smartCardNo) {
        try {
            return MultichoiceRequests.getAvailableMultichoicePlans(service, smartCardNo);
        } catch (Exception e1) {
            e1.printStackTrace();
            return new ArrayList<>();
        }
    }


    void onError() {
        Helper.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (listView != null)
                    listView.setVisibility(View.INVISIBLE);
                if (viewUnavailable != null)
                    viewUnavailable.setVisibility(View.VISIBLE);
                if (swipeLayout != null)
                    swipeLayout.setRefreshing(false);
            }
        });
    }


    @Override
    public void onRefresh() {
        if (!swipeLayout.isRefreshing()) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    swipeLayout.setRefreshing(true);
                    listView.setVisibility(View.VISIBLE);
                    viewUnavailable.setVisibility(View.INVISIBLE);
                }
            });
        }

        Helper.ThreadService.execute(new Runnable() {
            @Override
            public void run() {
                downloadProxy();
            }
        });
    }

    public void setSmartCardNumber(String smartCardNumber) {
        this.smartCardNumber = smartCardNumber;
    }
}

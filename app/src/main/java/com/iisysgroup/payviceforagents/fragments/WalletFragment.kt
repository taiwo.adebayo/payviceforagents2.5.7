package com.iisysgroup.payviceforagents.fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.iisysgroup.payviceforagents.PinAlertUtils
import com.iisysgroup.payviceforagents.activities.LinkedCardActivity
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.activities.WalletTransferActivity
import com.iisysgroup.payviceforagents.balance.service.BalanceService
import com.iisysgroup.payviceforagents.fundwallet.FundWalletActivity
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.securestorage.SecureStorageUtils
import com.iisysgroup.payviceforagents.util.Helper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.Appcompat
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.indeterminateProgressDialog

/**
 * Created by Bamitale@Itex on 8/3/2016.
 */

class WalletFragment : BaseBalanceFragment(), View.OnClickListener {

    private val mProgressDialog by lazy {
        indeterminateProgressDialog(message = "Processing your request")
    }

    private lateinit var customAlert : DialogInterface

    private val walletId by lazy {
        SecureStorage.retrieve(Helper.TERMINAL_ID, "")
    }

    private val password by lazy {
        SecureStorage.retrieve(Helper.STORED_PASSWORD, "")
    }

    private val userId by lazy {
        SecureStorage.retrieve( Helper.USER_ID, "")
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.content_wallet_new, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<View>(R.id.walletTranferBtn).setOnClickListener(this)
        view.findViewById<View>(R.id.linkedCardBtn).setOnClickListener(this)
        //view.findViewById<View>(R.id.addBank).setOnClickListener(this)
        view.findViewById<View>(R.id.commissionTranferBtn).setOnClickListener(this)
        view.findViewById<View>(R.id.gtbCard).setOnClickListener(this)
        view.findViewById<View>(R.id.ubaCard).setOnClickListener(this)


        walletBalanceText!!.text = Helper.getPreference(activity, Helper.BALANCE, "")
    }


    override fun onResume() {
        super.onResume()
    }

    override fun onClick(v: View) {
        var aClass: Class<*>? = null

        when (v.id) {
            R.id.walletTranferBtn -> aClass = WalletTransferActivity::class.java
            R.id.linkedCardBtn -> aClass = LinkedCardActivity::class.java
            //R.id.addBank -> aClass = AddAccount::class.java
//            R.id.commissionTranferBtn -> commissionTransfer()
            R.id.fundButton -> FundWalletActivity::class.java
            R.id.gtbCard -> showGTBDialog()
            R.id.ubaCard -> showUBADialog()
        }
        if (aClass != null)
            startActivity(Intent(context, aClass))

    }

    private fun showGTBDialog(){

        val dialogBuilder = AlertDialog.Builder(context)
        dialogBuilder.setMessage(Html.fromHtml("Dial *737#<br><br>" +
                "E.g *737# and Follow the Prompt <br><br>Enter <b>7</b> to Pay Bills <br><br> Enter <b>6</b> to Search Biller <br><br>Enter <b>itex</b> to enter biller <br><br>Enter <b>1</b> for confirm ITEX INTEGRATED SERVICES <br><br>Enter Amount <br><br>Enter Wallet ID<br><br>Enter USSD PIN or GTB Token<br><br>Your Wallet is Credited"))
                // if the dialog is cancelable
                .setCancelable(true)
                // positive button text and action
                .setPositiveButton(Html.fromHtml("<b>Proceed To Fund</b>"), DialogInterface.OnClickListener {
                    dialog, id -> val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse(String.format("tel:%s", Uri.encode("*737#")))
                    startActivity(intent)
                })

        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle("Fund Using GTB USSD?")
        // show alert dialog
        alert.show()

    }

    private fun showUBADialog(){
         alert {
                title = Html.fromHtml("Fund Using UBA Deposit?")
                message = Html.fromHtml("Deposit any amount of your choice with your Wallet ID as account name to Bank Number 1020919606<br><br>E.g Depositor Name : 99118899<br><br>Amount : 1000<br><br>Recipient : 1020919606")

        }.show()
    }

//    private fun commissionTransfer() {
//       customAlert = alert {
//            customView {
//                verticalLayout {
//                    padding = dip(18)
//
//                    textView {
//                        text = "How much would you want to transfer to main wallet?"
//                        textSize = 16f
//                    }
//
//                    val amount = editText {
//                        id = R.id.custom
//                        inputType = InputType.TYPE_CLASS_NUMBER
//                    }
//
//                    button("Submit"){
//
//
//                    }.setOnClickListener {
//
//                            //* 100 because server side requires amount in Kobo
//                            val amountEntered = amount.text.toString().toInt() * 100
//
//                            val view = LayoutInflater.from(activity).inflate(R.layout.activity_enter_pin, null)
//                            PinAlertUtils.getPin(activity, view){
//                                val pin = SecureStorageUtils.hashIt(it, password)!!
//
//                                customAlert.dismiss()
//
//                                mProgressDialog.show()
//
//
//                                GlobalScope.launch(Dispatchers.Default) {
//                                    val response = BalanceService.getService().transferCommissionToBalance(userid = userId, pin = pin, termid = walletId, amountInKobo = amountEntered.toString()).await()
//
//                                    if (!response.balance.isNullOrBlank()) {
//
//                                        Helper.savePreference(activity, Helper.BALANCE, "")
//                                        Helper.savePreference(activity, Helper.COMMISSION_BALANCE, "")
//                                    }
//
//                                    GlobalScope.launch(Dispatchers.Main) {
//                                        activity?.alert(Appcompat) {
//                                            title = "Transfer Response"
//                                            message = response.message
//                                            okButton { }
//                                        }?.show()
//
//                                        mProgressDialog.dismiss()
//
//                                    }
//
//
//                                }
//                            }
//
//                    }
//                }
//            }
//        }.show()
//    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onPause() {
        super.onPause()
        mProgressDialog.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        mProgressDialog.dismiss()
    }

}

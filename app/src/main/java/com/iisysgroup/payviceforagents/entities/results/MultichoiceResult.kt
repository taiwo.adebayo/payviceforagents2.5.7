package com.iisysgroup.payviceforagents.entities.results

import com.iisgroup.bluetoothprinterlib.Manaer.PrintfManager
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.base.interactor.MultichoiceInteractor
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.StatusObject
import com.iisysgroup.payviceforagents.util.toCurrencyString
import com.itex.richard.payviceconnect.model.DstvModel

class MultichoiceResult(val data: ServiceResult.Data,
                        val plan: String,
                        val response: DstvModel.PayResponse) : ServiceResult {

    override fun toStatusObject(): StatusObject {
        val statusObject = StatusObject(!response.error)
        statusObject.statusReason = response.message
        return statusObject
    }


    override fun printResult(printer: PrintfManager) {
        printer.printTabSpace(7)
        printer.printText("${data.beneficiary.assignedProduct} Subscription")
        printer.printfWrap(2)

        val status = if (!response.error) {
            StatusObject.STATUS_APPROVED
        } else {
            StatusObject.STATUS_DECLINED
        }

        printer.printTabSpace(6)
        printer.printText(status.toUpperCase())
        printer.printfWrap()

        printer.printTwoColumn("Customer Name", data.beneficiary.displayInfo)
        printer.printTwoColumn("IUC", data.beneficiary.data)
        printer.printTwoColumn("Purchased Plan", plan)
        printer.printTwoColumn("Date", response.date)
        printer.printTwoColumn("Reference", response.ref)

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap()

        printer.printTabSpace(10)
        printer.printText("N ${data.amount.toCurrencyString()}")
        printer.printfWrap()

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap(2)

        printer.printText(response.message)
        printer.printfWrap()
    }

    override fun getPrinterLogo(): Int? {

        return when (data.beneficiary.assignedProduct) {
            MultichoiceInteractor.MultichoiceProduct.DSTV.name -> R.drawable.dstv
            MultichoiceInteractor.MultichoiceProduct.GOTV.name -> R.drawable.gotv
            else -> null
        }
    }
}
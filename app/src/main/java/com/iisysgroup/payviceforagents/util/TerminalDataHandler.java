package com.iisysgroup.payviceforagents.util;

import android.app.Activity;
import android.util.Log;

import com.iisysgroup.newland.NewlandDeviceManager;
import com.newland.mtype.DeviceInfo;
import com.newland.mtype.module.common.emv.AIDConfig;
import com.newland.mtype.module.common.emv.EmvModule;
import com.newland.mtype.module.common.emv.TerminalConfig;
import com.newland.mtype.module.common.pin.KSNKeyType;
import com.newland.mtype.module.common.pin.KSNLoadResult;
import com.newland.mtype.module.common.pin.KSNLoadResultCode;
import com.newland.mtype.module.common.pin.LoadPKResultCode;
import com.newland.mtype.module.common.pin.LoadPKType;
import com.newland.mtype.module.common.pin.PinInput;
import com.newland.mtype.util.ISOUtils;

/**
 * Created by Bamitale @Itex on 10/16/2015.
 */
public class TerminalDataHandler {

    static NewlandDeviceManager deviceManager = NewlandDeviceManager.getInstance();

    static EmvModule emvModule;
    static PinInput pinInputModule;

   static final String NIGERIA_CURRENCY_CODE = "566";
   static final String KENYA_CURRENCY_CODE = "404";
   static public String currencyCode = NIGERIA_CURRENCY_CODE;

    public static void loadDukptKey(Activity activity) {

        String IPEK = "A24DDB8EF77BB5E0C9E0A6266E561212"; //ABU
//        IPEK = "090733CCFA6C47AA779862AF5443D905";  //XETI

        String KSN = "FFFF0248392015E00000";

        pinInputModule = deviceManager.getPinInputModule();

        byte[] checkSum = ISOUtils.hex2byte("E1F3406AEF7E1131"); //ABU
//        checkSum = ISOUtils.hex2byte("22E396149915D735"); //XETI


        KSNLoadResult ksnLoadResult = pinInputModule.ksnLoad(KSNKeyType.TRANSFERKEY_TYPE, 1,
                ISOUtils.hex2byte(KSN), ISOUtils.hex2byte(IPEK), 1, checkSum);


        if (ksnLoadResult.getResultCode() == KSNLoadResultCode.SUCCESS) {
         //   //UIHelper.notifyByToast(activity, "KSN and IPEK loaded successfully");
           // //UIHelper.notifyByToast(activity, "CheckVaule " + ISOUtils.hexString(ksnLoadResult.getCheckValue()));

        } else {
           // //UIHelper.notifyByNoTitleDialog(activity, "KSN loading failed with error " + ksnLoadResult.getResultCode());
        }

    }

    public static void loadPublicKey(final Activity activity) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                String message = "Public key injection ";
                int i = 1;
                for (CapkKey capkKey : CapkKeys.capkKeys) {
                    LoadPKResultCode loadResult = injectCAPKKey(capkKey, i);

                    if (loadResult == LoadPKResultCode.SUCCESS)
                        message = "Public key injection was successful";
                    else if (loadResult == LoadPKResultCode.FAILED)
                        message += "failed";
                    else if (loadResult == LoadPKResultCode.MAC_ERROR)
                        message += "failed with MAC error";
                    else if (loadResult == LoadPKResultCode.PKDATALENGTH_ERROR)
                        message += "failed with public Data length error";
                    else if (loadResult == LoadPKResultCode.PKLENGTH_ERROR)
                        message += "failed with public key length  error";
                    else if (loadResult == LoadPKResultCode.UNKNOWPKINDEX_ERROR)
                        message += "failed with unknown  public key index error";
                    else if (loadResult == LoadPKResultCode.UNKNOWMKINDEX_ERROR)
                        message += "failed with unknown main key error";


                    if (loadResult != LoadPKResultCode.SUCCESS) break;

                    i++;
                }
               // //UIHelper.notifyByNoTitleDialog(activity, message);

            }
        }).start();
    }

    static LoadPKResultCode injectCAPKKey(CapkKey capkKey, int pk_index) {
        int length = 1024;
        return deviceManager.getPinInputModule().LoadPublicKey(
                LoadPKType.NOKEY_TYPE, pk_index, String.valueOf(length),
                capkKey.getModulus(), capkKey.getExponent(), capkKey.getKeyIndex(), capkKey.getCheckSum());
    }


    public static void addAIDs(final Activity activity) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                emvModule = deviceManager.getEmvModule();
                AIDConfig aidConfig = new AIDConfig();
                aidConfig.setAid(ISOUtils.hex2byte("A000000333010102"));// 0x9f06
                aidConfig.setAppSelectIndicator(0);// 0xDF01
                aidConfig.setAppVersionNumberTerminal(new byte[]{
                        0x00, (byte) 0x20});// 0x9f09
                aidConfig.setTacDefault(ISOUtils.hex2byte("FC78FCF8F0"));// 0xDF11
                aidConfig.setTacOnLine(ISOUtils.hex2byte("FC78FCF8F0"));// 0xDF12
                aidConfig.setTacDenial(ISOUtils.hex2byte("0010000000"));// 0xDF13
                aidConfig.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x05});// 0x9f1b
                aidConfig
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x00, (byte) 0x28});// 0xDF15
                aidConfig
                        .setMaxTargetPercentageForBiasedRandomSelection(32);// 0xDF16
                aidConfig.setTargetPercentageForRandomSelection(14);// 0xDF17
                aidConfig.setDefaultDDOL(ISOUtils.hex2byte("9F3704"));// 0xDF14
                aidConfig.setOnLinePinCapability(1);// 0xDF18
                aidConfig.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                aidConfig.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                aidConfig.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                aidConfig.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                aidConfig.setEcCapability(0);// 0xDF24
                aidConfig.setCoreConfigType(2);// 0xDF25
                boolean isSuccessful = emvModule.addAID(aidConfig);
               // //UIHelper.notifyByToast(activity, "AID1 insertion successful: " + isSuccessful);

                AIDConfig aidConfig2 = new AIDConfig();
                aidConfig2.setAid(ISOUtils.hex2byte("A000000333010101"));// 0x9f06
                aidConfig2.setAppSelectIndicator(0);// 0xDF01
                aidConfig2.setAppVersionNumberTerminal(new byte[]{
                        0x00, (byte) 0x20});// 0x9f09
                aidConfig2.setTacDefault(ISOUtils
                        .hex2byte("FC78FCF8F0"));// 0xDF11
                aidConfig2.setTacOnLine(ISOUtils.hex2byte("FC78FCF8F0"));// 0xDF12
                aidConfig2.setTacDenial(ISOUtils.hex2byte("0010000000"));// 0xDF13
                aidConfig2.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x05});// 0x9f1b
                aidConfig2
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x00, (byte) 0x28});// 0xDF15
                aidConfig2
                        .setMaxTargetPercentageForBiasedRandomSelection(32);// 0xDF16
                aidConfig2.setTargetPercentageForRandomSelection(14);// 0xDF17
                aidConfig2.setDefaultDDOL(ISOUtils.hex2byte("9F3704"));// 0xDF14
                aidConfig2.setOnLinePinCapability(1);// 0xDF18
                aidConfig2.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                aidConfig2.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                aidConfig2.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                aidConfig2.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                aidConfig2.setEcCapability(0);// 0xDF24
                aidConfig2.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(aidConfig2);
               // //UIHelper.notifyByToast(activity, "AID2 insertion successful " + isSuccessful);


                AIDConfig aidConfig3 = new AIDConfig();
                aidConfig3.setAid(ISOUtils.hex2byte("A000000333010103"));// 0x9f06
                aidConfig3.setAppSelectIndicator(0);// 0xDF01
                aidConfig3.setAppVersionNumberTerminal(new byte[]{
                        0x00, (byte) 0x20});// 0x9f09
                aidConfig3.setTacDefault(ISOUtils
                        .hex2byte("FC78FCF8F0"));// 0xDF11
                aidConfig3.setTacOnLine(ISOUtils.hex2byte("FC78FCF8F0"));// 0xDF12
                aidConfig3.setTacDenial(ISOUtils.hex2byte("0010000000"));// 0xDF13
                aidConfig3.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x05});// 0x9f1b
                aidConfig3
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x00, (byte) 0x28});// 0xDF15
                aidConfig3
                        .setMaxTargetPercentageForBiasedRandomSelection(32);// 0xDF16
                aidConfig3.setTargetPercentageForRandomSelection(14);// 0xDF17
                aidConfig3.setDefaultDDOL(ISOUtils.hex2byte("9F3704"));// 0xDF14
                aidConfig3.setOnLinePinCapability(1);// 0xDF18
                aidConfig3.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                aidConfig3.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                aidConfig3.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                aidConfig3.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                aidConfig3.setEcCapability(0);// 0xDF24
                aidConfig3.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(aidConfig3);
                //UIHelper.notifyByToast(activity, "AID3 insertion successful " + isSuccessful);

                AIDConfig aidConfig4 = new AIDConfig();
                aidConfig4.setAid(ISOUtils.hex2byte("A000000333010106"));// 0x9f06
                aidConfig4.setAppSelectIndicator(0);// 0xDF01
                aidConfig4.setAppVersionNumberTerminal(new byte[]{
                        0x00, (byte) 0x20});// 0x9f09
                aidConfig4.setTacDefault(ISOUtils
                        .hex2byte("FC78FCF8F0"));// 0xDF11
                aidConfig4.setTacOnLine(ISOUtils.hex2byte("FC78FCF8F0"));// 0xDF12
                aidConfig4.setTacDenial(ISOUtils.hex2byte("0010000000"));// 0xDF13
                aidConfig4.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x05});// 0x9f1b
                aidConfig4
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x00, (byte) 0x28});// 0xDF15
                aidConfig4
                        .setMaxTargetPercentageForBiasedRandomSelection(32);// 0xDF16
                aidConfig4.setTargetPercentageForRandomSelection(14);// 0xDF17
                aidConfig4.setDefaultDDOL(ISOUtils.hex2byte("9F3704"));// 0xDF14
                aidConfig4.setOnLinePinCapability(1);// 0xDF18
                aidConfig4.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                aidConfig4.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                aidConfig4.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                aidConfig4.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                aidConfig4.setEcCapability(0);// 0xDF24
                aidConfig4.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(aidConfig4);
                //UIHelper.notifyByToast(activity, "AID4 insertion successful " + isSuccessful);


                AIDConfig verveConfig = new AIDConfig();
                verveConfig.setAid(ISOUtils.hex2byte("A0000003710001"));
                verveConfig.setAppSelectIndicator(01);
                verveConfig.setAppVersionNumberTerminal(new byte[]{
                        0x00, (byte) 0x02});// 0x9f09
                verveConfig.setTacDefault(ISOUtils
                        .hex2byte("DC4000A800"));// 0xDF11
                verveConfig.setTacOnLine(ISOUtils.hex2byte("DC4004F800"));// 0xDF12
                verveConfig.setTacDenial(ISOUtils.hex2byte("0000000000"));// 0xDF13
                verveConfig.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00});// 0x9f1b
                verveConfig.setDefaultDDOL(ISOUtils.hex2byte("9f3704"));// 0xDF14
                verveConfig
                        .setMaxTargetPercentageForBiasedRandomSelection(30);// 0xDF16
                verveConfig.setTargetPercentageForRandomSelection(10);// 0xDF17
                verveConfig
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x05, (byte) 0x00});// 0xDF15
                verveConfig.setOnLinePinCapability(1);// 0xDF18
                verveConfig.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                verveConfig.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                verveConfig.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                verveConfig.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                verveConfig.setEcCapability(0);// 0xDF24
                verveConfig.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(verveConfig);
                //UIHelper.notifyByToast(activity, "Verve insertion successful " + isSuccessful);
                Log.d("okh", "verve "+ isSuccessful);

                AIDConfig etranConfig = new AIDConfig();
                etranConfig.setAid(ISOUtils.hex2byte("A0000004540010"));
                etranConfig.setAppSelectIndicator(01);
                etranConfig.setAppVersionNumberTerminal(new byte[]{
                        0x00, (byte) 0x02});// 0x9f09
                etranConfig.setTacDefault(ISOUtils
                        .hex2byte("FC50ACA000"));// 0xDF11
                etranConfig.setTacOnLine(ISOUtils.hex2byte("F850ACF800"));// 0xDF12
                etranConfig.setTacDenial(ISOUtils.hex2byte("0400000000"));// 0xDF13
                etranConfig.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00});// 0x9f1b
                etranConfig.setDefaultDDOL(ISOUtils.hex2byte("9f3704"));// 0xDF14
                etranConfig
                        .setMaxTargetPercentageForBiasedRandomSelection(30);// 0xDF16
                etranConfig.setTargetPercentageForRandomSelection(10);// 0xDF17
                etranConfig
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x05, (byte) 0x00});// 0xDF15
                etranConfig.setOnLinePinCapability(1);// 0xDF18
                etranConfig.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                etranConfig.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                etranConfig.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                etranConfig.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                etranConfig.setEcCapability(0);// 0xDF24
                etranConfig.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(etranConfig);
                //UIHelper.notifyByToast(activity, "Etran Card insertion successful " + isSuccessful);

                AIDConfig mcConfig = new AIDConfig();
                mcConfig.setAid(ISOUtils.hex2byte("A0000000043060"));
                mcConfig.setAppSelectIndicator(01);
                mcConfig.setAppVersionNumberTerminal(new byte[]{
                        0x00, (byte) 0x02});// 0x9f09
                mcConfig.setTacDefault(ISOUtils
                        .hex2byte("FC50ACA000"));// 0xDF11
                mcConfig.setTacOnLine(ISOUtils.hex2byte("FC50ACF800"));// 0xDF12
                mcConfig.setTacDenial(ISOUtils.hex2byte("0000000000"));// 0xDF13
                mcConfig.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00});// 0x9f1b
                mcConfig.setDefaultDDOL(ISOUtils.hex2byte("9f3704"));// 0xDF14
                mcConfig
                        .setMaxTargetPercentageForBiasedRandomSelection(30);// 0xDF16
                mcConfig.setTargetPercentageForRandomSelection(10);// 0xDF17
                mcConfig
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x05, (byte) 0x00});// 0xDF15
                mcConfig.setOnLinePinCapability(1);// 0xDF18
                mcConfig.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                mcConfig.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                mcConfig.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                mcConfig.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                mcConfig.setEcCapability(0);// 0xDF24
                mcConfig.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(mcConfig);
                //UIHelper.notifyByToast(activity, "Master card(0) insertion successful " + isSuccessful);

                AIDConfig mcConfig1 = new AIDConfig();
                mcConfig1.setAid(ISOUtils.hex2byte("A0000000041010"));
                mcConfig1.setAppSelectIndicator(01);
                mcConfig1.setAppVersionNumberTerminal(new byte[]{
                        0x00, (byte) 0x02});// 0x9f09
                mcConfig1.setTacDefault(ISOUtils
                        .hex2byte("FC50ACA000"));// 0xDF11
                mcConfig1.setTacOnLine(ISOUtils.hex2byte("FC50ACF800"));// 0xDF12
                mcConfig1.setTacDenial(ISOUtils.hex2byte("0000000000"));// 0xDF13
                mcConfig1.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00});// 0x9f1b
                mcConfig1.setDefaultDDOL(ISOUtils.hex2byte("9f3704"));// 0xDF14
                mcConfig1
                        .setMaxTargetPercentageForBiasedRandomSelection(30);// 0xDF16
                mcConfig1.setTargetPercentageForRandomSelection(10);// 0xDF17
                mcConfig1
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x05, (byte) 0x00});// 0xDF15
                mcConfig1.setOnLinePinCapability(1);// 0xDF18
                mcConfig1.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                mcConfig1.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                mcConfig1.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                mcConfig1.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                mcConfig1.setEcCapability(0);// 0xDF24
                mcConfig1.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(mcConfig1);
                //UIHelper.notifyByToast(activity, "Master card(1) insertion successful " + isSuccessful);

                AIDConfig mcConfig2 = new AIDConfig();
                mcConfig2.setAid(ISOUtils.hex2byte("A0000000046000"));
                mcConfig2.setAppSelectIndicator(01);
                mcConfig2.setAppVersionNumberTerminal(new byte[]{
                        0x00, (byte) 0x02});// 0x9f09
                mcConfig2.setTacDefault(ISOUtils
                        .hex2byte("FC50ACA000"));// 0xDF11
                mcConfig2.setTacOnLine(ISOUtils.hex2byte("FC50ACF800"));// 0xDF12
                mcConfig2.setTacDenial(ISOUtils.hex2byte("0000000000"));// 0xDF13
                mcConfig2.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00});// 0x9f1b
                mcConfig2.setDefaultDDOL(ISOUtils.hex2byte("9f3704"));// 0xDF14
                mcConfig2
                        .setMaxTargetPercentageForBiasedRandomSelection(30);// 0xDF16
                mcConfig2.setTargetPercentageForRandomSelection(10);// 0xDF17
                mcConfig2
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x05, (byte) 0x00});// 0xDF15
                mcConfig2.setOnLinePinCapability(1);// 0xDF18
                mcConfig2.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                mcConfig2.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                mcConfig2.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                mcConfig2.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                mcConfig2.setEcCapability(0);// 0xDF24
                mcConfig2.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(mcConfig2);
                //UIHelper.notifyByToast(activity, "Master card(2) insertion successful " + isSuccessful);

                AIDConfig visaConfig = new AIDConfig();
                visaConfig.setAid(ISOUtils.hex2byte("a0000000031010"));
                visaConfig.setAppSelectIndicator(01);
                visaConfig.setAppVersionNumberTerminal(ISOUtils.hex2byte("008c"));// 0x9f09
                visaConfig.setTacDefault(ISOUtils
                        .hex2byte("DC4000A800"));// 0xDF11
                visaConfig.setTacOnLine(ISOUtils.hex2byte("DC4004F800"));// 0xDF12
                visaConfig.setTacDenial(ISOUtils.hex2byte("0010000000"));// 0xDF13
                visaConfig.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00});// 0x9f1b
                visaConfig.setDefaultDDOL(ISOUtils.hex2byte("9f3704"));// 0xDF14
                visaConfig
                        .setMaxTargetPercentageForBiasedRandomSelection(30);// 0xDF16
                visaConfig.setTargetPercentageForRandomSelection(11);// 0xDF17
                visaConfig
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x05, (byte) 0x00});// 0xDF15
                visaConfig.setOnLinePinCapability(1);// 0xDF18
                visaConfig.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                visaConfig.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                visaConfig.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                visaConfig.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                visaConfig.setEcCapability(0);// 0xDF24
                visaConfig.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(visaConfig);
                //UIHelper.notifyByToast(activity, "Visa insertion successful " + isSuccessful);

                AIDConfig visaConfig2 = new AIDConfig();
                visaConfig2.setAid(ISOUtils.hex2byte("a0000000032010"));
                visaConfig2.setAppSelectIndicator(01);
                visaConfig2.setAppVersionNumberTerminal(ISOUtils.hex2byte("008c"));// 0x9f09
                visaConfig2.setTacDefault(ISOUtils
                        .hex2byte("DC4000A800"));// 0xDF11
                visaConfig2.setTacOnLine(ISOUtils.hex2byte("DC4004F800"));// 0xDF12
                visaConfig2.setTacDenial(ISOUtils.hex2byte("0010000000"));// 0xDF13
                visaConfig2.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00});// 0x9f1b
                visaConfig2.setDefaultDDOL(ISOUtils.hex2byte("9f3704"));// 0xDF14
                visaConfig2
                        .setMaxTargetPercentageForBiasedRandomSelection(30);// 0xDF16
                visaConfig2.setTargetPercentageForRandomSelection(11);// 0xDF17
                visaConfig2
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x05, (byte) 0x00});// 0xDF15
                visaConfig2.setOnLinePinCapability(1);// 0xDF18
                visaConfig2.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                visaConfig2.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                visaConfig2.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                visaConfig2.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                visaConfig2.setEcCapability(0);// 0xDF24
                visaConfig2.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(visaConfig2);
                //UIHelper.notifyByToast(activity, "Visa2 insertion successful " + isSuccessful);


                AIDConfig visaConfig3 = new AIDConfig();
                visaConfig3.setAid(ISOUtils.hex2byte("a0000000032020"));
                visaConfig3.setAppSelectIndicator(01);
                visaConfig3.setAppVersionNumberTerminal(ISOUtils.hex2byte("008c"));// 0x9f09
                visaConfig3.setTacDefault(ISOUtils
                        .hex2byte("DC4000A800"));// 0xDF11
                visaConfig3.setTacOnLine(ISOUtils.hex2byte("DC4004F800"));// 0xDF12
                visaConfig3.setTacDenial(ISOUtils.hex2byte("0010000000"));// 0xDF13
                visaConfig3.setTerminalFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00});// 0x9f1b
                visaConfig3.setDefaultDDOL(ISOUtils.hex2byte("9f3704"));// 0xDF14
                visaConfig3
                        .setMaxTargetPercentageForBiasedRandomSelection(30);// 0xDF16
                visaConfig3.setTargetPercentageForRandomSelection(11);// 0xDF17
                visaConfig3
                        .setThresholdValueForBiasedRandomSelection(new byte[]{
                                0x00, 0x00, 0x05, (byte) 0x00});// 0xDF15
                visaConfig3.setOnLinePinCapability(1);// 0xDF18
                visaConfig3.setEcTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0x9F7B
                visaConfig3.setNciccOffLineFloorLimit(new byte[]{0x00,
                        0x00, 0x00, 0x00, 0x00, 0x00});// 0xDF19
                visaConfig3.setNciccTransLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00});// 0xDF20
                visaConfig3.setNciccCVMLimit(new byte[]{0x00, 0x00,
                        0x00, 0x00, (byte) 0x80, 0x00});// 0xDF21
                visaConfig3.setEcCapability(0);// 0xDF24
                visaConfig3.setCoreConfigType(2);// 0xDF25
                isSuccessful = emvModule.addAID(visaConfig3);
                //UIHelper.notifyByToast(activity, "Visa3 insertion successful " + isSuccessful);

                //UIHelper.notifyByNoTitleDialog(activity, "AIDs loaded successfully");
            }
        }).start();
    }

    public static void setTerminalProperties(final Activity activity) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                emvModule = deviceManager.getEmvModule();

                TerminalConfig trmnlConfig = new TerminalConfig();
                trmnlConfig.setTrmnlICSConfig(new byte[]{(byte) 0xF4,
                        (byte) 0xF0, (byte) 0xF0, (byte) 0xFA,
                        (byte) 0xAF, (byte) 0xFE, (byte) 0xA0});
                trmnlConfig.setTerminalType(0x22);
                trmnlConfig.setTerminalCapabilities(new byte[]{
                        (byte) 0xE0, (byte) 0xF8, (byte) 0xC8});
                trmnlConfig
                        .setAdditionalTerminalCapabilities(new byte[]{
                                (byte) 0xFF, (byte) 0x80, (byte) 0xF0,
                                (byte) 0xB0, 0x01});
                trmnlConfig.setPointOfServiceEntryMode(0x05);

                trmnlConfig.setTransactionCurrencyCode(currencyCode); //setTransactionCurrencyCode("156"); Nigeria is 566
                trmnlConfig.setTransactionCurrencyExp("1");
                trmnlConfig.setTerminalCountryCode(ISOUtils.hex2byte(currencyCode));//(new byte[]{0x08, 0x40});
                trmnlConfig.setInterfaceDeviceSerialNumber("11111111");
                trmnlConfig.setAidPartlyMatchSupported((byte) 0x01);
                boolean issuccess = emvModule.setTrmnlParams(trmnlConfig);

                String message = "";
                if (issuccess)
                    message = "Terminal properties update was successful ";
                else
                    message = "Terminal properties update failed ";

                ////UIHelper.notifyByNoTitleDialog(activity, message);

                addAIDs(activity);
            }
        }).start();
    }


    public static void resetConfigurations(Activity activity) {
        EmvModule emvModule = deviceManager.getEmvModule();
        boolean isSuccessful = emvModule.clearAllAID();
//        if (isSuccessful)
//           // //UIHelper.notifyByToast(activity, "cleared all AIDs");
//        isSuccessful = emvModule.clearAllCAPublicKey(null);
//        if (isSuccessful)
//          //  //UIHelper.notifyByToast(activity, "cleared CA Public Keys");


        //UIHelper.notifyByNoTitleDialog(activity, "Configuration reset complete");
    }

    public static String buildDeviceInfoString(DeviceInfo deviceInfo) {

        StringBuilder infoSB = new StringBuilder()
                .append("App version: " + deviceInfo.getAppVer()).append("\n")
                .append("Boot version: " + deviceInfo.getBootVersion()).append("\n");
        infoSB.append("Firmware version: " + deviceInfo.getFirmwareVer()).append("\n");
        infoSB.append("CSN: " + deviceInfo.getCSN()).append("\n");
        infoSB.append("Custom Serial Number: " + deviceInfo.getCustomSN()).append("\n");
        infoSB.append("KSN: " + deviceInfo.getKSN()).append("\n");
        infoSB.append("Firmware version: " + deviceInfo.getUdid()).append("\n");
        infoSB.append("PID: " + deviceInfo.getVID()).append("\n");
        infoSB.append("SN: " + deviceInfo.getSN()).append("\n");
        infoSB.append("UDID: " + deviceInfo.getUdid()).append("\n");
        infoSB.append("VID: " + deviceInfo.getVID()).append("\n\n");

        infoSB.append("Device Status").append("\n");
        infoSB.append("Dukpt Key Loaded: " + deviceInfo.isDUKPTkeyLoaded()).append("\n");
        infoSB.append("Still default factory settings: " + deviceInfo.isFactoryModel()).append("\n");
        infoSB.append("Main Key Loaded: " + deviceInfo.isMainkeyLoaded()).append("\n");
        infoSB.append("Working Key Loaded: " + deviceInfo.isWorkingkeyLoaded()).append("\n\n");

        infoSB.append("Device Capabilities").append("\n");
        infoSB.append("Has audio support: " + deviceInfo.isSupportAudio()).append("\n");
        infoSB.append("Bluetooth enabled: " + deviceInfo.isSupportBlueTooth()).append("\n");
        infoSB.append("Has Chip Support: " + deviceInfo.isSupportICCard()).append("\n");
        infoSB.append("Can perform offline transaction: " + deviceInfo.isSupportOffLine()).append("\n");
        infoSB.append("Has Magnetic Strip support: " + deviceInfo.isSupportMagCard()).append("\n");


        return infoSB.toString();
    }


}

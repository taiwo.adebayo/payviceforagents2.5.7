package com.iisysgroup.payviceforagents.util

import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.poslib.deviceinterface.DeviceState
import com.iisysgroup.poslib.utils.Utilities
import com.qd.testcodeobfuscation.MainActivity
import org.jetbrains.anko.indeterminateProgressDialog

class DefaultUIManager(private val parentContainerView : View, val uiModel: DefaultUIModel) {


    private val mProgressDialog by lazy {
        parentContainerView.context.indeterminateProgressDialog("Processing"){
            setCancelable(false)
        }
    }

    //Helper method to show UI details depending on the state
    fun setState(deviceState : DeviceState){

        setDetails(uiModel)

        when (deviceState){
            DeviceState.INSERT_CARD -> {
                Log.d("OkH" ,"Insert card")
                val insertCardView = parentContainerView.findViewById<View>(R.id.insert_card)
                showVisibility(insertCardView)
            }

            DeviceState.DECLINED, DeviceState.FAILED, DeviceState.APPROVED -> {
                if (mProgressDialog.isShowing)
                mProgressDialog.dismiss()

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    handleTransactionCompletion(deviceState)
                }
            }

            DeviceState.PROCESSING, DeviceState.AWAITING_ONLINE_RESPONSE -> {
                if (mProgressDialog.isShowing){
                    Log.d("Log", "Progress Showing")
                } else {
                    mProgressDialog.show()
                }

            }
        }
    }



    private fun setDetails(uiModel: DefaultUIModel){
        val insertCardView = parentContainerView.findViewById<View>(R.id.insert_card)

        val transactionType = insertCardView.findViewById<TextView>(R.id.transactionTypeText)
        transactionType.text = uiModel.transactionTitle



        val transactionAmount = insertCardView.findViewById<TextView>(R.id.transactionAmountText)
        transactionAmount.text =  "${Utilities.parseLongIntoNairaKoboString(uiModel.amount)}"

    }

    private fun showVisibility(view : View) {
        val layout_ids = intArrayOf(R.id.enter_amount,
                R.id.insert_card, R.id.transaction_status_layout)

        if (view.visibility == View.VISIBLE) {
            return
        }

        for (ids in layout_ids) {
            if (parentContainerView.findViewById<View>(ids) != null && ids != view.id)
                parentContainerView.findViewById<View>(ids).visibility = View.GONE
        }

        view.visibility = View.VISIBLE
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun handleTransactionCompletion(deviceState : DeviceState){
        val transactionCompleteView = parentContainerView.findViewById<View>(R.id.transaction_status_layout)

        showVisibility(transactionCompleteView)

        val statusView = transactionCompleteView.findViewById<TextView>(R.id.transactionStatusText)
        val statusImageView = transactionCompleteView.findViewById<ImageView>(R.id.transactionStatusImage)

        val finishButton = transactionCompleteView.findViewById<Button>(R.id.finishButton)
0
        when (deviceState){
            DeviceState.APPROVED -> {
                statusView.text = "Transaction Approved"
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    statusImageView.setImageDrawable(parentContainerView.context.getDrawable(R.drawable.transaction_approved))
                }
            }

            DeviceState.DECLINED -> {
                statusView.text = "Transaction Declined"
                statusImageView.setImageDrawable(parentContainerView.context.getDrawable(R.drawable.transaction_declined))
            }

            DeviceState.FAILED -> {
                statusView.text = "Transaction Failed"
                statusImageView.setImageDrawable(parentContainerView.context.getDrawable(R.drawable.transaction_declined))
            }
        }

        finishButton.setOnClickListener {
            val context = parentContainerView.context
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)

            (context as AppCompatActivity).finish()
        }
    }


}
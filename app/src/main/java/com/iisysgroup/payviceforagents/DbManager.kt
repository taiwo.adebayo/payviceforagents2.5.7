package com.iisysgroup.payviceforagents

import android.app.Application
import com.iisysgroup.poslib.host.entities.TransactionResult
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class DbManager(application : Application) {
    private val db by lazy {
        (application as App).db
    }

    fun saveTransactionData(transactionResult : TransactionResult){
        Single.fromCallable {
            db.transactionResultDao.save(transactionResult)
        }.subscribeOn(Schedulers.io())
                .subscribe { _ ->

                }

    }

//    fun deleteTransactionData(rrn : String){
//        Single.fromCallable {
//            db.transactionResultDao.delete(rrn)
//        }.subscribeOn(Schedulers.io())
//                .subscribe { _ ->
//                }
//
//    }



    fun retrieveTransactionByRrn(rrn : String) = db.transactionResultDao.get(rrn)

}
//package com.iisysgroup.payviceforagents.persistence.dao
//
//import android.arch.lifecycle.LiveData
//import android.arch.persistence.room.*
//import com.iisysgroup.payviceforagents.persistence.entitiy.Card
//import io.reactivex.Flowable
//
//@Dao
//interface CardDao {
//
//    @Query("SELECT * FROM  card")
//    fun getAll(): LiveData<List<Card>>
//
//    @Query("SELECT * FROM  card")
//    fun rxGetAll(): Flowable<List<Card>>
//
//    @Insert(onConflict = OnConflictStrategy.IGNORE)
//    fun insert(card: Card)
//
//    @Delete
//    fun delete(card: Card)
//
//    @Delete
//    fun deleteAll(cards: List<Card>)
//}
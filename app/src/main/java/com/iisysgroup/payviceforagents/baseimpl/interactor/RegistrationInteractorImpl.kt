package com.iisysgroup.payviceforagents.baseimpl.interactor

import android.content.Context
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.base.interactor.RegistrationInteractor
import com.iisysgroup.payviceforagents.entities.RegistrationData
import com.iisysgroup.payviceforagents.entities.VasResult
import com.iisysgroup.payviceforagents.payviceservices.Requests
import com.iisysgroup.payviceforagents.util.Helper
import io.reactivex.Single
import java.util.*

class RegistrationInteractorImpl(val context: Context) : RegistrationInteractor {

    override fun register(data: RegistrationData): Single<VasResult> {
        return Single.fromCallable {
            val url = context.resources.getString(R.string.tams_url)
            val REG_ACTION = "TAMS_REG"

            val terminalID = context.getString(R.string.tams_default_terminal_id)

            val userKey = Helper.generateVerificationKey("$terminalID${data.email}${data.name}")
//            val deviceID = Helper.getDeviceID(context).trim { it <= ' ' }

            val params = HashMap<String, String>()
            params["action"] = REG_ACTION
            params["userid"] = data.phone
            params["termid"] = terminalID
            params["username"] = data.name
            params["email"] = data.email
            params["key"] = userKey
//            params["device_id"] = deviceID

            val tamsResult = Requests.processRequest(url, params)

            tamsResult
        }
    }
}
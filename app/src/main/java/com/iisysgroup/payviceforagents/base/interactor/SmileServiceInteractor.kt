package com.iisysgroup.payviceforagents.base.interactor

import com.itex.richard.payviceconnect.model.SmileModel
import io.reactivex.Single

interface SmileServiceInteractor {
    fun validateUser(accountId: String, walletId : String): Single<SmileModel.SmileValidateCustomerResponse>
    fun getBundles(): Single<SmileModel.SmileGetBundleResponse>
    fun topUpAccount(accountId: String, amountInKobo: Int, encryptedPin: String): Single<SmileModel.SmileSuccessResponse>
    fun subscribe(accountId: String, plan: SmileModel.Bundle, encryptedPin: String): Single<SmileModel.SmileSuccessResponse>
}

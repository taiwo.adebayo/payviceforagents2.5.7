package com.iisysgroup.payviceforagents.fragments

import android.content.Context
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast

import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.callbacks.OnValueInputFragmentInteractionListener
import com.iisysgroup.payviceforagents.entities.Service
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.*
import com.iisysgroup.payviceforagents.util.Helper
import com.iisysgroup.payviceforagents.util.VasServices

import com.google.gson.Gson
import com.iisgroup.bluetoothprinterlib.Utils.Util.Language.it
import com.itex.richard.payviceconnect.model.*
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_bill_value_input.*
import org.jetbrains.anko.okButton
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.indeterminateProgressDialog


class BillValueInputFragment : BaseBillFragment() {

//    @BindView(R.id.amountEntryLayout)
//    internal var vtuLayout: LinearLayout? = null
//    @BindView(R.id.recipientTIL)
//    internal var recipientTil: TextInputLayout? = null
//    @BindView(R.id.beneficiaryEdit)
//    internal var serviceNumberEdit: EditText? = null

    var productCode: String?= null
    var meterNumber: String?= null
    var meterName: String?= null

   private val  validationDialogue by lazy {
      indeterminateProgressDialog(message = "Please Wait.....", title = "Validating Meter Number") { }
   }
    private var mListener: OnValueInputFragmentInteractionListener? = null

    internal val isValidInputs: Boolean
        get() {
             var response = true
            if (product == null) {
                serviceText.error = "Please select product"
                Helper.showErrorAnim(serviceText)
                Helper.showSnackBar(serviceText, "Please select product")
                response= false
                return false
            }

            if (beneficiaryEdit!!.text.toString().trim { it <= ' ' }.isEmpty()) {
                beneficiaryEdit!!.error = VasServices.VAS_SERVICE_INPUT_TEXT_DESC[service.name]
                beneficiaryEdit!!.requestFocus()

                Helper.showErrorAnim(beneficiaryEdit)
                response=false
                return false
            }

            return  response
//

        }
    internal var wallet = SecureStorage.retrieve(Helper.TERMINAL_ID, "")
    internal var terminal = SecureStorage.retrieve(Helper.TERMINAL_ID, "")
    internal var userName = SecureStorage.retrieve(Helper.USER_ID, "")
    internal var password = SecureStorage.retrieve(Helper.PLAIN_PASSWORD, "")
    internal var channel = "ANDROID"
    internal var deviceID = SecureStorage.retrieve(Helper.TERMINAL_ID, "")


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bill_value_input, null)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        naira_signRV!!.visibility = View.GONE
        phoneEdit!!.visibility = View.GONE
        recipientTIL!!.hint = Helper.getBeneficiaryTextHint(service.name)
        proceedBtn.text = "Validate"


        proceedBtn.setOnClickListener {

            //            Log.i("Checking.....", charSequence.toString().length.toString())

            if(proceedBtn.text.toString() == "Validate") {
                if (isValidInputs) {
                    context!!.runOnUiThread {
                        validationDialogue.show()
                    }
//                    Log.i("Checking.....", "Started")
                          meterNumber =beneficiaryEdit.text.toString()
                    validate(service.name,meterNumber!! )
                }

            }
            else{
//                Toast.makeText(context,"About to Make Payment",Toast.LENGTH_SHORT).show()
                proceed()
            }
        }



//        RxTextView.textChanges(serviceNumberEdit!!).delay(300L, TimeUnit.MILLISECONDS).subscribe { charSequence ->
//            if (charSequence.toString().length == 11) {
//
//                validate(service.name,serviceNumberEdit!!.text.toString())
//            }
//        }
           Log.i("Checking.....", "Here ")
//        beneficiaryEdit!!.textChanges().delay(300L, TimeUnit.MILLISECONDS)
//                .subscribe { charSequence ->
//                    Log.i("Checking.....", charSequence.toString().length.toString())
//                    if (charSequence.toString().length == 11) {
//                       context!!.runOnUiThread { validationDialogue.show() }
//                        Log.i("Checking.....", charSequence.toString().length.toString())
//                        Log.i("Checking.....","Started")
//                        validate(service.name,charSequence.toString())
//                    }
//                }


    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(product: Service.Product, amount: String,beneficiary:Beneficiary, phone:String,productCode:String,meterNumber: String,meterName: String) {
        if (mListener != null) {

            mListener!!.onValueInputFragmentInteraction(product, amount, beneficiary,phone,productCode,meterNumber,meterName)

        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnValueInputFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    fun checkPhoneNumber(phoneEditText: EditText) : Boolean{
        var isReturn = true
        if (!PhoneNumberUtils.isGlobalPhoneNumber(phoneEditText.text.toString())) {
            Helper.showErrorAnim(phoneEditText)
            phoneEditText.error = "Enter beneficiary"
            isReturn = false
            return false
        }

        return isReturn
    }

    internal fun proceed() {

        if (isValidInputs && Helper.isValidAmountInput(amountEdit!!) && checkPhoneNumber(phoneEdit)) {
            val beneficiary = beneficiaryEdit!!.text.toString()
            val phone = phoneEdit!!.text.toString()
            val productCode =productCode!!
            val meterNumber =meterNumber !!
            val meterName = meterName!!
//            Log.i("phone", phone)
//            Log.i("productCode", productCode)
//
//            Log.i("meterNumber", meterNumber)
//
//            Log.i("meterName", meterName)



            onButtonPressed(product, amountEdit!!.text.toString().trim { it <= ' ' }, Beneficiary(beneficiary, "", ""),phoneEdit.text.toString().trim{ it <= ' ' },productCode!!,meterNumber!!,meterName!!)

        }
    }

    internal fun validate(productName: String,meterNumber: String) {

        when (productName) {

            VasServices.IKEJA_ELECTRIC -> validateIkejaMeter(meterNumber)
            VasServices.IBADAN_ELECTRIC -> validateIbadanMeter(meterNumber)
            VasServices.ENUGU_ELECTRIC -> validateEnuguMeter(meterNumber)
            VasServices.EKO_ELECTRIC -> validateEkoMeter(meterNumber)
            VasServices.PORTHARCOURT_ELECTRIC -> validatePHCMeter(meterNumber)
            VasServices.ABUJA_ELECTRIC -> validateAbujaMeter(meterNumber)
//            VasServices.EKO_ELECTRIC -> validateEkoMeter(meterNumber)
            else -> Toast.makeText(context, "This feature is not yet available", Toast.LENGTH_SHORT).show()
        }
    }

    private fun validatePHCMeter(account: String) {


        var  type : String  = "prepaid"
        if (product.name == VasServices.PORTHARCOURT_ELECTRICITY_POSTPAID) {
            type ="postpaid"
        }

        val PortharcourtLookupDetails = PortharcourtModel.LookupDetails(terminal,wallet,userName,type,"ANDROID",account)

        context!!.payviceServices.portharcourtLookup(PortharcourtLookupDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<PortharcourtModel.lookUpResponse> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(PHCLookupResponse:PortharcourtModel.lookUpResponse) {


                        context!!.runOnUiThread { validationDialogue.dismiss() }
                        Log.i("PhcLookupResponse>>>>>", Gson().toJson(PHCLookupResponse))
                        try {


                            alert {
                                isCancelable =false
                                if (PHCLookupResponse.error){

                                    title = "Verification Failed "
                                    message = PHCLookupResponse.message!!
                                    okButton { }
                                }else {

                                    var phone: String = if (PHCLookupResponse.phone.isEmpty() ||PHCLookupResponse.phone == null)
                                        "\n \nNone"
                                    else
                                        "\n \n " + PHCLookupResponse.phone.toString()
//
//                                var accountType: String ="\n \nAccount Type - ${ibLookupResponse.}"
                                    meterName = PHCLookupResponse.name
                                    meterNumber =PHCLookupResponse.account
                                    productCode =PHCLookupResponse.productCode
                                    title = PHCLookupResponse.message
                                    SecureStorage.store("currentName", meterName)
                                    message = "\n Customer Name - ${meterName}" +
                                            "\n \nMeter Number - ${meterNumber}" +
                                            "\n \n Address  - ${PHCLookupResponse.address}"
//
                                    okButton {

                                        naira_signRV!!.visibility = View.VISIBLE
                                        phoneEdit!!.visibility = View.VISIBLE
                                        proceedBtn.text = "Proceed"
                                    }
                                }
                            }.show()
                        } catch (e:Throwable){
                            alert {
                                isCancelable =false
                                title = "Verification Failed "
                                message = PHCLookupResponse.message!!
                                okButton { }

                            }.show()
                        }




                    }

                    override fun onError(e: Throwable) {
                        context!!.runOnUiThread { validationDialogue.show() }
                        Log.i("ibadanFailureResponse>>>>>", Gson().toJson(e.printStackTrace()))
                        alert {
                            title = "Validation Status"
                            message = e.message!!
                            okButton {  }
                        }.show()
                    }
                    override fun onComplete() {

                    }
                })






    }

    private fun validateAbujaMeter(account:  String) {

        var  requestType : String  = "0"
        var  meterType : String  = "0"
        if (product.name == VasServices.ABUJA_ELECTRICITY_POSTPAID) {
            meterType ="2"
//            type,"ANDROID",account
        }

        val abujaDetails : AbujaModel.LookupDetails  = AbujaModel.LookupDetails(wallet,userName,requestType,meterType,account,"ANDROID")
        Log.i("abujaLookupDetails>>...", Gson().toJson(abujaDetails))
        context!!.payviceServices.abujaLookup(abujaDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<AbujaModel.LookUpResponse> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(abujaLookupResponse:AbujaModel.LookUpResponse) {


                        context!!.runOnUiThread { validationDialogue.dismiss() }
                        Log.i("abujaLookupResponse>>>>>", Gson().toJson(abujaLookupResponse))
                        try {


                            alert {
                                isCancelable =false
                                if (abujaLookupResponse.error){

                                    title = "Verification Failed "
                                    message = abujaLookupResponse.message!!
                                    okButton { }
                                }else {

//                                    var phone: String = if (abujaLookupResponse.   phone.isEmpty() ||PHCLookupResponse.phone == null)
//                                        "\n \nNone"
//                                    else
//                                        "\n \n " + PHCLookupResponse.phone.toString()
//
//                                var accountType: String ="\n \nAccount Type - ${ibLookupResponse.}"
                                    meterName = abujaLookupResponse.name
                                    meterNumber =abujaLookupResponse.customerMeterNo
                                    productCode =abujaLookupResponse.productCode
                                    title = abujaLookupResponse.message!!
                                    SecureStorage.store("currentName", meterName)
                                    message = "\n Customer Name - ${meterName}" +
                                            "\n \nMeter Number - ${meterNumber}"
//                                            "\n \nPhone - ${abujaLookupResponse.}"
//
                                    okButton {
                                        naira_signRV!!.visibility = View.VISIBLE
                                        phoneEdit!!.visibility = View.VISIBLE
                                        proceedBtn.text = "Proceed"
                                    }
                                }
                            }.show()
                        } catch (e:Throwable){
                            alert {
                                isCancelable =false
                                title = "Verification Failed "
                                message = abujaLookupResponse.message!!
                                okButton { }

                            }.show()
                        }




                    }

                    override fun onError(e: Throwable) {
                        context!!.runOnUiThread { validationDialogue.show() }
                        Log.i("abujaResponse>>>>>", Gson().toJson(e.printStackTrace()))
                        alert {
                            title = "Validation Status"
                            message = e.message!!
                            okButton {  }
                        }.show()
                    }
                    override fun onComplete() {

                    }
                })






    }

    private fun validateIbadanMeter(account: String) {

        var  type : String  = "prepaid"
        if (product.name == VasServices.IBADAN_ELECTRICITY_POSTPAID) {
            type ="postpaid"
            Log.d("validateIbadanMeter",type )
        }

        val ibadanLookupDetails = IbadanModel.IbLookupRequest(wallet,userName,type,"ANDROID",account)

        Log.d("validateIbadanMeter>>>",Gson().toJson(ibadanLookupDetails) )
        context!!.payviceServices.IbadanLookUp(ibadanLookupDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<IbadanModel.IbLookupResponse> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(ibLookupResponse:IbadanModel.IbLookupResponse) {


                        context!!.runOnUiThread { validationDialogue.dismiss() }
                        Log.i("IbadanLookupResponse>>>>>", Gson().toJson(ibLookupResponse))
                        try {


                        alert {
                            isCancelable =false
                            if (ibLookupResponse.error){

                                title = "Verification Failed "
                                message = ibLookupResponse.message!!
                                okButton { }
                            }else {

                                var phone: String = if (ibLookupResponse.phone.isEmpty() ||ibLookupResponse.phone == null)
                                    "\n \nNone"
                                else
                                      "\n \n " + ibLookupResponse.phone.toString()
//
//                                var accountType: String ="\n \nAccount Type - ${ibLookupResponse.}"
                                meterName =ibLookupResponse.name
                                meterNumber =ibLookupResponse.account
                                productCode =ibLookupResponse.productCode
                                title = ibLookupResponse.message
                                SecureStorage.store("currentName", meterName)
                                message = "\n Customer Name - ${ meterName}" +
                                        "\n \nMeter Number - ${meterNumber}"
//
                                okButton {

                                    naira_signRV!!.visibility = View.VISIBLE
                                    phoneEdit!!.visibility = View.VISIBLE
                                    proceedBtn.text = "Proceed"
                                }
                            }
                        }.show()
                        } catch (e:Throwable){
                         alert {
                             isCancelable =false
                            title = "Verification Failed "
                            message = ibLookupResponse.message!!
                            okButton { }

                        }.show()
                        }




                    }

                    override fun onError(e: Throwable) {
                        context!!.runOnUiThread { validationDialogue.show() }
                        Log.i("ibadanFailureResponse>>>>>", Gson().toJson(e.printStackTrace()))
                        alert {
                            title = "Validation Status"
                            message = e.message!!
                            okButton {  }
                        }.show()
                    }
                    override fun onComplete() {

                    }
                })




    }

    private fun validateIkejaMeter(meter: String) {

        var service = ""

         var ikejaLookupResponse : Observable<IkejaModel.IkejaLookupResponse>? =null
                if( product.name == VasServices.IKEJA_ELECTRICITY_PREPAID)  {
                service = "token"
                val ikejaLookupDetails = IkejaModel.IkejaLookupDetailsPrepaid(wallet, wallet, userName, password, meter, "getcus", "vend", service,"ANDROID")
                 ikejaLookupResponse = context!!.payviceServices.IkejaLookupPrepaid(ikejaLookupDetails)
            }

               if( product.name == VasServices.IKEJA_ELECTRICITY_POSTPAID){
                service = "postpaid"
                   val ikejaLookupDetails = IkejaModel.IkejaLookupDetailsPostPaid(wallet, wallet, userName, password, meter, "getcus", "pay", service,"ANDROID")

                   ikejaLookupResponse = context!!.payviceServices.IkejaLookupPostpaid(ikejaLookupDetails)

            }




        Log.i("ikejaLookupDetaiback ", Gson().toJson(ikejaLookupResponse))


        ikejaLookupResponse!!.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<IkejaModel.IkejaLookupResponse> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(ikejaLookupResponse: IkejaModel.IkejaLookupResponse) {

                        context!!.runOnUiThread { validationDialogue.dismiss() }
                        Log.i("ikejaLookupRespo>>", Gson().toJson(ikejaLookupResponse))

                            alert {
                                if (ikejaLookupResponse.error){
                                    title = "Validation Failed"
                                    message = ikejaLookupResponse.message!!
                                    okButton { }
                                }else {

                                    meterName =ikejaLookupResponse.name
                                    meterNumber =beneficiaryEdit.text.toString()
                                    productCode =""
                                    SecureStorage.store("currentName", meterName)
                                    SecureStorage.store("currentNumber", meterName)
                                    title = "Validation Successful "
                                    message = "\n Customer Name - ${ meterName}" +
                                            "\n \nMeter Number - ${meter}" +
                                            "\n \nAddress - ${ikejaLookupResponse.address}"
//                                    "\nMinimum Amount Payable  - ${ekoLookupResponse.}"
                                okButton {

                                    naira_signRV!!.visibility = View.VISIBLE
                                    phoneEdit!!.visibility = View.VISIBLE
                                    proceedBtn.text = "Proceed"
                                }
                            }
                        }.show()


                    }


                    override fun onError(e: Throwable) {
                        context!!.runOnUiThread { validationDialogue.show() }
                        Log.i("ikejaLookupFailureResponse>>>>>", Gson().toJson(e.printStackTrace()))
                        alert {
                            title = "Validation Failed"
                            message = e.message!!
                            okButton {  }
                        }.show()
                    }

                    override fun onComplete() {

                    }
                })

    }

    fun validateEnuguMeter(account : String) {

        var  type : String  = "prepaid"
        if (product.name == VasServices.ENUGU_ELECTRICITY_POSTPAID) {
            type ="postpaid"
        }

        val enuguLookupDetails = EnuguModel.LookupRequest(wallet,userName,type,"ANDROID",account)

        context!!.payviceServices.EnuguElectricValidaton(enuguLookupDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<EnuguModel.LookupResponse> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(enuguLookupResponse:EnuguModel.LookupResponse) {


                        context!!.runOnUiThread { validationDialogue.dismiss() }
                        Log.i("EnuguLookupResponse>>>>>", Gson().toJson(enuguLookupResponse))
                        try {


                            alert {
                                isCancelable =false
                                if (enuguLookupResponse.error){

                                    title = "Verification Failed "
                                    message = enuguLookupResponse.message!!
                                    okButton { }
                                }else {

//                                    var phone: String = if (enuguLookupResponse.     isEmpty() ||ibLookupResponse.phone == null)
//                                        "\n \nNone"
//                                    else
//                                        "\n \n " + ibLookupResponse.phone.toString()
////
//                                var accountType: String ="\n \nAccount Type - ${ibLookupResponse.}"
                                    meterName =enuguLookupResponse.name
                                    meterNumber =enuguLookupResponse.account
                                    productCode =enuguLookupResponse.productCode
                                    title = enuguLookupResponse.message
                                    SecureStorage.store("currentName", meterName)
                                    message = "\n Customer Name - ${ meterName}" +
                                            "\n \nMeter Number - ${meterNumber}" +
                                            "\n \nDescription  - ${enuguLookupResponse.description}"
//
                                    okButton {

                                        naira_signRV!!.visibility = View.VISIBLE
                                        phoneEdit!!.visibility = View.VISIBLE
                                        proceedBtn.text = "Proceed"
                                    }
                                }
                            }.show()
                        } catch (e:Throwable){
                            alert {
                                isCancelable =false
                                title = "Verification Failed "
                                message = enuguLookupResponse.message!!
                                okButton { }

                            }.show()
                        }




                    }

                    override fun onError(e: Throwable) {
                        context!!.runOnUiThread { validationDialogue.show() }
                        Log.i("EnuguFailureResponse>>>>>", Gson().toJson(e.printStackTrace()))
                        alert {
                            title = "Validation Status"
                            message = e.message!!
                            okButton {  }
                        }.show()
                    }
                    override fun onComplete() {

                    }
                })






    }

    private fun validateEkoMeter(meter : String) {

        val ekoDetails = EkoModel.EkoLookupDetails(meter)
        Log.i("ekoDetails", Gson().toJson(ekoDetails))
       context!!.payviceServices.EkoLookup(ekoDetails)
               .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<EkoModel.EkoLookUpResponse> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(ekoLookupResponse: EkoModel.EkoLookUpResponse) {

                        context!!.runOnUiThread { validationDialogue.dismiss() }
                        Log.i("ekoLookupResponse>>>>>", Gson().toJson(ekoLookupResponse))
                        alert {
                            if (ekoLookupResponse.error){
                                title = ekoLookupResponse.message
                            message = ekoLookupResponse.response!!
                            okButton { }
                        }else {

                                var accountNumber: String = if (ekoLookupResponse.accountNumber.toString() == null)
                                    ekoLookupResponse.meterNumber.toString()
                                else
                                    ekoLookupResponse.accountNumber.toString()

                                var accountType: String = if (ekoLookupResponse.account_type.toString().isEmpty())
                                    "\n \n"
                                else
                                    "\n \nAccount Type - ${ekoLookupResponse.account_type}"
                                meterName =ekoLookupResponse.name
                                meterNumber =accountNumber
                                productCode =""
                                title = ekoLookupResponse.message
                                SecureStorage.store("currentName", meterName)
                                message = "\n Customer Name - ${ meterName}" +
                                            accountType +
                                        "\n \nMeter Number - ${accountNumber}" +
                                        "\n \nAddress - ${ekoLookupResponse.address}" +
                                        "\n \nBusiness District - ${ekoLookupResponse.businessDistrict}"
//
                                okButton {
                                    naira_signRV!!.visibility = View.VISIBLE
                                    phoneEdit!!.visibility = View.VISIBLE
                                    proceedBtn.text = "Proceed"
                                }
                            }
                        }.show()



                    }

                    override fun onError(e: Throwable) {
                        context!!.runOnUiThread { validationDialogue.show() }
                        Log.i("ekoFailureResponse>>>>>", Gson().toJson(e.printStackTrace()))
                        alert {
                            title = "Validation Status"
                            message = e.message!!
                            okButton {  }
                        }.show()
                    }

                    override fun onComplete() {

                    }
                })


    }


    companion object {

        fun newInstance(service: String): BillValueInputFragment {
            val fragment = BillValueInputFragment()
            val args = Bundle()
            args.putString(BaseBillFragment.SERVICE, service)
            fragment.arguments = args
            return fragment
        }
    }


}// Required empty public constructor


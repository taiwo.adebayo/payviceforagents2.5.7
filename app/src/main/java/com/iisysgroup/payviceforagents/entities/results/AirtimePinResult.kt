package com.iisysgroup.payviceforagents.entities.results

import android.util.Log
import com.google.gson.Gson
import com.iisgroup.bluetoothprinterlib.Manaer.PrintfManager
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.StatusObject
import com.iisysgroup.payviceforagents.util.toCurrencyString
import com.itex.richard.payviceconnect.model.AirtimeModel


/**
 * Created by Olije Favour on 5/6/2019.
 *Copyright (c) 2019  Itex Integrated Services  All rights reserved.
 */


class AirtimePinResult(val data: ServiceResult.Data,
                       val airtimeResponse: AirtimeModel.AirtimePinResponse) : ServiceResult {

    override fun toStatusObject(): StatusObject {

        Log.d("airtimeResponse.>>>", Gson().toJson(airtimeResponse))
        val statusObject = StatusObject(!airtimeResponse.error)
//        if (!airtimeResponse.error && (airtimeResponse.product!! !==null)) {
//            statusObject.statusReason = "Status : + ${airtimeResponse.message} \n\n " +
//                    "PIN : + ${airtimeResponse.product!!.pin}" +
//                    "Serial : + ${airtimeResponse.product!!.pin}"
//        }else{
            statusObject.statusReason =  airtimeResponse.message
//        }

        return statusObject
    }

    override fun printResult(printer: PrintfManager) {
        printer.printTabSpace(7)
        printer.printText("Airtime PIN Purchase")
        printer.printfWrap(2)

        val status = if (!airtimeResponse.error) {
            StatusObject.STATUS_APPROVED
        } else {
            StatusObject.STATUS_DECLINED
        }

        printer.printTabSpace(6)
        printer.printText(status.toUpperCase())
        printer.printfWrap()

        printer.printTwoColumn("Network", data.beneficiary.assignedProduct)
        printer.printTwoColumn("Phone Number", data.beneficiary.displayInfo)
        printer.printTwoColumn("Date", airtimeResponse.date)
        printer.printTwoColumn("Reference", airtimeResponse.ref)

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap()

        printer.printTabSpace(10)
        printer.printText("N ${data.amount.toCurrencyString()}")
        printer.printfWrap()

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap(2)

        printer.printText(airtimeResponse.message)
        printer.printfWrap()
    }

    override fun getPrinterLogo(): Int? {
        return null
    }
}
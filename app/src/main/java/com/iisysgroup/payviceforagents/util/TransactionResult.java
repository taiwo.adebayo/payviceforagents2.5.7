//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.iisysgroup.payviceforagents.util;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import com.iisysgroup.poslib.host.Host.TransactionType;

import java.io.Serializable;

@Entity
public class TransactionResult implements Serializable {
    @PrimaryKey
    @NonNull
    public String RRN = "";
    public String authID = "";
    public String STAN = "";
    public String PAN = "";
    public TransactionType transactionType;
    public String transactionStatus = "";
    public String responseCode = "";
    public String transactionStatusReason = "";
    public String accountType = "";
    public String batchNumber = "";
    public String merchantID = "";
    public String isoTransmissionDateTime = "";
    public String terminalID = "";
    public String originalForwardingInstitutionCode = "";
    public String cardHolderName = "";
    public String cardExpiry = "";
    public long amount;
    public long additionalAmount;
    public long longDateTime;
    private boolean isRolledBack = false;
    @Ignore
    public String issuerAuthData91;
    @Ignore
    public String issuerScript71;
    @Ignore
    public String issuerScript72;

    public TransactionResult() {
    }

    public String toString() {
        return "TERMINAL ID: " + this.terminalID + "\nAUTH ID: " + this.authID + "\nSTAN: " + this.STAN + "\nRRN: " + this.RRN + "\nCARDHOLDER: " + this.cardHolderName + "\nPAN: " + this.PAN + "\nEXPIRY DATE: " + this.cardExpiry + "\nAMOUNT: " + this.amount + "\nADDITIONAL AMOUNT: " + this.additionalAmount + "\nTRANSACTION TYPE: " + this.transactionType + "\nRESPONSE CODE: " + this.responseCode + "\nTRANSACTION STATUS: " + this.transactionStatus + "\nTRANSACTION STATUS REASON: " + this.transactionStatusReason + "\nACCOUNT TYPE: " + this.accountType + "\nBATCH NUMBER: " + this.batchNumber + "\nMERCHANT ID: " + this.merchantID + "\nOriginal Forwarding Institution Code: " + this.originalForwardingInstitutionCode + "\nISO TRANSMISSION DATE & TIME: " + this.isoTransmissionDateTime + "\n";
    }

    public boolean isApproved() {
        return this.responseCode.trim().equals("00");
    }

    public boolean isRolledBack() {
        return this.isRolledBack;
    }

    public void setRolledBack(boolean rolledBack) {
        this.isRolledBack = rolledBack;
    }
}

package com.iisysgroup.payviceforagents.external;

import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.iisysgroup.payviceforagents.R;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */

public class CardNameFragment extends CreditCardFragment {
    private EditText mCardNameView;

    public CardNameFragment() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle state) {
        View v = inflater.inflate(R.layout.lyt_card_holder_name, group, false);
        this.mCardNameView = (EditText) v.findViewById(com.cooltechworks.creditcarddesign.R.id.card_name);
        this.mCardNameView.addTextChangedListener(this);
        String name = "";
        if (this.getArguments() != null && this.getArguments().containsKey("card_holder_name")) {
            name = this.getArguments().getString("card_holder_name");
        }

        if (name == null) {
            name = "";
        }

        this.mCardNameView.setText(name);
        return v;
    }

    public void afterTextChanged(Editable s) {
        this.onEdit(s.toString());
        if (s.length() == 16) {
            this.onComplete();
        }

    }

    public void focus() {
        if (this.isAdded()) {
            this.mCardNameView.selectAll();
        }

    }
}

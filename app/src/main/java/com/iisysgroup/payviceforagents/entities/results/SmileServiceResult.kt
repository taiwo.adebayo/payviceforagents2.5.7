package com.iisysgroup.payviceforagents.entities.results

import com.iisgroup.bluetoothprinterlib.Manaer.PrintfManager
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.StatusObject
import com.iisysgroup.payviceforagents.util.isApproved
import com.iisysgroup.payviceforagents.util.toCurrencyString
import com.itex.richard.payviceconnect.model.SmileModel

class SmileServiceResult(private val data: ServiceResult.Data,
                         private val bundle: String? = null,
                         private val smileResponse: SmileModel.SmileSuccessResponse) : ServiceResult {

    override fun toStatusObject(): StatusObject {
        val statusObject = StatusObject(smileResponse.isApproved())
        statusObject.statusReason = smileResponse.message
        return statusObject
    }

    override fun printResult(printer: PrintfManager) {
        printer.printTabSpace(7)
        printer.printText(data.beneficiary.assignedProduct)
        printer.printfWrap(2)

        val status = if (smileResponse.isApproved()) {
            StatusObject.STATUS_APPROVED
        } else {
            StatusObject.STATUS_DECLINED
        }

        printer.printTabSpace(6)
        printer.printText(status.toUpperCase())
        printer.printfWrap()

        printer.printTwoColumn("Account Name", data.beneficiary.displayInfo)
        printer.printTwoColumn("Account ID", data.beneficiary.data)

        bundle?.let {
            printer.printTwoColumn("Purchased Bundle", it)
        }

//        printer.printTwoColumn("Date", response.date)
        printer.printTwoColumn("Reference", smileResponse.reference)

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap()

        printer.printTabSpace(10)
        printer.printText("N ${data.amount.toCurrencyString()}")
        printer.printfWrap()

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap(2)

        printer.printText(smileResponse.message)
        printer.printfWrap()
    }

    override fun getPrinterLogo(): Int? {
        return R.drawable.smile
    }
}
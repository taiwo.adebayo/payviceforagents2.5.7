package com.iisysgroup.payvicegamesdk.utils;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Bamitale @Itex on 2/26/2016.
 */
public class CardUtils {


    public static final KeyCrypto getKeyCrypto(String clrKey) throws Exception {
        byte dKey[] = Arrays.copyOf(hash512(clrKey), KeyCrypto.AES_MIN_KEY_LENGTH);

        return new KeyCrypto(dKey, "RC4", "RC4");
    }

    public static String turnToLV(String string) {
        if (string == null)
            throw new IllegalArgumentException("input cannot be null");

        int len = string.length();

        String lv = pad(len + "", 3, '0', true);

        string = lv + string;

        return string;
    }

    public static final String pad(String string, int maxLength, String padding, boolean addPaddingToLeft){
        if (string == null)
            throw new IllegalArgumentException("input cannot be null");

        if (string.length() >= maxLength)
            return string;

        String pads = "";

        int requiredLength = maxLength - string.length();

        for (int i = 0; i < requiredLength; i++) {
            pads += padding;
        }

        if (addPaddingToLeft)
            string = pads + string.trim();
        else
            string = string.trim() + pads;

        return string;
    }

    public static final String pad(String string, int maxLength, char padding, boolean addPaddingToLeft) {
        return pad(string, maxLength, String.valueOf(padding), addPaddingToLeft);
    }


    public static final String decodeLV(String string, int lv_length) {
        if (string == null)
            throw new IllegalArgumentException("input cannot be null");

        int len = Integer.parseInt(string.substring(0, lv_length));

        string = string.substring(lv_length, len + lv_length);

        return string;
    }

    public static byte[] hash512(String input) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");

            return messageDigest.digest(input.getBytes("UTF-8"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public static String processPan(String pan) {
        String acqCode = pan.substring(0, 6);
        String endCode = pan.substring(pan.length() - 4, pan.length());

        int elen = pan.length() - acqCode.length() - endCode.length();

        String etext = pad("*", elen, '*', true);

        String newPan = acqCode + etext + endCode;

        return newPan;
    }

    public static String normalizeCardNumber(String number) {
        return number == null ? null : number.trim().replaceAll("\\s+|-", "");
    }


}

package com.iisysgroup.payviceforagents.util;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.callbacks.OnRecyclerViewItemClickListener;

import java.util.List;


/**
 * Created by Bamitale @Itex on 11/12/2015.
 */
public class ImageTitleObject {

    public int icon;
    public String title;
    public String description;

    public ImageTitleObject(int icon, String title, String description) {
        this.icon = icon;
        this.title = title;
        this.description = description;
    }

    public ImageTitleObject(int icon, String title) {
        this(icon, title, "");
    }


    public static class SimpleImageObjectAdapter extends RecyclerView.Adapter<ImageTitleViewHolder> {
        List<ImageTitleObject> items;
        Context context;
        OnRecyclerViewItemClickListener itemClickListener;

        RecyclerView recyclerView;

        public SimpleImageObjectAdapter(List<ImageTitleObject> items, Context context, OnRecyclerViewItemClickListener itemClickListener) {

            this.items = items;
            this.context = context;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public ImageTitleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.bill_image_item, parent, false);

            return new ImageTitleViewHolder(view);
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
            this.recyclerView = recyclerView;
        }

        @Override
        public void onBindViewHolder(final ImageTitleViewHolder holder, final int position) {
            ImageTitleObject object = items.get(position);
            holder.bind(object);

            holder.containerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(recyclerView, position);
                }
            });
        }


        public ImageTitleObject getItem(int position) {
            return items.get(position);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

    }


    public static class ImageTitleViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView imageView;
        AppCompatTextView titleView, descriptionView;
        View containerView;

        public ImageTitleViewHolder(View itemView) {
            super(itemView);
            containerView = itemView;
            imageView = itemView.findViewById(R.id.imageView);
//            titleView = itemView.findViewById(R.id.primaryTitleText);
            descriptionView = itemView.findViewById(R.id.descriptionText);
        }

        public void bind(ImageTitleObject imageTitleObject) {
            imageView.setImageResource(imageTitleObject.icon);
        }
    }


    public static class ImageTitleListAdapter extends BaseAdapter {
        List<ImageTitleObject> items;
        Context context;

        public ImageTitleListAdapter(List<ImageTitleObject> items, Context context) {
            this.items = items;
            this.context = context;
        }


        @Override
        public int getCount() {
            return items.size();
        }


        @Override
        public Object getItem(int position) {
            return items.get(position);
        }


        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = LayoutInflater.from(context).inflate(R.layout.service_list_item, parent, false);

            AppCompatImageView imageView = rootView.findViewById(R.id.imageView);

            if (items.get(position).icon > -1)
                imageView.setImageResource(items.get(position).icon);

            TextView titleView = rootView.findViewById(R.id.primaryTitleText);
            titleView.setText(items.get(position).title);
            AppCompatTextView descriptionView = rootView.findViewById(R.id.descriptionText);
            descriptionView.setText(items.get(position).description);
            return rootView;
        }
    }
}

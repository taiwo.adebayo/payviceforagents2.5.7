package com.iisysgroup.payviceforagents.entities

import com.iisgroup.bluetoothprinterlib.Manaer.PrintfManager
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary
import java.io.Serializable

interface ServiceResult : Serializable {
    fun toStatusObject(): StatusObject
    fun printResult(printer: PrintfManager)
    fun getPrinterLogo(): Int?


    data class Data(val beneficiary: Beneficiary, val amount: Int) : Serializable
}
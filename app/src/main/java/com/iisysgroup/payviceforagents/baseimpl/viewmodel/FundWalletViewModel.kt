package com.iisysgroup.payviceforagents.baseimpl.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.iisysgroup.payviceforagents.base.presenter.FundWalletPresenter
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.results.FundWalletResult
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.util.processTransaction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FundWalletViewModel : ViewModel(), FundWalletPresenter {
    private val error = MutableLiveData<Throwable>()
    private val progressDialog = MutableLiveData<Pair<Boolean, String>>()
    private val response = MutableLiveData<ServiceResult>()

    override fun fund(amount: Int, card: Card?, processor: DebitCardProcessor) {
        processor.processTransaction(amount, card)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({result->
                    result?.let { tamsResponse ->
                        response.value = FundWalletResult(amount, tamsResponse)

                } }, {
                    error ->
                })
    }


    override fun onFundResponse(): LiveData<ServiceResult> = response

    override fun setError(throwable: Throwable) {
        error.value = throwable
    }

    override fun onError(): LiveData<Throwable> = error

    override fun showProgress(show: Boolean, message: String) {
        progressDialog.postValue(Pair(show, message))
    }

    override fun onProgressUpdate(): LiveData<Pair<Boolean, String>> = progressDialog
}
package com.iisysgroup.payviceforagents.util

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log

/**
 * Created by Itex-PC on 25/02/2019.
 */



abstract class PaginationScrollListiner(val linearLayoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        Log.i(" dx",  dx.toString())
        Log.i("dy", dy.toString())

        var totalItemCount: Int = linearLayoutManager.itemCount
        var visibleItemCount: Int = linearLayoutManager.childCount
        var firstVisibleItem: Int = linearLayoutManager.findFirstVisibleItemPosition()

//        Log.d("isLoading.toString() outside the abstract", isLoading().toString())
//        Log.d("dy.toString() outside the abstract",dy.toString())
//        Log.d("(dy > 0).toString() outside the abstract",(dy > 0).toString())
//        Log.d("isLastPage().toString() outside the abstract",isLastPage().toString())
        if (dy > 0){
//                    if (!isLoading() && !isLastPage()) {
            if (!isLoading() && !isLastPage()) {

//                        Log.i("isLoading inside the abstract", isLoading().toString())
//                        Log.i("isLastPage inside the abstract", isLastPage().toString())

                        if ((visibleItemCount + firstVisibleItem) >= totalItemCount && firstVisibleItem >= 0) {

//                            Log.i("visibleItemCount inside the abstract ", visibleItemCount.toString())
//                            Log.i("firstVisibleItem inside the abstract", firstVisibleItem.toString())
//
//                            Log.i("totalItemCount inside the abstract", totalItemCount.toString())
//
//                            Log.i("visibleItemCount + firstVisibleItem inside the abstract ", (visibleItemCount + firstVisibleItem).toString())

                            loadMoreItems()
                        }
                    }
                }

    }

    abstract fun isLastPage(): Boolean
    abstract fun isLoading(): Boolean
    abstract fun loadMoreItems()

}

package com.iisysgroup.payviceforagents.fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.iisysgroup.payviceforagents.activities.MainActivity;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.RestWrapper;

public class RecoverPasswordFragment extends Fragment {


    DialogInterface.OnClickListener action = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            NavUtils.navigateUpTo(getActivity(), new Intent(getActivity(), MainActivity.class));
        }
    };


    public RecoverPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.recover_password_layout, container, false);

        getActivity().setTitle("Password Recovery");

        final EditText emailEdit = view.findViewById(R.id.emailEdit);

        final TextInputLayout userIdTIL = view.findViewById(R.id.userIdTIL),
                emailTIL = view.findViewById(R.id.emailTIL);

        view.findViewById(R.id.recover_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userId = emailEdit.getText().toString();
                if (!(Helper.validateEmail(userId) || PhoneNumberUtils.isGlobalPhoneNumber(userId))) {
                    emailTIL.setErrorEnabled(true);
                    emailEdit.setError("Invalid input");
                    emailEdit.requestFocus();
                    Helper.showErrorAnim(emailEdit);
                } else {
                    if (Helper.hasInternetConnectivity(getActivity())) {
                        final ProgressDialog progressDialog =
                                ProgressDialog.show(getActivity(), null, "Processing... Please Wait", true, false);

                        final String userid = emailEdit.getText().toString();

                        final String tamsAction = "RESET";

                        final RestWrapper restWrapper = new RestWrapper(getString(R.string.recover_password_url));
                        restWrapper.addParam("action", tamsAction);
                        restWrapper.addParam("usrEmail", userid.trim());
                        restWrapper.addParam("usrName", userid.trim());
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String response = "";
                                try {
                                    response = restWrapper.processRequest(RestWrapper.Request.GET);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    response = "An error occurred. Please try again";
                                } finally {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressDialog.dismiss();
                                        }
                                    });
                                    Helper.showInfoDialogWithAction(getActivity(), "Recover Password", response, action);

                                }
                            }
                        }).start();

                    }
                }
            }

        });

        return view;
    }


}

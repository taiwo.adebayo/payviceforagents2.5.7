package com.iisysgroup.payviceforagents.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.iisysgroup.payviceforagents.activities.MainActivity;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.activities.PromoOffer;

import java.util.Map;

public class PayviceFirebaseMessagingService extends FirebaseMessagingService {
    public static final String APP_DEFAULT_CHANNEL = "Default";
    private static String TAG = "PayviceMsgService";
    private static int NOTIFICATION_REQUEST_CODE = 0;
    private static String APP_UPDATE_KEY = "app_update";
    private static String APP_PROMO_KEY = "app_promo";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        sendNotification(remoteMessage.getNotification(), remoteMessage.getData());
    }


    private void sendNotification(RemoteMessage.Notification remoteNotification, Map<String, String> data) {
        Intent intent;
        if (data.containsKey(APP_UPDATE_KEY) && data.get(APP_UPDATE_KEY).equalsIgnoreCase("Yes")) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + this.getPackageName()));
        } else if (data.containsKey(APP_PROMO_KEY) && data.get(APP_PROMO_KEY).equalsIgnoreCase("Yes")) {
            intent = new Intent(this, PromoOffer.class);
        } else {
            intent = new Intent(this, MainActivity.class);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, NOTIFICATION_REQUEST_CODE, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String message = remoteNotification.getBody();
        String title = remoteNotification.getTitle();

        Log.i(TAG, "Notification Data");
        for (Map.Entry<String, String> entry : data.entrySet()) {
            Log.i(TAG, entry.getKey() + " - " + entry.getValue());
        }

        Notification notification = new NotificationCompat.Builder(this, APP_DEFAULT_CHANNEL)
                .setColor(ContextCompat.getColor(this, R.color.accent))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .setBigContentTitle(title).bigText(message))
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSound(soundUri)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_REQUEST_CODE, notification);
    }
}

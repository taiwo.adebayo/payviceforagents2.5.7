package com.iisysgroup.payviceforagents.external;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */

import com.cooltechworks.creditcarddesign.R.drawable;
import com.iisysgroup.payviceforagents.R;

public class CardSelector {
    public static final CardSelector VISA;
    public static final CardSelector MASTER;
    public static final CardSelector AMEX;
    public static final CardSelector DEFAULT;
    public static final CardSelector VERVE;
    private static final String AMEX_PREFIX = "3";

    static {
        VISA = new CardSelector(drawable.card_color_round_rect_purple, drawable.chip, drawable.chip_inner, 17170445, drawable.ic_billing_visa_logo);
        MASTER = new CardSelector(drawable.card_color_round_rect_pink, drawable.chip_yellow, drawable.chip_yellow_inner, 17170445, drawable.ic_billing_mastercard_logo);
        AMEX = new CardSelector(drawable.card_color_round_rect_green, 17170445, 17170445, drawable.img_amex_center_face, drawable.ic_billing_amex_logo1);
        DEFAULT = new CardSelector(drawable.card_color_round_rect_default, drawable.chip, drawable.chip_inner, 17170445, 17170445);
        VERVE = new CardSelector(drawable.card_color_round_rect_brown, drawable.chip, drawable.chip_inner, 17170445, R.drawable.verve_logo);

    }

    private int mResCardId;
    private int mResChipOuterId;
    private int mResChipInnerId;
    private int mResCenterImageId;
    private int mResLogoId;

    public CardSelector(int mDrawableCard, int mDrawableChipOuter, int mDrawableChipInner, int mDrawableCenterImage, int logoId) {
        this.mResCardId = mDrawableCard;
        this.mResChipOuterId = mDrawableChipOuter;
        this.mResChipInnerId = mDrawableChipInner;
        this.mResCenterImageId = mDrawableCenterImage;
        this.mResLogoId = logoId;
    }

    public static CardSelector selectCard(char cardFirstChar) {
        switch (cardFirstChar) {
            case '3':
                return AMEX;
            case '4':
                return VISA;
            case '2':
            case '5':
                return MASTER;
            default:
                return DEFAULT;
        }
    }

    public static CardSelector selectCard(String cardNumber) {
        if (cardNumber != null && cardNumber.length() >= 3) {
            CardSelector selector = selectCard(cardNumber.charAt(0));
            if (cardNumber.startsWith("3")) {
                return AMEX;
            }

            if (cardNumber.startsWith("5061") || cardNumber.startsWith("5612")) {
                return VERVE;
            }

            if (selector != DEFAULT) {
                int[] drawables = new int[]{drawable.card_color_round_rect_brown, drawable.card_color_round_rect_green, drawable.card_color_round_rect_pink, drawable.card_color_round_rect_purple, drawable.card_color_round_rect_blue};
                int hash = cardNumber.substring(0, 3).hashCode();
                if (hash < 0) {
                    hash *= -1;
                }

                int index = hash % drawables.length;
                int chipIndex = hash % 3;
                int[] chipOuter = new int[]{drawable.chip, drawable.chip_yellow, 17170445};
                int[] chipInner = new int[]{drawable.chip_inner, drawable.chip_yellow_inner, 17170445};
                selector.setResCardId(drawables[index]);
                selector.setResChipOuterId(chipOuter[chipIndex]);
                selector.setResChipInnerId(chipInner[chipIndex]);
                return selector;
            }
        }

        return DEFAULT;
    }

    public int getResCardId() {
        return this.mResCardId;
    }

    public void setResCardId(int mResCardId) {
        this.mResCardId = mResCardId;
    }

    public int getResChipOuterId() {
        return this.mResChipOuterId;
    }

    public void setResChipOuterId(int mResChipOuterId) {
        this.mResChipOuterId = mResChipOuterId;
    }

    public int getResChipInnerId() {
        return this.mResChipInnerId;
    }

    public void setResChipInnerId(int mResChipInnerId) {
        this.mResChipInnerId = mResChipInnerId;
    }

    public int getResCenterImageId() {
        return this.mResCenterImageId;
    }

    public void setResCenterImageId(int mResCenterImageId) {
        this.mResCenterImageId = mResCenterImageId;
    }

    public int getResLogoId() {
        return this.mResLogoId;
    }

    public void setResLogoId(int mResLogoId) {
        this.mResLogoId = mResLogoId;
    }
}

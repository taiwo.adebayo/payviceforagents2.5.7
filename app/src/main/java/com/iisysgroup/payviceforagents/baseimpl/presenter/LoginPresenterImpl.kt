package com.iisysgroup.payviceforagents.baseimpl.presenter

import com.iisysgroup.payviceforagents.base.interactor.LoginInteractor
import com.iisysgroup.payviceforagents.base.presenter.LoginPresenter
import com.iisysgroup.payviceforagents.base.view.LoginView
import com.iisysgroup.payviceforagents.entities.VasResult
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.securestorage.SecureStorageUtils
import com.iisysgroup.payviceforagents.util.Helper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class LoginPresenterImpl(private val interactor: LoginInteractor,
                         private val view: LoginView) : LoginPresenter {

    override fun login(userID: String, password: String)
    {

        if (userID.isBlank()) {
            view.setInvalidUser()
            return
        }

        if (password.isBlank()) {
            view.setInvalidPassword()
            return
        }

        view.showProgress()

        interactor.getUserInfo(userID).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result, error ->
                    error?.let(this::handleError)

                    result?.let {
                        if (result.result != VasResult.Result.APPROVED) {
                            handleError(RuntimeException(result.message))
                        } else {
                            onUserInitResponse(userID, password, result)
                        }
                    }
                }
    }

    private fun onUserInitResponse(userId: String, password: String, result: VasResult) {

        val temp = result.message.trim().split("\\|".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
        val serverKey = temp[0] + "|" + temp[1]

        SecureStorage.store(Helper.USER_KEY, serverKey)

        /*if (temp.size >= 3) {
            val returnedDeviceID = temp[2]
            val deviceID = interactor.getDeviceId()

            *//*if (returnedDeviceID != deviceID) {
                view.dismissProgress()
                view.setDeviceChangedError()
                return
            }*//*
        }*/



        val terminalId = result.macrosTID
        val userKey = Helper.decryptUserKey(terminalId, serverKey)
        SecureStorage.store(Helper.PLAIN_PASSWORD,password)
        val ePassword = SecureStorageUtils.hashIt(password, userKey)!!

        interactor.login(userId, ePassword, terminalId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    response, error ->

                    error?.let {
                        handleError(it)
                    }
                    response?.let {

                        if (temp.size == 4) {
                            val status = temp[3].trim({ it <= ' ' }).toLowerCase()
                            if (status == "yes") {
                                view.setShouldUpdatePin()

                            }
                        }

                        if (it.result == VasResult.Result.APPROVED){

                            interactor.storeLoginDetails(userId, ePassword, serverKey, result)
                            /*it.payviceModel?.let { payviceMerchantModel ->
                                interactor.storePfmData(payviceMerchantModel, it.payviceForMerchantsSummary)
                            }*/

                            view.setLoginSuccessful()
                        } else {
                            view.dismissProgress()
                            view.showMessage(it.message)

                        }




                    }
                }

    }


    private fun handleError(error: Throwable) {
        view.dismissProgress()
        view.setLoginError(error)
    }
}
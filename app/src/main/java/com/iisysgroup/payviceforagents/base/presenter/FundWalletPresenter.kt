package com.iisysgroup.payviceforagents.base.presenter

import android.arch.lifecycle.LiveData
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Card

interface FundWalletPresenter {
    fun setError(throwable: Throwable)
    fun onError(): LiveData<Throwable>

    fun showProgress(show: Boolean, message: String = "Processing...")
    fun onProgressUpdate(): LiveData<Pair<Boolean, String>>

    fun fund(amount: Int, card: Card?, processor: DebitCardProcessor)
    fun onFundResponse(): LiveData<ServiceResult>
}
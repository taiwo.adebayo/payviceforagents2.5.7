package com.iisysgroup.payviceforagents.callbacks;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Bamitale@Itex on 04/05/2017.
 */
public interface OnRecyclerViewItemClickListener {
    void onItemClick(RecyclerView recyclerView, int position);
}

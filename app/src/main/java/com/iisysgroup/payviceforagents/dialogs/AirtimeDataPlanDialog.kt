package com.iisysgroup.payviceforagents.dialogs

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.iisysgroup.payviceforagents.R
import com.itex.richard.payviceconnect.model.DataModel
import kotlinx.android.synthetic.main.fragment_plan_list_dialog_item.view.*


class AirtimeDataPlanDialog : BasePlanDialogFragment<AirtimeDataPlanDialog.ViewHolder>() {

    override fun getAdapter() = Adapter(arguments?.getParcelableArrayList(PLAN_LIST)
            ?: arrayListOf())


    inner class ViewHolder internal constructor(inflater: LayoutInflater, parent: ViewGroup)
        : RecyclerView.ViewHolder(inflater.inflate(R.layout.fragment_plan_list_dialog_item, parent, false)) {

        private val descriptionTextView = itemView.descriptionText
        private val amountTextView = itemView.amountText

        init {
            itemView.setOnClickListener {

                selector?.invoke(adapterPosition)
            }
        }

        fun bindView(item: DataModel.DataResponseElements) {
            descriptionTextView.text = item.description
            amountTextView.text = "\u20A6 ${item.amount}"
        }
    }

    inner class Adapter(private val list: ArrayList<DataModel.DataResponseElements>) :
            RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            holder.bindView(list[position])
        }

        override fun getItemCount(): Int {
            return list.size
        }


    }


    companion object {
        fun newInstance(dataList: ArrayList<DataModel.DataResponseElements>,
                        selectionHandler: (Int) -> Unit): AirtimeDataPlanDialog =
                AirtimeDataPlanDialog().apply {
                    arguments = Bundle().apply {
                        putParcelableArrayList(PLAN_LIST, dataList)
                    }

                    this.selector = selectionHandler
                }

    }
}

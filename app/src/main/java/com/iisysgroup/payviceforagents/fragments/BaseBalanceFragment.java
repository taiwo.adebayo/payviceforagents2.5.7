package com.iisysgroup.payviceforagents.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.fundwallet.FundWalletActivity;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;


/**
 * A simple {@link Fragment} subclass.
 */
public class BaseBalanceFragment extends Fragment {

    public static final String EXTRA_AMOUNT = "amount";
    static final int FUND_REQUEST = 89;

    @Nullable
    @BindView(R.id.walletBalanceLayout)
    LinearLayout walletBalanceLayout;

    @Nullable
    @BindView(R.id.walletBalanceText)
    TextView walletBalanceText;

    @Nullable
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Nullable
    @BindView(R.id.fundButton)
    CardView fundButton;

    @BindView(R.id.commissionBalanceText)
    TextView commissionBalanceText;


    Handler handler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_base_balance, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handler = new Handler();
    }

    public void onStart() {
        super.onStart();

        if (Helper.getPreference(getContext(), Helper.DOWNLOAD_BALANCE, false)) {
            getBalance();
        }
    }

    @Optional
    @OnClick(R.id.walletBalanceLayout)
    void redownloadBalance() {
        getBalance();
    }

    @Optional
    @OnClick(R.id.fundButton)
    void fundWallet() {
        startActivityForResult(new Intent(getActivity(), FundWalletActivity.class), FUND_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getBalance();
    }

    void getBalance() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (progressBar != null)
                                progressBar.setVisibility(View.VISIBLE);
                        }
                    });

                    String url = getString(R.string.tams_url);
                    String userId = SecureStorage.retrieve(Helper.USER_ID, "");
                    String terminalId = SecureStorage.retrieve(Helper.TERMINAL_ID, "");


                    String action = getString(R.string.tams_webapi_action);
                    String control = "BALANCE";
                    Map<String, String> params = new HashMap<>();
                    params.put("action", action);
                    params.put("userid", userId);
                    params.put("termid", terminalId);
                    params.put("control", control);

                    final VasResult tamsResult = Requests.processRequest(url, params);

                    if (tamsResult.result != VasResult.Result.APPROVED)
                        throw new IllegalStateException(tamsResult.message);


                    final String balance = "\u20A6 " + tamsResult.balance.replace("N", "").trim();

                    Helper.savePreference(getActivity(), Helper.BALANCE, balance);
                    Helper.savePreference(getActivity(), Helper.DOWNLOAD_BALANCE, false);

                    Helper.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (walletBalanceText != null)
                                walletBalanceText.setText(balance);

                            commissionBalanceText.setText(tamsResult.commission);
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();

                } finally {
                    Helper.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (progressBar != null)
                                progressBar.setVisibility(View.GONE);
                        }
                    });
                }
            }
        }).start();

    }

}

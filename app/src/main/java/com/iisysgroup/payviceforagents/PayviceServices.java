package com.iisysgroup.payviceforagents;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.telephony.CellInfoGsm;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.itex.richard.payviceconnect.Processes.AbujaProcess;
import com.itex.richard.payviceconnect.Processes.AirtimeProcesss;
import com.itex.richard.payviceconnect.Processes.DataProcess;
import com.itex.richard.payviceconnect.Processes.DstvProcess;
import com.itex.richard.payviceconnect.Processes.EkoProcess;
import com.itex.richard.payviceconnect.Processes.EnuguProcess;
import com.itex.richard.payviceconnect.Processes.GenesisProcess;
import com.itex.richard.payviceconnect.Processes.HistoryProcess;
import com.itex.richard.payviceconnect.Processes.IbdcProcess;
import com.itex.richard.payviceconnect.Processes.IkejaProcess;
import com.itex.richard.payviceconnect.Processes.PortharcourtProcess;
import com.itex.richard.payviceconnect.Processes.RequeryProcess;
import com.itex.richard.payviceconnect.Processes.SessionKeyProcess;
import com.itex.richard.payviceconnect.Processes.SmileProcess;
import com.itex.richard.payviceconnect.Processes.StartimesProces;
import com.itex.richard.payviceconnect.Processes.ViceBankingProcess;
import com.itex.richard.payviceconnect.model.AbujaModel;
import com.itex.richard.payviceconnect.model.DataModel;
import com.itex.richard.payviceconnect.model.DstvModel;
import com.itex.richard.payviceconnect.model.EkoModel;
import com.itex.richard.payviceconnect.model.EnuguModel;
import com.itex.richard.payviceconnect.model.Genesis;
import com.itex.richard.payviceconnect.model.HistoryModel;
import com.itex.richard.payviceconnect.model.IbadanModel;
import com.itex.richard.payviceconnect.model.IkejaModel;
import com.itex.richard.payviceconnect.model.PortharcourtModel;
import com.itex.richard.payviceconnect.model.Requery;
import com.itex.richard.payviceconnect.model.SessionKey;
import com.itex.richard.payviceconnect.model.SmileModel;
import com.itex.richard.payviceconnect.model.StartimesModel;
import com.itex.richard.payviceconnect.model.ViceBankingModel;
import com.itex.richard.payviceconnect.utils.PaviceConstants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import io.reactivex.Observable;


/**
 * Created by Richard on 6/1/2018.
 */

public class PayviceServices {

    private static PayviceServices _instance;
//    private static final String BASE_URL = "http://197.253.19.76:443/";
    private static final String BASE_URL = "http://197.253.19.75:8029/";

    private static final String GENESIS_URL = "http://197.253.19.75:8092/api/movies/";
    private PayviceServices(){ }
    Gson gson = new Gson();
    static  Context context;
    static boolean  test = false;

    //Initialize service Providers
    public static PayviceServices getInstance(Context context) {
        PayviceServices.context = context;
        if (_instance == null)
        {
            _instance = new PayviceServices();
        }
        return _instance;
    }
    public static PayviceServices getInstance(Context context, boolean isTest) {
        PayviceServices.context = context;
        if (_instance == null)
        {
            _instance = new PayviceServices();
        }

        PayviceServices.test = isTest;
        return _instance;
    }

    //Data Lookup
    public Observable<DataModel.DataLookUpResponse> DataLookup(DataModel.DataLookUpDetails lookUpDetails){
        String body = gson.toJson(lookUpDetails, DataModel.DataLookUpDetails.class);
        DataProcess dataProcess = new DataProcess();
        return dataProcess.lookUp(BASE_URL + "vas/data/lookup", body);
    }


    //Data subsribe
    public Observable<DataModel.DataSubscriptionResponse> DataSubscribe(DataModel.DataSubscriptionDetails details){
        String body = gson.toJson(details, DataModel.DataSubscriptionDetails.class);
        DataProcess dataProcess = new DataProcess();
        return dataProcess.subscribe(BASE_URL + "vas/data/subscribe", body);

    }

    //Data subscribe with card
    public Observable<DataModel.DataSubscriptionResponse> DataSubscribeWithCard(DataModel.DataSubscriptionDetails details){
        String body = gson.toJson(details, DataModel.DataSubscriptionDetails.class);
        DataProcess dataProcess = new DataProcess();
       return  dataProcess.subscribe(BASE_URL + "vas/card/data/subscribe", body);
    }

    //Dstv Loookup
    public Observable<DstvModel.DstvResponse> DstvLookup(DstvModel.DstvLookup lookupDetails){
        String body = gson.toJson(lookupDetails, DstvModel.DstvLookup.class);
        DstvProcess dstvProcess = new DstvProcess();
       return dstvProcess.lookUp(BASE_URL + "vas/multichoice/lookup", body);


    }
    public Observable<DstvModel.DstvResponse> dstvValidate(DstvModel.DstvLookup lookupDetails){
        String body = gson.toJson(lookupDetails, DstvModel.DstvLookup.class);
        DstvProcess dstvProcess = new DstvProcess();
        return dstvProcess.lookUp(BASE_URL + "vas/multichoice/validate", body);
    }


    //Subscribe DsTv
    public Observable<DstvModel.PayResponse> DstvSubscribe(DstvModel.PayDetails details){
        if(test){
            String body = gson.toJson(details , DstvModel.PayDetails.class);
            DstvProcess dstvProcess = new DstvProcess();
            return dstvProcess.subscribe(BASE_URL + "vas/multichoice/test/tpay",body);
        }
        String body = gson.toJson(details , DstvModel.PayDetails.class);
        DstvProcess dstvProcess = new DstvProcess();
        return dstvProcess.subscribe(BASE_URL + "vas/multichoice/pay",body);

    }



    //Subscribe Dstv with card
    public Observable<DstvModel.PayResponse> DstvSubscribeWithCard(DstvModel.PayDetails details){
        if(test){
            String body = gson.toJson(details , DstvModel.PayDetails.class);
            DstvProcess dstvProcess = new DstvProcess();
            return dstvProcess.subscribe(BASE_URL + "vas/card/multichoice/test/pay",body);
        }
        String body = gson.toJson(details , DstvModel.PayDetails.class);
        DstvProcess dstvProcess = new DstvProcess();
        return dstvProcess.subscribe(BASE_URL + "vas/card/multichoice/pay",body);
    }

    //Eko look up
    public Observable<EkoModel.EkoLookUpResponse> EkoLookup(EkoModel.EkoLookupDetails details){
        String body = gson.toJson(details, EkoModel.EkoLookupDetails.class);
        EkoProcess ekoProcess = new EkoProcess();
       return ekoProcess.lookUp(BASE_URL + "ekedc/vas/validate", body);
    }

    //Eko pay bills
    public Observable<EkoModel.EkoPayResponse> EkoPayBill(EkoModel.EkoPayDetails details){
        String body = gson.toJson(details, EkoModel.EkoPayDetails.class);
        EkoProcess ekoProcess = new EkoProcess();
       return ekoProcess.paybills(BASE_URL + "ekedc/vas/payment/process", body);

    }

    //Portharcourt lookup
    public Observable<PortharcourtModel.lookUpResponse> portharcourtLookup(PortharcourtModel.LookupDetails lookupDetails){
        String body = new Gson().toJson(lookupDetails, PortharcourtModel.LookupDetails.class);
        PortharcourtProcess portharcourtProcess = new PortharcourtProcess();
        return  portharcourtProcess.lookUp(BASE_URL +"vas/phed/validation", body);
    }

    //Portharcourt payment
    public Observable<PortharcourtModel.purchaseResponse> portharcourtPayBill(PortharcourtModel.purchaseDetails lookupDetails){
        String body = new Gson().toJson(lookupDetails, PortharcourtModel.purchaseDetails.class);
        PortharcourtProcess portharcourtProcess = new PortharcourtProcess();
        return  portharcourtProcess.payBill(BASE_URL +"vas/phed/payment", body);
    }


    //Abuja lookup
    public Observable<AbujaModel.LookUpResponse> abujaLookup(AbujaModel.LookupDetails lookupDetails){
        Log.d("lookupDetails>>>>" , new Gson().toJson(lookupDetails));

        String body = new Gson().toJson(lookupDetails, AbujaModel.LookupDetails.class);
        Log.d("lookupDetails " , body);
        AbujaProcess abujaProcess = new AbujaProcess();
        return  abujaProcess.lookup(BASE_URL +"vas/abuja/validation", body);
    }


    //Abuja  Payment
    public Observable<AbujaModel.PurchaseResponse> abujaPayBill(AbujaModel.PurchaseDetails purchaseDetails){
        String body = new Gson().toJson(purchaseDetails, AbujaModel.PurchaseDetails.class);
        AbujaProcess abujaProcess = new AbujaProcess();
        return  abujaProcess.payBill(BASE_URL +"vas/abuja/payment", body);
    }


    //Ikeja Lookup
    public Observable<IkejaModel.IkejaLookupResponse> IkejaLookupPrepaid(IkejaModel.IkejaLookupDetailsPrepaid details){
        String body = gson.toJson(details, IkejaModel.IkejaLookupDetailsPrepaid.class);
        IkejaProcess ikejaProcess = new IkejaProcess();
        Log.i("okh", body);
        return ikejaProcess.lookUp(BASE_URL + "vas/ie/validate", body);
    }

    public Observable<IkejaModel.IkejaLookupResponse> IkejaLookupPostpaid(IkejaModel.IkejaLookupDetailsPostPaid details){
        String body = gson.toJson(details, IkejaModel.IkejaLookupDetailsPostPaid.class);
        IkejaProcess ikejaProcess = new IkejaProcess();
        Log.i("okh", body);
        return ikejaProcess.lookUp(BASE_URL + "vas/ie/validate", body);
    }


    //Session Lookup
    public Observable<SessionKey.Response> ObtainSessionKey(SessionKey.Request details){
        String body = gson.toJson(details, SessionKey.Request.class);
        SessionKeyProcess sessionKeyProcess = new SessionKeyProcess();
        Log.i("okh", body);
        return sessionKeyProcess.getKey(BASE_URL + "api/v1/account/generate-session-key", body);
    }

    //Ikeja PayBills
    public Observable<IkejaModel.IkejaPayResponse> IkejaPayBillPrepaid(IkejaModel.IkejaPayDetailsPrepaid details){
        String body = gson.toJson(details, IkejaModel.IkejaPayDetailsPrepaid.class);
        IkejaProcess ikejaProcess = new IkejaProcess();
        return ikejaProcess.paybills(BASE_URL + "vas/ie/purchase", body);

    }

    //Ikeja PayBills
    public Observable<IkejaModel.IkejaPayResponse> IkejaPayBillPostPaid(IkejaModel.IkejaPayDetailsPostPaid details){
        String body = gson.toJson(details, IkejaModel.IkejaPayDetailsPostPaid.class);
        IkejaProcess ikejaProcess = new IkejaProcess();
        return ikejaProcess.paybills(BASE_URL + "vas/ie/purchase", body);

    }




    //Ikeja payBill with Card
//    public Observable<IkejaModel.IkejaPayResponse> IkejaPayBillWithCard(IkejaModel.IkejaPayDetails details){
//        String body = gson.toJson(details, IkejaModel.IkejaPayDetails.class);
//        IkejaProcess ikejaProcess = new IkejaProcess();
//       return ikejaProcess.paybills(BASE_URL + "vas/card/ie/purchase", body);
//
//    }

     //Smile get smile bundle
    public Observable<SmileModel.SmileGetBundleResponse> SmileGetBundles(SmileModel.SmileGetBudlesRequest details){
        String body = gson.toJson(details, SmileModel.SmileGetBudlesRequest.class);
        SmileProcess smileProcess = new SmileProcess();
        return smileProcess.getBundleResponseObservable(BASE_URL + "smile/get-bundles",body);
    }

    //Smile subscribe to a bundle
    public Observable<SmileModel.SmileSuccessResponse> SmileSubscribe(SmileModel.SmileSubscribe details){
        if(test){
            String body = gson.toJson(details, SmileModel.SmileSubscribe.class);
            SmileProcess smileProcess = new SmileProcess();
            return smileProcess.subscribeSmile(BASE_URL + "smile/api-test/buy-bundle",body);
        }
        String body = gson.toJson(details, SmileModel.SmileSubscribe.class);
        SmileProcess smileProcess = new SmileProcess();
        return smileProcess.subscribeSmile(BASE_URL + "smile/buy-bundle",body);
    }

    //Smile top up
    public Observable<SmileModel.SmileSuccessResponse> SmileTopUp(SmileModel.SmileTopUpRequestDetails details){
        if(test){
            String body = gson.toJson(details, SmileModel.SmileTopUpRequestDetails.class);
            SmileProcess smileProcess = new SmileProcess();
            return smileProcess.subscribeSmile(BASE_URL + "smile/api-test/top-up", body);
        }
        String body = gson.toJson(details, SmileModel.SmileTopUpRequestDetails.class);
        SmileProcess smileProcess = new SmileProcess();
        return smileProcess.subscribeSmile(BASE_URL + "smile/top-up", body);
    }

    //Smile Validate Customer Infomation
    public Observable<SmileModel.SmileValidateCustomerResponse> SmileValidateCustomer(SmileModel.SmileValidateCustomerRequest details){
        String body = gson.toJson(details, SmileModel.SmileValidateCustomerRequest.class);
        SmileProcess smileProcess = new SmileProcess();
        return  smileProcess.validateSmileCus(BASE_URL + "smile/validate", body);
    }

    public Observable<SmileModel.SmileSuccessResponse> SmileTranscationStatus(SmileModel.SmileTransactionStatusRequest details){
        String body = gson.toJson(details, SmileModel.SmileTransactionStatusRequest.class);
        SmileProcess smileProcess = new SmileProcess();
        return smileProcess.smileTransactionStatus(BASE_URL + "smile/transaction-status",body);
    }

    //Fetch customer all History
    public Observable<HistoryModel.fetchHistoryResponse> GetHistory(String customerId){
        HistoryProcess historyProcess = new HistoryProcess();
        return historyProcess.fetchAll(BASE_URL + "api/vas/history/" + customerId);
    }

    //Fetch customer all History
    public Observable<HistoryModel.fetchHistoryResponse> GetServiceHistory(String customerId, PaviceConstants ServiceName){
        HistoryProcess historyProcess = new HistoryProcess();
        return historyProcess.fetchAll(BASE_URL + "api/vas/history/"+ ServiceName +"/" + customerId);
    }

    public Observable<HistoryModel.TransactionDetailsResponse> GetMultichoiceTrasactionDetails(String iuc, String ref){
        HistoryProcess historyProcess = new HistoryProcess();
        return historyProcess.getTrans(BASE_URL + "vas/multichoice/get-transaction/"+ iuc + "/" + ref);
    }

    public Observable<Genesis.GenesisResponses> GenesisGetMovies(Genesis.GetMoviesRequest getMoviesRequest){
        GenesisProcess genesisProcess = new GenesisProcess();
        String body = gson.toJson(getMoviesRequest, Genesis.GetMoviesRequest.class);
        return genesisProcess.allpost(GENESIS_URL + "list", body);
    }

    public Observable<Genesis.GenesisResponses> GenesisBuyTickets(Genesis.BuyTicketRequest buyTicketRequest){
        GenesisProcess genesisProcess = new GenesisProcess();
        String body = gson.toJson(buyTicketRequest, Genesis.BuyTicketRequest.class);
        
        return genesisProcess.allpost(GENESIS_URL +"ticket/buy", body);
    }

    public Observable<Genesis.GenesisResponses> GenesisBuyTicketsCard(Genesis.BuyTicketRequest buyTicketRequest){
        GenesisProcess genesisProcess = new GenesisProcess();
        String body = gson.toJson(buyTicketRequest, Genesis.BuyTicketRequest.class);
        return genesisProcess.allpost(GENESIS_URL +"ticket/card/buy", body);
    }

    public Observable<Genesis.GenesisResponses> GenesisValidateTickets(Genesis.ValidateTicketRequest validateTicketRequest){
        GenesisProcess genesisProcess = new GenesisProcess();
        String body  = gson.toJson(validateTicketRequest, Genesis.ValidateTicketRequest.class);
        return genesisProcess.allpost(GENESIS_URL +"ticket/validate", body);
    }

    public Observable<Genesis.GenesisResponses> GenesisDeactiviateTickets(Genesis.DeactivateTicketRequest deactivateTicketRequest){
        GenesisProcess genesisProcess = new GenesisProcess();
        String body = gson.toJson(deactivateTicketRequest, Genesis.DeactivateTicketRequest.class);
        return genesisProcess.allpost(GENESIS_URL + "ticket/deactivate", body);
    }

    public Observable<Genesis.GenesisResponses> GenesisGetCinemas(){
        GenesisProcess genesisProcess = new GenesisProcess();
        return genesisProcess.allget(GENESIS_URL + "cinemas");
    }

    //Enugu Lookup
    public Observable<EnuguModel.LookupResponse> EnuguElectricValidaton(EnuguModel.LookupRequest lookupRequest){
        EnuguProcess process = new EnuguProcess();
        String body = gson.toJson(lookupRequest, EnuguModel.LookupRequest.class);
        return process.LookUp(BASE_URL + "vas/eedc/validation", body);
    }

    //Enugu Payment
    public Observable<EnuguModel.PayResponse> EnuguElectricPayment(EnuguModel.PayRequest payRequest){
        EnuguProcess process = new EnuguProcess();
        String body = gson.toJson(payRequest, EnuguModel.PayRequest.class);
        return process.pay(BASE_URL + "vas/eedc/payment", body);
    }



    //Ibadan PayBills
    public Observable<IbadanModel.IbPayResponse> ibadanPayBill(IbadanModel.IbPayRequest request){

        String body = gson.toJson(request, IbadanModel.IbPayRequest.class);
        IbdcProcess ibdcProcess = new IbdcProcess();
        return ibdcProcess.paybills(BASE_URL + "vas/ibedc/payment", body);

    }

    //Ibadan Lookup
    public Observable<IbadanModel.IbLookupResponse> IbadanLookUp(IbadanModel.IbLookupRequest response){
        String body = gson.toJson(response, IbadanModel.IbLookupRequest.class);
        IbdcProcess ibdcProcess = new IbdcProcess();
//        Log.i("okh", body);
        return ibdcProcess.lookUp(BASE_URL + "vas/ibedc/validation", body);
    }



    //Requery
    public Observable<Requery.Response> Requery(Requery.Request request){
        RequeryProcess process = new RequeryProcess();
        String body = gson.toJson(request, Requery.Request.class);
        return  process.requery(BASE_URL + "api/v1/account/requery", body);
    }

    //SesionKey Lookup
    public Observable<SessionKey.Response> GetSesionParam(SessionKey.Request request){
        SessionKeyProcess process = new SessionKeyProcess();
        String body = gson.toJson(request, SessionKey.Request.class);
        return process.getKey(BASE_URL + "api/v1/account/generate-session-key", body);
    }



    public Observable<StartimesModel.lookupResponse> StartimesLookup(StartimesModel.lookupRequest lookupRequest){
        StartimesProces process = new StartimesProces();
        String body = gson.toJson(lookupRequest, StartimesModel.lookupRequest.class);
        return  process.validate(BASE_URL + "vas/startimes/validation", body);
    }

    public Observable<StartimesModel.payResponse> StartimesPayment(StartimesModel.payRequest payRequest){
        StartimesProces process = new StartimesProces();
        String body = gson.toJson(payRequest, StartimesModel.payRequest.class);
        return  process.pay(BASE_URL + "vas/startimes/payment", body);
    }

    //Vice banking

        public Observable<ViceBankingModel.WithdrawalValidationResponse> ViceBankingWithdrawalValidation(ViceBankingModel.WithdrawalValidationRequest validationRequest){
        ViceBankingProcess process = new ViceBankingProcess();
        String body = gson.toJson(validationRequest, ViceBankingModel.WithdrawalValidationRequest.class);
        return process.withDrawValidation(BASE_URL + "vas/vice-banking/withdrawal/lookup",body);
    }

        public Observable<ViceBankingModel.WithdrawalPaymentResponse> ViceBankingWithdrawalPayment(ViceBankingModel.WithdrawalPaymentRequest paymentRequest)  {
        ViceBankingProcess process = new ViceBankingProcess();
        String body = gson.toJson(paymentRequest, ViceBankingModel.WithdrawalPaymentRequest.class);
        String base64encoded = new String(Base64.encode(body.getBytes(),Base64.DEFAULT));
        String encoded = null;
        try {
            encoded = URLEncoder.encode(base64encoded, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String nonce = Calendar.getInstance().getTimeInMillis()+ paymentRequest.getWallet() + encoded ;
        String  encryptedStuff = nonce + "IL0v3Th1sAp11111111UC4NDoV4SSWITHVICEBANKING";
        String signature = hash(encryptedStuff);
        HashMap<String, String> map = new HashMap<>();
        map.put("ITEX-Nonce", nonce);
        map.put("ITEX-Signature", signature);
        return process.withDrawPayment(BASE_URL + "vas/vice-banking/withdrawal/payment",body, map);
    }

        public Observable<ViceBankingModel.TranferValidationResponse> ViceBankingTransferValidation(ViceBankingModel.TransferValidationRequest validationRequest){
        ViceBankingProcess process = new ViceBankingProcess();
        String body = gson.toJson(validationRequest, ViceBankingModel.TransferValidationRequest.class);
        return process.transferValidation(BASE_URL + "vas/vice-banking/transfer/lookup",body);
    }
        public Observable<ViceBankingModel.TransferPaymentResponse> ViceBankingTranferPayment(ViceBankingModel.TransferPaymentRequest paymentRequest){
        ViceBankingProcess process = new ViceBankingProcess();
        String body = gson.toJson(paymentRequest, ViceBankingModel.TransferPaymentRequest.class);
        String base64encoded = new String(Base64.encode(body.getBytes(),Base64.DEFAULT));
        String encoded = null;
        try {
            encoded = URLEncoder.encode(base64encoded, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String nonce = Calendar.getInstance().getTimeInMillis()+ paymentRequest.getWallet() + encoded ;
        String  encryptedStuff = nonce + "IL0v3Th1sAp11111111UC4NDoV4SSWITHVICEBANKING";
        String signature = hash(encryptedStuff);
        HashMap<String, String> map = new HashMap<>();
        map.put("ITEX-Nonce", nonce);
        map.put("ITEX-Signature", signature);
        return process.transferPayment(BASE_URL + "vas/vice-banking/transfer/payment",body, map);
    }

    //End of vice banking








    private boolean isOnline() {
        boolean connected = false;
        try {
            ConnectivityManager  connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable() &&
                    networkInfo.isConnected();
            return connected;

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("CheckConnectivity Exception: " + e.getMessage());
        }
        return connected;
    }

    //The method below is used to generate PFMSTATE
    @SuppressLint({"MissingPermission", "NewApi"})
    private DstvModel.PfmState  getPfmState(String terminalid){
        DstvModel.PfmState pfmState;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            pfmState  = new DstvModel.PfmState(Build.getSerial(),new Date().getTime() + "", BatteryManager.EXTRA_LEVEL,
                    isCharging(),"",terminalid, getNetworkType(),getCloc(),getss(),
                    Build.MODEL,Build.MANUFACTURER,"true",Build.VERSION.SDK_INT +"",new Date().getTime() + "","NoPinLoaded");
        }
        else{
            pfmState  = new DstvModel.PfmState(Build.SERIAL,new Date().getTime() + "", BatteryManager.EXTRA_LEVEL,
                    isCharging(),"",terminalid, getNetworkType(),getCloc(),getss(),
                    Build.MODEL,Build.MANUFACTURER,"true",Build.VERSION.SDK_INT +"",new Date().getTime() + "","NoPinLoaded");

        }
        return pfmState;
    }

    @NonNull
    private  String  isCharging() {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        if( plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB || plugged == BatteryManager.BATTERY_PLUGGED_WIRELESS){
            return "Charging";
        }
        else{
            return "NotCharging";
        }
    }
    private String getNetworkType() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info.getTypeName();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private String getCloc(){
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission") NeighboringCellInfo info = tm.getNeighboringCellInfo().get(0);
        String networkOperator = tm.getNetworkOperator();
        @SuppressLint("MissingPermission") CellInfoGsm cellinfogsm = (CellInfoGsm)tm.getAllCellInfo().get(0);
        CellSignalStrengthGsm cellSignalStrengthGsm = cellinfogsm.getCellSignalStrength();
        String cloc = "{cid:"+ info.getCid() +",lac:"+ info.getLac()+",mcc:"+Integer.parseInt(networkOperator.substring(0, 3))+",mnc:"+Integer.parseInt(networkOperator.substring(3))+
                ", ss:"+cellSignalStrengthGsm.getDbm()+", longitude:000, latitude:0000}";
        return cloc;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private String getss(){
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission") NeighboringCellInfo info = tm.getNeighboringCellInfo().get(0);
        String networkOperator = tm.getNetworkOperator();
        @SuppressLint("MissingPermission") CellInfoGsm cellinfogsm = (CellInfoGsm)tm.getAllCellInfo().get(0);
        CellSignalStrengthGsm cellSignalStrengthGsm = cellinfogsm.getCellSignalStrength();
        return cellSignalStrengthGsm.getLevel() +"";
    }

  private String hash(String input) {
      MessageDigest md = null;
      try {
          md = MessageDigest.getInstance("SHA-512");
      } catch (NoSuchAlgorithmException e) {
          e.printStackTrace();
      }
      byte[] digest = md.digest(input.getBytes());
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < digest.length; i++) {
          sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
      }
      return sb.toString();
  }




}

package com.iisysgroup.payviceforagents.vas.genesis.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.vas.genesis.GenesisMovieListActivity;
import com.itex.richard.payviceconnect.model.Genesis;

import java.util.List;


public class GenesisCinemaHouseAdapter extends RecyclerView.Adapter<GenesisCinemaHouseAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    List<Genesis.CimaHouse> mData;

    public GenesisCinemaHouseAdapter(List<Genesis.CimaHouse> data, Context context){

        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cinema_list_card,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Genesis.CimaHouse cimaHouse = mData.get(position);
         holder.setData(cimaHouse);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {
        TextView name;
        LinearLayout layout;
        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.cinemaName);
            layout = itemView.findViewById(R.id.movie);
        }

        public void setData(final Genesis.CimaHouse cimaHouse) {

            name.setText(cimaHouse.getName());
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   openMovieListActivity(cimaHouse);
                }
            });
        }
    }

    private void openMovieListActivity(Genesis.CimaHouse cimaHouse){
        Intent intent = new Intent(context, GenesisMovieListActivity.class);
        intent.putExtra("cima", cimaHouse.getCinema_id());
        context.startActivity(intent);
    }
}

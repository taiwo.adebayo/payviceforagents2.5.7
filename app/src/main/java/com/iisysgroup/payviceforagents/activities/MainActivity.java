package com.iisysgroup.payviceforagents.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.TermMagmActivity;
import com.iisysgroup.payviceforagents.airtime.AirtimeActivity;
import com.iisysgroup.payviceforagents.entities.Service;
import com.iisysgroup.payviceforagents.fragments.BillListFragment;
import com.iisysgroup.payviceforagents.fragments.MainFragment;
import com.iisysgroup.payviceforagents.fragments.MainSettingsActivityFragment;
import com.iisysgroup.payviceforagents.fragments.WalletFragment;
import com.iisysgroup.payviceforagents.transfer.TransferAmountEntry;
import com.iisysgroup.payviceforagents.transfer.TransferBankSelection;
import com.iisysgroup.payviceforagents.util.FunctionsKt;
import com.iisysgroup.payviceforagents.vas.genesis.GenesisMovieActivity;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.VasServices;
import com.itex.richard.payviceconnect.model.SessionKey;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationItem;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationView;
import com.luseen.luseenbottomnavigation.BottomNavigation.OnBottomNavigationItemClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BillListFragment.OnFragmentInteractionListener {


    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.bottom_view)
    BottomNavigationView bottomView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    String[] titles;
    int[] colors;
    int[] drawables;

    FragmentPagerAdapter pagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new MainFragment();
                case 1:
                    return new BillListFragment();
                case 2:
                    return new WalletFragment();
                case 3:
                    return new MainSettingsActivityFragment();
            }

            return null;
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

    };
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        viewPager.setOffscreenPageLimit(2);
        navigationView.getMenu().getItem(0).setVisible(false);
        getSessionKey();
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        String body = "fav:4839";
        String base64encoded = new String(android.util.Base64.encode(body.getBytes(), Base64.DEFAULT));
        Log.d("base64encoded", base64encoded);
        if (Helper.getPreference(getApplication(), Helper.PASSWORD_IS_RESET, false) &&
                Helper.getPreference(getApplication(), Helper.ID_TO_RESET, "not")
                        .equals(SecureStorage.retrieve(Helper.USER_ID, "nill"))) {
            finish();
            startActivity(new Intent(this, PinResetActivity.class));
        }

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("hams", "onClick: hamburgers");
                ((TextView) view.findViewById(R.id.nav_header_username)).setText(userName);
            }
        });
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView) view.findViewById(R.id.nav_header_username)).setText(userName);
            }
        });
        titles = getResources().getStringArray(R.array.main_screens);
        colors = getResources().getIntArray(R.array.colors);

        drawables = new int[]{R.drawable.ic_home_white_24dp,
                R.drawable.ic_devices_black_24dp,
                R.drawable.ic_account_balance_wallet_white_24dp};

        viewPager.setAdapter(pagerAdapter);

        for (int i = 0; i < titles.length; i++) {
            bottomView.addTab(new BottomNavigationItem(titles[i], colors[i], drawables[i]));
        }
        bottomView.disableShadow();


        bottomView.isColoredBackground(false);
        //  bottomView.setItemActiveColorWithoutColoredBackground(R.color.white_pale);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setTitle(titles[position]);
                bottomView.selectTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        bottomView.setOnBottomNavigationItemClickListener(new OnBottomNavigationItemClickListener() {
            @Override
            public void onNavigationItemClick(int index) {
                viewPager.setCurrentItem(index);
                setTitle(index == 0 ? getString(R.string.app_name) : titles[index]);

            }
        });

        loadSliderUserDetails();
    }
    private  void getSessionKey(){

        if (MyApplication.getSessionKey() == null) {


            Thread mThread =  new Thread(new Runnable() {
                @Override
                public void run() {
                    String wallet = SecureStorage.retrieve(Helper.TERMINAL_ID, "");
                    String terminal = SecureStorage.retrieve(Helper.TERMINAL_ID, "");
                    String userName = SecureStorage.retrieve(Helper.USER_ID, "");
                    String password = SecureStorage.retrieve(Helper.PLAIN_PASSWORD, "");
                    String channel = "ANDROID";
                    String deviceID =SecureStorage.retrieve(Helper.TERMINAL_ID, "");


                    SessionKey.Request params = new SessionKey.Request(wallet, terminal, userName, password, channel, deviceID);
//                      Log.d("Param..........", new Gson().toJson(params));
                    FunctionsKt.getPayviceServices(getBaseContext())
                            .GetSesionParam(params)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<SessionKey.Response>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onNext(SessionKey.Response response) {
                                    MyApplication.setSessionKey(response.getSessionKey());

                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.i("Response MAIN Error....", new Gson().toJson(e.getMessage()));

                                }

                                @Override
                                public void onComplete() {

                                }
                            });
                }

            }) ;
            mThread.start();





        }
    }

    @Override
    protected void onStart() {
        super.onStart();

//        if (Helper.getPreference(getApplication(), Helper.PASSWORD_IS_RESET, false) &&
//                Helper.getPreference(getApplication(), Helper.ID_TO_RESET, "not")
//                        .equals(SecureStorage.retrieve(Helper.USER_ID, "nil"))) {
//
//            startActivity(new Intent(this, PinResetActivity.class));
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        int currentIndex = bottomView.getCurrentItem();
        viewPager.setCurrentItem(currentIndex);
        setTitle(currentIndex == 0 ? getString(R.string.app_name) : titles[currentIndex]);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_about) {
            startActivity(new Intent(this, SettingsActivity.class));
        }


        if (id == R.id.action_log_out) {
            new AlertDialog.Builder(this)
                    .setTitle("Log Out")
                    .setMessage("Proceed with log out?")
                    .setNegativeButton(android.R.string.cancel, null)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            logUserOut();
                        }
                    }).show();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_history:
                startActivity(new Intent(this, TransactionHistoryActivity_D.class));
                break;
            case R.id.nav_fund_wallet:
                viewPager.setCurrentItem(2);
                break;
            case R.id.nav_settings:
//                startActivity(new Intent(this, SettingsActivity.class));
                startActivity(new Intent(this, MainSettingsActivity.class));
                break;
            case R.id.prep_terminal:
                startActivity(new Intent(this, TermMagmActivity.class));
                break;
            case R.id.nav_log_out:
                new AlertDialog.Builder(this)
                        .setTitle("Log Out")
                        .setMessage("Proceed with log out?")
                        .setNegativeButton(android.R.string.cancel, null)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                logUserOut();
                            }
                        }).show();
                break;

            case R.id.nav_pay_bills:
                viewPager.setCurrentItem(1);
                break;
//            case R.id.nav_refer:
//                sendGoogleAppInvite();
//                break;
//            case R.id.nav_refer_facebook:
//                sendFacebookInvite();
//                break;
//            case R.id.nav_scan_referral_code:
//                showReferralQrDialog();
//                break;
//            case R.id.nav_receive_tip:
//                startActivity(new Intent(this, ReceiveTipActivity.class));
//                break;
//            case R.id.nav_quick_tip:
//                startActivity(new Intent(this, QuickTipActivity.class));
//                break;

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    void loadSliderUserDetails() {
        if (navigationView != null) {
            view = navigationView.inflateHeaderView(R.layout.nav_header_main);
            ((TextView) view.findViewById(R.id.nav_header_username)).setText(userName);
            ((TextView) view.findViewById(R.id.nav_header_user_email)).setText(userId);
            ((TextView) view.findViewById(R.id.nav_header_terminal_id)).setText(terminalID);

            view.findViewById(R.id.imageViewProf).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(getBaseContext(), Profile_Detail.class);
                    startActivity(intent);
                }
            });
        }
    }

    public void showPromoDialog() {
        startActivity(new Intent(this, PromoOffer.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Helper.REQUEST_INVITE) {
            if (resultCode != RESULT_OK) {
                // Sending failed or it was canceled, show failure message to the user
                // [START_EXCLUDE]
                Helper.showSnackBar(this, "Could not send invites. Please try again");
                // [END_EXCLUDE]
            }
        }
    }


    @Override
    public void onBillListFragmentInteraction(String service) {
        Service theService = VasServices.SERVICES.get(service);
        Log.d("Service Main Activity " ,new  Gson().toJson(theService));
        Intent intent;
        Log.d("Service" ,theService.type.toString());
        if (theService.type == Service.Type.AIRTIME) {
            intent = new Intent(this, AirtimeActivity.class);
        } else {
            switch (service) {
                case VasServices.GOTV:
                case VasServices.DSTV:
                    intent = new Intent(this, MultichoiceActivity.class);
                    break;
                case VasServices.SMILE:
                    intent = new Intent(this, SmileServiceActivity.class);
                    break;
                case VasServices.GENESIS:
                    intent = new Intent(this, GenesisMovieActivity.class);
                    break;                
                case VasServices.TRANSFER:
                    intent = new Intent(this, TransferBankSelection.class);
                    intent.putExtra("transfer_type", TransferAmountEntry.TRANSACTION_TYPE.TRANSFER);
                    break;
                case VasServices.WITHDRAWAL:
                    intent = new Intent(this, TransferBankSelection.class);
                    intent.putExtra("transfer_type", TransferAmountEntry.TRANSACTION_TYPE.WITHDRAWAL);
                    break;
                default:
                    intent = new Intent(this, BillPaymentActivity.class);
            }
        }

        intent.putExtra(BillPaymentActivity.SERVICE, service);
        startActivity(intent);
    }




}


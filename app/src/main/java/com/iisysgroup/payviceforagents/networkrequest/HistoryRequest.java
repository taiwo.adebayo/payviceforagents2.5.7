package com.iisysgroup.payviceforagents.networkrequest;

import com.google.gson.Gson;
import com.iisysgroup.payviceforagents.models.TransactionHistory;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Itex-PC on 31/10/2018.
 */

public class HistoryRequest {
    OkHttpClient client = new OkHttpClient();
    RequestBody body;
    Request request;
    Gson gson = new Gson();
    int lastpage, currentapge = 1;

     public HistoryRequest(String walletId,String email, String password){
         MediaType mediaType = MediaType.parse("application/json");
         body = RequestBody.create(mediaType, "{\r\t\"wallet\": \""+walletId+"\",\r\t\"username\": \""+email+"\",\r\t\"password\": \""+password);
         request = new Request.Builder()
                 //.url("http://197.253.19.76:443/api/account/transaction-history")
                 .url("http://vas.itexapp.comapi/vas/history/")
                 .post(body)
                 .addHeader("Authorization", "IISYSGROUP c1e750cf89b05b0fc56eecf6fc25cce85e2bb8e0c46d7bfed463f6c6c89d4b8e")
                 .addHeader("sysid", "ee2dadd1e684032929a2cea40d1b9a2453435da4f588c1ee88b1e76abb566c31")
                 .addHeader("Content-Type", "application/json")
                 .addHeader("cache-control", "no-cache")
                 .build();
     }


     public TransactionHistory getFirstPage() throws IOException {
         Response response = client.newCall(request).execute();
         return  gson.fromJson(response.body().string(), TransactionHistory.class);
     }




}

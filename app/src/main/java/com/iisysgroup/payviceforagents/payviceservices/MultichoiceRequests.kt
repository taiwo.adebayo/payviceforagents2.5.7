package com.iisysgroup.payviceforagents.payviceservices

import com.dg.http.HttpRequest
import com.google.gson.Gson
import com.iisysgroup.payviceforagents.base.interactor.MultichoiceInteractor
import com.iisysgroup.payviceforagents.entities.PlanValue
import com.iisysgroup.payviceforagents.securestorage.SecureStorageUtils
import com.iisysgroup.payviceforagents.util.Helper
import com.iisysgroup.payviceforagents.util.TripleDES
import org.json.JSONObject
import java.io.IOException


object MultichoiceRequests {
    private const val BASE_MULTICHOICE_URL = "http://197.253.19.76:443/vas/multichoice"

    @JvmStatic
    @Throws(IOException::class)
    fun validateSmartCardNumber(product: MultichoiceInteractor.MultichoiceProduct, smartCardNo: String): IUCVerificationResult {
        val validatePath = "validate"

        val httpRequest = HttpRequest("$BASE_MULTICHOICE_URL/$validatePath", "POST")
        httpRequest.setHeaders(getDefaultHeaders())
        httpRequest.addParams(mapOf(Pair("unit", product.toString()), Pair("iuc", smartCardNo)))

        return Gson().fromJson(httpRequest.response.responseText, IUCVerificationResult::class.java)
    }

    @JvmStatic
    @Throws(IOException::class)
    fun getAvailableMultichoicePlans(product: MultichoiceInteractor.MultichoiceProduct, smartCardNo: String): List<PlanValue> {
        val lookupPath = "lookup"
        val httpRequest = HttpRequest("$BASE_MULTICHOICE_URL/$lookupPath", "POST")

        httpRequest.setHeaders(getDefaultHeaders())
        httpRequest.addParams(mapOf(
                Pair("unit", product.toString()),
                Pair("iuc", smartCardNo)
        ))

        val response = httpRequest.response.responseText
        println(response)
        return Helper.parseMultichoiceLookupResponse(JSONObject(response))
    }

    @JvmStatic
    @Throws(IOException::class)
    fun payMultichoiceSubscription(smartCardNo: String, userID: String,
                                   walletID: String, productCode: String, convenienceFee: Int, userPin: String): PaymentResult {
        val paymentPath = "pay"
        val httpRequest = HttpRequest("$BASE_MULTICHOICE_URL/$paymentPath", "POST")
        httpRequest.setHeaders(getDefaultHeaders())
        httpRequest.setContentType("application/x-www-form-urlencoded")

        val params = mapOf<String, String>(
                Pair("iuc", smartCardNo),
                Pair("user_id", userID),
                Pair("terminal_id", walletID),
                Pair("product_code", productCode.toString()),
                Pair("convenience", convenienceFee.toString()),
                Pair("pin", userPin)
        )

        httpRequest.setParams(params)

        val response = httpRequest.response.responseText
        println(response)

        return Gson().fromJson(response, PaymentResult::class.java)
    }


    @JvmStatic
    private fun getDefaultHeaders(): Map<String, String> = mapOf(
            Pair("Authorization", "IISYSGROUP c1e750cf89b05b0fc56eecf6fc25cce85e2bb8e0c46d7bfed463f6c6c89d4b8e"),
            Pair("sysid", TripleDES.bytesToHexString(SecureStorageUtils.hash("iisysgroup_18559652", "SHA-256"))
            ))


    data class PaymentResult(val error: Boolean, val message: String)
    data class IUCVerificationResult(val response_code: String,
                                     val tran_id: String,
                                     val ref: String,
                                     val unit: String,
                                     val fullname: String) {

        fun isValid() = response_code == "00"
    }

}

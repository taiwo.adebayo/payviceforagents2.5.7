package com.iisysgroup.payviceforagents.baseimpl.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.iisysgroup.payviceforagents.base.presenter.SmileServicePresenter
import com.iisysgroup.payviceforagents.baseimpl.interactor.SmileServiceInteractorImpl
import com.iisysgroup.payviceforagents.entities.Service
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.results.SmileServiceResult
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
import com.iisysgroup.payviceforagents.util.SMILE_APPROVED_STATUS
import com.iisysgroup.payviceforagents.util.processTransaction
import com.iisysgroup.payvicegamesdk.TamsResponse
import com.itex.richard.payviceconnect.model.SmileModel

class SmileServiceViewModel(application: Application) : AndroidViewModel(application), SmileServicePresenter {


    private val interactor by lazy {
        SmileServiceInteractorImpl(application)
    }

    private val mWalletId by lazy {
        SecureStorage.retrieve(Helper.TERMINAL_ID, "")
    }

    private val accountName = MutableLiveData<String>()
    private val bundles = MutableLiveData<List<SmileModel.Bundle>>()
    private val selectedBundle = MutableLiveData<SmileModel.Bundle>()
    private val product = MutableLiveData<Service.Product>()
    private val error = MutableLiveData<Throwable>()
    private val progressDialog = MutableLiveData<Pair<Boolean, String>>()
    private val paymentResponse = MutableLiveData<SmileServiceResult>()

    override fun validate(accountId: String) {
        showProgress(true, "Validating account: $accountId")

        interactor.validateUser(accountId, mWalletId).subscribe { result, error ->
            showProgress(false)

            error?.let(this::setError)

            result?.let {
                if (it.status == SMILE_APPROVED_STATUS) {
                    SecureStorage.store("currentName", it.customerName)
                    accountName.value = it.customerName
                } else {
                    setError(RuntimeException(it.message))
                }
            }
        }
    }

    override fun onValidateAccount(): LiveData<String> = accountName

    override fun getBundles(): LiveData<List<SmileModel.Bundle>> {

        if (bundles.value == null) {
            showProgress(true, "Getting bundles...")

            interactor.getBundles().subscribe { result, error ->
                showProgress(false)

                error?.let(this::setError)

                result?.let {
                    if (it.status == SMILE_APPROVED_STATUS) {
                        bundles.value = it.bundles
                    } else {
                        setError(RuntimeException(it.message))
                    }
                }
            }
        }

        return bundles
    }

    override fun setBundleSelection(which: Int) {
        selectedBundle.value = bundles.value?.get(which)
    }

    override fun observeSelectedBundle(): LiveData<SmileModel.Bundle> = selectedBundle

    override fun topUp(accountId: String, encryptedPin: String, amount: Int) {
        showProgress(true, "Topping up $accountId...")

        interactor.topUpAccount(accountId, amount * 100, encryptedPin).subscribe { result, error ->
            showProgress(false)
            error?.let(this::setError)

            result?.let {
                Log.d("okh", it.toString())
                val beneficiary = Beneficiary(accountId, accountName.value ?: "",
                        product.value?.name ?: "")

                val data = ServiceResult.Data(beneficiary, amount)

                SecureStorage.store("currentservice", "Smile")
                SecureStorage.store("currentAmount", data.amount.toString())
                SecureStorage.store("currentPhone", data.beneficiary.data)
                SecureStorage.store("currentProduct", beneficiary.assignedProduct)
                SecureStorage.store("currentReason", it.message)
                SecureStorage.store("currentRef", it.reference)
                if (it.status==1){
                    SecureStorage.store("currentApproval", "Approved")
                }
                else{
                    SecureStorage.store("currentApproval", "Declined")
                }
                paymentResponse.value = SmileServiceResult(data, null, it)
            }
        }

    }

    override fun topUpWithCard(accountId: String, amount: Int, card: Card?, cardProcessor: DebitCardProcessor) {
        cardProcessor.processTransaction(amount, card).subscribe { result ->
            result?.let {
                if (it.result == TamsResponse.Status.APPROVED) {
                    topUp(accountId, cardProcessor.userPin, amount)
                } else {
                    setError(RuntimeException(it.message))
                }
            }

        }

    }

    override fun subscribe(accountId: String, encryptedPin: String) {
        showProgress(true, "Subscribing to ${selectedBundle.value?.name}")

        interactor.subscribe(accountId, selectedBundle.value!!, encryptedPin).subscribe { result, error ->
            showProgress(false)

            error?.let(this::setError)

            result?.let {
                Log.d("okh", "response "+ it.toString())

                val beneficiary = Beneficiary(accountId, accountName.value ?: "",
                        product.value?.name ?: "")
                val amount = selectedBundle.value?.fee?.toDouble()?.toInt() ?: 0
                val bundle = selectedBundle.value?.name ?: ""
                val data = ServiceResult.Data(beneficiary, amount * 100)

                SecureStorage.store("currentservice", "Smile")
                SecureStorage.store("currentAmount", data.amount.toString())
                SecureStorage.store("currentPhone", data.beneficiary.data)
                SecureStorage.store("currentProduct", beneficiary.assignedProduct)
                SecureStorage.store("currentReason", it.message)
                SecureStorage.store("currentRef", it.reference)
                if (it.status==1){
                    SecureStorage.store("currentApproval", "Approved")
                }
                else{
                    SecureStorage.store("currentApproval", "Declined")
                }

                paymentResponse.value = SmileServiceResult(data, bundle, it)
            }
        }

    }

    override fun subscribeWithCard(accountId: String, card: Card?, cardProcessor: DebitCardProcessor) {

        val amount = selectedBundle.value!!.price / 100
        cardProcessor.processTransaction(amount, card).subscribe { result ->
            result?.let {
                if (it.result == TamsResponse.Status.APPROVED) {
                    subscribe(accountId, cardProcessor.userPin)
                } else {
                    setError(RuntimeException(it.message))
                }
            }

        }
    }

    override fun observePaymentResponse(): LiveData<SmileServiceResult> = paymentResponse

    override fun setProduct(product: Service.Product) {
        this.product.value = product
    }

    override fun onSetProduct(): LiveData<Service.Product> = product

    override fun setError(throwable: Throwable) {
        error.value = throwable
    }

    override fun onError(): LiveData<Throwable> = error

    override fun showProgress(show: Boolean, message: String) {
        progressDialog.postValue(Pair(show, message))
    }

    override fun onProgressUpdate(): LiveData<Pair<Boolean, String>> = progressDialog
}
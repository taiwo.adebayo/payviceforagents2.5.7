package com.iisysgroup.payviceforagents.customviews

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.view.inputmethod.InputConnectionWrapper
import android.widget.EditText


class PinEditText(theContext: Context?,
                  theAttributeSet: AttributeSet?,
                  theDefStyleAttr: Int) :
        EditText(theContext, theAttributeSet, theDefStyleAttr), View.OnKeyListener, TextWatcher {

    constructor(theContext: Context?,
                theAttributeSet: AttributeSet?,
                theDefStyleAttr: Int,
                theDefStyleRes: Int) : this(theContext, theAttributeSet, theDefStyleAttr)

    constructor(theContext: Context?,
                theAttributeSet: AttributeSet?) : this(theContext, theAttributeSet, android.R.attr.editTextStyle)


    constructor(theContext: Context?) : this(theContext, null, 0)

    init {
        this.setOnKeyListener(this)
        this.addTextChangedListener(this)
    }

    var action: PinEditAction? = null


    var nextFocusable: EditText? = null
    var previousFocusable: EditText? = null


    private fun moveToNext() {
        nextFocusable?.let {
            this.clearFocus()
            moveTo(it)
        } ?: kotlin.run {
            action?.invoke()
        }
    }

    private fun moveToPrevious() {
        previousFocusable?.let {
            this.clearFocus()
            this.isEnabled = false

            moveTo(it)
        }
    }

    private fun moveTo(editText: EditText) {
        editText.isEnabled = true
        editText.requestFocus()
    }

    override fun onCreateInputConnection(outAttrs: EditorInfo?): InputConnection {
        return ZanyInputConnection(super.onCreateInputConnection(outAttrs),
                true)
    }

    override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
        val editText = v as PinEditText

        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            action?.invoke()
        }

//        if (event.action == KeyEvent.ACTION_UP && keyCode != KeyEvent.KEYCODE_DEL) {
//            editText.moveToNext()
//        }

        if (keyCode == KeyEvent.KEYCODE_DEL) {
            if (editText.text.isEmpty()) {
                editText.moveToPrevious()
            }
        }


        return false
    }


    override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)
    }

    override fun afterTextChanged(s: Editable?) {
        if (!s.isNullOrEmpty()) {
            this.moveToNext()
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    private class ZanyInputConnection(target: InputConnection,
                                      mutable: Boolean) : InputConnectionWrapper(target, mutable) {

        //Courtesy of StackOverflow User Idris and Jeff

        override fun sendKeyEvent(event: KeyEvent): Boolean {
            if (event.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_DEL) {

                // Un-comment if you wish to cancel the backspace:
                // return false;
            }

            return super.sendKeyEvent(event)
        }

        override fun deleteSurroundingText(beforeLength: Int, afterLength: Int): Boolean {
            // magic: in latest Android, deleteSurroundingText(1, 0) will be called for backspace
            if (beforeLength == 1 && afterLength == 0) {
                //backspace
                return sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL))
                        && sendKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL))
            }

            return super.deleteSurroundingText(beforeLength, afterLength)
        }

    }


}

typealias PinEditAction = () -> Unit
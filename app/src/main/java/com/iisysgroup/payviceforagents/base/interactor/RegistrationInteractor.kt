package com.iisysgroup.payviceforagents.base.interactor

import com.iisysgroup.payviceforagents.entities.RegistrationData
import com.iisysgroup.payviceforagents.entities.VasResult
import io.reactivex.Single

interface RegistrationInteractor {
    fun register(data: RegistrationData): Single<VasResult>
}
package com.iisysgroup.payviceforagents.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.iisysgroup.payviceforagents.activities.BaseActivity;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary;
import com.iisysgroup.payviceforagents.util.Helper;

/**
 * A placeholder fragment containing a simple view.
 */
public class WalletTransferActivityFragment extends Fragment {


    OnFragmentInteractionListener mListener;
    private EditText recipientWalletId;
    private EditText amountEdit;
    private RadioButton radioUserEmailTransfer;
    private RadioButton radioWalletIdTransfer;
    private View recipientEmailLayout;
    private EditText recipientEmailEdit;
    private View recipientWalletIdLayout;
    private String walletID;

    public WalletTransferActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wallet_transfer, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recipientEmailLayout = view.findViewById(R.id.recipientEmailLayout);
        recipientWalletIdLayout = view.findViewById(R.id.recipientWalletIdLayout);

        ((RadioGroup) view.findViewById(R.id.radioGrpTransferOption)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.radioUserIdTransfer:
                        recipientEmailLayout.setVisibility(View.VISIBLE);
                        recipientWalletIdLayout.setVisibility(View.GONE);
                        break;
                    case R.id.radioWalletIdTransfer:
                        recipientEmailLayout.setVisibility(View.GONE);
                        recipientWalletIdLayout.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        radioUserEmailTransfer = view.findViewById(R.id.radioUserIdTransfer);
        radioWalletIdTransfer = view.findViewById(R.id.radioWalletIdTransfer);

        recipientEmailEdit = view.findViewById(R.id.recipientEmailEdit);
        recipientWalletId = view.findViewById(R.id.recipientWalletId);
        amountEdit = view.findViewById(R.id.amountEdit);
        amountEdit.addTextChangedListener(new AmountTextWatcher(amountEdit));

        view.findViewById(R.id.proceedBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    if (radioWalletIdTransfer.isChecked()) {
                        String walletID = recipientWalletId.getText().toString();
                        mListener.onWalletTransferFragmentInteraction(new Beneficiary(walletID, walletID, ""),
                                Helper.sanitizeStringAmount(amountEdit.getText().toString()));
                        return;
                    }

                    if (radioUserEmailTransfer.isChecked()) {
                        proceedWithUserId();
                    }
                }
            }
        });

        view.findViewById(R.id.contactButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContacts();
            }
        });


        final View temp = view.findViewById(R.id.wallet_account_info);
        temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(temp,
                        "The 8-digit number on the profile information menu", Snackbar.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new IllegalArgumentException("Activity must implement OnFragmentInteractionListener");
        }
    }

    boolean isValid() {

        if (radioUserEmailTransfer.isChecked()) {
            String recipient = recipientEmailEdit.getText().toString();
            if (!(PhoneNumberUtils.isGlobalPhoneNumber(recipient) || Helper.validateEmail(recipient))) {
                recipientEmailEdit.setError("Please enter valid recipient");
                recipientEmailEdit.requestFocus();
                Helper.showErrorAnim(recipientEmailEdit);
                return false;
            }
        }

        if (radioWalletIdTransfer.isChecked()) {
            if (recipientWalletId.getText().toString().isEmpty()) {
                recipientWalletId.setError("Please enter recipient");
                recipientWalletId.requestFocus();
                Helper.showErrorAnim(recipientWalletId);
                return false;
            }
        }


        String amount = Helper.sanitizeStringAmount(amountEdit.getText().toString());

        if (amount.isEmpty()) {
            amountEdit.setError("Please enter amount");
            Helper.showErrorAnim(amountEdit);
            amountEdit.requestFocus();
            return false;
        }


        if (Integer.parseInt(amount) < 50) {
            amountEdit.setError("Amount cannot be less than 50");
            Helper.showErrorAnim(amountEdit);
            amountEdit.requestFocus();
            return false;
        }

        return true;
    }


    void getContacts() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.CommonDataKinds.Email.CONTENT_TYPE);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(Intent.createChooser(intent, "Choose Recipient"), 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();

            String projection[] = {ContactsContract.CommonDataKinds.Email.ADDRESS,
                    ContactsContract.CommonDataKinds.Phone.NUMBER};

            Cursor c = getContext().getContentResolver().query(uri, projection, null, null, null);

            if (c.moveToFirst()) {
                final String email = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                final String phone = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                if (phone != null && PhoneNumberUtils.isGlobalPhoneNumber(phone)) {
                    setNewUserId(phone);
                } else if (email != null && !email.isEmpty() && Helper.validateEmail(email)) {
                    setNewUserId(email);
                } else {
                    Helper.showInfoDialog(getContext(), "Error", "Could not load the selected contact, please input email manually.");
                }
            }

        }


    }

    private void setNewUserId(final String userId) {
        if (!recipientEmailEdit.getText().toString().isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setTitle("Replace Contact")
                    .setMessage("Do you want to replace the existing recipient with this?")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            recipientEmailEdit.setText(userId);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
        } else
            recipientEmailEdit.setText(userId);
    }

    void proceedWithUserId() {
        if (Helper.hasInternetConnectivity(getActivity())) {
            final String recipient = recipientEmailEdit.getText().toString();
            final String amount = Helper.sanitizeStringAmount(amountEdit.getText().toString());
            final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), null, "Verifying contact", true, false);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    VasResult result = Requests.initUser(getActivity(), recipient);

                    if (result.result == VasResult.Result.APPROVED) {
                        final String walletID = result.macrosTID;
                        Helper.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                mListener.onWalletTransferFragmentInteraction(
                                        new Beneficiary(walletID, recipient + " - " + walletID, ""), amount);
                            }
                        });

                    } else {
                        Helper.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                new AlertDialog.Builder(getContext())
                                        .setTitle("Invite Contact")
                                        .setMessage("This contact is not registered on " + getString(R.string.app_name) +
                                                ". Do you want to refer this contact?")
                                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                ((BaseActivity) getActivity()).sendGoogleAppInvite();
                                            }
                                        })
                                        .setNegativeButton(android.R.string.cancel, null)
                                        .show();
                            }
                        });
                    }
                }
            }).start();

        }


    }


    public interface OnFragmentInteractionListener {
        void onWalletTransferFragmentInteraction(Beneficiary beneficiary, String amount);
    }
}

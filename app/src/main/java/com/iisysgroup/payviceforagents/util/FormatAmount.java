package com.iisysgroup.payviceforagents.util;

import java.util.ArrayList;

/**
 * Created by simileoluwaaluko on 12/04/2018.
 */

public class FormatAmount {

    private static String kobos = "00";
    private static String naira = "0";

    public static String formatAmount(String amount, boolean addNaira) {
        String nairaSymbol = null;
        if (addNaira) {
            nairaSymbol = "\u20A6";
        } else {
            nairaSymbol = "";
        }

        if (amount == null) {
            return nairaSymbol + naira + "." + kobos;
        }

        if (!amount.isEmpty()) {

            int numberOfDots = amount.length() - amount.replace(".", "").length();

            if (amount.contains(".") && !(numberOfDots > 1)) {

                String[] amountParts = amount.split("\\.");
                String amountPart1 = amountParts[0];
                String amountPart2 = amountParts[1];

                naira = parseAmount(amountPart1);
                String prekoboString = "0." + amountPart2;

                try {
                    Double.parseDouble(prekoboString);
                } catch (Exception e) {
                    return nairaSymbol + naira + "." + kobos;
                }

                if (amountPart2.length() > 2) {

                    double prekoboDouble = Double.parseDouble(prekoboString);
                    double roundedOffPreKoboDouble = Math.round(prekoboDouble * 100.0) / 100.0;
                    String koboString = Double.toString(roundedOffPreKoboDouble);

                    String[] koboParts = koboString.split("\\.");
                    if (koboParts[1].length() == 1) {
                        koboParts[1] = koboParts[1] + "0";
                    }
                    kobos = koboParts[1];

                } else if (amountPart2.length() == 2) {

                    kobos = amountPart2;

                } else if (amountPart2.length() == 1) {

                    kobos = amountPart2 + "0";

                } else if (amountPart2.isEmpty()) {

                    kobos = "00";

                }

            } else if (!amount.contains(".")) {
                naira = parseAmount(amount);
                kobos = "00";
            }
        }

        return nairaSymbol + naira + "." + kobos;
    }

    private static String parseAmount(String amount) {

        if (amount.contains(",")) {
            amount = amount.replace(",", "");
        }

        try {
            Integer.parseInt(amount);
        } catch (Exception e) {
            return "0";
        }

        ArrayList<Character> tempAmount = new ArrayList<>();
        char[] amountArray = amount.toCharArray();

        int i = 0;

        for (int j = amount.length(); j > 0; j--) {
            i++;

            int index = j - 1;
            tempAmount.add(amountArray[index]);

            if (i == 3 && index != 0) {
                tempAmount.add(',');
                i = 0;
            }
        }

        char[] parsedAmount = new char[tempAmount.size()];

        int k = 0;
        for (int j = tempAmount.size(); j > 0; j--) {
            int index = j - 1;
            parsedAmount[k] = tempAmount.get(index);
            k++;
        }

        String parsedAmountString = new String(parsedAmount);

        return parsedAmountString;
    }

}

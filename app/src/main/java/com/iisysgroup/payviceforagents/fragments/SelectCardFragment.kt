package com.iisysgroup.payviceforagents.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.persistence.entitiy.Card


class SelectCardFragment : LinkedCardsFragment() {

    private var mListener: SelectCardFragment.OnFragmentInteractionListener? = null

    init {
        cardAdapter = SelectableCardAdapter(cards)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        activity!!.title = "Select Card"
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    fun onButtonPressed(card: Card) {
        if (mListener != null) {
            mListener!!.onLinkedFragmentInteraction(card)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }


    interface OnFragmentInteractionListener {
        fun onLinkedFragmentInteraction(card: Card)
    }

    internal inner class SelectableCardAdapter(cardList: List<Card>) : CardAdapter(cardList) {

        override fun getItemCount(): Int {
            return cardList.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {


            val view = LayoutInflater.from(parent.context).inflate(R.layout.card_list_item, parent, false)

            return CardViewHolder(view)
        }

        override fun onBindViewHolder(holder: CardViewHolder, position: Int) {

            val card = cardList[position]

            holder.creditCardView.cvv = card.CVV
            holder.creditCardView.setCardExpiry(card.expiryMonth + card.expiryYear)
            holder.creditCardView.cardNumber = card.maskedPan
            holder.creditCardView.visibility = View.VISIBLE
            holder.creditCardView.cardHolderName = "---"

            val selectAction = View.OnClickListener { onButtonPressed(card) }

            holder.creditCardView.setOnClickListener(selectAction)
            holder.view.setOnClickListener(selectAction)
            holder.view.visibility = View.VISIBLE
        }


    }


}



package com.iisysgroup.payviceforagents.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.fragments.ActivateUserFragment;
import com.iisysgroup.payviceforagents.fragments.ChangeDeviceFragment;
import com.iisysgroup.payviceforagents.fragments.LoginSettingsActivityFragment;
import com.iisysgroup.payviceforagents.fragments.RecoverPasswordFragment;

public class LoginSettingsActivity extends AppCompatActivity implements LoginSettingsActivityFragment.OnFragmentInteractionListener {

    public static final String OPERATION = "link device";
    public static final int CHANGE_DEVICE = 1;
    public static final int ACTIVATE_USER = 2;
    public static final int RECOVER_PASSWORD = 0;

    FragmentManager fm;
    FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fm = getSupportFragmentManager();

        int fragmentOperation = getIntent().getIntExtra(LoginSettingsActivity.OPERATION, -1);

        if (fragmentOperation >= LoginSettingsActivity.RECOVER_PASSWORD) {
            onSettingSelected(fragmentOperation);
        } else {
            loadFragment(new LoginSettingsActivityFragment());
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if (fm.getBackStackEntryCount() > 1) {
                fm.popBackStack();
            } else {
                NavUtils.navigateUpFromSameTask(this);
                overridePendingTransition(android.support.design.R.anim.abc_popup_enter, android.support.design.R.anim.abc_fade_out);
            }

        }


        return true;
    }

    @Override
    public void onBackPressed() {
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();
        } else {
            NavUtils.navigateUpFromSameTask(this);
            overridePendingTransition(android.support.design.R.anim.abc_popup_enter, android.support.design.R.anim.abc_fade_out);
        }

    }

    public void loadFragment(Fragment fragment) {
        ft = fm.beginTransaction();
        if (fm.findFragmentByTag(fragment.getTag()) != null)
            ft.remove(fragment);

        ft.replace(R.id.container, fragment, fragment.getClass().getSimpleName());
        ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

    @Override
    public void onSettingSelected(int pos) {
        switch (pos) {
            case LoginSettingsActivity.RECOVER_PASSWORD:
                loadFragment(new RecoverPasswordFragment());
                break;
            case LoginSettingsActivity.CHANGE_DEVICE:
                loadFragment(new ChangeDeviceFragment());
                break;
            case LoginSettingsActivity.ACTIVATE_USER:
                loadFragment(new ActivateUserFragment());
                break;
        }
    }
}

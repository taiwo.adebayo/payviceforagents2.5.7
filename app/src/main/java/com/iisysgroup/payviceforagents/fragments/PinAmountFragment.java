package com.iisysgroup.payviceforagents.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.PinValue;
import com.iisysgroup.payviceforagents.util.Helper;

import java.text.DecimalFormat;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PinAmountFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class PinAmountFragment extends Fragment implements View.OnClickListener {

    TextView serviceText, productText, totalAmountText;
    EditText quantityEdit, pinEdit;
    TextInputLayout quantityTIL, pinTIL;
    Button nextButton;
    String service = "", product = "";
    PinValue pinValue = new PinValue();
    boolean isOverflowed;
    boolean is_bill_payment;
    private OnFragmentInteractionListener mListener;

    public PinAmountFragment() {
        // Required empty public constructor
    }

    public static PinAmountFragment getInstance(String service, String product, PinValue pinValue, boolean is_bill_payment) {
        PinAmountFragment pinAmountFragment = new PinAmountFragment();
        pinAmountFragment.service = service;
        pinAmountFragment.product = product;
        pinAmountFragment.pinValue = pinValue;
        pinAmountFragment.is_bill_payment = is_bill_payment;

        return pinAmountFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pin_amount, container, false);
        getActivity().setTitle("Enter Number Of Pin");

        if (!is_bill_payment)
            view.findViewById(R.id.commission_layout).setVisibility(View.GONE);

        serviceText = view.findViewById(R.id.serviceTitle);
        serviceText.setText(service);


        productText = view.findViewById(R.id.productOption);
        productText.setText(product);

        totalAmountText = view.findViewById(R.id.totalAmount);

        quantityEdit = view.findViewById(R.id.quantityEdit);
        quantityEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isOverflowed = false;
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                int total = 0;

                if (!TextUtils.isEmpty(text)) {
                    try {
                        total = Integer.parseInt(text) * pinValue.value;
                    } catch (Exception e) {
                    }
                }

                if (total > (pinValue.value * pinValue.available_quantity)) {
                    isOverflowed = true;
                    quantityTIL.setError("You cannot purchase more than the available quantity: " + pinValue.available_quantity);
                    quantityEdit.requestFocus();
                }

                text = DecimalFormat.getInstance().format(total);
                totalAmountText.setText("\u20a6 " + text);
            }
        });
        quantityEdit.setText("1");

        pinEdit = view.findViewById(R.id.pinEdit);

        quantityTIL = view.findViewById(R.id.quantityTIL);
        pinTIL = view.findViewById(R.id.pinTIL);

        nextButton = view.findViewById(R.id.proceedBtn);
        nextButton.setOnClickListener(this);
        return view;
    }

    public void onButtonPressed(int qty, String pin) {
        if (mListener != null) {
            mListener.onPinAmountFragmentInteraction(qty, pin);
        }
    }

    boolean isValid() {

        String quantity = quantityEdit.getText().toString();
        if (isOverflowed) {
            quantityTIL.setErrorEnabled(true);
            quantityTIL.setError("You cannot purchase more than the available quantity: " + pinValue.available_quantity);
            quantityEdit.requestFocus();
            Helper.showErrorAnim(quantityEdit);
            return false;
        }
        if (quantity.isEmpty()) {
            quantityTIL.setErrorEnabled(true);
            quantityTIL.setError("Please enter the number of pins you want");
            quantityEdit.requestFocus();
            Helper.showErrorAnim(quantityEdit);
            return false;
        }

        if (Integer.parseInt(quantity) < 1) {
            quantityTIL.setErrorEnabled(true);
            quantityTIL.setError("The least number is 1");
            quantityEdit.requestFocus();
            Helper.showErrorAnim(quantityEdit);
            return false;
        }

        if (pinEdit.getText().toString().isEmpty()) {
            pinTIL.setErrorEnabled(true);
            pinTIL.setError("Enter pin");
            pinEdit.requestFocus();
            Helper.showErrorAnim(pinEdit);
            return false;
        }
        quantityTIL.setErrorEnabled(false);
        pinTIL.setErrorEnabled(false);

        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

        if (isValid()) {
            new AlertDialog.Builder(getContext())
                    .setTitle("Confirm Transaction")
                    .setMessage("Proceed with transaction?")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int qty = Integer.parseInt(quantityEdit.getText().toString());
                            String pin = pinEdit.getText().toString();
                            onButtonPressed(qty, pin);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
        }

    }

    public interface OnFragmentInteractionListener {
        void onPinAmountFragmentInteraction(int quantity, String pin);
    }
}

package com.iisysgroup.payvicegamesdk;

import java.io.Serializable;

/**
 * Created by Bamitale@Itex on 5/4/2016.
 */
public class TamsResponse implements Serializable{
    String message;
    String balance = "";
    int resultCode = -1;
    String macrosTID = "";
    String key = "";
    Status  result = Status.DECLINED;

    public TamsResponse(String message) {
        this.message = message;
    }

    public TamsResponse(String message, String balance, int resultCode, String macrosTID, String key, Status result) {
        this.message = message;
        this.balance = balance;
        this.resultCode = resultCode;
        this.macrosTID = macrosTID;
        this.key = key;
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public String getBalance() {
        return balance;
    }

    public int getResultCode() {
        return resultCode;
    }

    public String getMacrosTID() {
        return macrosTID;
    }

    public String getKey() {
        return key;
    }

    public Status getResult() {
        return result;
    }

    public enum Status {
        APPROVED, DECLINED
    }
}

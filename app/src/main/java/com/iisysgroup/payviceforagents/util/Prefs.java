package com.iisysgroup.payviceforagents.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Prefs {
    // file name
    private static final String PREF_NAME = "spaceo-demo";
    private static final String SHOULD_SKIP = "IsFirstTime";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    //mode
    int PRIVATE_MODE = 0;

    public Prefs(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setShouldSkip(boolean isFirstTime) {
        editor.putBoolean(SHOULD_SKIP, isFirstTime);
        editor.commit();
    }

    public boolean shouldSkip() {
        return pref.getBoolean(SHOULD_SKIP, false);
    }

}
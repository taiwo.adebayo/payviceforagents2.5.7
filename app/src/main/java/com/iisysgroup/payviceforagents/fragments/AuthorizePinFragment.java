package com.iisysgroup.payviceforagents.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AuthorizePinFragment extends Fragment {

    @BindView(R.id.pinEdit)
    EditText pinEdit;
    @BindView(R.id.pinEdit2)
    EditText pinEdit2;
    @BindView(R.id.pinEdit3)
    EditText pinEdit3;
    @BindView(R.id.pinEdit4)
    EditText pinEdit4;
    private OnFragmentInteractionListener mListener;

    public AuthorizePinFragment() {
        // Required empty public constructor
    }


    @OnClick(R.id.proceedBtn)
    void authorizePin() {

        if (pinEdit.getText().toString().isEmpty() ||
                pinEdit2.getText().toString().isEmpty() ||
                pinEdit3.getText().toString().isEmpty() ||
                pinEdit4.getText().toString().isEmpty()) {

            Snackbar.make(pinEdit, "Invalid Pin", Snackbar.LENGTH_SHORT).show();
            pinEdit.requestFocus();
        } else {
            String pin = String.format("%s%s%s%s", pinEdit.getText().toString()
                    , pinEdit2.getText().toString()
                    , pinEdit3.getText().toString()
                    , pinEdit4.getText().toString());
            Log.d("enteredpin", "authorizePin: "+ pin+"");
            SecureStorage.store(Helper.PIN, Helper.preparePin(pin));
            Log.d("encryptedpin", "authorizePin: "+ Helper.preparePin(pin));
            onButtonPressed(pin);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_authorize_pin, null);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        pinEdit.addTextChangedListener(new FocusTextWatcher(getActivity(), null, pinEdit2));
        pinEdit2.addTextChangedListener(new FocusTextWatcher(getActivity(), pinEdit, pinEdit3));
        pinEdit3.addTextChangedListener(new FocusTextWatcher(getActivity(), pinEdit2, pinEdit4));
        pinEdit4.addTextChangedListener(new FocusTextWatcher(getActivity(), pinEdit3, null));

        Helper.showSoftKeyboard(getActivity());
    }

    public void onButtonPressed(String pin) {
        if (mListener != null) {
            mListener.onAuthorizePinFragmentInteraction(pin);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onAuthorizePinFragmentInteraction(String pin);
    }


    class FocusTextWatcher implements TextWatcher {

        EditText leftFocusNode, rightFocusNode;
        Context context;

        public FocusTextWatcher(Context context, EditText leftFocusNode, EditText rightFocusNode) {
            this.context = context;
            this.leftFocusNode = leftFocusNode;
            this.rightFocusNode = rightFocusNode;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() >= 1) {
                if (rightFocusNode != null) {
                    rightFocusNode.setEnabled(true);
                    rightFocusNode.requestFocus();
                } else //The last EditText
                    Helper.hideSoftKeyboard((Activity) context);
            } else {
                if (leftFocusNode != null) {
                    leftFocusNode.requestFocus();
                } else {//The first  EditText
                    pinEdit2.setEnabled(false);
                    pinEdit3.setEnabled(false);
                    pinEdit4.setEnabled(false);

                }
            }
        }

    }
}

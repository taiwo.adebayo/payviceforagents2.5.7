package com.iisysgroup.payviceforagents.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Itex-PC on 09/11/2018.
 */

    public class SubAgent {

        @SerializedName("user")
        @Expose
        private String user;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone")
        @Expose
        private Object phone;
        @SerializedName("walletID")
        @Expose
        private String walletID;

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getPhone() {
            return phone;
        }

        public void setPhone(Object phone) {
            this.phone = phone;
        }

        public String getWalletID() {
            return walletID;
        }

        public void setWalletID(String walletID) {
            this.walletID = walletID;
        }

    }


package com.iisysgroup.payviceforagents.entities.results;

import com.iisysgroup.payviceforagents.entities.VasResult;

/**
 * Created by Itex-PC on 31/10/2018.
 */

public class HistoryResult {
    public VasResult.Result result = VasResult.Result.DECLINED;
    public String balance = "";
    public String message = "";
    public String macrosTID = "";
    public String key = "";
    public String commission = "";


    public enum Result {APPROVED, DECLINED}
}

package com.iisysgroup.payviceforagents.util;

import android.app.Activity;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.widget.Toast;

import com.dg.http.HttpRequest;
import com.dg.http.HttpResponse;
import com.iisysgroup.payviceforagents.R;

import java.util.Map;

/**
 * Created by Bamitale @Itex on 10/13/2015.
 */
public class EmailNotifier {
    Activity activity;

    public EmailNotifier(Activity activity) {
        this.activity = activity;
    }


    public void sendEmail(EmailObject emailObject, String recipient) {

        try {
            Map<String, String> params = new ArrayMap<>();
            params.put("from", "receipt@payvice.com");
            params.put("to", recipient);
//            params.put("to", "web-84t6j@mail-tester.com");

            params.put("subject", emailObject.subject);
            params.put("html", emailObject.message);
            params.put("text", emailObject.message);

            Map<String, String> headers = new ArrayMap<>();
            headers.put("content-type", "application/x-www-form-urlencoded");
            headers.put("authorization", "Basic YXBpOmtleS0zYWU2ZjZiOThjZWFiMWZlZWYwY2ZiOGE5M2EwOTJhNg==");
            headers.put("cache-control", "no-cache");

            HttpRequest request = HttpRequest.postRequest(activity.getString(R.string.mailgun_url), params);
            request.setHeaders(headers);

            HttpResponse response = request.getResponse();
            if (response.getStatusCode() != 200) throw new Exception(response.getStatusMessage());

            Log.i("Email", response.getStatusMessage());

            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, "email notification sent", Toast.LENGTH_SHORT).show();
                }
            });

        } catch (final Exception e) {
            e.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, "Could not send email", Toast.LENGTH_LONG).show();
                }
            });
        }


    }


    public static class EmailObject {
        public String subject;
        public String message;

    }
}

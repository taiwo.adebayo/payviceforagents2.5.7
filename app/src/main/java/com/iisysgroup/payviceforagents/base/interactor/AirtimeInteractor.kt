package com.iisysgroup.payviceforagents.base.interactor


import com.itex.richard.payviceconnect.model.AirtimeModel
import com.itex.richard.payviceconnect.model.DataModel
import io.reactivex.Single


interface AirtimeInteractor {
    fun getDataPlans(product: String): Single<DataModel.DataLookUpResponse>
    fun payForAirtime(phoneNumber: String, amount: Int, product: String, pin: String): Single<AirtimeModel.AirtimeResponse>
    fun payForData(phoneNumber: String,
                   plan: DataModel.DataResponseElements, pin: String): Single<DataModel.DataSubscriptionResponse>

    fun payForPin(phoneNumber: String,
                  plan: DataModel.DataResponseElements, pin: String): Single<DataModel.DataSubscriptionResponse>
   fun payForAirtimePin(phoneNumber: String, amount: Int, product: String, pin: String): Single<AirtimeModel.AirtimePinResponse>

}
package com.iisysgroup.payviceforagents.baseimpl.interactor

import android.content.Context
import com.iisysgroup.payviceforagents.base.interactor.MultichoiceInteractor
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
import com.iisysgroup.payviceforagents.util.StringUtils
import com.iisysgroup.payviceforagents.util.payviceServices
import com.itex.richard.payviceconnect.model.DstvModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MultichoiceInteractorImpl(val context: Context) : MultichoiceInteractor {

    private val terminalId by lazy {
        SecureStorage.retrieve(Helper.TERMINAL_ID, "")
    }

    private val userId by lazy {
        SecureStorage.retrieve(Helper.USER_ID, "")
    }

    override fun validateIucAndGetPlans(iuc: String, product: MultichoiceInteractor.MultichoiceProduct): Single<DstvModel.DstvResponse> {
        val lookupDetails = DstvModel.DstvLookup(product.name, iuc)

        return Single.fromObservable(context.payviceServices.DstvLookup(lookupDetails))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun subscribe(iuc: String, plan: DstvModel.Data,
                           product: MultichoiceInteractor.MultichoiceProduct, authPin: String): Single<DstvModel.PayResponse> {
        val clientReference = StringUtils.getClientRef(context, "")
        val subscriptionDetails = DstvModel.PayDetails(iuc, plan.product_code,
                userId, terminalId, null,  authPin, product.toString(), clientReference)

        return Single.fromObservable(context.payviceServices.DstvSubscribe(subscriptionDetails))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
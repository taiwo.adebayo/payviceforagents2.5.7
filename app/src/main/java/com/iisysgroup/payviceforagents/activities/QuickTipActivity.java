package com.iisysgroup.payviceforagents.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.zxing.Result;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.fragments.AuthorizePinFragment;
import com.iisysgroup.payviceforagents.fragments.StatusFragment;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.EmailHandler;
import com.iisysgroup.payviceforagents.util.Helper;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QuickTipActivity extends DefaultVasActivity implements ZXingScannerView.ResultHandler,
        AuthorizePinFragment.OnFragmentInteractionListener {
    public static final String RESULT = "result data";
    public static final int REF_CODE = 109;

    ZXingScannerView scannerView;

    EditText amountEdit, recipientEdit;
    Button amountProceedButton;
    View amountLayout, containerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_tip);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        scannerView = findViewById(R.id.scannerView);
        amountEdit = findViewById(R.id.amountEdit);
        recipientEdit = findViewById(R.id.recipientWalletId);
        amountLayout = findViewById(R.id.tipAmountLayout);
        containerView = findViewById(R.id.container);

        Log.d("encryptedPinQuickTip", "onCreate: "+ Helper.PIN);

        findViewById(R.id.tip_amount_proceed_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Helper.isValidAmountInput(amountEdit)) {
                    showAuthorizePinScreen();
                }
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        Dexter.withActivity(this).withPermission(android.Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Snackbar.make(scannerView, "Camera permission required to read QR",
                                Snackbar.LENGTH_SHORT).setAction(android.R.string.ok
                                , new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                                        intent.setData(uri);
                                        startActivityForResult(intent, 100);
                                    }
                                }).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).onSameThread().check();
    }


    @Override
    protected void onResume() {
        super.onResume();
        scannerView.startCamera();
        scannerView.resumeCameraPreview(this);

    }


    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        String code = result.getText().trim();
        verifyWalletIdQrCode(code);
    }

    private void showAmountScreen(String wallet) {
        recipientEdit.setText(wallet);
        amountLayout.setVisibility(View.VISIBLE);
    }

    private void showAuthorizePinScreen() {
        amountLayout.setVisibility(View.GONE);
        containerView.setVisibility(View.VISIBLE);
        loadFragment(new AuthorizePinFragment());
    }

    @Override
    public void onAuthorizePinFragmentInteraction(String pin) {
        String amount = Helper.sanitizeStringAmount(amountEdit.getText().toString());
        String beneficiary = recipientEdit.getText().toString();
        //pin = Helper.preparePin(pin);
        String pins = SecureStorage.retrieve(Helper.PIN, "");
        Log.d("quicktip", "amount " + amount + " beneficiary " + beneficiary + " pin " + pins);
        doQuickTip(amount, beneficiary, pins);
    }

    void doQuickTip(final String amount, final String beneficiary, final String userPin) {
        showProgressDialog("Quick Tip", "Tipping beneficiary.... Please wait", false);
        statusObject.setStatus(false);
        statusObject.setStatusAmount(Double.parseDouble(amount));

        statusObject.statusRecipient = beneficiary;
        statusObject.statusService = "Wallet Transfer";

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String pins = SecureStorage.retrieve(Helper.PIN, "");
                    Log.d("encryptpins", "run: "+ pins+" ");
                   // VasResult result = Requests.sendTransferRequest(context, beneficiary, amount, userPin);
                    VasResult result = Requests.sendTransferRequest(context, beneficiary, amount, pins);
                    String message = result.message, balance = result.balance;

                    statusObject.statusReason = message;
                    if (result.result == VasResult.Result.APPROVED) {
                        statusObject.setStatus(true);
                        EmailHandler.sendNotificationMail(QuickTipActivity.this, statusObject);
                    }

                } catch (IllegalStateException e) {
                    statusObject.statusReason = e.getMessage();
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    statusObject.statusReason = "An error occurred. Please try again later";
                } finally {
                    dismissProgressDialog(); 
                    Helper.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadFragment(StatusFragment.getInstance(statusObject));
                        }
                    });
                }
            }
        }).start();
    }

    void verifyWalletIdQrCode(final String code) {
        if (Helper.hasInternetConnectivity(this)) {
            final ProgressDialog progressDialog = ProgressDialog.show(this, getString(R.string.app_name), "Authenticating...", true, false);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final VasResult result = Requests.initUser(QuickTipActivity.this, code, "ernest.uduje@iisysgroup.com");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if (result.result == VasResult.Result.APPROVED) {
                                scannerView.stopCamera();
                                showAmountScreen(code);
                            } else {
                                String message = result.message;
                                if (message.contains("not found")) {
                                    message = "Invalid code";
                                }
                                Helper.showInfoDialog(QuickTipActivity.this, getString(R.string.app_name),
                                        message);
                                scannerView.resumeCameraPreview(QuickTipActivity.this);
                            }
                        }
                    });
                }
            }).start();
        }

    }


}

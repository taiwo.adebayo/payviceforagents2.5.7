package com.iisysgroup.payviceforagents.baseimpl.interactor


import android.content.Context
import android.util.Log
import com.iisysgroup.payviceforagents.base.interactor.AirtimeInteractor
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.Helper
import com.iisysgroup.payviceforagents.util.StringUtils
import com.iisysgroup.payviceforagents.util.payviceServices
import com.itex.richard.payviceconnect.model.*

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AirtimeInteractorImpl(private val context: Context) : AirtimeInteractor {

    private val terminalId by lazy {
        SecureStorage.retrieve(Helper.TERMINAL_ID, "")
    }

    private val userId by lazy {
        SecureStorage.retrieve(Helper.USER_ID, "")
    }

    override fun getDataPlans(product: String): Single<DataModel.DataLookUpResponse> {
        val lookDetails = DataModel.DataLookUpDetails(product)

        return Single.fromObservable(context.payviceServices.DataLookup(lookDetails).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()))
    }

    override fun payForAirtime(phoneNumber: String, amount: Int, product: String, pin: String): Single<AirtimeModel.AirtimeResponse> {
        val clientReference = StringUtils.getClientRef(context, "")
        Log.d("clientrefAIrtime", clientReference+"")
        val airtimeDetails = AirtimeModel.AirtimeRequestDetails(terminalId, userId, amount.toString(), phoneNumber, product, pin, "", clientReference)

        return Single.fromObservable(context.payviceServices.PurchaseAirtime(airtimeDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()))
    }

    override fun payForAirtimePin(phoneNumber: String, amount: Int, product: String, pin: String): Single<AirtimeModel.AirtimePinResponse> {

        val clientReference = StringUtils.getClientRef(context, "")
        Log.d("clientrefAIrtime", clientReference+"")
        val airtimeDetails = AirtimeModel.AirtimeRequestDetails(terminalId, userId, amount.toString(), phoneNumber, product, pin, "", clientReference)

        return Single.fromObservable(context.payviceServices.PurchaseAirtimePin(airtimeDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()))
    }

    override fun payForData(phoneNumber: String, plan: DataModel.DataResponseElements, pin: String): Single<DataModel.DataSubscriptionResponse> {
        val clientReference = StringUtils.getClientRef(context, "")
        val dataDetails = DataModel.DataSubscriptionDetails(terminalId, userId, "",
                pin, phoneNumber, plan.type, plan.amount, plan.description, plan.code, clientReference)

        return Single.fromObservable(context.payviceServices.DataSubscribe(dataDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()))
    }

    override fun payForPin(phoneNumber: String, plan: DataModel.DataResponseElements, pin: String): Single<DataModel.DataSubscriptionResponse> {

        return payForData(phoneNumber, plan, pin)
    }
}
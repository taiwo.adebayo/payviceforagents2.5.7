package com.iisysgroup.payviceforagents.baseimpl.presenter

import android.telephony.PhoneNumberUtils
import com.iisysgroup.payviceforagents.base.interactor.RegistrationInteractor
import com.iisysgroup.payviceforagents.base.presenter.RegistrationPresenter
import com.iisysgroup.payviceforagents.base.view.RegistrationView
import com.iisysgroup.payviceforagents.entities.RegistrationData
import com.iisysgroup.payviceforagents.entities.VasResult
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.securestorage.SecureStorageUtils
import com.iisysgroup.payviceforagents.util.Helper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RegistrationPresenterImpl(private val interactor: RegistrationInteractor,
                                private val view: RegistrationView) : RegistrationPresenter {


    override fun validateData(data: RegistrationData) {

        if (!isValidName(data.name)) {
            view.onNameValidationError()
            return
        }

        if (!isValidPhone(data.phone)) {
            view.onPhoneValidationError()
            return
        }

        if (data.email.isNotBlank() && !isValidEmail(data.email)) {
            view.onEmailValidationError()
            return
        }

        if (!isValidPassword(data.password)) {
            view.onPasswordValidationError()
            return
        }

        if (!isValidPin(data.pin)) {
            view.onPinValidationError()
            return
        }

        registerUser(data)
    }

    private fun registerUser(data: RegistrationData) {
        view.onShowProgress()
        interactor.register(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result, error ->
                    view.onDismissProgress()

                    error?.let {
                        view.setRegistrationError(it)
                    }

                    result?.let {
                        if (result.result != VasResult.Result.APPROVED) {
                            val message = if (result.message == "USER ALREADY REGISTER") {
                                "This user id is in use by another user. Please choose another"
                            } else {
                                result.message
                            }
                            view.onDismissProgress()
                            view.setRegistrationError(RuntimeException(message))
                        } else {
                            saveDataForVerification(data, result)

                            view.onRegistrationSuccess()
                        }
                    }

                }
    }


    private fun saveDataForVerification(data: RegistrationData, vasResult: VasResult) {

        val terminalID = vasResult.macrosTID
        val balance = vasResult.balance
        val userKey = vasResult.key


        SecureStorage.store(Helper.VERIFYING, true)
        SecureStorage.store(Helper.USER_ID, data.userID)
        SecureStorage.store(Helper.USER_EMAIL, data.email)
        SecureStorage.store(Helper.USERNAME, data.name)
        SecureStorage.store(Helper.USER_PHONE, data.phone)
        SecureStorage.store(Helper.USER_KEY, userKey)
        SecureStorage.store(Helper.TERMINAL_ID, terminalID)
        SecureStorage.store(Helper.BALANCE, balance)

        val key = Helper.decryptUserKey(terminalID, userKey)

        val hashedPassword = SecureStorageUtils.hashIt(data.password, key)


        val hashedPin = SecureStorageUtils.hashIt(data.pin, hashedPassword!!)
        SecureStorage.store(Helper.PLAIN_PASSWORD,data.password)
        SecureStorage.store(Helper.PIN, hashedPin)
        SecureStorage.store(Helper.STORED_PASSWORD, hashedPassword)
    }

    override fun setReferralCode(code: String) {
        view.onDisplayReferral(code)
    }


    private fun isValidEmail(email: String) = Helper.validateEmail(email)

    private fun isValidPhone(phone: String) = PhoneNumberUtils.isGlobalPhoneNumber(phone)

    private fun isValidName(name: String) = name.isNotBlank()

    private fun isValidPassword(password: String) = password.isNotBlank() && password.length >= 8

    private fun isValidPin(pin: String) = pin.isNotBlank() && pin.length >= 4
}
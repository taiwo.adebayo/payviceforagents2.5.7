package com.iisysgroup.payviceforagents.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.fragments.BillPinInputFragment;
import com.iisysgroup.payviceforagents.fragments.BillPlanInputFragment;
import com.iisysgroup.payviceforagents.fragments.BillPlanValueInputFragment;
import com.iisysgroup.payviceforagents.fragments.BillValueInputFragment;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.VasServices;

public class BillPaymentActivity extends DefaultVasActivity {

    public static final String SERVICE = "SERVICE";

     static String service;
    Fragment fragmentToLoad = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_default_vas);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //loadFragment(new PayBillsFragment());

        service = getIntent().getStringExtra(SERVICE);
        Log.i("service................................" , service);

        Log.i("VasServices.SERVICES.get(service).type;;;; " , VasServices.SERVICES.get(service).type.toString());

        if (service == null || !VasServices.SERVICES.containsKey(service))
            finish();


        is_bill_payment = true;
        convenienceFee = SecureStorage.retrieve(Helper.COMMISSION_KEY, Helper.default_commission);



        switch (VasServices.SERVICES.get(service).type) {
            case PIN:
                Log.i("service................................" , "ok");

                fragmentToLoad = BillPinInputFragment.newInstance(service);
                break;
            case PLAN:
                Log.i("service................................" , "ok");

                fragmentToLoad = BillPlanInputFragment.newInstance(service);
                break;
            case PLAN_VALUE:
                Log.i("service................................" , "ok");

                fragmentToLoad = BillPlanValueInputFragment.newInstance(service);
                break;
            default:
                Log.i("service................................" , "Here");

                fragmentToLoad = BillValueInputFragment.Companion.newInstance(service);
                DefaultVasActivity.service =service;
                Log.i(DefaultVasActivity.service +"................................" , DefaultVasActivity.service);
        }

        loadFragment(fragmentToLoad);

    }


    @Override
    public void onBackPressed() {
        Log.i("fragment.getClass().getSimpleName() Bill Payment Activity",fragmentToLoad.getClass().getSimpleName());
        if (fragmentToLoad.getClass().getSimpleName().trim().equals("StatusFragment"))
           finish();
//        BillValueInputFragment
        else {
            if (fm.getBackStackEntryCount() > 1) {
                fm.popBackStack();
            } else
                NavUtils.navigateUpFromSameTask(this);
        }
    }




}

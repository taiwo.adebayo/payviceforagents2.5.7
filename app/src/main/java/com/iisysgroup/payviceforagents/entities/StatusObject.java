package com.iisysgroup.payviceforagents.entities;

import com.iisysgroup.payviceforagents.R;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Created by Bamitale @Itex on 3/7/2016.
 */
public class StatusObject implements Serializable {

    public static final String STATUS_APPROVED = "Transaction Approved";
    public static final String STATUS_DECLINED = "Transaction Declined";

    public static final int STATUS_APPROVED_DRAWABLE = R.drawable.ic_check_circle_24dp;
    public static final int STATUS_DECLINED_DRAWABLE = R.drawable.ic_declined_vector;
    public String statusRecipient = "Self", statusService = "", statusReason;
    private boolean successful;
    private String statusAmount = "\u20A6 0";

    private int status_drawable;
    private String statusMessage;

    public StatusObject(boolean successful) {
        setStatus(successful);
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public int getStatus_drawable() {
        return status_drawable;
    }

    public String getStatusAmount() {
        return statusAmount;
    }

    public void setStatusAmount(double statusAmount) {
        String temp = DecimalFormat.getInstance().format(statusAmount);
        temp = '₦' + " " + temp;
        this.statusAmount = temp;
    }

    public void setStatusAmountInKobo(long statusAmount) {
        double amt = statusAmount / 100;
        setStatusAmount(amt);
    }

    public final void setStatus(boolean successful) {
        this.successful = successful;

        processStatus();
    }

    public final boolean statusIsApproved() {
        return successful;
    }

    private void processStatus() {
        if (successful) {
            status_drawable = STATUS_APPROVED_DRAWABLE;
            statusMessage = STATUS_APPROVED;
        } else {
            status_drawable = STATUS_DECLINED_DRAWABLE;
            statusMessage = STATUS_DECLINED;
        }

    }
}

package com.iisysgroup.payviceforagents.models;

public class Journal
{
    public String stan;

    public Journal(String stan, String mPan, String rrn, String acode, Long amount, String timestamp, String mti, String ps, String resp, Boolean tap, String rep, String vm, String ostan, String orrn, String oacode, String mid, String mcc, String transMethod) {
        this.stan = stan;
        this.mPan = mPan;
        this.rrn = rrn;
        this.acode = acode;
        this.amount = amount;
        this.timestamp = timestamp;
        this.mti = mti;
        this.ps = ps;
        this.resp = resp;
        this.tap = tap;
        this.rep = rep;
        this.vm = vm;
        this.ostan = ostan;
        this.orrn = orrn;
        this.oacode = oacode;
        this.mid = mid;
        this.mcc = mcc;
        this.transMethod = transMethod;
    }

    public String mPan;
    public String rrn;
    public String acode;
    public Long amount;
    public String timestamp;
    public String mti;
    public String ps;
    public String resp;
    public Boolean tap;
    public String rep;
    public String vm ;
    public String ostan;
    public String orrn;
    public String oacode;
    public String mid;
    public String mcc;
    public String transMethod;


}

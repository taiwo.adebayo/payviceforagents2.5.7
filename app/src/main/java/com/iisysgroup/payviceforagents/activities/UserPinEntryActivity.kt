package com.iisysgroup.payviceforagents.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.Menu
import android.view.MenuItem
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.util.Helper
import kotlinx.android.synthetic.main.activity_user_pin_entry.*

class UserPinEntryActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_pin_entry)

        initializeView()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.default_activity_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_cancel -> finish()
        }
        return super.onOptionsItemSelected(item)
    }


    private fun initializeView() {
        pinEdit.nextFocusable = pinEdit2

        pinEdit2.nextFocusable = pinEdit3
        pinEdit2.previousFocusable = pinEdit

        pinEdit3.nextFocusable = pinEdit4
        pinEdit3.previousFocusable = pinEdit2

        pinEdit4.previousFocusable = pinEdit3
        pinEdit4.action = {
            proceedBtn.performClick()
        }


        proceedBtn.setOnClickListener {
            if (pinEdit.text.toString().isEmpty() ||
                    pinEdit2.text.toString().isEmpty() ||
                    pinEdit3.text.toString().isEmpty() ||
                    pinEdit4.text.toString().isEmpty()) {

                Snackbar.make(pinEdit, "Invalid Pin", Snackbar.LENGTH_SHORT).show()
                pinEdit.requestFocus()
            } else {

                onButtonPressed(extractPin())
            }
        }

        Helper.showSoftKeyboard(this)
    }


    private fun onButtonPressed(pin: String) {
        val encryptedPin = Helper.preparePin(pin)

        val intent = Intent()

        intent.putExtra(PIN_RESPONSE_DATA, encryptedPin)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun extractPin(): String {
        val pin = String.format("%s%s%s%s", pinEdit.text.toString(), pinEdit2.text.toString(),
                pinEdit3.text.toString(), pinEdit4.text.toString())

        return pin
    }


    companion object {
        const val PIN_REQUEST_CODE = 1009
        const val PIN_RESPONSE_DATA = "pin_data"
    }
}

package com.iisysgroup.payviceforagents.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.persistence.entitiy.Card;
import com.iisysgroup.payviceforagents.util.Helper;


public class CvvCheckFragment extends Fragment implements View.OnClickListener {

    Card card;
    TextView cvvEdit, pinEdit;
    TextInputLayout cvvTIL, pinTIL;
    Button button;
    String currentString = "";
    Vibrator vibrator;

    int numberPadKeys[] = {R.id.btn_0, R.id.btn_1, R.id.btn_2, R.id.btn_3, R.id.btn_4, R.id.btn_5
            , R.id.btn_6, R.id.btn_7, R.id.btn_8, R.id.btn_9, R.id.btn_Del};

    private OnFragmentInteractionListener mListener;

    public CvvCheckFragment() {
        // Required empty public constructor
    }

    public static CvvCheckFragment newInstance(Card card) {
        CvvCheckFragment fragment = new CvvCheckFragment();
        fragment.card = card;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("Enter Card Pin");

        View view = inflater.inflate(R.layout.fragment_cvv_check, container, false);

        for (int key : numberPadKeys) {
            View keyView = view.findViewById(key);
            keyView.setOnClickListener(this);
            keyView.setHapticFeedbackEnabled(true);
        }

        pinEdit = view.findViewById(R.id.editPin);
        button = view.findViewById(R.id.proceedBtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid())
                    onButtonPressed(null);
            }
        });

        return view;
    }

    public void onButtonPressed(String input) {
        mListener.onCardPinInteraction(card, pinEdit.getText().toString().trim());
        Helper.hideSoftKeyboard(getActivity());
    }


    boolean isValid() {

        if (pinEdit.getText().toString().length() < 4) {
            Snackbar.make(pinEdit, "Enter a valid pin", Snackbar.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View v) {
        // v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        vibrator.vibrate(100);

        if (v.getId() == R.id.btn_Del)
            processDelete();
        else {
            FrameLayout fl = (FrameLayout) v;

            TextView textView = (TextView) fl.getChildAt(0);

            processInput(textView.getText().toString());
        }
    }


    protected void processDelete() {
        currentString = pinEdit.getText().toString().trim();

        if (!TextUtils.isEmpty(currentString)) {
            currentString = currentString.substring(0, currentString.length() - 1);
        }

        pinEdit.setText(currentString);
    }


    protected void processInput(String input) {
        if (input != null && !input.isEmpty()) {
            currentString = pinEdit.getText().toString();

            currentString += input;
            pinEdit.setText(currentString);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onCardPinInteraction(Card card, String cardPin);
    }
}

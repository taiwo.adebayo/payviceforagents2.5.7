package com.iisysgroup.payviceforagents.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.login.LoginActivity;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;

import java.util.HashMap;
import java.util.Map;


public class VerificationActivity extends AppCompatActivity {

    static String defaultPassword = "1234";
    Button verifyButton;
    EditText verifyEdit;
    TextView emailView;
    TextInputLayout verifyTIL;
    ProgressDialog progressDialog;
    Handler handler = new Handler();


//    boolean receiverIsRegistered;
//    BroadcastReceiver smsReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
//                Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
//                SmsMessage[] msgs = null;
//                String msg_from;
//                if (bundle != null) {
//                    //---retrieve the SMS message received---
//                    try {
//                        Object[] pdus = (Object[]) bundle.get("pdus");
//                        msgs = new SmsMessage[pdus.length];
//                        for (int i = 0; i < msgs.length; i++) {
//                            msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
//                            msg_from = msgs[i].getOriginatingAddress();
//                            String msgBody = msgs[i].getMessageBody();
//
//
//                            if (msg_from.equalsIgnoreCase("Payvice")) {
//                                final String code = msgBody.substring((msgBody.indexOf(":") + 1)).trim();
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        verifyEdit.setText(code);
//                                        doVerification();
//                                    }
//                                });
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }

 //       }
 //   };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_verification_menu, menu);

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        verifyButton = findViewById(R.id.verify_button);
        verifyEdit = findViewById(R.id.ver_code);
        emailView = findViewById(R.id.supplied_email);
        verifyTIL = findViewById(R.id.ver_code_til);

        String userID = SecureStorage.retrieve(Helper.USER_ID, "");

        emailView.setText(userID);
        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Helper.hasInternetConnectivity(VerificationActivity.this))
                    doVerification();

            }
        });

//        Dexter.withActivity(this)
//                .withPermissions(android.Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS)
//                .withListener(new MultiplePermissionsListener() {
//                    @Override
//                    public void onPermissionsChecked(MultiplePermissionsReport report) {
//                        if (report.areAllPermissionsGranted()) {
//                            IntentFilter intentFilter = new IntentFilter();
//                            intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
//                            registerReceiver(smsReceiver, intentFilter);
//                            receiverIsRegistered = true;
//                        }
//                    }
//
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//
//                    }
//                })
//                .check();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_cancel) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    void doVerification() {
        if (isValidated()) {
            progressDialog = ProgressDialog.show(this, "Verifying", "Please wait...", true, false);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        verify();
                    } catch (IllegalAccessException e) {
                        Helper.showInfoDialog(VerificationActivity.this, "Verification Failed",
                                e.getMessage());
                        e.printStackTrace();
                        dismissProgressDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Helper.showInfoDialog(VerificationActivity.this, "Verification Failed",
                                "Please check your details and try again.");
                        dismissProgressDialog();
                    }
                }
            }).start();
        }
    }


    boolean isValidated() {
        String code = verifyEdit.getText().toString();
        if (code.length() < 4) {
            verifyTIL.setErrorEnabled(true);
            verifyTIL.setError("Enter a valid code");
            verifyEdit.requestFocus();
            Helper.showErrorAnim(verifyEdit);
            return false;
        }

        return true;
    }

    void verify() throws Exception {
        if (!SecureStorage.retrieve(Helper.VERIFYING, false)) {
            finish();
        }

        String result = "", message = "";
        final String url = getResources().getString(R.string.tams_url),
                PIN_ACTION = "TAMS_PIN_UPDATE";

        String userId = SecureStorage.retrieve(Helper.USER_ID, "");
        String terminalID = SecureStorage.retrieve(Helper.TERMINAL_ID, "");
        String password = SecureStorage.retrieve(Helper.STORED_PASSWORD, "");
        String verCode = verifyEdit.getText().toString();
        String pin = SecureStorage.retrieve(Helper.PIN, "");
        String referralCode = SecureStorage.retrieve(Helper.REFERRAL_CODE, ""); //12204418

        Map<String, String> params = new HashMap<>();

        params.put("action", PIN_ACTION);
        params.put("userid", userId);
        params.put("termid", terminalID);
        params.put("password", defaultPassword);
        params.put("newpassword", password);
        params.put("pin", defaultPassword);
        params.put("newpin", pin);
        params.put("vercode", verCode);

        if (!referralCode.isEmpty())
            params.put("referalcode", referralCode);

        VasResult tamsResult = Requests.processRequest(url, params);

        if (tamsResult.result != VasResult.Result.APPROVED)
            throw new IllegalAccessException("Invalid verification code");


        SecureStorage.store(Helper.VERIFYING, false);
        SecureStorage.store(Helper.LOGGED_IN, true);
        SecureStorage.delete(Helper.PIN);

        dismissProgressDialog();
        finish();
        startActivity(new Intent(this, MainActivity.class));

    }

    void dismissProgressDialog() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        Toast.makeText(this, "Can't go back at this stage.", Toast.LENGTH_SHORT).show();
        new AlertDialog.Builder(this)
                .setTitle("User Verification")
                .setMessage("Are you sure you want to cancel verification?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    }
                })
                .show();
    }

    @Override
    protected void onPause() {
        super.onPause();

//        if (receiverIsRegistered) {
//            unregisterReceiver(smsReceiver);
//        }
    }


}

package com.iisysgroup.payviceforagents.activities

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.base.presenter.SmileServicePresenter
import com.iisysgroup.payviceforagents.baseimpl.viewmodel.SmileServiceViewModel
import com.iisysgroup.payviceforagents.dialogs.SmileBundleDialog
import com.iisysgroup.payviceforagents.entities.Service
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.util.*
import com.itex.richard.payviceconnect.model.SmileModel
import com.jakewharton.rxbinding2.widget.textChanges
import kotlinx.android.synthetic.main.activity_smile_service.*
import kotlinx.android.synthetic.main.content_smile_service.*
import java.util.concurrent.TimeUnit

class SmileServiceActivity : BaseServiceActivity() {

    private var bundleDialog: SmileBundleDialog? = null
    private lateinit var topUpProduct: Service.Product
    private lateinit var bundleProduct: Service.Product
    private var accountName = ""

    private val viewModel: SmileServicePresenter by lazy {
        SmileServiceViewModel(application)
    }

    private var userIsValidated = false
    private var selectedBundle: SmileModel.Bundle? = null

    private val alertDialog by lazy {
        AlertDialog.Builder(this)
                .setTitle("Validate Account")
                .setNegativeButton(android.R.string.cancel) { _, _ ->
                    userIsValidated = false
                    beneficiaryEdit.requestFocus()
                }.setPositiveButton("Confirm Account") { _, _ ->
                    userIsValidated = true
                }.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_smile_service)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        intent?.let {
            it.getStringExtra(BillPaymentActivity.SERVICE)?.let {
                service = VasServices.SERVICES[it]!!
                topUpProduct = service.products[0]
                bundleProduct = service.products[1]
                recipientTIL.hint = VasServices.VAS_SERVICE_INPUT_TEXT_DESC[service.name]
            }
        } ?: kotlin.run {
            finish()
        }

    }

    override fun initControls() {
        super.initControls()

        beneficiaryEdit.textChanges().delay(300L, TimeUnit.MILLISECONDS)
                .subscribe {
                    it?.let {
                        if (it.length >= 10) {
                            viewModel.validate(it.toString())
                        }
                    }
                }

        topUpRadBtn.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.setProduct(topUpProduct)
            }
        }

        bundlesRadBtn.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.setProduct(bundleProduct)
            }
        }

        selectAmountLayout.setOnClickListener {
            viewModel.getBundles().observe(this, Observer {
                it?.let {
                    bundleDialog?.dismiss()
                    bundleDialog = SmileBundleDialog.newInstance(it, viewModel::setBundleSelection)
                    bundleDialog?.show(supportFragmentManager, "Bundle Dialog")
                }
            })
        }

        proceedBtn.setOnClickListener {
            if (isValidInput()) {
                showPinEntry()
            }
        }

        viewModel.onSetProduct().observe(this, Observer {
            it?.let {
                productText.text = it.name
                when (it.name) {
                    topUpProduct.name -> {
                        selectAmountLayout.visibility = View.GONE
                        amountEntryLayout.visibility = View.VISIBLE
                    }

                    bundleProduct.name -> {
                        selectAmountLayout.visibility = View.VISIBLE
                        amountEntryLayout.visibility = View.GONE
                    }
                }
            }
        })

        viewModel.onError().observe(this, Observer {
            it?.let(this::showError)
        })

        viewModel.onProgressUpdate().observe(this, Observer {
            it?.let {
                showProgressDialog(it.first, it.second)
            }
        })

        viewModel.onValidateAccount().observe(this, Observer {
            it?.let {
                accountName = it
                if (!alertDialog.isShowing) {
                    alertDialog.setMessage("Customer Name: $accountName" +
                            "\nAccount ID: ${beneficiaryEdit.text}")
                    alertDialog.show()
                }
            }
        })


        viewModel.observeSelectedBundle().observe(this, Observer {
            selectedBundle = it
            it?.let {
                bundleDialog?.dismiss()
                selectableAmountText.text = "${it.displayPrice / 100} - ${it.name}"
            }
        })

        viewModel.observePaymentResponse().observe(this, Observer {
            it?.let(this::showResultScreen)
        })

    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        bundlesRadBtn.isChecked = true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == UserPinEntryActivity.PIN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                encryptedUserPin = data!!.getStringExtra(UserPinEntryActivity.PIN_RESPONSE_DATA)

                val amount = if (isBundleProduct()) {
                    selectedBundle!!.displayPrice / 100
                } else {
                    Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
                }

                showPaymentOption(PaymentOption.Mode.PAY, amount, accountName)
            }
        }

        if (requestCode == PaymentOptionActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val paymentOption = data?.getSerializableExtra(PaymentOptionActivity.RESULT_OPTION) as PaymentOption
                val card = data.getSerializableExtra(PaymentOptionActivity.RESULT_CARD) as? Card
                continuePayment(paymentOption, card)
            }
        }
    }

    private fun continuePayment(paymentOption: PaymentOption, card: Card?) {
        val accountId = beneficiaryEdit.text.toString()
        when (paymentOption) {
            PaymentOption.WALLET -> {
                if (isBundleProduct()) {
                    viewModel.subscribe(accountId, encryptedUserPin)
                } else {
                    val amount = Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
                    viewModel.topUp(accountId, encryptedUserPin, amount)
                }
            }

            else -> {
                val cardProcessor = DebitCardProcessor(this, encryptedUserPin)
                if (isBundleProduct()) {
                    viewModel.subscribeWithCard(accountId, card, cardProcessor)
                } else {
                    val amount = Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
                    viewModel.topUpWithCard(accountId, amount, card, cardProcessor)
                }
            }
        }
    }

    private fun isValidInput(): Boolean {
        if (!userIsValidated || beneficiaryEdit.length() < 8) {
            beneficiaryEdit.error = "Please Confirm Account"
            Helper.showErrorAnim(beneficiaryEdit)
            return false
        }

        if (topUpRadBtn.isChecked) {
            if (amountEdit.text.isNullOrBlank() ||
                    Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt() < 50) {

                Helper.showErrorAnim(amountEdit)
                amountEdit.error = "Invalid amount"
                return false
            }
        }

        if (isBundleProduct() && selectedBundle == null) {
            Helper.showErrorAnim(selectAmountLayout)
            selectableAmountText.error == "Please select a bundle"
            return false
        }

        return true
    }


    private fun isBundleProduct() = bundlesRadBtn.isChecked


    override fun getHistoryLayout(): View? {
        return historyLayout
    }

    override fun getHistoryListView(): RecyclerView? {
        return list
    }

    override fun getSubTitleView(): TextView? {
        return subTitleText
    }

    override fun getServiceImageView(): ImageView? {
        return serviceImage
    }

    override fun onSelectProduct(product: Service.Product) {

    }
}

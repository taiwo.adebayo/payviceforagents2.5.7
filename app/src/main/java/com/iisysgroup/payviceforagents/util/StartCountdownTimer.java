package com.iisysgroup.payviceforagents.util;

import android.app.Activity;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Toast;

import com.iisysgroup.payviceforagents.R;

public class StartCountdownTimer extends Activity {

    private CountDownTimer countDownTimer;
    private long timeCountInMilliSeconds = 20000;

    public void startCountDownTimer() {

        countDownTimer = new CountDownTimer(timeCountInMilliSeconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Toast.makeText(getBaseContext(), "This is taking time, please check your network or tap out", Toast.LENGTH_LONG).show();
            }

        }.start();
        countDownTimer.start();
    }

    public void stopCountDownTimer() {
        if (countDownTimer!=null){
            countDownTimer.cancel();
        }

    }
}

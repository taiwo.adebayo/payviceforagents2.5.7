package com.iisysgroup.payviceforagents.base.presenter

import android.arch.lifecycle.LiveData
import com.iisysgroup.payviceforagents.entities.results.SmileServiceResult
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.itex.richard.payviceconnect.model.SmileModel

interface SmileServicePresenter : BaseServicePresenter {

    fun validate(accountId: String)
    fun onValidateAccount(): LiveData<String>

    fun getBundles(): LiveData<List<SmileModel.Bundle>>

    fun setBundleSelection(which: Int)
    fun observeSelectedBundle(): LiveData<SmileModel.Bundle>

    fun topUp(accountId: String, encryptedPin: String, amount: Int)
    fun topUpWithCard(accountId: String, amount: Int, card: Card?, cardProcessor: DebitCardProcessor)

    fun subscribe(accountId: String, encryptedPin: String)
    fun subscribeWithCard(accountId: String, card: Card?, cardProcessor: DebitCardProcessor)

    fun observePaymentResponse(): LiveData<SmileServiceResult>

}
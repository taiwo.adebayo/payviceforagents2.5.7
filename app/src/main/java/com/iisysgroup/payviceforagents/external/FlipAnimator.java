package com.iisysgroup.payviceforagents.external;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */
public class FlipAnimator extends Animation {
    public static final int DIRECTION_X = 1;
    public static final int DIRECTION_Y = 2;
    public static final int DIRECTION_Z = 3;
    private Camera camera;
    private View fromView;
    private View toView;
    private float centerX;
    private float centerY;
    private boolean forward = true;
    private boolean visibilitySwapped;
    private int rotationDirection = 1;
    private int translateDirection = 3;

    public FlipAnimator(View fromView, View toView, int centerX, int centerY) {
        this.fromView = fromView;
        this.toView = toView;
        this.centerX = (float) centerX;
        this.centerY = (float) centerY;
        this.setDuration(500L);
        this.setFillAfter(true);
        this.setInterpolator(new AccelerateDecelerateInterpolator());
    }

    public int getRotationDirection() {
        return this.rotationDirection;
    }

    public void setRotationDirection(int rotationDirection) {
        this.rotationDirection = rotationDirection;
    }

    public int getTranslateDirection() {
        return this.translateDirection;
    }

    public void setTranslateDirection(int translateDirection) {
        this.translateDirection = translateDirection;
    }

    public void reverse() {
        this.forward = false;
        View temp = this.toView;
        this.toView = this.fromView;
        this.fromView = temp;
    }

    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
        this.camera = new Camera();
    }

    protected void applyTransformation(float interpolatedTime, Transformation t) {
        double radians = 3.141592653589793D * (double) interpolatedTime;
        float degrees = (float) (180.0D * radians / 3.141592653589793D);
        if (interpolatedTime >= 0.5F) {
            degrees -= 180.0F;
            if (!this.visibilitySwapped) {
                this.fromView.setVisibility(View.GONE);
                this.toView.setVisibility(View.VISIBLE);
                this.visibilitySwapped = true;
            }
        }

        if (this.forward) {
            degrees = -degrees;
        }

        Matrix matrix = t.getMatrix();
        this.camera.save();
        if (this.translateDirection == 3) {
            this.camera.translate(0.0F, 0.0F, (float) (150.0D * Math.sin(radians)));
        } else if (this.translateDirection == 2) {
            this.camera.translate(0.0F, (float) (150.0D * Math.sin(radians)), 0.0F);
        } else {
            this.camera.translate((float) (150.0D * Math.sin(radians)), 0.0F, 0.0F);
        }

        if (this.rotationDirection == 3) {
            this.camera.rotateZ(degrees);
        } else if (this.rotationDirection == 2) {
            this.camera.rotateY(degrees);
        } else {
            this.camera.rotateX(degrees);
        }

        this.camera.getMatrix(matrix);
        this.camera.restore();
        matrix.preTranslate(-this.centerX, -this.centerY);
        matrix.postTranslate(this.centerX, this.centerY);
    }
}


package com.iisysgroup.payviceforagents.fragments;


import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.StatusObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class StatusFragment extends DialogFragment {

    static StatusFragment instance;
    protected StatusObject statusObject = new StatusObject(false);
    ImageView statusImageView;
    TextView statusMessageTextView, statusReasonTextView;
    Button finishButton;


    public StatusFragment() {
        // Required empty public constructor
    }

    public static StatusFragment getInstance(StatusObject statusObject) {
        instance = new StatusFragment();
        instance.statusObject = statusObject;

        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.status_screen, container, false);
        getActivity().setTitle("Status");


        initViews(view);
        return view;
    }

    protected void initViews(View view) {
        if (statusObject == null)
            getActivity().finish();

        AppBarLayout appBarLayout = getActivity().findViewById(R.id.appbar);



        if (appBarLayout != null)
            appBarLayout.setExpanded(false, true);


        statusImageView = view.findViewById(R.id.status_image);
        statusMessageTextView = view.findViewById(R.id.status_message);
        statusReasonTextView = view.findViewById(R.id.status_reason);
        try{
           statusImageView.setImageResource(statusObject.getStatus_drawable());
           statusMessageTextView.setText(statusObject.getStatusMessage());
           statusReasonTextView.setText(statusObject.statusReason);
       }catch (Exception e){

        }


        finishButton = view.findViewById(R.id.status_action);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

    }


}

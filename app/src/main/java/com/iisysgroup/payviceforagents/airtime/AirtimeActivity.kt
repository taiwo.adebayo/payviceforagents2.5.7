package com.iisysgroup.payviceforagents.airtime

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.telephony.PhoneNumberUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.activities.BaseServiceActivity
import com.iisysgroup.payviceforagents.activities.BillPaymentActivity
import com.iisysgroup.payviceforagents.activities.PaymentOptionActivity
import com.iisysgroup.payviceforagents.activities.UserPinEntryActivity
import com.iisysgroup.payviceforagents.baseimpl.viewmodel.AirtimeActivityViewModel
import com.iisysgroup.payviceforagents.dialogs.AirtimeDataPlanDialog
import com.iisysgroup.payviceforagents.entities.Service
import com.iisysgroup.payviceforagents.fragments.AmountTextWatcher
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.util.*
import com.itex.richard.payviceconnect.model.DataModel
import kotlinx.android.synthetic.main.activity_airtime.*
import kotlinx.android.synthetic.main.content_airtime.*
import java.util.*

class AirtimeActivity : BaseServiceActivity() {

    private val viewModel by lazy {
        ViewModelProviders.of(this)[AirtimeActivityViewModel::class.java]
    }

    private var dialog: BottomSheetDialogFragment? = null
    private var plan: DataModel.DataResponseElements? = null

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airtime)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        intent?.let {
            it.getStringExtra(BillPaymentActivity.SERVICE)?.let {
                service = VasServices.SERVICES[it]!!
//                viewModel.setProduct(service.products[0])
            }
        } ?: kotlin.run {
            finish()
        }

    }


    override fun initControls() {
        super.initControls()
        amountEdit?.addTextChangedListener(AmountTextWatcher(amountEdit))

        contactButton.setOnClickListener {
            getPhoneContacts()
        }

        selectProductBtn.setOnClickListener {
            selectProduct()
        }

        proceedBtn.setOnClickListener {
//            Log.i("Yeah button",   product!!.proxyCode)
//            Log.i("yeah isValidInputs",  isValidInputs().toString())

            if (isValidInputs()) {
                showPinEntry()
            }

        }

        selectAmountLayout.setOnClickListener {
          Log.i("Yeah ",   product!!.proxyCode)
            Log.i("Yeah Bool ",   (product!!.proxyCode.contains(Regex("DATA"))).toString())

            viewModel.loadPlans().observe(this, Observer {
                it?.let {
                    dialog?.dismiss()
                    dialog = AirtimeDataPlanDialog.newInstance(ArrayList(it)) {
                        viewModel.setSelectedPlan(it)
                    }
                    dialog?.show(supportFragmentManager, "Data Dialog")
                }

            })
        }


        viewModel.onSetProduct().observe(this, Observer {
            it?.let {
                product = it
                productText.text = product?.name

                if (currentProductIsDataOrPin()) {

                    if (product!!.proxyCode.contains(Regex("DATA"))) {
                        selectAmountLayout.visibility = View.VISIBLE
                        amountLayout.visibility = View.GONE
                    }else{
                        selectAmountLayout.visibility = View.GONE
                        amountLayout.visibility = View.VISIBLE
//
                    }



                } else {
                    selectAmountLayout.visibility = View.GONE
                    amountLayout.visibility = View.VISIBLE
                }
            }
        })

        viewModel.onError().observe(this, Observer {
            showProgressDialog(false)
            showError(it)
        })

        viewModel.selectedPlanLiveData.observe(this, Observer {
            dialog?.dismiss()
            it?.let {
                plan = it
                selectableAmountText.text = plan?.amount
            }
        })

        viewModel.onProgressUpdate().observe(this, Observer {
            it?.let {
                showProgressDialog(it.first, it.second)
            }
        })

        viewModel.paymentResponseLiveData.observe(this, Observer {
            it?.let(this::showResultScreen)
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GET_CONTACT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.let {
                processSelectedContact(it)
            }
        }

        if (requestCode == UserPinEntryActivity.PIN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                encryptedUserPin = data!!.getStringExtra(UserPinEntryActivity.PIN_RESPONSE_DATA)
                Log.d("Pin ",  encryptedUserPin   )


                val amount = if (currentProductIsDataOrPin()) {
                    if(product!!.proxyCode.contains(Regex("DATA"))){
                        plan!!.amount.toInt()
                    }else {
                        Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
                    }

                } else {
                    Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
                }
                val phoneNumber = beneficiaryEditAirtime.text.toString()

                showPaymentOption(PaymentOption.Mode.PAY, amount, phoneNumber)
            }
        }

        if (requestCode == PaymentOptionActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val paymentOption = data?.getSerializableExtra(PaymentOptionActivity.RESULT_OPTION) as PaymentOption
                val card = data.getSerializableExtra(PaymentOptionActivity.RESULT_CARD) as? Card
                continuePayment(paymentOption, card)
            }
        }
    }


    private fun continuePayment(paymentOption: PaymentOption, card: Card?) {
        val phoneNumber = beneficiaryEditAirtime.text.toString()
        when (paymentOption) {
            PaymentOption.WALLET -> {
                if (currentProductIsDataOrPin()) {
                    if (product!!.proxyCode.contains(Regex("DATA"))) {
                        viewModel.purchaseData(phoneNumber, encryptedUserPin)
                    }else{
                        val amount = Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
                        viewModel.purchasePin(amount, phoneNumber, encryptedUserPin)

                    }
                } else {
                    val amount = Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
                    viewModel.purchaseVtu(amount, phoneNumber, encryptedUserPin)
                }
            }

            else -> {
                val cardProcessor = DebitCardProcessor(this, encryptedUserPin)
                if (currentProductIsDataOrPin()) {
                    viewModel.purchaseDataWithCard(phoneNumber, card, cardProcessor)
                } else {
                    val amount = Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt()
                    viewModel.purchaseVtWithCard(amount, phoneNumber, card, cardProcessor)
                }
            }
        }
    }

    private fun processSelectedContact(data: Intent) {
        val uri = data.data
        val projection = arrayOf(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)
        val c = contentResolver.query(uri!!, projection, null, null, null)
        var number: String? = ""

        if (c.moveToFirst()) {
            number = c.getString(c.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER))
            if (number != null && !number.isEmpty()) {
                val inNo = number.normalizePhoneNumber()

                if (!beneficiaryEditAirtime.text.toString().isEmpty()) {
                    AlertDialog.Builder(this)
                            .setTitle("Replace Contact")
                            .setMessage("Do you want to replace the existing recipient with this?")
                            .setPositiveButton(android.R.string.ok) { dialog, which -> beneficiaryEditAirtime.setText(inNo) }
                            .setNegativeButton(android.R.string.cancel, null)
                            .show()
                } else
                    beneficiaryEditAirtime.setText(inNo)
            } else {
                Helper.showInfoDialog(this, "Error", "Could not load the selected contact, please input number manually.")
            }
        }

        c.close()
    }

    private fun currentProductIsDataOrPin() = !product?.proxyCode.isNullOrBlank()

    private fun isValidInputs(): Boolean {
        Log.i(" yeah product",(product == null).toString() )
        if (product == null) {

            Helper.showErrorAnim(productText)
            return false
        }
        Log.i(" yeah product",(product == null).toString() )
        Log.i(" PhoneNumberUtils.i",(!PhoneNumberUtils.isGlobalPhoneNumber(beneficiaryEditAirtime.text.toString())).toString() )
        Log.i(" !PhoneNumberUtilsg())",(!PhoneNumberUtils.isGlobalPhoneNumber(beneficiaryEditAirtime.text.toString())).toString() )
        Log.i(" currentPrin()",(currentProductIsDataOrPin()).toString() )

        Log.i(" !currn()",(!currentProductIsDataOrPin()).toString() )

        if (!PhoneNumberUtils.isGlobalPhoneNumber(beneficiaryEditAirtime.text.toString())) {
            Helper.showErrorAnim(beneficiaryEditAirtime)
            beneficiaryEditAirtime.error = "Enter beneficiary"
            return false
        }
//        product!!.proxyCode.contains(Regex("DATA"))
        if (!currentProductIsDataOrPin() || product!!.proxyCode.contains(Regex("VOT"))) {
            if (amountEdit.text.isNullOrEmpty() ||
                    Helper.sanitizeStringAmount(amountEdit.text.toString()).toInt() < 50) {
                Helper.showErrorAnim(amountEdit)
                amountEdit.error = "Please enter #50 or above"

                return false
            }
        } else {
            if (plan == null) {
                Helper.showErrorAnim(selectAmountLayout)
                selectableAmountText.error == "Please select an option"
                return false
            }
        }


        return true
    }


    override fun onSelectProduct(product: Service.Product) {
        viewModel.setProduct(product)
    }

    override fun getHistoryLayout(): View? {
        return historyLayout
    }

    override fun getHistoryListView(): RecyclerView? {
        return list
    }

    override fun getSubTitleView(): TextView? {
        return subTitleText
    }

    override fun getServiceImageView(): ImageView? {
        return serviceImage
    }
}

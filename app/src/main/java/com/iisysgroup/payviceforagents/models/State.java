package com.iisysgroup.payviceforagents.models;

public class State
{
    public State(String serial, String ctime, String bl, String cs, String tid, String coms, String sim, String cloc, String ss, String tmn, String tmanu, String hb, String sv, String lTxnAt, String pads) {
        this.serial = serial;
        this.ctime = ctime;
        this.bl = bl;
        this.cs = cs;
        this.tid = tid;
        this.coms = coms;
        this.sim = sim;
        this.cloc = cloc;
        this.ss = ss;
        this.tmn = tmn;
        this.tmanu = tmanu;
        this.hb = hb;
        this.sv = sv;
        this.lTxnAt = lTxnAt;
        this.pads = pads;
    }

    public String serial;
    public String ctime;
    public String bl;
    public String cs;
    public String tid;
    public String coms;
    public String sim;
    public String cloc;
    public String ss;
    public String tmn;
    public String tmanu;
    public String hb;
    public String sv;
    public String lTxnAt;
    public String pads;
}

package com.iisysgroup.payviceforagents.fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.iisysgroup.payviceforagents.activities.MainActivity;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.payviceservices.VasTransactionManager;
import com.iisysgroup.payviceforagents.securestorage.SecureStorageUtils;
import com.iisysgroup.payviceforagents.util.Helper;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangeDeviceFragment extends Fragment {

    @BindView(R.id.userIdEdit)
    EditText userIdEdit;
    @BindView(R.id.passwordEdit)
    EditText passwordEdit;
    @BindView(R.id.passwordTIL)
    TextInputLayout passwordTIL;
    @BindView(R.id.userIdTIL)
    TextInputLayout userIdTIL;
    ProgressDialog progressDialog;
    DialogInterface.OnClickListener action = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            NavUtils.navigateUpTo(getActivity(), new Intent(getActivity(), MainActivity.class));
        }
    };

    public ChangeDeviceFragment() {
        // Required empty public constructor
    }

    @OnClick(R.id.change_device_button)
    void OnChangeDevice() {

        final String password = passwordEdit.getText().toString();
        final String userId = userIdEdit.getText().toString();

        if (userId.isEmpty()) {
            userIdTIL.setErrorEnabled(true);
            userIdTIL.setError("Enter your user id");
            Helper.showErrorAnim(userIdEdit);
            userIdEdit.requestFocus();
        } else if (password.isEmpty() || !(password.length() > 4)) {
            passwordTIL.setErrorEnabled(true);
            passwordTIL.setError("Enter a valid password");
            Helper.showErrorAnim(passwordEdit);
            passwordEdit.requestFocus();
        } else {
            if (Helper.hasInternetConnectivity(getActivity())) {
                progressDialog = ProgressDialog.show(getActivity(), null, "Processing request... Please wait");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            doChangeDevice(userId, password);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Helper.showInfoDialog(getActivity(), "Change Device", e.getMessage());
                        } finally {
                            Helper.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.dismiss();
                                }
                            });

                        }
                    }
                }).start();


            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_device_layout, container, false);
        ButterKnife.bind(this, view);

        getActivity().setTitle("Change Device");
        return view;
    }

    void doChangeDevice(String userId, String password) throws Exception {
        Map<String, String> params = new HashMap<>();

        String url = getResources().getString(R.string.tams_url);

        String tamsAction = getString(R.string.tams_login_action);
        String initControl = getString(R.string.tams_init_action);
        String terminalID = getString(R.string.tams_default_terminal_id);
        String deviceID = Helper.getDeviceID(getActivity());

        String key = null;

        //for init set CONTROL = INIT
        params.put("action", tamsAction);
        params.put("termid", terminalID);
        params.put("userid", userId);
        params.put("control", initControl);

        VasTransactionManager transactionManager = new VasTransactionManager(url, params);
        VasResult tamsResult = transactionManager.processTransaction();

        terminalID = tamsResult.macrosTID;

        if (tamsResult.result == VasResult.Result.APPROVED)
            key = tamsResult.message.trim();


        if (key != null && !key.isEmpty()) {

            params.clear();

            String[] temp = key.split("\\|");
            key = temp[0] + "|" + temp[1];

            String clrKey = Helper.decryptUserKey(terminalID, key);
            // Log.d("Payvice", "Key: " + clrKey);
            String ePassword = SecureStorageUtils.hashIt(password, clrKey);
            // Log.d("Payvice", "Password: " + ePassword);

            String control = "CHANGE_DEVICE";

            params.put("action", tamsAction);
            params.put("termid", terminalID);
            params.put("userid", userId);
            params.put("username", "");
            params.put("password", ePassword);
            params.put("control", control);
            params.put("device_id", deviceID);

            tamsResult = Requests.processRequest(url, params);

            if (tamsResult.result == VasResult.Result.APPROVED) {
                Helper.showInfoDialogWithAction(getActivity(),
                        "Device Update Successful", "This device has now replaced your former attached device." +
                                " You can now log in", action);
            } else
                throw new IllegalAccessException(tamsResult.message);
        } else
            throw new IllegalAccessException("Invalid user");

    }
}

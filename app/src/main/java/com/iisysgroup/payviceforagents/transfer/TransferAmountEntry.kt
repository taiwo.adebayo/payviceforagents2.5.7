package com.iisysgroup.payviceforagents.transfer

//import AmpEmvL2Android.AMPDevice
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.iisgroup.bluetoothprinterlib.Utils.Util
import com.iisgroup.bluetoothprinterlib.Utils.Util.Language.it
import com.iisysgroup.payviceforagents.*
import com.iisysgroup.payviceforagents.models.*
import com.iisysgroup.payviceforagents.Services.TransferService
import com.iisysgroup.payviceforagents.Services.TransferServices
import com.iisysgroup.payviceforagents.activities.PrintActivity
import com.iisysgroup.payviceforagents.activities.ResultVasActivity
import com.iisysgroup.payviceforagents.activities.TransactionCompleteDisplay
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.models.WithdrawalWalletResponse.WithdrawalWalletCreditModel
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary
import com.iisysgroup.payviceforagents.utils.HashUtils
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.securestorage.SecureStorageUtils
import com.iisysgroup.payviceforagents.util.*
import com.iisysgroup.payviceforagents.util.StringUtils.getClientRef
import com.iisysgroup.payviceforagents.utils.ClientReferenceKey
import com.iisysgroup.payviceforagents.utils.Hashing
import com.iisysgroup.poslib.commons.emv.EmvCard
import com.iisysgroup.poslib.deviceinterface.DeviceState
import com.iisysgroup.poslib.host.Host
import com.iisysgroup.poslib.host.entities.ConfigData
import com.iisysgroup.poslib.host.entities.TransactionResult
import com.iisysgroup.poslib.utils.AccountType
import com.wizarpos.emvsample.models.transfer.TransferSuccessModel
import kotlinx.android.synthetic.main.activity_transfer_amount_entry.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.UnsupportedEncodingException
import java.math.BigDecimal
import java.net.URLEncoder
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class TransferAmountEntry : AppCompatActivity(), View.OnClickListener {

    enum class TRANSACTION_TYPE {TRANSFER, WITHDRAWAL, DEPOSIT}

    private lateinit var mConvenienceFee : String
    private lateinit var mRrn : String

    private lateinit var mEncryptedPin : String
    private var inview :  Boolean = false

    lateinit var withdrawalResponse : WithdrawalLookupSuccessModel

    private val mTerminalId by lazy {
        SharedPreferenceUtils.getTerminalId(this)
    }

    private var withdrawalDetails: WithdrawalDetails? = null

    private val mTransactionType by lazy {
        intent.getSerializableExtra(TransferBankSelection.TRANSACTION_TYPE) as TRANSACTION_TYPE
    }

//    private val progressDialog by lazy {
//        indeterminateProgressDialog(message = "Processing")
//    }

    private val mBankName by lazy {
        intent.getStringExtra(TransferBankSelection.BANK_NAME)
    }

    private val mBankCode by lazy {
        intent.getStringExtra(TransferBankSelection.BANK_CODE)
    }

    private val mAccountNumber by lazy {
        intent.getStringExtra(TransferBankSelection.ACCOUNT_NUMBER)
    }

    private val mWalletUsername by lazy {
        SharedPreferenceUtils.getPayviceUsername(this)
    }

    private val mPlainPassword by lazy {
        SharedPreferenceUtils.getPlainPassword(this)
    }
//  private val mUserPhone by lazy {
//        SharedPreferenceUtils.getUserPhone(this)
//    }

    lateinit var mWalletId: String

    private val mWalletPassword by lazy {
        SharedPreferenceUtils.getPayvicePassword(this)
    }


    private lateinit var mAccountName : String
    private lateinit var mProductCode : String

    private  var approved = "Approved"
    private  var declined = "Declined"
    private  var failed = "Failed"
    private  var pending = "Pending"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfer_amount_entry)
        //Log.e("details", mPlainPassword )
        initializeAmountEntryElements()
    }

    override fun onClick(view: View) {

        val displayStr = StringBuilder(txtAmount.text.toString())
        when (view.id) {
            R.id.btn1 -> fixAppend(displayStr, "1")
            R.id.btn2 -> fixAppend(displayStr, "2")
            R.id.btn3 -> fixAppend(displayStr, "3")
            R.id.btn4 -> fixAppend(displayStr, "4")
            R.id.btn5 -> fixAppend(displayStr, "5")
            R.id.btn6 -> fixAppend(displayStr, "6")
            R.id.btn7 -> fixAppend(displayStr, "7")
            R.id.btn8 -> fixAppend(displayStr, "8")
            R.id.btn9 -> fixAppend(displayStr, "9")
            R.id.btn0 -> fixAppend(displayStr, "0")
            R.id.btn00 -> fixAppend(displayStr, "00")
            R.id.btnenter -> onEnterPressed()
            R.id.btnclr -> {
                if (displayStr.length == 1) {
                    displayStr.deleteCharAt(0)
                    fixDelete(displayStr)
                } else if (displayStr.length > 1) {
                    val index = displayStr.length - 1
                    displayStr.deleteCharAt(index)
                    fixDelete(displayStr)
                }
            }
            R.id.btncancel -> onBackPressed()

        }
    }

    private fun onEnterPressed() {
        if (txtAmount.text.toString().isNotEmpty()){
            if (mTransactionType.toString().equals("WITHDRAWAL")){
                verifyWithdrawalAccountDetails()
            }else if (mTransactionType.toString().equals("TRANSFER")){
                verifyTransferAccountDetails()
            }


        } else {
            txtAmount.error = "Enter valid amount"
        }

    }

    private fun payWithCard(response: WithdrawalLookupSuccessModel) {
        val view = View.inflate(this, R.layout.activity_enter_pin, null)
        val encryptedPassword = SecureStorage.retrieve(Helper.STORED_PASSWORD, "")

        PinAlertUtils.getPin(this, view){
            mEncryptedPin = SecureStorageUtils.hashIt(it!!, encryptedPassword)!!

            val clientReferenceKey : ClientReferenceKey

            Log.d("okh", "$amountToDebit init amount");
            //  intent.putExtra("withdrawaldetails", withdrawaldetails);
            SecureStorage.store("amount", 2.0)
            SecureStorage.store("channel", "ANDROIDPOS")
            SecureStorage.store("password", mPlainPassword)
            SecureStorage.store("paymentMethod", "card")
            SecureStorage.store("pin", mEncryptedPin)
            SecureStorage.store("productCode", mProductCode)
            SecureStorage.store("type", "default")
            SecureStorage.store("vendorBankCode", mBankCode)
            SecureStorage.store("wallet", mWalletId)
            SecureStorage.store("username", mWalletUsername)
            SecureStorage.store("phone", "")
            val vendorBankCode = SecureStorage.retrieve("vendorBankCode", "")
            Log.d("okh", "saved "+ vendorBankCode)

            val intent = Intent(this, VasPurchaseProcessor::class.java)
            intent.putExtra(BasePaymentActivity.TRANSACTION_ACCOUNT_TYPE, AccountType.DEFAULT_UNSPECIFIED)
            intent.putExtra("product", "${SharedPreferenceUtils.device}-${SharedPreferenceUtils.getPayviceWalletId(this)}-${"Transfer"}")

            //times 100 because of the conversion to kobo
            val amount = (txtAmount.text.toString().toFloat() * 100) + response.convenienceFee

            intent.putExtra(BasePaymentActivity.TRANSACTION_AMOUNT,  amount.toLong())
            intent.putExtra(BasePaymentActivity.TRANSACTION_ADDITIONAL_AMOUNT, 0L)
            startActivityForResult(intent, 9090)

        }

    }

    private fun initializeAmountEntryElements() {
        findViewById<View>(R.id.btn1).setOnClickListener(this)
        findViewById<View>(R.id.btn2).setOnClickListener(this)
        findViewById<View>(R.id.btn3).setOnClickListener(this)
        findViewById<View>(R.id.btn4).setOnClickListener(this)
        findViewById<View>(R.id.btn5).setOnClickListener(this)
        findViewById<View>(R.id.btn6).setOnClickListener(this)
        findViewById<View>(R.id.btn7).setOnClickListener(this)
        findViewById<View>(R.id.btn8).setOnClickListener(this)
        findViewById<View>(R.id.btn9).setOnClickListener(this)
        findViewById<View>(R.id.btn0).setOnClickListener(this)
        findViewById<View>(R.id.btn00).setOnClickListener(this)
        findViewById<View>(R.id.btnclr).setOnClickListener(this)
        findViewById<View>(R.id.btnenter).setOnClickListener(this)
        findViewById<View>(R.id.btncancel).setOnClickListener(this)
    }

    private fun fixAppend(displayStr : StringBuilder, digit : String) {
        if(displayStr.length <= 11)
        {
            displayStr.append(digit)
            var newAmount = displayStr.toString().toDouble()
            // fix new input
            newAmount *= 10.00
            if("00" == digit) newAmount *= 10.00

            val updatedAmount = DecimalFormat("0.00").format(newAmount)
            txtAmount.text = updatedAmount.toString()
        }
    }

    private fun fixDelete(displayStr: StringBuilder) {
        var bd = BigDecimal(displayStr.toString())
        bd = bd.movePointLeft(1)

        txtAmount.text = bd.toString()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 9090){
            Log.d("okh", "coming back withdrawal")
            if (resultCode == Activity.RESULT_OK) {

                val result : TransactionModel

                    try {
                        result = data?.getSerializableExtra("result") as TransactionModel
                        val state = data.getSerializableExtra("state") as DeviceState
                        val rrn = data.getStringExtra("rrn")

                        Log.d("okh", state.toString() + rrn +"")
                        val resultStatus = result.transactionStatus

                        when (resultStatus) {
                            declined, failed -> {

                                toast("Transaction Result \n\nTransaction declined. Please try again later")

                            }

                            approved -> {
                                // completeCardPayment()
                                Log.d("okh", "credit wallet now")
//                                creditWallet(this, result)
//
//                                toast("Transaction Result \n\nTransaction approved.")

                            val intent = Intent(this@TransferAmountEntry, TransactionCompleteDisplay::class.java)
                            intent.putExtra("state", state)
                            intent.putExtra("rrn", rrn)

                            //whether or not to print the value
                            intent.putExtra("print", true)
                            printTransaction(rrn)

                            }

                            else -> {
                                toast("No data!")
                            }

                    }

                } catch (e : Exception){

                    }
            }
        }
    }

    private fun verifyWithdrawalAccountDetails(){
        val progressDialog = ProgressDialog(this)
        progressDialog.setCancelable(true)
        progressDialog.setTitle("Verification")
        progressDialog.setMessage("Now looking for account details")
//        progressDialog.show()

        mWalletId = SharedPreferenceUtils.getPayviceWalletId(this@TransferAmountEntry)

        try {
            val accountDetails = AccountLookUpDetailWithdrawal(wallet = mWalletId, username = mWalletUsername, type = "default", password = mWalletPassword, amount = txtAmount.text.toString().toFloat() * 100, channel = "POS")
            TransferService.create().lookUpAccountNumberWithdrawal(accountDetails).enqueue(object : Callback<WithdrawalLookupSuccessModel> {

                override fun onFailure(call: Call<WithdrawalLookupSuccessModel>, t: Throwable) {
                    Log.d("okh", t.message)
                }

                override fun onResponse(call: Call<WithdrawalLookupSuccessModel>, response: retrofit2.Response<WithdrawalLookupSuccessModel>) {

                    Log.d("okh", response.toString())
                    val amount = txtAmount.text.toString()
                    if (response.body()!!.status != 1) {
                        alert {
                            title = "Response"
                            message = response.body()!!.message
                            okButton { }
                        }.show()
                    } else {
                        mProductCode = response.body()!!.productCode
                        mAccountName = response.body()!!.beneficiaryName
                        mConvenienceFee = response.body()!!.convenienceFee.toString()

                        SecureStorage.store("currentName", mAccountName)
                        SecureStorage.store("conveniencefee", mConvenienceFee)

                        alert {
                            title = "${response.body()!!.message}"
                            message = "${response.body()!!.beneficiaryName}\nAmount - N$amount\nConvenience fee - N${response.body()!!.convenienceFee.toFloat() / 100}"
                            positiveButton("Continue") {
                                payWithCard(response.body()!!)
                            }
                        }.show()
                    }
                }

            })


        }
        catch (e : Exception) {
        }
    }

    private fun verifyTransferAccountDetails(){

        lateinit var response : LookupSuccessModel
        mWalletId = SharedPreferenceUtils.getPayviceWalletId(this@TransferAmountEntry)

        val  accountDetails = AccountLookUpDetailTransfer(wallet = mWalletId, username = mWalletUsername, type = "default", password = mWalletPassword, amount = txtAmount.text.toString().toDouble() * 100, channel = "POS", beneficiary = mAccountNumber, vendorBankCode = mBankCode)

        val amount = txtAmount.text.toString()

        if (Helper.isOnline(this@TransferAmountEntry)) {

            try {
                val progressDialog = ProgressDialog.show(this@TransferAmountEntry, "Verification",
                        "Now looking for account details", true, true)
                progressDialog.show()

                val service = TransferService.create()
                service.lookUpAccountNumberTransfer(accountDetails).enqueue(object : Callback<LookupSuccessModel> {

                    override fun onFailure(call: Call<LookupSuccessModel>, t: Throwable) {
                        Log.d("okh", "error " + t.message)
                    }

                    override fun onResponse(call: Call<LookupSuccessModel>, response: retrofit2.Response<LookupSuccessModel>) {
                        //  val responses = response as LookupSuccessModel
                        progressDialog.cancel()
                        if (response.body() != null) {
                            Log.d("okh", "response " + response.body().toString())
                            if (response.body()!!.status != 1) {
                                alert {
                                    title = "Response"
                                    message = response.body()!!.message
                                    okButton { }
                                }.show()
                            } else {
                                mProductCode = response.body()!!.productCode
                                mAccountName = response.body()!!.beneficiaryName
                                mConvenienceFee = response.body()!!.convenienceFee.toString()

                                SecureStorage.store("currentName", mAccountName)
                                SecureStorage.store("conveniencefee", mConvenienceFee)
                                SecureStorage.store("currentAccount", mAccountNumber)

                                if (inview) {
                                    alert {
                                        title = "${response.body()!!.message}"
                                        message = "${response.body()!!.message} - ${response.body()!!.beneficiaryName}\nAmount - N$amount\nConvenience fee - N${response.body()!!.convenienceFee.toFloat() / 100}"
                                        positiveButton("Continue") {
                                            debitWallet()
                                        }

                                    }.show()
                                }

                            }
                        }

                    }
                })
            } catch (e: Exception) {

            }
        }
        else{
            toast("Please check your internet connection")
        }

    }

    public fun debitWallet(){
        val progressDialog = ProgressDialog(this@TransferAmountEntry)
        //progressDialog.show()
        val clientReference = getClientRef(this@TransferAmountEntry, "")
        lateinit var transferResponse : TransferSuccessModel
        lateinit var transferDetails : TransferDetails
        val view = View.inflate(this, R.layout.activity_enter_pin, null)

        val encryptedPassword = SecureStorage.retrieve(Helper.STORED_PASSWORD, "")
        var amount =  txtAmount.text.toString().toFloat()

        Log.d("debit amount", txtAmount.text.toString().toFloat().toString())
        //Log.d("debit mConvenienceFee",  (mConvenienceFee.toFloat() / 100).toString())
        Log.d("debit amount to debit",  amount.toString())
        PinAlertUtils.getPin(this, view) {
            mEncryptedPin = SecureStorageUtils.hashIt(it!!, encryptedPassword)!!
            GlobalScope.launch(Dispatchers.Default) {
                try {

                    val action = when (mTransactionType) {
                        TRANSACTION_TYPE.TRANSFER -> "transfer"
                        TRANSACTION_TYPE.DEPOSIT -> "deposit"
                        TRANSACTION_TYPE.WITHDRAWAL -> "withdrawal"
                    }

                    transferDetails = TransferDetails(wallet = mWalletId, username = mWalletUsername, password = mPlainPassword, pin = mEncryptedPin, type = "default", amount = amount.toInt().toFloat()*100, beneficiary = mAccountNumber, vendorBankCode = mBankCode, channel = "ANDROIDPOS",  phone = "", paymentMethod = "cash", productCode = mProductCode)

                    val clientReference = getClientRef(this@TransferAmountEntry, "")

                    Log.e("transfer details", mWalletId + " " + mWalletUsername + " " + mWalletPassword + " " + mPlainPassword + " " + mEncryptedPin + " " + " " + txtAmount.text.toString() + " " + mAccountNumber + " " + mBankCode )
                    val gson = Gson()
                    val jsonPayload = gson.toJson(transferDetails)
                    val base64encoded = String(org.apache.commons.codec.binary.Base64.encodeBase64(jsonPayload.toByteArray()))
                    val encoded = URLEncoder.encode(base64encoded, "UTF-8")
                    val nonce = clientReference
                    Log.e("sign", base64encoded + " " + encoded)
                    val encryptedStuff = "${nonce}IL0v3Th1sAp11111111UC4NDoV4SSWITHVICEBANKING$encoded"
                    val signature = HashUtils.sha512(encryptedStuff)

                    val amount = txtAmount.text.toString()

                    if (Helper.isOnline(this@TransferAmountEntry)){
                        try{
                            TransferService.create().transfer(transferDetails, "application/json", signature, nonce).enqueue(object  : Callback<TransferSuccessModel>{
                                override fun onFailure(call: Call<TransferSuccessModel>, t: Throwable) {
                                    Log.d("okh", t.message)
                                    val progressDialog = ProgressDialog.show(this@TransferAmountEntry, "Response",
                                            t.message, true, false)
                                    progressDialog.show()
                                }

                                override fun onResponse(call: Call<TransferSuccessModel>, response: retrofit2.Response<TransferSuccessModel>) {
                                    Log.d("okh", "result "+response.body()!!)
                                    if (response.body()!!.status != 1) {

                                        alert {
                                            title = "Response"
                                            message = response.body()!!.message + "\n"+ response.body()!!.reason
                                            okButton {
                                                val map = hashMapOf<String, String>(
                                                        "Reference" to response.body()!!.reference,
                                                        "Message" to response.body()!!.message,
                                                        "Account Name" to response.body()!!.beneficiaryName,
                                                        "Bank Name" to mBankName,
                                                        "Account Number" to response.body()!!.beneficiary,
                                                        "Amount Debited" to (response.body()!!.amountDebited/100).toString(),
                                                        "Convenience Fee" to (response.body()!!.convenienceFee/100).toString()
                                                )

                                                val userId = SecureStorage.retrieve(Helper.USER_ID, "")
                                                val transaction = TransactionModel(userId, "", "", "", "", "", "", (response.body()!!.amountDebited/100).toLong(), 0, Host.TransactionType.CASH_IN, "",  true, "Declined", "", mBankName, "", "", "")
                                                SecureStorage.store("currentservice", "Transfer")
                                                SecureStorage.store("currentReason", "Transfer Unsuccessful")
                                                SecureStorage.store("currentApproval", "Declined")
                                                SecureStorage.store("currentRef", response.body()!!.reference)
                                                SecureStorage.store("currentAmount", (response.body()!!.amountDebited/100).toString())
                                                SecureStorage.store("currentPhone", "0")
                                                val intent = Intent(this@TransferAmountEntry, ResultVasActivity::class.java)
                                                intent.putExtra("transaction", transaction)
                                                startActivity(intent)
                                            }
                                        }.show()
                                    } else {

                                        alert {
                                            progressDialog.cancel()
                                            title = "Response"
                                            message = "${response.body()!!.message}. Your wallet has been debitted \n " +
                                                    "\n#${response.body()!!.amountDebited/100} \nBeneficiary : ${response.body()!!.beneficiaryName}"
                                            positiveButton(buttonText = "Print") {

                                                val map = hashMapOf<String, String>(
                                                        "Reference" to response.body()!!.reference,
                                                        "Message" to response.body()!!.message,
                                                        "Account Name" to response.body()!!.beneficiaryName,
                                                        "Bank Name" to mBankName,
                                                        "Account Number" to response.body()!!.beneficiary,
                                                        "Amount Debited" to (response.body()!!.amountDebited/100).toString(),
                                                        "Convenience Fee" to (response.body()!!.convenienceFee/100).toString()
                                                )
                                               val card =  SecureStorage.retrieve("accountname", "")
                                                val transaction = TransactionModel(mTerminalId, "", "", response.body()!!.reference, card, "", "", (response.body()!!.amountDebited/100).toLong(), 0, Host.TransactionType.CASH_IN, "",  true, "Approved", "", mBankName, response.body()!!.beneficiary, "", "")
                                                SecureStorage.store("currentservice", "Transfer")
                                                SecureStorage.store("currentReason", "Transfer Successful")
                                                SecureStorage.store("currentApproval", "Approved")
                                                SecureStorage.store("currentAmount", (response.body()!!.amountDebited/100).toString())
                                                SecureStorage.store("currentPhone", "0")
                                                val intent = Intent(this@TransferAmountEntry, ResultVasActivity::class.java)
                                                intent.putExtra("transaction", transaction)
                                                startActivity(intent)

                                            }
                                        }.show()

                                    }
                                }

                            })
                        }catch (e : Exception){

                        }
                    }else{
                        toast("Please Check your internet connection")
                    }


                }catch (E : java.lang.Exception){
                }


            }
        }

    }

    var amountToDebit : Double = 0.0

    public fun creditWallet(context : Context, result : TransactionModel) {
        val amount = result.amount.toDouble()
        Log.d("okh", amount!!.toString() + "")
        val channel = "ANDROIDPOS"
        val password = SecureStorage.retrieve("password", "")
        val pin = SecureStorage.retrieve("pin", "")
        val productCode = SecureStorage.retrieve("productCode", "")
        val type = SecureStorage.retrieve("type", "")
        val vendorBankCode = SecureStorage.retrieve("vendorBankCode", "")
        val wallet = SecureStorage.retrieve("wallet", "")
        val username = SecureStorage.retrieve("username", "")
        val phone = SecureStorage.retrieve("phone", "")
        Log.d("okh", " settocredit$vendorBankCode")
        val transferResponse :WithdrawalWalletCreditModel
        val clientReference = getClientRef(this, "")

        val pinInfo = EmvCard.PinInfo(null, null, null)
        val emvCard = EmvCard(result.cardholderName, "", "", pinInfo)

        val configData = ConfigData()
        val pfm = Pfm(PfmStateGenerator(context).generateState(), PfmJournalGenerator(result, configData, false, amount.toLong(), emvCard).generateJournal())

        withdrawalDetails = WithdrawalDetails(wallet, username, password, pin, "default", amount, "", vendorBankCode, "ANDROIDPOS", "card", productCode, pfm)

        Log.d("okh", result.terminalID)

        val gson = Gson()
        val jsonPayload = gson.toJson(withdrawalDetails)
        val base64encoded = String(org.apache.commons.codec.binary.Base64.encodeBase64(jsonPayload.toByteArray()))
        var encoded: String? = null
        try {
            encoded = URLEncoder.encode(base64encoded, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        Log.e("sign", "$base64encoded $encoded")
        val encryptedStuff = clientReference + "IL0v3Th1sAp11111111UC4NDoV4SSWITHVICEBANKING" + encoded
        val signature = Hashing(encryptedStuff)
        val transferServices = TransferServices()

            transferServices.TransferService().withdraw(withdrawalDetails!!, "application/json", signature, clientReference).enqueue(object : Callback<WithdrawalWalletCreditModel> {

                override fun onResponse(call: Call<WithdrawalWalletCreditModel>, response: Response<WithdrawalWalletCreditModel>) {


                    Log.d("okh", "result " + response.body()!!)
                    if (response.body()!!.status == 1) {

                        val serviceresult = ServiceResult.Data(Beneficiary(result.cardholderName, result.amount.toString(), "Transfer"), (amount/100).toInt())

                        SecureStorage.store("currentservice", "Withdrawal")
                        SecureStorage.store("currentAmount", amount/100)
                        SecureStorage.store("currentReason", "Withdrawal Successful")
                        SecureStorage.store("currentPhone", "0")
                        val intent = Intent(this@TransferAmountEntry, ResultVasActivity::class.java)
                        intent.putExtra("transaction", result)
                        intent.putExtra("serviceresult", serviceresult)
                        startActivity(intent)
                        // Toast.makeText(baseContext, response.body()!!.message + "", Toast.LENGTH_LONG).show()
                    } else {
                        val serviceresult = ServiceResult.Data(Beneficiary(result.cardholderName, result.amount.toString(), "Transfer"), (amount/100).toInt())

                        SecureStorage.store("currentservice", "Withdrawal")
                        SecureStorage.store("currentAmount", amount/100)
                        SecureStorage.store("currentReason", "Withdrawal Unsuccessful")
                        SecureStorage.store("currentPhone", "0")
                        val intent = Intent(context, ResultVasActivity::class.java)
                        intent.putExtra("transaction", result)
                        intent.putExtra("serviceresult", serviceresult)
                        startActivity(intent)
//                        Toast.makeText(baseContext, "Your wallet has been debitted \n " + response.body()!!.amountSettled / 100 + " \n " + "Beneficiary : " + response.body()!!.beneficiaryName, Toast.LENGTH_LONG).show()
                    }


                }

                override fun onFailure(call: Call<WithdrawalWalletCreditModel>, t: Throwable) {

                }
            })

            val serviceresult = ServiceResult.Data(Beneficiary(result.cardholderName, (amount/100).toString(), "Withdrawal"), (amount/100).toInt())

            SecureStorage.store("currentservice", "Withdrawal")
            if (result.isApproved){
                SecureStorage.store("currentReason", "Withdrawal Successful")
            }
            else{
                SecureStorage.store("currentReason", "Withdrawal Unsuccessful")
            }
            SecureStorage.store("currentAmount", amount/100)
            SecureStorage.store("currentPhone", "0")
            val intent = Intent(context, ResultVasActivity::class.java)
            intent.putExtra("transaction", result)
            intent.putExtra("serviceresult", serviceresult)
            context.startActivity(intent)


    }

    override fun finish() {

        super.finish()
    }

    override fun onBackPressed() {
        finish()
        super.onBackPressed()
    }

    private fun printTransaction(transactionRef : String) {
        (application as App).db.transactionResultDao.get(transactionRef).observe({lifecycle}){ result ->


            result?.let {

                val intent = Intent(this@TransferAmountEntry, PrintActivity::class.java)
                val map = HashMap<String, String>()

                val transactionType = result.transactionType.name
                val amount = result.amount
                val additionalAmount = result.additionalAmount
                val cardHolderName = result.cardHolderName
                val cardExpiry  = result.cardExpiry
                val merchantID = result.merchantID
                val authID = result.authID
                val terminalID = result.terminalID
                val PAN = result.PAN
                val RRN = result.RRN
                val responseCode = result.responseCode
                val STAN = result.STAN
                val accountType = result.accountType
                val transactionStatus = result.transactionStatus
                val calendar = Calendar.getInstance()
                val date = calendar.time

                map.put("ADDITIONAL AMOUNT", additionalAmount.toString())
                map.put("NAME", cardHolderName)
                map.put("EXPIRY DATE", cardExpiry)
                map.put("MID", merchantID)
                map.put("AID", authID)
                map.put("TERMINAL ID", terminalID)
                map.put("PAN", PAN)
                map.put("LABEL", responseCode)
                map.put("SEO NO", STAN)
                map.put("RRN", RRN)
                map.put("AUTH ID", authID)
                map.put("ACCOUNT TYPE", accountType)


                val convertedAmount = (amount.toDouble()/100).toString()

                val receiptModel = ReceiptModel(date.toString(), transactionType, transactionStatus, map, convertedAmount, result.transactionStatusReason)
                intent.putExtra(PrintActivity.KEYS.PRINT_RECEIPT_VAS_TYPE, PrintActivity.VasType.PURCHASE)
                intent.putExtra("print_map", receiptModel)
                startActivity(intent)
                finish()

            }
        }
    }

    override fun onResume() {
            inview = true
        super.onResume()
    }

    override fun onPause() {
        inview = false
        super.onPause()
    }

}

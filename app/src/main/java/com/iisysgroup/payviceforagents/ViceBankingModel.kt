package com.iisysgroup.payviceforagents

import com.google.gson.annotations.Expose

class ViceBankingModel {
    data class WithdrawalValidationRequest(@Expose var wallet : String, @Expose val username : String, @Expose val password : String,
                                    @Expose val type : String, @Expose val amount : Int, @Expose val channel : String)


    data class WithdrawalValidationResponse(@Expose val status : Int, @Expose val error : Boolean, @Expose val message : String, @Expose val description : String,
                                  @Expose val convenienceFee : Int, @Expose val amountSettled : Int, @Expose val amountToDebit : Int, @Expose val percentageCharged : Int,
                                  @Expose val beneficiaryName : String, @Expose val beneficiaryWallet : String, @Expose val productCode : String){
        constructor(): this(0,false,"","",0,0,0,0,"","","")
    }


    data class WithdrawalPaymentRequest(@Expose val wallet: String, @Expose val username: String, @Expose val password: String, @Expose val type: String,
                                          @Expose val  amount: Int, @Expose val channel: String, @Expose val paymentMethod :String, @Expose val pin : String, @Expose val clientReference : String)


    data class WithdrawalPaymentResponse(@Expose val transactionID : String, @Expose val status : Int, @Expose val error: Boolean, @Expose val message: String, @Expose val description: String, @Expose val insertId : Int,
                                         @Expose val convenienceFee: Int, @Expose val amountSettled: Int, @Expose val percentageCharged: Int,
                                         @Expose val beneficiaryWallet: String , @Expose val beneficiaryName: String, @Expose val reference : String){
        constructor():this("",0,false,"","",0,0,0,0,"","","")
    }

    data class TransferValidationRequest(@Expose val wallet: String, @Expose val username: String, @Expose val password: String, @Expose val beneficiary: String, @Expose val vendorBankCode : String,
                                         @Expose val  type: String, @Expose val amount: Int, @Expose val channel: String)

    data class TranferValidationResponse(@Expose val status: Int, @Expose val error: Boolean, @Expose val message: String,@Expose val convenienceFee: Int,
                                         @Expose val amountSettled: Int, @Expose val amountToDebit: Int, @Expose val amountCharged: Int, @Expose val beneficiaryName : String, @Expose val account : String,
                                         @Expose val vendorBankCode : String, @Expose val  productCode: String){
        constructor(): this(0,false,"",0,0,0,0,"","","","")
    }


    data class TransferPaymentRequest(@Expose val wallet: String, @Expose val username: String, @Expose val password: String, @Expose val pin : String,
                                      @Expose val type: String, @Expose val amount: Int, @Expose val beneficiary: String, @Expose val vendorBankCode: String, @Expose val channel: String,@Expose val productCode : String ,@Expose val clientReference : String, @Expose val pfm : PfmNotification)

    data class TransferPaymentResponse(@Expose val status: Int, @Expose val error: Boolean, @Expose val message: String, @Expose val transactionID: String, @Expose val convenienceFee: Int, @Expose val amountSettled: Int, @Expose val amountDebited : Int,
                                       @Expose val beneficiaryName: String, @Expose val beneficiary: String, @Expose val reference: String){
        constructor():this(0,false,"","",0,0,0,"","","")
    }

}
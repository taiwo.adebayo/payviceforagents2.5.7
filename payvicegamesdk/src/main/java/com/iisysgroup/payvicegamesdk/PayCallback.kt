package com.iisysgroup.payvicegamesdk

import com.interswitchng.sdk.payment.IswCallback
import com.interswitchng.sdk.payment.model.PurchaseResponse

open class PayCallback (private val paymentProcessor: PaymentProcessor) : IswCallback<PurchaseResponse>() {

    override fun onError(e: Exception) {
        paymentProcessor.setError(e)
    }

    override fun onSuccess(purchaseResponse: PurchaseResponse) {
        paymentProcessor.completeTransaction(purchaseResponse)
    }
}
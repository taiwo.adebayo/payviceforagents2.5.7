package com.iisysgroup.payviceforagents.activities

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.baseimpl.viewmodel.MultichoiceViewModel
import com.iisysgroup.payviceforagents.dialogs.MultichoicePlanDialog
import com.iisysgroup.payviceforagents.entities.Service
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.util.*
import com.itex.richard.payviceconnect.model.DstvModel
import com.jakewharton.rxbinding2.widget.textChanges
import kotlinx.android.synthetic.main.activity_multichoice.*
import kotlinx.android.synthetic.main.content_multichoice.*
import java.util.concurrent.TimeUnit

class MultichoiceActivity : BaseServiceActivity() {

    private var planDialog: MultichoicePlanDialog? = null
    private var selectedPlan: DstvModel.Data? = null
    private var isValidated = false

    private val viewModel by lazy {
        MultichoiceViewModel(application)
    }

    private val alertDialog by lazy {
        AlertDialog.Builder(this)
                .setTitle("Validate Card")
                .setNegativeButton(android.R.string.cancel) { _, _ ->
                    viewModel.setSmartCardIsValidated(false)
                    beneficiaryEdit.requestFocus()
                }.setPositiveButton("Confirm Card") { _, _ ->
                    viewModel.setSmartCardIsValidated(true)
                }.create()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multichoice)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        intent?.let {
            it.getStringExtra(BillPaymentActivity.SERVICE)?.let {
                service = VasServices.SERVICES[it]!!
                viewModel.setService(service)
            }
        } ?: kotlin.run {
            finish()
        }
    }
    override fun initControls() {
        super.initControls()

        beneficiaryEdit.hint = VasServices.VAS_SERVICE_INPUT_TEXT_DESC[service.name]

        beneficiaryEdit.textChanges().delay(300L, TimeUnit.MILLISECONDS)
                .subscribe {
                    it?.let {
                        if (it.length >= 10) {
                            viewModel.validateIuc(it.toString())
                        }
                    }
                }

        selectAmountLayout.setOnClickListener {
            if (isValidated) {
                viewModel.getPlans().observe(this, Observer<List<DstvModel.Data>> {
                    it?.let {
                        planDialog?.dismiss()
                        planDialog = MultichoicePlanDialog.newInstance(it) {
                            viewModel.setSelectedPlan(it)
                        }

                        planDialog?.show(supportFragmentManager, "Plan Dialog")
                    }
                })
            } else {
                beneficiaryEdit.error = "Smart card is not validated"
                Helper.showErrorAnim(beneficiaryEdit)
            }
        }

        proceedBtn.setOnClickListener {
            if (isValidInput()) {
                showPinEntry()
            }
        }

        viewModel.errorWatcher.observe(this, Observer {
            it?.printStackTrace()
            showProgressDialog(false)
            showError(it)
        })

        viewModel.progressDialogLiveData.observe(this, Observer {
            it?.let {
                val (show, message) = it
                showProgressDialog(show, message)
            }
        })

        viewModel.paymentResponseLiveData.observe(this, Observer {
            it?.let(this::showResultScreen)
        })

        viewModel.productLiveData.observe(this, Observer {
            it?.let {
                productText.text = it.name.capitalize()
                selectProductBtn.isEnabled = false
            }
        })

        viewModel.isIucValidated.observe(this, Observer {
            it?.let {
                isValidated = it
            }
        })

        viewModel.iucNameLiveData.observe(this, Observer {
            it?.let {
                if (!alertDialog.isShowing) {
                    alertDialog.setMessage("Customer Name: $it\nIUC: ${beneficiaryEdit.text}")
                    alertDialog.show()
                }

            }
        })

        viewModel.selectPlanLiveData.observe(this, Observer {
            it?.let {
                selectedPlan = it
                selectableAmountText.text = "${it.amount} - ${it.name}"
                planDialog?.dismiss()
            }
        })


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == UserPinEntryActivity.PIN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                encryptedUserPin = data!!.getStringExtra(UserPinEntryActivity.PIN_RESPONSE_DATA)

                val amount = selectedPlan?.amount?.toDouble()?.toInt() ?: 0
                val beneficiary = beneficiaryEdit.text.toString()

                showPaymentOption(PaymentOption.Mode.PAY, amount, beneficiary)
            }
        }

        if (requestCode == PaymentOptionActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val paymentOption = data?.getSerializableExtra(PaymentOptionActivity.RESULT_OPTION) as PaymentOption
                val card = data.getSerializableExtra(PaymentOptionActivity.RESULT_CARD) as? Card
                continuePayment(paymentOption, card)
            }
        }
    }


    private fun continuePayment(paymentOption: PaymentOption, card: Card?) {
        val iucNumber = beneficiaryEdit.text.toString()
        when (paymentOption) {
            PaymentOption.WALLET -> {
                viewModel.subscribe(iucNumber, encryptedUserPin)
            }

            else -> {
                val cardProcessor = DebitCardProcessor(this, encryptedUserPin)
                viewModel.subscribeWithCard(iucNumber, card, cardProcessor)
            }
        }
    }


    private fun isValidInput(): Boolean {
        if (beneficiaryEdit.text.length < 10 || !isValidated) {
            beneficiaryEdit.error = "Invalid Smart card"
            Helper.showErrorAnim(beneficiaryEdit)
            return false
        }

        if (selectedPlan == null) {
            selectableAmountText.error = "Select plan"
            Helper.showErrorAnim(selectAmountLayout)
            return false
        }

        return true
    }


    override fun getHistoryLayout(): View? {
        return historyLayout
    }

    override fun getHistoryListView(): RecyclerView? {
        return list
    }

    override fun getSubTitleView(): TextView? {
        return subTitleText
    }

    override fun getServiceImageView(): ImageView? {
        return serviceImage
    }

    override fun onSelectProduct(product: Service.Product) {

    }

}

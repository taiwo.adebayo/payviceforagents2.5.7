package com.iisysgroup.payviceforagents.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.fragments.SelectCardFragment
import com.iisysgroup.payviceforagents.persistence.entitiy.Card

class SelectCardActivity : BaseActivity(), SelectCardFragment.OnFragmentInteractionListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_card)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.default_activity_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_cancel -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onLinkedFragmentInteraction(card: Card) {
        card.let {
            val data = Intent()
            data.putExtra(EXTRA_DATA, it)
            setResult(Activity.RESULT_OK, data)
            finish()
        }
    }


    companion object {
        const val REQUEST_CODE = 230
        const val EXTRA_DATA = "card_data"
    }
}

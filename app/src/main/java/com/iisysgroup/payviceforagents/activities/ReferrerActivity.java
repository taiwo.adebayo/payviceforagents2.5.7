package com.iisysgroup.payviceforagents.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.google.zxing.Result;
import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.entities.VasResult;
import com.iisysgroup.payviceforagents.payviceservices.Requests;
import com.iisysgroup.payviceforagents.util.Helper;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ReferrerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    public static final String RESULT = "result data";
    public static final int REF_CODE = 109;
    EditText refText;
    ZXingScannerView scannerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.referred_by_dialog);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        scannerView = findViewById(R.id.scannerView);
        refText = findViewById(R.id.referrerCodeText);

        findViewById(R.id.verifyReferrerBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = refText.getText().toString();
                if (code.length() > 7) {
                    verifyReferralCode(code);
                } else {
                    Snackbar.make(scannerView, "Enter a Valid Referral Code or Scan QR", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Dexter.withActivity(this).withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Snackbar.make(scannerView, "Camera permission required to read QR",
                                Snackbar.LENGTH_SHORT).setAction(android.R.string.ok
                                , new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                                        intent.setData(uri);
                                        startActivityForResult(intent, 100);
                                    }
                                }).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).onSameThread().check();
    }


    @Override
    protected void onResume() {
        super.onResume();
        scannerView.startCamera();
        scannerView.resumeCameraPreview(this);

    }


    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }


    @Override
    public void handleResult(Result result) {
        String code = result.getText().trim();
        verifyReferralCode(code);
    }


    void verifyReferralCode(final String code) {
        if (Helper.hasInternetConnectivity(this)) {
            final ProgressDialog progressDialog = ProgressDialog.show(this, "Referral", "Verifying Code...", true, false);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final VasResult result = Requests.initUser(ReferrerActivity.this, code, "ernest.uduje@iisysgroup.com");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if (result.result == VasResult.Result.APPROVED) {
                                Intent intent = new Intent();
                                intent.putExtra(RESULT, code);
                                setResult(RESULT_OK, intent);
                                finish();
                            } else {

                                String message = result.message;
                                if (message.contains("not found")) {
                                    message = "Invalid referral code";
                                }
                                Helper.showInfoDialog(ReferrerActivity.this, "Referral",
                                        message);
                                scannerView.resumeCameraPreview(ReferrerActivity.this);
                            }
                        }
                    });
                }
            }).start();
        }

    }


}

package com.iisysgroup.payviceforagents.callbacks;

/**
 * Created by Bamitale@Itex on 18/05/2017.
 */
public interface OnItemSelectedListener<T> {
    void onItemSelected(T item);
}

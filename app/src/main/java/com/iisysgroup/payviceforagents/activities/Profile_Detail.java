package com.iisysgroup.payviceforagents.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.securestorage.SecureStorage;
import com.iisysgroup.payviceforagents.util.Helper;

public class Profile_Detail extends BaseActivity {

    ImageView imageView;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile__detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        TextView username_profile = findViewById(R.id.nav_header_username_profile);
        TextView terminalid_profile = findViewById(R.id.nav_header_terminal_id_profile);
        TextView emailid_profile = findViewById(R.id.nav_header_user_email_profile);

        username_profile.setText(SecureStorage.retrieve(Helper.USERNAME, ""));
        terminalid_profile.setText(SecureStorage.retrieve(Helper.TERMINAL_ID, ""));
        emailid_profile.setText(SecureStorage.retrieve(Helper.USER_EMAIL, ""));

        ImageView facebookimg = findViewById(R.id.facebookShare);
        ImageView contactsimg = findViewById(R.id.contactShare);
        ImageView barcodeimg = findViewById(R.id.barcodeShare);


        facebookimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendFacebookInvite();
            }
        });

        contactsimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendGoogleAppInvite();
            }
        });

        barcodeimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReferralQrDialog();
            }
        });

//        facebookimg.setOnClickListener(new View.OnClickListener()





     /*   imageView.findViewById(R.id.facebookShare).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ReferrerActivity.class);
                startActivity(intent);
            }
        });
*/


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

}

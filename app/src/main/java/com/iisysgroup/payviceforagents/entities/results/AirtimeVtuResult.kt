package com.iisysgroup.payviceforagents.entities.results

import android.util.Log
import com.google.gson.Gson
import com.iisgroup.bluetoothprinterlib.Manaer.PrintfManager
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.entities.StatusObject
import com.iisysgroup.payviceforagents.util.toCurrencyString
import com.itex.richard.payviceconnect.model.AirtimeModel

class AirtimeVtuResult(val data: ServiceResult.Data,
                       val airtimeResponse: AirtimeModel.AirtimeResponse) : ServiceResult {

    override fun toStatusObject(): StatusObject {

        Log.d("airtimeResponse.>>>", Gson().toJson(airtimeResponse))

        val statusObject = StatusObject(!airtimeResponse.error)
        statusObject.statusReason = airtimeResponse.message
        return statusObject
    }

    override fun printResult(printer: PrintfManager) {
        printer.printTabSpace(7)
        printer.printText("VTU Purchase")
        printer.printfWrap(2)

        val status = if (!airtimeResponse.error) {
            StatusObject.STATUS_APPROVED
        } else {
            StatusObject.STATUS_DECLINED
        }

        printer.printTabSpace(6)
        printer.printText(status.toUpperCase())
        printer.printfWrap()

        printer.printTwoColumn("Network", data.beneficiary.assignedProduct)
        printer.printTwoColumn("Phone Number", data.beneficiary.displayInfo)
        printer.printTwoColumn("Date", airtimeResponse.date)
        printer.printTwoColumn("Reference", airtimeResponse.ref)

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap()

        printer.printTabSpace(10)
        printer.printText("N ${data.amount.toCurrencyString()}")
        printer.printfWrap()

        printer.printTabSpace(8)
        printer.printText("**********")
        printer.printfWrap(2)

        printer.printText(airtimeResponse.message)
        printer.printfWrap()
    }

    override fun getPrinterLogo(): Int? {
        return null
    }



}











//D/OkHttp: <-- 200 OK http://197.253.19.75:8029/vas/vtu/purchase (8029ms)
//2019-05-07 10:37:53.590 9733-9824/com.iisysgroup.payvice D/OkHttp: Date: Tue, 07 May 2019 09:37:48 GMT
//2019-05-07 10:37:53.591 9733-9824/com.iisysgroup.payvice D/OkHttp: Server: Apache/2.4.29 (Ubuntu)
//2019-05-07 10:37:53.591 9733-9824/com.iisysgroup.payvice D/OkHttp: Vary: Authorization
//2019-05-07 10:37:53.591 9733-9824/com.iisysgroup.payvice D/OkHttp: Set-Cookie: PHPSESSID=hr3tf4q5vaue7ess1in7s3mrq7; path=/
//2019-05-07 10:37:53.591 9733-9824/com.iisysgroup.payvice D/OkHttp: Expires: Thu, 19 Nov 1981 08:52:00 GMT
//2019-05-07 10:37:53.591 9733-9824/com.iisysgroup.payvice D/OkHttp: Cache-Control: no-store, no-cache, must-revalidate
//2019-05-07 10:37:53.591 9733-9824/com.iisysgroup.payvice D/OkHttp: Pragma: no-cache
//2019-05-07 10:37:53.591 9733-9824/com.iisysgroup.payvice D/OkHttp: Cache-Control: no-cache, private
//2019-05-07 10:37:53.592 9733-9824/com.iisysgroup.payvice D/OkHttp: Set-Cookie: itex_vas3_session=eyJpdiI6Inh4K1ZzNUpOOGhrNSs3WXgza3pvR2c9PSIsInZhbHVlIjoiSFN5REJrU3l1amRGbkdZZlZvNXJWMzBKVjNLMU5kOWVFQ1lITUZBSHVWU2hQeU8zeDNrKzQycFdvUmZGT0RTRSIsIm1hYyI6IjZjOTZiZTM2MGZlYzJhNTNlOTNmOTRmNmYxNzYxMjM2MzJkM2U3ODU2ZjBlYzVmZGI0OGVmMzg2YTYyN2YwMDgifQ%3D%3D; expires=Tue, 07-May-2019 11:37:55 GMT; Max-Age=7200; path=/; httponly
//2019-05-07 10:37:53.592 9733-9824/com.iisysgroup.payvice D/OkHttp: Content-Length: 1625
//2019-05-07 10:37:53.592 9733-9824/com.iisysgroup.payvice D/OkHttp: Keep-Alive: timeout=5, max=100
//2019-05-07 10:37:53.592 9733-9824/com.iisysgroup.payvice D/OkHttp: Connection: Keep-Alive
//2019-05-07 10:37:53.592 9733-9824/com.iisysgroup.payvice D/OkHttp: Content-Type: application/json
//2019-05-07 10:37:53.594 9733-9824/com.iisysgroup.payvice D/OkHttp: {"error":false,"message":"Airtime Purchase Successful","amount":"100","pin_data":{"message":"QUANTITY : 1","amount":" 100.0 ","pin":" 5577200256952194 ","expiry":" 08\/12\/2021 00:00:00 ","serial":" 51009051565864525770 ","dial":"*126*PIN#"},"ref":"ITEX-VTU5CD151EC24A0DW20VV1XV","date":"2019-05-07 10:37:54","transactionID":84016900,"product":{"amount":10000,"clientReference":"eyAgICAic2Vzc2lvbktleSI6ICJleUozWVd4c1pYUWlPaUk1T1RVek9UWTJPU0lzSW5WelpYSnVZVzFsSWpvaWJXbHJaV3hwY3pFek5VQm5iV0ZwYkM1amIyMGlMQ0owWlhKdGFXNWhiQ0k2Ym5Wc2JDd2laR1YyYVdObFNVUWlPaUpPVWtRNU1FMGlMQ0pqYUdGdWJtVnNJam9pUVU1RVVrOUpSRkJQVXlJc0lrOUZUU0k2Ym5Wc2JDd2lkVzVwY1hWbFVtVm1Jam9pTldOaU9EVmlZVEZqWmpnek15SXNJbkpoYm1RaU9qazFOalFzSW1SaGRHVWlPaUl5TURFNUxUQTBMVEU0SURFeU9qRXlPak16SWl3aWNtVm1aWEpsYm1ObElqb3lOVFo5IiwgICAgInRpbWVzdGFtcCI6ICJZLW0tZCBIOmk6cy51IiwgICAgInJybiI6ICIiLCAgICAicmFuZG9tU3RyaW5nIjoiLTEtMS00LTEtMS0yLTItMi0yLTItMS0xLTEtMS01LTItNS0xLTEtMS0xLTMtMS0zLTEtMS0xLTItNC0yIgp9","phone":"07091152533","service":"AIRTELPIN","terminal_id":"27613263","user_id":"itextest@hotmail.com","nairaAmount":"100","wallet":"27613263","username":"itextest@hotmail.com","terminal":"27613263","channel":null,"category":"Airtime","product":"AIRTELPIN","name":"AIRTELPIN Airtime","description":"AIRTELPIN Airtime of 10000 to 07091152533","paymentMethod":"cash","VASCustomerAccount":"07091152533","VASCustomerPackageName":"AIRTELPIN","VASCustomerPackageCode":10000,"VASBillerName":"AIRTELPIN","VASProviderName":"ITEX-AIRTELPIN","VASCustomerPhone":"07091152533","reference":"ITEX-VTU5CD151EC24A0DW20VV1XV","id":"5CD151EDCD1E75CD151EDCD1E5","transactionID":84016900}}
//2019-05-07 10:37:53.594 9733-9824/com.iisysgroup.payvice D/OkHttp: <-- END HTTP (1625-byte body)
//2019-05-07 10:37:53.595 9733-9824/com.iisysgroup.payvice D/NetworkInterator: {"error":false,"message":"Airtime Purchase Successful","amount":"100","pin_data":{"message":"QUANTITY : 1","amount":" 100.0 ","pin":" 5577200256952194 ","expiry":" 08\/12\/2021 00:00:00 ","serial":" 51009051565864525770 ","dial":"*126*PIN#"},"ref":"ITEX-VTU5CD151EC24A0DW20VV1XV","date":"2019-05-07 10:37:54","transactionID":84016900,"product":{"amount":10000,"clientReference":"eyAgICAic2Vzc2lvbktleSI6ICJleUozWVd4c1pYUWlPaUk1T1RVek9UWTJPU0lzSW5WelpYSnVZVzFsSWpvaWJXbHJaV3hwY3pFek5VQm5iV0ZwYkM1amIyMGlMQ0owWlhKdGFXNWhiQ0k2Ym5Wc2JDd2laR1YyYVdObFNVUWlPaUpPVWtRNU1FMGlMQ0pqYUdGdWJtVnNJam9pUVU1RVVrOUpSRkJQVXlJc0lrOUZUU0k2Ym5Wc2JDd2lkVzVwY1hWbFVtVm1Jam9pTldOaU9EVmlZVEZqWmpnek15SXNJbkpoYm1RaU9qazFOalFzSW1SaGRHVWlPaUl5TURFNUxUQTBMVEU0SURFeU9qRXlPak16SWl3aWNtVm1aWEpsYm1ObElqb3lOVFo5IiwgICAgInRpbWVzdGFtcCI6ICJZLW0tZCBIOmk6cy51IiwgICAgInJybiI6ICIiLCAgICAicmFuZG9tU3RyaW5nIjoiLTEtMS00LTEtMS0yLTItMi0yLTItMS0xLTEtMS01LTItNS0xLTEtMS0xLTMtMS0zLTEtMS0xLTItNC0yIgp9","phone":"07091152533","service":"AIRTELPIN","terminal_id":"27613263","user_id":"itextest@hotmail.com","nairaAmount":"100","wallet":"27613263","username":"itextest@hotmail.com","terminal":"27613263","channel":null,"category":"Airtime","product":"AIRTELPIN","name":"AIRTELPIN Airtime","description":"AIRTELPIN Airtime of 10000 to 07091152533","paymentMethod":"cash","VASCustomerAccount":"07091152533","VASCustomerPackageName":"AIRTELPIN","VASCustomerPackageCode":10000,"VASBillerName":"AIRTELPIN","VASProviderName":"ITEX-AIRTELPIN","VASCustomerPhone":"07091152533","reference":"ITEX-VTU5CD151EC24A0DW20VV1XV","id":"5CD151EDCD1E75CD151EDCD1E5","transactionID":84016900}}
//2019-05-07 10:37:53.657 9733-9823/com.iisysgroup.payvice D/Surface: Surface::disconnect(this=0xa5420700,api=1)
//2019-05-07 10:37:53.657 9733-9823/com.iisysgroup.payvice D/GraphicBuffer: unregister, handle(0xa543aef0) (w:812 h:450 s:816 f:0x1 u:0x000f02)
//2019-05-07 10:37:53.658 9733-9823/com.iisysgroup.payvice D/GraphicBuffer: unregister, handle(0xa543a710) (w:812 h:450 s:816 f:0x1 u:0x000f02)
//2019-05-07 10:37:53.659 9733-9823/com.iisysgroup.payvice D/GraphicBuffer: unregister, handle(0xa543a2b0) (w:812 h:450 s:816 f:0x1 u:0x000f02)
//2019-05-07 10:37:53.660 9733-9823/com.iisysgroup.payvice D/GraphicBuffer: unregister, handle(0xa543b270) (w:812 h:450 s:816 f:0x1 u:0x000f02)
//2019-05-07 10:37:53.691 9733-9733/com.iisysgroup.payvice D/WindowClient: Remove from mViews: DecorView@a7c3a0b[Airtime], this = android.view.WindowManagerGlobal@2086148
//2019-05-07 10:37:53.704 9733-9733/com.iisysgroup.payvice D/it>>: {"amount":"100","date":"2019-05-07 10:37:54","error":false,"message":"Airtime Purchase Successful","pin_data":{"VASCustomerPackageCode":0,"amount":100,"transactionID":0},"product":{"amount":"10000"},"ref":"ITEX-VTU5CD151EC24A0DW20VV1XV","transactionID":"84016900"}
//2019-05-07 10:37:53.766 9733-9733/com.iisysgroup.payvice W/System.err: io.reactivex.exceptions.UndeliverableException: java.lang.RuntimeException: Parcelable encountered IOException writing serializable object (name = com.iisysgroup.payvice.entities.results.AirtimePinResult)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at io.reactivex.plugins.RxJavaPlugins.onError(RxJavaPlugins.java:367)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at io.reactivex.internal.observers.BiConsumerSingleObserver.onSuccess(BiConsumerSingleObserver.java:60)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at io.reactivex.internal.operators.observable.ObservableSingleSingle$SingleElementObserver.onComplete(ObservableSingleSingle.java:111)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at io.reactivex.internal.operators.observable.ObservableObserveOn$ObserveOnObserver.checkTerminated(ObservableObserveOn.java:281)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at io.reactivex.internal.operators.observable.ObservableObserveOn$ObserveOnObserver.drainNormal(ObservableObserveOn.java:192)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at io.reactivex.internal.operators.observable.ObservableObserveOn$ObserveOnObserver.run(ObservableObserveOn.java:252)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at io.reactivex.android.schedulers.HandlerScheduler$ScheduledRunnable.run(HandlerScheduler.java:109)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at android.os.Handler.handleCallback(Handler.java:836)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at android.os.Handler.dispatchMessage(Handler.java:103)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at android.os.Looper.loop(Looper.java:203)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at android.app.ActivityThread.main(ActivityThread.java:6251)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at java.lang.reflect.Method.invoke(Native Method)
//2019-05-07 10:37:53.767 9733-9733/com.iisysgroup.payvice W/System.err:     at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:1073)
//2019-05-07 10:37:53.768 9733-9733/com.iisysgroup.payvice W/System.err:     at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:934)
//2019-05-07 10:37:53.768 9733-9733/com.iisysgroup.payvice W/System.err: Caused by: java.lang.RuntimeException: Parcelable encountered IOException writing serializable object (name = com.iisysgroup.payvice.entities.results.AirtimePinResult)
//2019-05-07 10:37:53.768 9733-9733/com.iisysgroup.payvice W/System.err:     at android.os.Parcel.writeSerializable(Parcel.java:1526)
//2019-05-07 10:37:53.768 9733-9733/com.iisysgroup.payvice W/System.err:     at android.os.Parcel.writeValue(Parcel.java:1474)
//2019-05-07 10:37:53.768 9733-9733/com.iisysgroup.payvice W/System.err:     at android.os.Parcel.writeArrayMapInternal(Parcel.java:723)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.os.BaseBundle.writeToParcelInner(BaseBundle.java:1408)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.os.Bundle.writeToParcel(Bundle.java:1133)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.os.Parcel.writeBundle(Parcel.java:763)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.content.Intent.writeToParcel(Intent.java:8668)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.app.ActivityManagerProxy.startActivity(ActivityManagerNative.java:3387)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.app.Instrumentation.execStartActivity(Instrumentation.java:1518)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.app.Activity.startActivityForResult(Activity.java:4247)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.support.v4.app.BaseFragmentActivityApi16.startActivityForResult(BaseFragmentActivityApi16.java:54)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.support.v4.app.FragmentActivity.startActivityForResult(FragmentActivity.java:68)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.app.Activity.startActivityForResult(Activity.java:4206)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.support.v4.app.FragmentActivity.startActivityForResult(FragmentActivity.java:751)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.app.Activity.startActivity(Activity.java:4530)
//2019-05-07 10:37:53.769 9733-9733/com.iisysgroup.payvice W/System.err:     at android.app.Activity.startActivity(Activity.java:4498)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err:     at com.iisysgroup.payvice.util.FunctionsKt.showResultScreen(Functions.kt:102)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err:     at com.iisysgroup.payvice.airtime.AirtimeActivity$initControls$9.onChanged(AirtimeActivity.kt:161)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err:     at com.iisysgroup.payvice.airtime.AirtimeActivity$initControls$9.onChanged(AirtimeActivity.kt:36)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err:     at android.arch.lifecycle.LiveData.considerNotify(LiveData.java:109)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err:     at android.arch.lifecycle.LiveData.dispatchingValue(LiveData.java:126)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err:     at android.arch.lifecycle.LiveData.setValue(LiveData.java:282)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err:     at android.arch.lifecycle.MutableLiveData.setValue(MutableLiveData.java:33)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err:     at com.iisysgroup.payvice.baseimpl.viewmodel.AirtimeActivityViewModel$purchasePin$1.accept(AirtimeActivityViewModel.kt:118)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err:     at com.iisysgroup.payvice.baseimpl.viewmodel.AirtimeActivityViewModel$purchasePin$1.accept(AirtimeActivityViewModel.kt:25)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err:     at io.reactivex.internal.observers.BiConsumerSingleObserver.onSuccess(BiConsumerSingleObserver.java:57)
//2019-05-07 10:37:53.770 9733-9733/com.iisysgroup.payvice W/System.err: 	... 12 more
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err: Caused by: java.io.NotSerializableException: com.itex.richard.payviceconnect.model.AirtimeModel$PinData
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err:     at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1224)
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err:     at java.io.ObjectOutputStream.defaultWriteFields(ObjectOutputStream.java:1584)
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err:     at java.io.ObjectOutputStream.writeSerialData(ObjectOutputStream.java:1549)
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err:     at java.io.ObjectOutputStream.writeOrdinaryObject(ObjectOutputStream.java:1472)
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err:     at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1218)
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err:     at java.io.ObjectOutputStream.defaultWriteFields(ObjectOutputStream.java:1584)
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err:     at java.io.ObjectOutputStream.writeSerialData(ObjectOutputStream.java:1549)
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err:     at java.io.ObjectOutputStream.writeOrdinaryObject(ObjectOutputStream.java:1472)
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err:     at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1218)
//2019-05-07 10:37:53.771 9733-9733/com.iisysgroup.payvice W/System.err:     at java.io.ObjectOutputStream.writeObject(ObjectOutputStream.java:346)
//2019-05-07 10:37:53.772 9733-9733/com.iisysgroup.payvice W/System.err:     at android.os.Parcel.writeSerializable(Parcel.java:1521)
//2019-05-07 10:37:53.772 9733-9733/com.iisysgroup.payvice W/System.err: 	... 37 more
//2019-05-07 10:37:53.774 9733-9733/com.iisysgroup.payvice E/AndroidRuntime: FATAL EXCEPTION: main
//Process: com.iisysgroup.payvice, PID: 9733
//io.reactivex.exceptions.UndeliverableException: java.lang.RuntimeException: Parcelable encountered IOException writing serializable object (name = com.iisysgroup.payvice.entities.results.AirtimePinResult)
//at io.reactivex.plugins.RxJavaPlugins.onError(RxJavaPlugins.java:367)
//at io.reactivex.internal.observers.BiConsumerSingleObserver.onSuccess(BiConsumerSingleObserver.java:60)
//at io.reactivex.internal.operators.observable.ObservableSingleSingle$SingleElementObserver.onComplete(ObservableSingleSingle.java:111)
//at io.reactivex.internal.operators.observable.ObservableObserveOn$ObserveOnObserver.checkTerminated(ObservableObserveOn.java:281)
//at io.reactivex.internal.operators.observable.ObservableObserveOn$ObserveOnObserver.drainNormal(ObservableObserveOn.java:192)
//at io.reactivex.internal.operators.observable.ObservableObserveOn$ObserveOnObserver.run(ObservableObserveOn.java:252)
//at io.reactivex.android.schedulers.HandlerScheduler$ScheduledRunnable.run(HandlerScheduler.java:109)
//at android.os.Handler.handleCallback(Handler.java:836)
//at android.os.Handler.dispatchMessage(Handler.java:103)
//at android.os.Looper.loop(Looper.java:203)
//at android.app.ActivityThread.main(ActivityThread.java:6251)
//at java.lang.reflect.Method.invoke(Native Method)
//at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:1073)
//at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:934)
//Caused by: java.lang.RuntimeException: Parcelable encountered IOException writing serializable object (name = com.iisysgroup.payvice.entities.results.AirtimePinResult)
//at android.os.Parcel.writeSerializable(Parcel.java:1526)
//at android.os.Parcel.writeValue(Parcel.java:1474)
//at android.os.Parcel.writeArrayMapInternal(Parcel.java:723)
//at android.os.BaseBundle.writeToParcelInner(BaseBundle.java:1408)
//at android.os.Bundle.writeToParcel(Bundle.java:1133)
//at android.os.Parcel.writeBundle(Parcel.java:763)
//at android.content.Intent.writeToParcel(Intent.java:8668)
//at android.app.ActivityManagerProxy.startActivity(ActivityManagerNative.java:3387)
//at android.app.Instrumentation.execStartActivity(Instrumentation.java:1518)
//at android.app.Activity.startActivityForResult(Activity.java:4247)
//at android.support.v4.app.BaseFragmentActivityApi16.startActivityForResult(BaseFragmentActivityApi16.java:54)
//at android.support.v4.app.FragmentActivity.startActivityForResult(FragmentActivity.java:68)
//at android.app.Activity.startActivityForResult(Activity.java:4206)
//at android.support.v4.app.FragmentActivity.startActivityForResult(FragmentActivity.java:751)
//at android.app.Activity.startActivity(Activity.java:4530)
//at android.app.Activity.startActivity(Activity.java:4498)
//at com.iisysgroup.payvice.util.FunctionsKt.showResultScreen(Functions.kt:102)
//at com.iisysgroup.payvice.airtime.AirtimeActivity$initControls$9.onChanged(AirtimeActivity.kt:161)
//at com.iisysgroup.payvice.airtime.AirtimeActivity$initControls$9.onChanged(AirtimeActivity.kt:36)
//at android.arch.lifecycle.LiveData.considerNotify(LiveData.java:109)
//at android.arch.lifecycle.LiveData.dispatchingValue(LiveData.java:126)
//at android.arch.lifecycle.LiveData.setValue(LiveData.java:282)
//at android.arch.lifecycle.MutableLiveData.setValue(MutableLiveData.java:33)
//at com.iisysgroup.payvice.baseimpl.viewmodel.AirtimeActivityViewModel$purchasePin$1.accept(AirtimeActivityViewModel.kt:118)
//at com.iisysgroup.payvice.baseimpl.viewmodel.AirtimeActivityViewModel$purchasePin$1.accept(AirtimeActivityViewModel.kt:25)
//at io.reactivex.internal.observers.BiConsumerSingleObserver.onSuccess(BiConsumerSingleObserver.java:57)
//at io.reactivex.internal.operators.observable.ObservableSingleSingle$SingleElementObserver.onComplete(ObservableSingleSingle.java:111) 
//at io.reactivex.internal.operators.observable.ObservableObserveOn$ObserveOnObserver.checkTerminated(ObservableObserveOn.java:281) 
//at io.reactivex.internal.operators.observable.ObservableObserveOn$ObserveOnObserver.drainNormal(ObservableObserveOn.java:192) 
//at io.reactivex.internal.operators.observable.ObservableObserveOn$ObserveOnObserver.run(ObservableObserveOn.java:252) 
//at io.reactivex.android.schedulers.HandlerScheduler$ScheduledRunnable.run(HandlerScheduler.java:109) 
//at android.os.Handler.handleCallback(Handler.java:836) 
//at android.os.Handler.dispatchMessage(Handler.java:103) 
//at android.os.Looper.loop(Looper.java:203) 
//at android.app.ActivityThread.main(ActivityThread.java:6251) 
//at java.lang.reflect.Method.invoke(Native Method) 
//at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:1073) 
//at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:934) 
//Caused by: java.io.NotSerializableException: com.itex.richard.payviceconnect.model.AirtimeModel$PinData
//at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1224)
//at java.io.ObjectOutputStream.defaultWriteFields(ObjectOutputStream.java:1584)
//2019-05-07 10:37:53.774 9733-9733/com.iisysgroup.payvice E/AndroidRuntime:     at java.io.ObjectOutputStream.writeSerialData(ObjectOutputStream.java:1549)
//at java.io.ObjectOutputStream.writeOrdinaryObject(ObjectOutputStream.java:1472)
//at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1218)
//at java.io.ObjectOutputStream.defaultWriteFields(ObjectOutputStream.java:1584)
//at java.io.ObjectOutputStream.writeSerialData(ObjectOutputStream.java:1549)
//at java.io.ObjectOutputStream.writeOrdinaryObject(ObjectOutputStream.java:1472)
//at java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1218)
//at java.io.ObjectOutputStream.writeObject(ObjectOutputStream.java:346)
//at android.os.Parcel.writeSerializable(Parcel.java:1521)
//... 37 more
package com.iisysgroup.payviceforagents.util;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.activities.RepeatTransactionActivity;
import com.iisysgroup.payviceforagents.activities.TransactionHistoryDetailActivity_D;
import com.iisysgroup.payviceforagents.models.SubAgent;
import com.iisysgroup.payviceforagents.models.Transaction;
import com.iisysgroup.payviceforagents.models.UserTransactions;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Itex-PC on 05/11/2018.
 */

public class TransactionHistotyAdapter  extends  RecyclerView.Adapter<TransactionHistotyAdapter.HistoryViewHolder_D> {

    List<Transaction> historyList;
    Context context;
    Boolean isTransactionctivity;
    public UserTransactions userTransactions;

    public TransactionHistotyAdapter(Context context, List<Transaction> historyList) {
        this.context = context;
        this.historyList = historyList;
        this.isTransactionctivity =isTransactionctivity;

        Log.i("AdaptTrans", "HistoryAdapter_D: Before " +isTransactionctivity + " HistoryAdapter_D: After " +isTransactionctivity);
    }



    public TransactionHistotyAdapter.History processRecords_D(int position) {

        TransactionHistotyAdapter.History history =  new TransactionHistotyAdapter.History();


        Transaction actualTransaction =  historyList.get(position);

        Log.i("RetrofitTransactionddd", "Size...." + actualTransaction);
        history.transactionType =actualTransaction.getCategory() == ""? "" : actualTransaction.getCategory();
        history.product =actualTransaction.getProduct() == "" ? "" : actualTransaction.getProduct();
        history.type =actualTransaction.getType() == "" ? "" : actualTransaction.getType();
        history.transactionReference = actualTransaction.getReference()== null? "" : actualTransaction.getReference();
        history.amount =  actualTransaction.getAmount()== null ? "" :  " " +actualTransaction.getAmount();
        history.commisionEarned =actualTransaction.getCommissionAmount() == null ? "0.00" : actualTransaction.getCommissionAmount();
        history.balanceAfter = actualTransaction.getBalanceAfter() == null ? " " : actualTransaction.getBalanceAfter().toString();
        history.category = actualTransaction.getCategory().trim().isEmpty() ? " " : actualTransaction.getCategory();
        history.transDescription = actualTransaction.getDescription().trim().isEmpty() ? " " : actualTransaction.getDescription();
        history.beneficiary = actualTransaction.getBeneficiary() == null ? ""  : actualTransaction.getBeneficiary();
        history.originalTransRef =actualTransaction.getOriginalTransactionReference() == null ? " " : actualTransaction.getOriginalTransactionReference().toString();
        history.originalTransAmt = actualTransaction.getOriginalTransactionAmount() == null ? " " : actualTransaction.getOriginalTransactionAmount().toString();
        history.originalTransCat = actualTransaction.getOriginalTransactionCategory() == null ? " " : actualTransaction.getOriginalTransactionCategory().toString();
        history.originalTransType =actualTransaction.getOriginalTransactionType() == null ? " " : actualTransaction.getOriginalTransactionType().toString();
        history.originalTransProduct = actualTransaction.getOriginalTransactionProduct() == null? " " : actualTransaction.getOriginalTransactionProduct().toString();
        history.originalTransDesc =actualTransaction.getOriginalTransactionDescription() == null ? " " : actualTransaction.getOriginalTransactionDescription().toString();



        history.date = actualTransaction.getDate();





        return history;
    }


    @Override
    public int getItemCount() {

        return historyList.size();
    }

    @Override
    public TransactionHistotyAdapter.HistoryViewHolder_D onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.new_history_item_r, parent, false);

        return new TransactionHistotyAdapter.HistoryViewHolder_D(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder_D holder, int position) {
        TransactionHistotyAdapter.History transaction=  processRecords_D(position);
        holder.bind(context, transaction);
    }

    public void addAll(List<Transaction> transactions) {
        historyList.addAll(transactions);
        notifyDataSetChanged();
    }



//    @Override
//    public void allTransactionHistory(History history_y) {
//
//    }

//    @Override
//    public void allTransactionHistory(HistoryAdapter_D.History history) {
//        OnBaseFragmentCalled onBaseFragmentCalled = (OnBaseFragmentCalled)new BaseActivity();
//        Log.i("AllHistories", "allTransactionHistory__Adapter: " + history.amount);
//        Log.i("AllHistories", "allTransactionHistory__Adapter: " + history.subAgents.size());
//        onBaseFragmentCalled.allTransactionHistory(this.history);
//    }


    public static class History implements Serializable {
        //[0] - Beneficiary  [1] - amount  [2] - Request Code [3] - Terminal ID [4] - Username [5] - Transaction Reference
        //[6] - Transaction type  [7] - Date
        public String transactionReference, amount,commisionEarned, balanceAfter,category,product,transDescription,beneficiary,originalTransRef;
        public List<SubAgent>subAgents;
        public String wallet_Id,originalTransAmt,originalTransCat,originalTransType,originalTransProduct,originalTransDesc,date,transactionType,type, remark;


        public boolean isPurchase() {
            String tranType = transactionType.toLowerCase().trim();
            return !(tranType.contains("fund") || tranType.contains("transfer") || tranType.contains("promo") || tranType.contains("deposit"));
        }

        public boolean isRecharge() {
            String productCode = product.toLowerCase().trim();

            return (productCode.contains("etisalat") ||
                    productCode.contains("mtn") || productCode.contains("glo") || productCode.contains("airtel"));
        }
    }

    class HistoryViewHolder_D extends RecyclerView.ViewHolder {
        LinearLayout layout_background;
        View repayView, contentView;
        TextView balanceBefore;
        RoundedImageView productLogo;
        TextView tranTypeTitleView, amountView, referenceView,
                beneficiaryView, productView, dateView;


        public HistoryViewHolder_D(View itemView) {
            super(itemView);
            contentView = itemView;
            layout_background = (LinearLayout)itemView.findViewById(R.id.details_layout);
            repayView = itemView.findViewById(R.id.reload_view);
//            balanceBefore = itemView.findViewById(R.id.balanceafter_txt_view);
            tranTypeTitleView = itemView.findViewById(R.id.history_tran_type_title);
            amountView = itemView.findViewById(R.id.history_amount_textview);
            beneficiaryView = itemView.findViewById(R.id.history_beneficiary_textview);
            productView = itemView.findViewById(R.id.history_product_textview);
            dateView = itemView.findViewById(R.id.history_date_textview);
            referenceView = itemView.findViewById(R.id.history_reference_textview);
            productLogo = itemView.findViewById(R.id.productLogo);
        }
        private void colorView(LinearLayout layout, TextView textView, String type){

        }
        public void bind(final Context context, final TransactionHistotyAdapter.History history) {
//        if (history.transactionType.toLowerCase().trim().contains("fund")) {
//            Log.i("TransCatgory", "bind: " + history.transactionType.toLowerCase().trim());
//            tranTypeTitleView.setText("Fund");
////            tranTypeTitleView.setTextColor(ContextCompat.getColor(context, R.color.green_color));
//
////            history.product = "Fund";
////            history.beneficiary = CardUtils.processPan(history.beneficiary);
//        } else if (history.transactionType.toLowerCase().trim().contains("promo")) {
//            tranTypeTitleView.setText("Promo");
////            tranTypeTitleView.setTextColor(ContextCompat.getColor(context, R.color.green_color));
////            history.product = "Promo";
//        } else if (history.transactionType.toLowerCase().trim().contains("transfer")) {
//            tranTypeTitleView.setText("Transfer");
////            history.product = "Wallet Transfer";
////            tranTypeTitleView.setTextColor(ContextCompat.getColor(context, R.color.red));
//        }else if (history.transactionType.toLowerCase().trim().contains("deposit")) {
//            tranTypeTitleView.setText("Deposit");
////            history.product = "Wallet Transfer";
////            tranTypeTitleView.setTextColor(ContextCompat.getColor(context, R.color.red));
//        }else {
//            tranTypeTitleView.setText("Purchase");
////            tranTypeTitleView.setTextColor(ContextCompat.getColor(context, R.color.red));
//        }

            tranTypeTitleView.setText(history.product.toUpperCase());
            beneficiaryView.setText(history.beneficiary);
            amountView.setText('\u20A6' + "" + history.amount);
            dateView.setText(history.date);
            referenceView.setText(history.transactionReference);
            productView.setText(history.product);

                repayView.setVisibility(View.VISIBLE);

            String [] services = {"mtn", "glo", "airtel", "gotv", "dstv", "starttimes", "9mobile","smile", "ikeja", "eko", "lcc", "waec", "movies","ibadan","phed","enugu","abuja"};
            int [] servicesLogo = {R.drawable.mtn, R.drawable.glo, R.drawable.airtel, R.drawable.gotv, R.drawable.dstv, R.drawable.startime, R.drawable.etisalat, R.drawable.smile, R.drawable.ikedc, R.drawable.ekedc, R.drawable.lcc, R.drawable.waec, R.drawable.ic_genesis,R.drawable.ibedc,R.drawable.phdc,R.drawable.eedc,R.drawable.aedc}; ;
            //productView.setText(history.product);
            productLogo.setImageResource(R.drawable.ic_account_balance_wallet_white_24dp);
            for (int k = 0; k < services.length; k++){
                if (history.product.toLowerCase().contains(services[k])){
                    Log.d("printlogo", "bind: "+ "logo"+ history.product);
                    productLogo.setImageResource(servicesLogo[k]);
                }
            }

            if (history.type.contains("Debit")) {
                amountView.setTextColor(ContextCompat.getColor(context, R.color.red));
            }
            else{
                amountView.setTextColor(ContextCompat.getColor(context, R.color.green_text));

            }
//        productView.setText(history.transactionType);

            if (history.isPurchase()) {
                //Changed from INVISIBLE to GONE
                repayView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, RepeatTransactionActivity.class);
                        intent.putExtra(RepeatTransactionActivity.EXTRA_HISTORY, history);
                        context.startActivity(intent);
                    }
                });
            } else {
                repayView.setVisibility(View.INVISIBLE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, TransactionHistoryDetailActivity_D.class);
                    intent.putExtra(TransactionHistoryDetailActivity_D.EXTRA_HISTORY, history);
                    context.startActivity(intent);
                }
            });

        }
    }


}


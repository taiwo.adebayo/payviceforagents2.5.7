package com.iisysgroup.payviceforagents

import android.os.Bundle
import android.util.Log
import com.google.android.gms.tasks.Tasks.await
import com.iisgroup.bluetoothprinterlib.Utils.Util.Language.it
import com.iisysgroup.payviceforagents.util.DefaultUIModel
import com.iisysgroup.payviceforagents.util.sendNotify
import com.iisysgroup.poslib.commons.emv.EmvCard
import com.iisysgroup.poslib.deviceinterface.interactors.EmvInteractor
import com.iisysgroup.poslib.host.Host
import com.iisysgroup.poslib.host.entities.ConfigData
import com.iisysgroup.poslib.host.entities.ConnectionData
import com.iisysgroup.poslib.host.entities.KeyHolder
import com.iisysgroup.poslib.host.entities.VasTerminalData
import com.iisysgroup.poslib.utils.AccountType
import com.iisysgroup.poslib.utils.InputData
import com.iisysgroup.poslib.utils.TransactionData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.SchedulerSupport.IO
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import org.jetbrains.anko.UI
import org.jetbrains.anko.custom.async
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import java.util.concurrent.Callable
import android.app.Activity
import android.content.Intent
import com.iisysgroup.payviceforagents.activities.ResultVasActivity
import com.iisysgroup.payviceforagents.entities.ServiceResult
import com.iisysgroup.payviceforagents.models.TransactionModel
import com.iisysgroup.payviceforagents.persistence.entitiy.Beneficiary
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.transfer.TransferAmountEntry
import com.iisysgroup.payviceforagents.util.TransactionResult
import org.jetbrains.anko.indeterminateProgressDialog


class VasPurchaseProcessor: BaseCardPaymentProcessor(){
    //Receives amount, additional amount, account type,
    //Returns value to calling library or class


    private val mAmount by lazy {
        intent.getLongExtra(BasePaymentActivity.TRANSACTION_AMOUNT, 0L)
    }

    private val mAdditionalAmount by lazy {
        intent.getLongExtra(BasePaymentActivity.TRANSACTION_ADDITIONAL_AMOUNT, 0L)
    }

    private val mAccountType by lazy {
        intent.getSerializableExtra(BasePaymentActivity.TRANSACTION_ACCOUNT_TYPE) as AccountType
    }

//    override fun initializeDefaultUI(): DefaultUIModel {
//        return DefaultUIModel(transactionTitle = "VAS Purchase", amount = mAmount)
//    }

    private val mDb by lazy {
        (application as App).db
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

       // setDeviceStatusObserver()

        val mProgressDialog by lazy {
            this.indeterminateProgressDialog("Processing") {
                setCancelable(false)
            }
        }

        mProgressDialog.show()

        Single.fromCallable {
            (application as App).db.vasTerminalDataDao.get()
        }.subscribeOn(Schedulers.io())
                .flatMap { vasTerminalDetails ->
                    val connectionData = ConnectionData(vasTerminalDetails.tid, "196.6.103.73", 5043, true)

                    val keyHolder = KeyHolder(vasTerminalDetails.masterKey, vasTerminalDetails.sessionKey, vasTerminalDetails.pinKey)

                    val configData = ConfigData()


                    //Time out
                    configData.storeConfigData("04002", "90")

                    //Country Code
                    configData.storeConfigData("06003", vasTerminalDetails.countryCode)

                    //MCC
                    configData.storeConfigData("08004", vasTerminalDetails.mcc)


                    //Merchant's name - 40 length
                    configData.storeConfigData("52040", vasTerminalDetails.merchantName)

                    //Merchant Id - 15 length
                    configData.storeConfigData("03015", vasTerminalDetails.mid)

                    //Currency Code
                    configData.storeConfigData("05003", vasTerminalDetails.currencyCode)

                    val inputData = InputData(mAmount, mAdditionalAmount, mAccountType)
                    val vasPurchase = VasPurchase(this, mDb, inputData, mHostInteractor, connectionData, mEmvInteractor, configData, keyHolder, this@VasPurchaseProcessor)

                    vasPurchase.getTransactionResult()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result, error ->

                    mProgressDialog.dismiss()
                    result?.let {

                        toast(it.transactionStatus)
                        Log.d("OkHTransactionResult",  it.toString())
                        EmvInteractor.getInstance(mNewlandDevice).processOnlineResponse(it.responseCode, it.issuerAuthData91, it.issuerScript71, it.issuerScript72)
                        val transaction = TransactionModel(it.terminalID, it.authID, it.STAN, it.RRN, it.cardHolderName, it.PAN, it.cardExpiry, it.amount, it.additionalAmount, it.transactionType, it.responseCode,  it.isApproved, it.transactionStatus, it.transactionStatusReason, it.accountType, it.merchantID, it.originalForwardingInstitutionCode, it.isoTransmissionDateTime)
                        DbManager(application).saveTransactionData(it)
                        setTransactionRrn(it.RRN)
                        val returnIntent = Intent()
                        returnIntent.putExtra("result", transaction)
                        setResult(Activity.RESULT_OK, returnIntent)
                        if (it.isApproved){
//                            val serviceresult = ServiceResult.Data(Beneficiary(it.cardHolderName, it.amount.toString(), "transfer"), it.amount.toInt())

                            SecureStorage.store("currentservice", "Withdrawal")
                            SecureStorage.store("currentAmount", it.amount)
                            SecureStorage.store("currentPhone", "0")
                            val intent = Intent(this@VasPurchaseProcessor, ResultVasActivity::class.java)
                            intent.putExtra("transaction", transaction)
                            //intent.putExtra("serviceresult", serviceresult)
                            startActivity(intent)

                            val transfer = TransferAmountEntry()
                            transfer.creditWallet(this@VasPurchaseProcessor, transaction)

                        }
                        else{
                            toast("Transaction Declined")
                        }


                    }

                    error?.let {
                        toast(it.message!!)
                    }
                }



    }

//    override fun finish() {
//        VerifoneDevice.removeService(this)
//        super.finish()
//    }
}

package com.iisysgroup.payviceforagents.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.util.Helper;
import com.iisysgroup.payviceforagents.util.TransactionHistotyAdapter;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RepeatTransactionFragment extends Fragment {

    static final String HISTORY = "history";

    @BindView(R.id.subTitleText)
    TextView subTitleText;
    @BindView(R.id.main_layout)
    NestedScrollView mainLayout;
    @BindView(R.id.main_layout2)
    LinearLayout mainLayout2;
    @BindView(R.id.productOptionText)
    TextView productOptionText;
    @BindView(R.id.productOption)
    TextView productOption;
    @BindView(R.id.phoneText)
    TextView phoneText;
    @BindView(R.id.beneficiaryText)
    TextView beneficiaryText;
    @BindView(R.id.amountTIL)
    TextInputLayout amountTIL;
    @BindView(R.id.amountEdit)
    EditText amountEdit;
    @BindView(R.id.proceedBtn)
    AppCompatButton proceedButton;
    TransactionHistotyAdapter.History history;
    String beneficiary = "", productString = "";
    int amount;
    private OnFragmentInteractionListener mListener;

    public RepeatTransactionFragment() {
        // Required empty public constructor
    }

    public static RepeatTransactionFragment getInstance(TransactionHistotyAdapter.History history) {
        RepeatTransactionFragment instance = new RepeatTransactionFragment();

        Bundle args = new Bundle();
        args.putSerializable(HISTORY, history);

        instance.setArguments(args);

        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            history = (TransactionHistotyAdapter.History) args.getSerializable(HISTORY);
            beneficiary = history.beneficiary;
            productString = history.product;
            amount = (int) Double.parseDouble(Helper.sanitizeStringAmount(history.amount)
                    .replace('N', ' ').replace('\u20A6', ' ').replaceAll(",", "").trim());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_repeat_transaction, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        beneficiaryText.setText(beneficiary);
        amountEdit.setText("" + amount);
        amountEdit.addTextChangedListener(new TextWatcher() {

            String temp;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                temp = s.toString().replaceAll(",", "").trim();
            }

            @Override
            public void afterTextChanged(Editable s) {
                amountEdit.removeTextChangedListener(this);
                if (temp.length() > 0 && temp.charAt(0) != '\u20A6') {
                    temp = DecimalFormat.getInstance().format(Integer.parseInt(temp));
                } else if (temp.length() == 1 && temp.charAt(0) == '\u20A6')
                    temp = "";

                amountEdit.setText(temp);
                amountEdit.addTextChangedListener(this);
                amountEdit.setSelection(amountEdit.length());
            }
        });
        productOption.setText(productString);

        if (history.isRecharge()) {
            subTitleText.setText("Recharge");
        } else {
            subTitleText.setText("Bill Payment");
        }
    }


    public void onButtonPressed(String amount) {
        if (mListener != null) {
            mListener.onFragmentInteraction(amount);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    boolean isValid() {
        String amount = Helper.sanitizeStringAmount(amountEdit.getText().toString());
        if (amount.isEmpty()) {
            amountEdit.setError("Enter amount");
            amountEdit.requestFocus();
            Helper.showErrorAnim(amountEdit);
            return false;
        }

        if (Integer.parseInt(amount) < 50) {
            amountEdit.setError("Amount cannot be less than 50");
            amountEdit.requestFocus();
            Helper.showErrorAnim(amountEdit);
            return false;
        }

        return true;
    }


    @OnClick(R.id.proceedBtn)
    void proceed() {
        if (isValid()) {
            onButtonPressed(amountEdit.getText().toString());
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String amount);
    }
}

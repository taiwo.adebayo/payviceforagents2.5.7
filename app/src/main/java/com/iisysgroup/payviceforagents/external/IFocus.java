package com.iisysgroup.payviceforagents.external;

/**
 * Created by Bamitale@Itex on 6/7/2016.
 */
public interface IFocus {
    void focus();
}

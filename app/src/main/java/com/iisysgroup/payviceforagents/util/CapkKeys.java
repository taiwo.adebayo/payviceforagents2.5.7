package com.iisysgroup.payviceforagents.util;

import com.newland.mtype.util.ISOUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bamitale@Itex on 01/06/2017.
 */

public class CapkKeys {

    public static List<CapkKey> capkKeys =  new ArrayList<>();

    static{
        String pkmodulestring = "c0 d8 5f 6a 86 a3 1e e6 14 9c 70 44 36 32 83 1a 75 b1 2b 69 0f a9 e3 7d 1b 2f 0c 5e 65 45 60 d0 44 3f 10 78 4e 61 cf c4 c7 9b 5e c6 39 da 9a c6 af 17 02 ec 71 7f 24 92 3d b2 da b7 60 d6 4f fb 57 22 99 ee 3c 52 c1 c9 99 c3 e7 e3 b4 a0 24 8d bd 8e 8d af fd 99 2c a2 c7 66 df 74 79 6d e9 7a c4 4c 48 23 83 22 31 15 89 f6 61 c6 f4 20 fe 42 99 05 08 a8 83 20 20 d9 0a bb 24 d1 7c a5 cb 2c cc ae a5 a4 8c ad e0 ed 51 fe 60 2b fa ef d7 c0 6b c2 7d f2 de a9 0e f6 2e 88 bc 49 4a 93 ca dd d2 08 78 e5 ff 4f 28 a0 c3 b9 fe e4 4d 20 78 e9 41 43 dc fb 02 20 96 1a 5d b5 7a 39 a0 64 4f fa 69 3c 0a fd 88 67 d3 33 6d 6b be 93 33 87 4e 55 1c 24 bd e9 70 1b f7 b3 85 5d 2d 36 5f 19 8b 4b f9 cc a0 ed 9e 4b a9 92 11 40 22 2c 85 12 db 97 48 68 c1 21 a9 61 47 d1 7f 81 2a 22 ec 87 df e9";
        String pkExponentstring = "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 01";
        final  byte[] pkModule = ISOUtils.hex2byte(pkmodulestring);
        final byte[] pkExponent = ISOUtils.hex2byte(pkExponentstring);
        capkKeys.add(new CapkKey(pkModule,pkExponent,null,null,null, 1024));

        capkKeys.add(new CapkKey("B036A8CAE0593A480976BFE84F8A67759E52B3D9F4A68CCC37FE720E594E5694CD1AE2" + // Verve 05
                "0E1B120D7A18FA5C70E044D3B12E932C9BBD9FDEA4BE11071EF8CA3AF48FF2B5DDB30\n" +
                "7FC752C5C73F5F274D4238A92B4FCE66FC93DA18E6C1CC1AA3CFAFCB071B67DAACE96\n" +
                "D9314DB494982F5C967F698A05E1A8A69DA931B8E566270F04EAB575F5967104118E4F12\n" +
                "ABFF9DEC92379CD955A10675282FE1B60CAD13F9BB80C272A40B6A344EA699FB9EFA68\n" +
                "67", "03","676822D335AB0D2C3848418CB546DF7B6A6C32C0","05","230228",1024));

        capkKeys.add(new CapkKey("D13CD5E1B921E4E0F0D40E2DE14CCE73E3A34ED2DCFA826531D8195641091\n" +  // Verve 04
                "E37C8474D19B686E8243F089A69F7B18D2D34CB4824F228F7750F96D1EFBDFF881F259A\n" +
                "8C04DE64915A3A3D7CB846135F4083C93CDE755BC808886F600542DFF085558D5EA7F45\n" +
                "CB15EC835064AA856D602A0A44CD021F54CF8EC0CC680B54B3665ABE74A7C43D02897F\n" +
                "F84BB4CB98BC91D", "03","8B36A3E3D814CE6C6EBEAAF27674BB7BC67275B1","04","171231",1024));

        capkKeys.add(new CapkKey("B036A8CAE0593A480976BFE84F8A67759E52B3D9F4A68CCC37FE720E594E5694CD1AE20E1B120D7A18FA5C70E044D3B12E932C9BBD9FDEA4BE11071EF8CA3AF48FF2B5DDB307FC752C5C73F5F274D4238A92B4FCE66FC93DA18E6C1CC1AA3CFAFCB071B67DAACE96D9314DB494982F5C967F698A05E1A8A69DA931B8E566270F04EAB575F5967104118E4F12ABFF9DEC92379CD955A10675282FE1B60CAD13F9BB80C272A40B6A344EA699FB9EFA6867","03","676822D335AB0D2C3848418CB546DF7B6A6C32C0", "09", "151231", 1024));

        capkKeys.add(new CapkKey("D13CD5E1B921E4E0F0D40E2DE14CCE73E3A34ED2DCFA826531D8195641091E37C8474D19B686E8243F089A69F7B18D2D34CB4824F228F7750F96D1EFBDFF881F259A8C04DE64915A3A3D7CB846135F4083C93CDE755BC808886F600542DFF085558D5EA7F45CB15EC835064AA856D602A0A44CD021F54CF8EC0CC680B54B3665ABE74A7C43D02897FF84BB4CB98BC91D", "03", "8B36A3E3D814CE6C6EBEAAF27674BB7BC67275B1", "10", "151231", 1024));

        capkKeys.add(new CapkKey("D9FD6ED75D51D0E30664BD157023EAA1FFA871E4DA65672B863D255E81E137A51DE4F72BCC9E44ACE12127F87E263D3AF9DD9CF35CA4A7B01E907000BA85D24954C2FCA3074825DDD4C0C8F186CB020F683E02F2DEAD3969133F06F7845166ACEB57CA0FC2603445469811D293BFEFBAFAB57631B3DD91E796BF850A25012F1AE38F05AA5C4D6D03B1DC2E568612785938BBC9B3CD3A910C1DA55A5A9218ACE0F7A21287752682F15832A678D6E1ED0B", "03", "20D213126955DE205ADC2FD2822BD22DE21CF9A8", "11", "151231", 1024));

        capkKeys.add(new CapkKey("9D912248DE0A4E39C1A7DDE3F6D2588992C1A4095AFBD1824D1BA74847F2BC4926D2EFD904B4B54954CD189A54C5D1179654F8F9B0D2AB5F0357EB642FEDA95D3912C6576945FAB897E7062CAA44A4AA06B8FE6E3DBA18AF6AE3738E30429EE9BE03427C9D64F695FA8CAB4BFE376853EA34AD1D76BFCAD15908C077FFE6DC5521ECEF5D278A96E26F57359FFAEDA19434B937F1AD999DC5C41EB11935B44C18100E857F431A4A5A6BB65114F174C2D7B59FDF237D6BB1DD0916E644D709DED56481477C75D95CDD68254615F7740EC07F330AC5D67BCD75BF23D28A140826C026DBDE971A37CD3EF9B8DF644AC385010501EFC6509D7A41", "03", "1FF80A40173F52D7D27E0F26A146A1C8CCB29046", "12", "151231", 1024));

        capkKeys.add(new CapkKey("A8\n" +
                "9F\n" +
                "25\n" +
                "A5\n" +
                "6F\n" +
                "A6\n" +
                "DA\n" +
                "25\n" +
                "8C\n" +
                "8C\n" +
                "A8\n" +
                "B4\n" +
                "04\n" +
                "27\n" +
                "D9\n" +
                "27\n" +
                "B4\n" +
                "A1\n" +
                "EB\n" +
                "4D\n" +
                "7E\n" +
                "A3\n" +
                "26\n" +
                "BB\n" +
                "B1\n" +
                "2F\n" +
                "97\n" +
                "DE\n" +
                "D7\n" +
                "0A\n" +
                "E5\n" +
                "E4\n" +
                "48\n" +
                "0F\n" +
                "C9\n" +
                "C5\n" +
                "E8\n" +
                "A9\n" +
                "72\n" +
                "17\n" +
                "71\n" +
                "10\n" +
                "A1\n" +
                "CC\n" +
                "31\n" +
                "8D\n" +
                "06\n" +
                "D2\n" +
                "F8\n" +
                "F5\n" +
                "C4\n" +
                "84\n" +
                "4A\n" +
                "C5\n" +
                "FA\n" +
                "79\n" +
                "A4\n" +
                "DC\n" +
                "47\n" +
                "0B\n" +
                "B1\n" +
                "1E\n" +
                "D6\n" +
                "35\n" +
                "69\n" +
                "9C\n" +
                "17\n" +
                "08\n" +
                "1B\n" +
                "90\n" +
                "F1\n" +
                "B9\n" +
                "84\n" +
                "F1\n" +
                "2E\n" +
                "92\n" +
                "C1\n" +
                "C5\n" +
                "29\n" +
                "27\n" +
                "6D\n" +
                "8A\n" +
                "F8\n" +
                "EC\n" +
                "7F\n" +
                "28\n" +
                "49\n" +
                "20\n" +
                "97\n" +
                "D8\n" +
                "CD\n" +
                "5B\n" +
                "EC\n" +
                "EA\n" +
                "16\n" +
                "FE\n" +
                "40\n" +
                "88\n" +
                "F6\n" +
                "CF\n" +
                "AB\n" +
                "4A\n" +
                "1B\n" +
                "42\n" +
                "32\n" +
                "8A\n" +
                "1B\n" +
                "99\n" +
                "6F\n" +
                "92\n" +
                "78\n" +
                "B0\n" +
                "B7\n" +
                "E3\n" +
                "31\n" +
                "1C\n" +
                "A5\n" +
                "EF\n" +
                "85\n" +
                "6C\n" +
                "2F\n" +
                "88\n" +
                "84\n" +
                "74\n" +
                "B8\n" +
                "36\n" +
                "12\n" +
                "A8\n" +
                "2E\n" +
                "4E\n" +
                "00\n" +
                "D0\n" +
                "CD\n" +
                "40\n" +
                "69\n" +
                "A6\n" +
                "78\n" +
                "31\n" +
                "40\n" +
                "43\n" +
                "3D\n" +
                "50\n" +
                "72\n" +
                "5F", "03",
                "B4 \n" +
                        "BC \n" +
                        "56 \n" +
                        "CC \n" +
                        "4E\n" +
                        "88\n" +
                        "32\n" +
                        "49\n" +
                        "32\n" +
                        "CB\n" +
                        "C6\n" +
                        "43\n" +
                        "D6\n" +
                        "89\n" +
                        "8F\n" +
                        "6FE593B172","07","171231",1152));
    }
}

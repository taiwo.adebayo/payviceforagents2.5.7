package com.iisysgroup.payviceforagents;

import android.content.Context;

import com.iisysgroup.poslib.ISO.common.IsoAdapter;
import com.iisysgroup.poslib.ISO.common.IsoReversalProcessData;
import com.iisysgroup.poslib.ISO.common.IsoReversalTransactionData;
import com.iisysgroup.poslib.ISO.common.IsoTimeManager;
import com.iisysgroup.poslib.ISO.common.IsoTransactionExecutor;
import com.iisysgroup.poslib.commons.TripleDES;
import com.iisysgroup.poslib.commons.Utility;
import com.iisysgroup.poslib.host.Host;
import com.iisysgroup.poslib.host.entities.ConnectionData;
import com.iisysgroup.poslib.host.entities.KeyHolder;
import com.iisysgroup.poslib.host.entities.TransactionResult;
import com.iisysgroup.poslib.utils.TransactionData;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

import org.jpos.iso.ISOMsg;

public class VasRollBackCommunicator {
    private String originalDataElements;
    private String messageReasonCode;
    private String replacementAmount;
    private String originalSequenceNumber;
    private IsoReversalTransactionData reversalTransactionData;
    protected static final String posDataCode = "510101511344101";
    protected static final String posEntryMode = "051";
    protected static final String posConditionCode = "00";
    protected static final String posPinCaptureMode = "12";
    protected static final String amountTransactionFee = "D00000000";
    protected Context context;
    protected ConnectionData connectionData;
    protected TransactionData transactionData;
    protected IsoTimeManager timeMgr = new IsoTimeManager();
    protected TransactionResult transactionResult;
    protected String amount;
    protected String track2Data;
    protected String iccData;
    protected String pan;
    protected String acquiringInstitutionIdCode;
    protected String merchantType;
    protected String merchantNameLocation;
    protected String cardAcceptorIdCode;
    protected String serviceCode;
    protected String terminalID;
    protected String retrievalRefNumber;
    protected String fromAccountType;
    protected String processingCode;
    protected String expiryDate;
    protected String panSequenceNumber = "001";
    protected String transmissionDateTime;
    protected String timeLocalTransaction;
    protected String dateLocalTransaction;
    protected String transactionCurrencyCode = "566";
    protected String sequenceNumber;
    protected KeyHolder keysh;

    public VasRollBackCommunicator(Context cont, ConnectionData connectionData1, IsoReversalTransactionData transactionData) {
        this.context = cont;
        this.connectionData = connectionData1;
        this.amount = transactionData.getInputData().getAmount() + transactionData.getInputData().getAdditionalAmount() + "";
        this.reversalTransactionData = transactionData;
        this.processingCode = "00" + this.fromAccountType + "00";
        this.originalSequenceNumber = this.reversalTransactionData.getOriginalSequenceNumber();
        this.messageReasonCode = IsoReversalProcessData.ReversalReasonCode.TIMEOUT_AWAITING_RESPONSE.toString();
        String acqCode = transactionData.getEmvCard().getTrack2Data().substring(0, 6).toUpperCase();
        acqCode = Utility.padLeft(acqCode, 11, '0');
        String originalForwardingInstCode = Utility.padLeft(transactionData.getOriginalForwardingInstitutionCode(), 11, '0');
        this.originalDataElements = "0200" + this.originalSequenceNumber + this.reversalTransactionData.getIsoTransmissionDateTime() + acqCode + originalForwardingInstCode;
        this.replacementAmount = Utility.padLeft(this.amount, 12, '0');
    }

    /*public TransactionResult processOnlineTransaction() {
        try {
            IsoMessage isoMsge = new IsoMessage();
            isoMsge.setType(1056);
            isoMsge.setField(2, new IsoValue(IsoType.LLVAR, this.pan));
            isoMsge.setField(3, new IsoValue(IsoType.ALPHA, this.processingCode, 6));
            isoMsge.setField(4, new IsoValue(IsoType.ALPHA, Utility.padLeft(this.amount, 12, '0'), 12));
            isoMsge.setField(7, new IsoValue(IsoType.ALPHA, this.transmissionDateTime, 10));
            isoMsge.setField(11, new IsoValue(IsoType.NUMERIC, Utility.padLeft(this.sequenceNumber + "", 6, '0'), 6));
            isoMsge.setField(12, new IsoValue(IsoType.ALPHA, this.timeLocalTransaction, 6));
            isoMsge.setField(13, new IsoValue(IsoType.ALPHA, this.dateLocalTransaction, 4));
            isoMsge.setField(14, new IsoValue(IsoType.ALPHA, this.expiryDate, 4));
            isoMsge.setField(18, new IsoValue(IsoType.ALPHA, this.merchantType, 4));
            isoMsge.setField(22, new IsoValue(IsoType.ALPHA, "051", 3));
            isoMsge.setField(23, new IsoValue(IsoType.ALPHA, this.getPanSequenceNumber(), 3));
            isoMsge.setField(25, new IsoValue(IsoType.ALPHA, "00", 2));
            isoMsge.setField(26, new IsoValue(IsoType.ALPHA, "12", 2));
            isoMsge.setField(28, new IsoValue(IsoType.ALPHA, "D00000000", 9));
            isoMsge.setField(32, new IsoValue(IsoType.LLVAR, this.acquiringInstitutionIdCode));
            isoMsge.setField(35, new IsoValue(IsoType.LLVAR, this.track2Data));
            isoMsge.setField(37, new IsoValue(IsoType.ALPHA, this.retrievalRefNumber, 12));
            isoMsge.setField(40, new IsoValue(IsoType.ALPHA, this.serviceCode, 3));
            isoMsge.setField(41, new IsoValue(IsoType.ALPHA, this.terminalID, 8));
            isoMsge.setField(42, new IsoValue(IsoType.ALPHA, this.cardAcceptorIdCode, 15));
            isoMsge.setField(43, new IsoValue(IsoType.ALPHA, this.merchantNameLocation, 40));
            isoMsge.setField(49, new IsoValue(IsoType.ALPHA, this.transactionCurrencyCode, 3));
            String pinBlock = this.getDecryptedPinBlock(this.transactionData.getEmvCard().getPinInfo(), this.transactionData.getKeyHolder());
            if (!pinBlock.isEmpty()) {
                isoMsge.setField(52, new IsoValue(IsoType.ALPHA, pinBlock, 16));
            }

            isoMsge.setField(55, new IsoValue(IsoType.LLLVAR, this.iccData));
            isoMsge.setField(56, new IsoValue(IsoType.LLLVAR, this.messageReasonCode));
            isoMsge.setField(90, new IsoValue(IsoType.ALPHA, this.originalDataElements, 42));
            isoMsge.setField(95, new IsoValue(IsoType.ALPHA, "000000000000000000000000D00000000D00000000", 42));
            isoMsge.setField(123, new IsoValue(IsoType.LLLVAR, "510101511344101"));
            isoMsge.setField(128, new IsoValue(IsoType.ALPHA, "", 40));
            String isoMessage = (new String(isoMsge.writeData())).trim();
            String hash = TripleDES.generateHash256Value(isoMessage, this.getSessionKey());
            isoMessage = isoMessage + hash;
            this.buildTransactionResultData(isoMsge);
            String response = IsoTransactionExecutor.execute(this.context, IsoAdapter.prepareByteStream(isoMessage.getBytes("UTF-8")), this.connectionData);
            ISOMsg isoMsg = (new IsoAdapter(this.context)).processISOBitStream(response);
            this.processHostResponse(isoMsg);
            if (this.transactionResult.responseCode.equals("00")) {
                this.transactionResult.transactionStatus = "Ok";
                this.transactionResult.transactionStatusReason = "transaction with ref number " + isoMsg.getString(37) + " has been reversed ";
            } else {
                this.transactionResult.transactionStatus = "Declined";
            }
        } catch (Exception var7) {
            this.handleException(var7, false);
        }

        return this.transactionResult;
    }*/

    protected void buildTransactionResultData(IsoMessage isoMessage) {
        //super.buildTransactionResultData(isoMessage);
        this.transactionResult.transactionType = Host.TransactionType.REVERSAL;
    }
}

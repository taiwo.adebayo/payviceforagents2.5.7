package com.iisysgroup.payviceforagents.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.util.SingleImageTitleObject;

import java.util.ArrayList;
import java.util.List;


public class LoginSettingsActivityFragment extends ListFragment {

    OnFragmentInteractionListener listener;

    List<SingleImageTitleObject> objectList = new ArrayList<>();
    SingleImageTitleObject.SingleImageTitleAdapter adapter;
    String[] loginSettings = {"Recover lost password", "Link this device to profile",};

    {
        objectList.add(new SingleImageTitleObject("Recover lost password", R.drawable.ic_lock_black_24dp));
        objectList.add(new SingleImageTitleObject("Link this device to profile", R.drawable.ic_smartphone_black_24dp));
        objectList.add(new SingleImageTitleObject("Activate My Account", R.drawable.ic_person_add_black_24dp));
    }

    public LoginSettingsActivityFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().setTitle(R.string.title_activity_login_settings);
        adapter = new SingleImageTitleObject.SingleImageTitleAdapter(objectList, getContext());
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (listener != null) {
            listener.onSettingSelected(position);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnFragmentInteractionListener {
        void onSettingSelected(int pos);
    }
}

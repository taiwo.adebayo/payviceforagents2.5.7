package com.iisysgroup.payviceforagents.util;

import com.iisysgroup.payviceforagents.models.HistoryParam;
import com.iisysgroup.payviceforagents.models.TransactionHistory;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Itex-PC on 31/10/2018.
 */

public interface TransactionHistoryApi{
    @POST("/api/account/transaction-history")
//   Call<TransactionHistory> fetchHistoty(
//            @Field("wallet") String wallet,
//            @Field("username") String username,
//            @Field("password")String password );
////            @Field("startDate")String startDate,
////            @Field("endDate")String endDate,
////            @Field("limit")String limit,
////            @Field ("currentPage")String currrentPage

//    Call fetchHistoty(@Body HistoryParam paramModel);
    Call<TransactionHistory> fetchHistoty(@Body HistoryParam paramModel);
    //Call<TransactionHistory> fetchHistoty( @Path("wallet") String walletId);
//    Call<TransactionSummary> fetchHistoty(@Body HistoryParam paramModel);

//            @Field("startDate")String startDate,
//            @Field("endDate")String endDate,
//            @Field("limit")String limit,
//            @Field ("currentPage")String currrentPage




}
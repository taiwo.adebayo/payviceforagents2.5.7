package com.iisysgroup.payviceforagents.activities

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.iisysgroup.payviceforagents.R
import com.iisysgroup.payviceforagents.external.CreditCardUtils
import com.iisysgroup.payviceforagents.fragments.CardPinEntryFragment
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.iisysgroup.payviceforagents.util.Helper
import com.iisysgroup.payviceforagents.util.PaymentOption
import com.iisysgroup.payviceforagents.util.getSubscriber
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxCompoundButton
import kotlinx.android.synthetic.main.activity_payment_option.*
import kotlinx.android.synthetic.main.fragment_base_balance.*

class PaymentOptionActivity : BaseActivity() {

    private var paymentOption = PaymentOption.WALLET
    private lateinit var mode: PaymentOption.Mode

    private var card: Card? = null

    private var amount = 0
    private var modeText = ""
    private var beneficiary = ""


    private val convenienceFee by lazy {
        Helper.getPreference(this, Helper.COMMISSION_KEY, Helper.default_commission)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_option)

        intent?.let {
            beneficiary = it.getStringExtra(EXTRA_BENEFICIARY)
            amount = it.getIntExtra(EXTRA_AMOUNT, 0)
            mode = it.getSerializableExtra(EXTRA_MODE) as? PaymentOption.Mode ?: PaymentOption.Mode.PAY

        }

        initializeControls()
    }


    private fun initializeControls() {
        //fundButton.visibility = View.GONE
        walletBalanceText.text = Helper.getPreference(this, Helper.BALANCE, "")
        amountText.text = "\u20A6 $amount"
        beneficiaryText.text = beneficiary
        convenienceFeeText.text = "\u20A6 $convenienceFee"

        if (mode == PaymentOption.Mode.FUND) {
            modeText = "Fund"
            radWalletLayout.isEnabled = false
            radPayWithWallet.isEnabled = false

//            fundButton.isEnabled = false
//            fundButton.visibility = View.INVISIBLE
        } else {
            modeText = "Pay"

            if (mode == PaymentOption.Mode.REPEAT) {
                radLinkedCardLayout.isEnabled = false
                radPayWithLinkedCard.isEnabled = false

                radDebitCardLayout.isEnabled = false
                radPayWithDebitCard.isEnabled = false
            }
        }

        modeTextView.text = "$modeText With"
        proceedBtn.text = String.format("%s %c %d", modeText, '\u20A6', amount + convenienceFee)

        proceedBtn.setOnClickListener {
//            Log.d("pinstored", SecureStorage.retrieve(Helper.PIN, "")+"hg")
//            Log.d("userid", SecureStorage.retrieve(Helper.USER_ID, "")+"hg")
//            Log.d("password", SecureStorage.retrieve(Helper.STORED_PASSWORD, "")+"hg")
            if (isValidInputs()) {
                AlertDialog.Builder(this)
                        .setTitle(R.string.app_name)
                        .setMessage("Proceed with payment?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.ok) { dialog, which ->
                            val data = Intent()
                            data.putExtra(RESULT_OPTION, paymentOption)

                            card?.pin?.let {
                                if (radPayWithDebitCard.isChecked || radPayWithLinkedCard.isChecked) {
                                    data.putExtra(RESULT_CARD, card)
                                }
                            }

                            setResult(Activity.RESULT_OK, data)
                            finish()
                        }.show()
            }
        }


        val walletOptionSubscriber = getSubscriber<Boolean> {
            if (it) {
                paymentOption = PaymentOption.WALLET
                radPayWithDebitCard.isChecked = false
                radPayWithLinkedCard.isChecked = false
            }
        }

        val linkedCardSubscriber = getSubscriber<Boolean> {
            if (it) {
                paymentOption = PaymentOption.LINKED_CARD
                radPayWithWallet.isChecked = false
                radPayWithDebitCard.isChecked = false
                startActivityForResult(Intent(this,
                        SelectCardActivity::class.java), SelectCardActivity.REQUEST_CODE)
            }
        }

        val debitCardSubscriber = getSubscriber<Boolean> {
            if (it) {
                paymentOption = PaymentOption.DEBIT_CARD
                radPayWithLinkedCard.isChecked = false
                radPayWithWallet.isChecked = false
//                startActivityForResult(Intent(this, MyCardEditActivity::class.java),
//                        MyApplication.GET_CARD_REQUEST_CODE)
            }
        }



        RxView.clicks(radWalletLayout).map {
            radPayWithWallet.isChecked = true
        }.subscribe()

        RxView.clicks(radDebitCardLayout).map {
            radPayWithDebitCard.isChecked = true
        }.subscribe()

        RxView.clicks(radLinkedCardLayout).map {
            radPayWithLinkedCard.isChecked = true
        }.subscribe()


        RxCompoundButton.checkedChanges(radPayWithDebitCard).subscribe(debitCardSubscriber)
        RxCompoundButton.checkedChanges(radPayWithLinkedCard).subscribe(linkedCardSubscriber)
        RxCompoundButton.checkedChanges(radPayWithWallet).subscribe(walletOptionSubscriber)

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.default_activity_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_cancel -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == MyApplication.GET_CARD_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data?.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY) != null) {

                val cardHolderName = data.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME)
                val cardNumber = data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER)
                val expiry = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY)
                val cvv = data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV)

                card = Card()
                card?.cardHolder = cardHolderName
                card?.PAN = cardNumber
                card?.CVV = cvv
                card?.expiryMonth = expiry.split("\\/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
                card?.expiryYear = expiry.split("\\/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]

                getCardPin()
            } else {
                radPayWithDebitCard.isChecked = false
            }
        }

        if (requestCode == SelectCardActivity.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                card = data?.getSerializableExtra(SelectCardActivity.EXTRA_DATA) as Card
                getCardPin()
            } else {
                radPayWithLinkedCard.isChecked = false
            }
        }

    }


    private fun getCardPin() {
        val pinDialogFragment = CardPinEntryFragment()
        pinDialogFragment.pin.observe(this, Observer {
            it?.let {
                pinDialogFragment.dismiss()
                card?.pin = it
                Log.d("pin", it.toString())
            }
        })

        pinDialogFragment.show(supportFragmentManager, "PinDialog")
    }

    private fun isValidInputs(): Boolean {
        if (radPayWithLinkedCard.isChecked ||
                radPayWithWallet.isChecked || radPayWithDebitCard.isChecked)
            return true

        Helper.showSnackBar(walletBalanceText, "Please select a payment method")
        return false
    }

    companion object {
        const val REQUEST_CODE = 300
        const val EXTRA_BENEFICIARY = "beneficiary"
        const val EXTRA_AMOUNT = "amount"
        const val EXTRA_MODE = "mode"

        const val RESULT_OPTION = "option"
        const val RESULT_CARD = "selected_card"
    }

}



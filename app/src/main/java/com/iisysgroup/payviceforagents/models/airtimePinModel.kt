package com.iisysgroup.payviceforagents.models

data class airtimePinModel(val error : String, val status : String, val message : String, val amount : String, val pin_data: pinData, val ref :String, val date : String, val transactionID : String)

data class pinData(val message : String, val amount : String, val pin : String, val expiry : String, val pin_ref : String, val serial : String, val dial : String)
package com.iisysgroup.payviceforagents.persistence.entitiy

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/**
 * Created by Bamitale@Itex on 28/02/2018.
 */

//@Entity
data class Beneficiary(
        @PrimaryKey
        @ColumnInfo(name = "data") var data: String,
        @ColumnInfo(name = "displayInfo") var displayInfo: String,
        @ColumnInfo(name = "assigned_product") var assignedProduct: String = "") : Serializable

package com.iisysgroup.payvicegamesdk

import com.iisysgroup.payvicegamesdk.utils.CardUtils
import com.interswitchng.sdk.payment.model.Card

data class Cards(val pan: String,
                val expiryMonth: Int,
                val expiryYear: Int,
                val cvv: String,
                var cardHolder: String,
                val pin: String)



fun Cards.toString(): String {
    return "cardHolder $maskedPan $expiryMonth $expiryYear $cvv"
}

val Cards.maskedPan: String
get() {
    return CardUtils.processPan(this.pan)
}

val Cards.expiryMonthString: String
get() {
    return CardUtils.pad(this.expiryMonth.toString(), 2, '0',true)
}

val Cards.expiryYearString: String
get() {
    return CardUtils.pad(this.expiryYear.toString(), 2, "20", true)
}
package com.iisysgroup.payvicegamesdk;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Bamitale @Itex on 2/8/2016.
 */
class TamsRequestProcessor {
    /**
     * @param url
     * @param params
     */
    TamsResponse  processRequest(String url, Map<String, String> params) {
        RestWrapper restWrapper = new RestWrapper(url);
        TamsResponse tamsResponse;

        restWrapper.setParams(params);

        Log.d("OkHRequest", url);
        Log.d("OkHRequest", params.toString());
        String responseString = null;


        try {
            responseString = restWrapper.processRequest(RestWrapper.Request.GET);
            if (!responseString.isEmpty())
                tamsResponse = getTamsResponse(responseString);
            else
                tamsResponse =  new TamsResponse("Server communication error. Please try again later.");
        } catch (Exception e) {
            e.printStackTrace();

            tamsResponse = new TamsResponse("An error occurred. Please try again later");
        }

        return tamsResponse;
    }

    private static TamsResponse getTamsResponse(String responseString) {

            TamsResponse tamsResponse  =  new TamsResponse("");

            try {
                String resultCodeString = "";
                String message = "";
                String balance = "";
                String macrosTid = "";
                String key = "";

                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                InputSource is = new InputSource(new StringReader(responseString.trim()));

                Document document = docBuilder.parse(is);
                Element parentElement = document.getDocumentElement();

                if (responseString.contains("errmsg")) {
                    Element em = (Element) parentElement.getElementsByTagName("errcode").item(0);
                    resultCodeString = em.getTextContent();

                    em = (Element) parentElement.getElementsByTagName("errmsg").item(0);
                    message = em.getTextContent();

                    tamsResponse.message = message;
                    tamsResponse.result = TamsResponse.Status.DECLINED;
                    tamsResponse.resultCode = Integer.parseInt(resultCodeString);
                } else {
                    NodeList nodeList = parentElement.getElementsByTagName("tran");

                    Element tranElement = (Element)nodeList.item(0);

                    NodeList tranNodeList = tranElement.getChildNodes();

                    for (int i = 0; i < tranNodeList.getLength(); i++) {
                        Element node = (Element) tranNodeList.item(i);
                        if (node.getNodeName().equals("result")) {
                            resultCodeString = node.getTextContent();
                        }

                        if (node.getNodeName().equals("message")) {
                            message = node.getTextContent();
                        }

                        if (node.getNodeName().equals("balance")) {
                            balance = node.getTextContent();
                        }

                        if (node.getNodeName().equals("macros_tid")) {
                            macrosTid = node.getTextContent();
                        }


                        if (node.getNodeName().equals( "key")) {
                            key = node.getTextContent();
                        }

                        if (resultCodeString.equals("0")) {
                            tamsResponse.resultCode = 0;
                            tamsResponse.result = TamsResponse.Status.APPROVED;
                            tamsResponse.message = message;
                            tamsResponse.balance = balance;
                            tamsResponse.macrosTID = macrosTid;
                            tamsResponse.key = key;
                        } else {
                            tamsResponse.result = TamsResponse.Status.DECLINED;
                            tamsResponse.message = message;
                            try{
                                tamsResponse.resultCode = Integer.parseInt(resultCodeString);
                            }catch (NumberFormatException e){
                                tamsResponse.resultCode = -1;
                            }
                        }
                    }
                }
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
                tamsResponse.message = "An error has occurred. Please try again later.";
            } catch (SAXException e) {
                e.printStackTrace();
                tamsResponse.message = "An error has occurred. Please try again later.";
            } catch (IOException e) {
                e.printStackTrace();
                tamsResponse.message = "Server communication error. Please try again later.";
            }

            return tamsResponse;
        }


}

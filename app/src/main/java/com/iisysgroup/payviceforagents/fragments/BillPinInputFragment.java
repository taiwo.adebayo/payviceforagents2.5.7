package com.iisysgroup.payviceforagents.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iisysgroup.payviceforagents.R;
import com.iisysgroup.payviceforagents.callbacks.OnPinInputFragmentInteractionListener;
import com.iisysgroup.payviceforagents.entities.PinValue;
import com.iisysgroup.payviceforagents.entities.Service;
import com.iisysgroup.payviceforagents.util.Helper;

import butterknife.BindView;
import butterknife.OnClick;


public class BillPinInputFragment extends BaseBillFragment {

    @BindView(R.id.pinLayout)
    LinearLayout pinLayout;
    @BindView(R.id.selectAmountLayout)
    RelativeLayout selectPinAmount;
    @BindView(R.id.selectableAmountText)
    TextView pinAmountText;
    @BindView(R.id.pinCountText)
    EditText pinCountText;


    private OnPinInputFragmentInteractionListener mListener;

    public BillPinInputFragment() {
        // Required empty public constructor
    }

    public static BillPinInputFragment newInstance(String service) {
        BillPinInputFragment fragment = new BillPinInputFragment();
        Bundle args = new Bundle();
        args.putString(SERVICE, service);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bill_pin_input, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Service.Product product, PinValue pinValue, int quantity) {
        if (mListener != null) {
            mListener.onPinInputFragmentInteraction(product, pinValue, quantity);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPinInputFragmentInteractionListener) {
            mListener = (OnPinInputFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.selectAmountLayout)
    void selectPinAmount() {
        selectPin();
    }


    boolean isValidInputs() {

        if (product == null) {
            serviceText.setError("Please select product");
            Helper.showErrorAnim(serviceText);
            Helper.showSnackBar(serviceText, "Please select product");
            return false;
        }

        if (pinValue == null) {
            pinAmountText.setError("Please select pin");
            Helper.showErrorAnim(pinAmountText);
            Helper.showSnackBar(pinAmountText, "Please select pin");
            return false;
        }

        if (pinCountText.getText().toString().trim().isEmpty()) {
            Helper.showErrorAnim(pinCountText);
            pinCountText.setError("Please enter number of pin");
            pinCountText.requestFocus();
            return false;
        }

        return true;
    }

    @OnClick(R.id.proceedBtn)
    void proceed() {
        if (isValidInputs()) {
            onButtonPressed(product, pinValue, Integer.parseInt(pinCountText.getText().toString()));
        }
    }


}

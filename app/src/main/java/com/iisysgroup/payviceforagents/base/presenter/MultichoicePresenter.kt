package com.iisysgroup.payviceforagents.base.presenter

import android.arch.lifecycle.LiveData
import com.iisysgroup.payviceforagents.base.interactor.MultichoiceInteractor
import com.iisysgroup.payviceforagents.entities.Service
import com.iisysgroup.payviceforagents.paymentprocessors.DebitCardProcessor
import com.iisysgroup.payviceforagents.persistence.entitiy.Card
import com.itex.richard.payviceconnect.model.DstvModel

interface MultichoicePresenter {
    fun setService(service: Service)
    fun setProduct(product: MultichoiceInteractor.MultichoiceProduct)
    fun setError(throwable: Throwable)
    fun setSelectedPlan(position: Int)
    fun validateIuc(iuc: String)
    fun subscribe(iuc: String, authPin: String)
    fun setPlans(plans: List<DstvModel.Data>)
    fun setSmartCardIsValidated(isValidated: Boolean)
    fun getPlans(): LiveData<List<DstvModel.Data>>

    fun subscribeWithCard(iucNumber: String, card: Card?, processor: DebitCardProcessor)
}
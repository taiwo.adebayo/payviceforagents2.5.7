package com.iisysgroup.payviceforagents

import android.app.Activity
import android.arch.lifecycle.LiveDataReactiveStreams
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.iisysgroup.newland.NewlandDevice
import com.iisysgroup.payviceforagents.securestorage.SecureStorage
import com.iisysgroup.payviceforagents.util.DefaultUIManager
import com.iisysgroup.payviceforagents.util.DefaultUIModel
import com.iisysgroup.payviceforagents.util.SharedPreferenceUtils
import com.iisysgroup.poslib.deviceinterface.DeviceState
import com.iisysgroup.poslib.deviceinterface.interactors.EmvInteractor
import com.iisysgroup.poslib.host.HostInteractor
import com.iisysgroup.poslib.host.entities.ConnectionData
import com.iisysgroup.poslib.host.entities.KeyHolder
import com.iisysgroup.poslib.host.entities.TransactionResult
//import com.telpo.moduled.Telpo900Device
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.contentView
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast

abstract class BaseCardPaymentProcessor : AppCompatActivity() {

    private  var rollbackRequest = ""

    lateinit var mNewlandDevice : NewlandDevice
    lateinit var mEmvInteractor : EmvInteractor
    lateinit var mConnectionData: ConnectionData
    lateinit var mHostInteractor: HostInteractor

    private var mRrn : String = ""

    lateinit var keyHolder : KeyHolder

    private val mDb by lazy {
        (application as App).db
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_process)


        doAsync {
            keyHolder =  mDb.keyHolderDao.get()
            Log.d("masterKey", keyHolder.masterKey)
            Log.d("pinKey", keyHolder.pinKey)

            NewlandDevice(this@BaseCardPaymentProcessor, keyHolder.masterKey, keyHolder.pinKey)
        }

        setUpDeviceInteractor()
        setDeviceStatusObserver()

        setUpHostInteractor()
        setUpConnectionData()

    }

    private fun setUpDeviceInteractor() {

        mNewlandDevice = NewlandDevice(this)
        mEmvInteractor = EmvInteractor.getInstance(mNewlandDevice)
    }

    private fun setUpHostInteractor() {
        mHostInteractor = (application as App).hostInteractor
    }

    private fun setUpConnectionData() {
        val terminalId = SharedPreferenceUtils.getTerminalId(this)
        val ipAddress = SharedPreferenceUtils.getIpAddress(this)
        val ipPort = SharedPreferenceUtils.getPort(this)
        val isSSL = SharedPreferenceUtils.isSsl(this)

        when {
            terminalId.isNullOrEmpty() -> {
                toast("Enter valid terminal Id")
                return
            }
            ipAddress.isNullOrEmpty() -> toast("Enter valid IP Address")
            ipPort.toString().isNullOrEmpty() -> toast("Enter valid IP Port")
        }

        mConnectionData = ConnectionData(terminalId, ipAddress, ipPort.toInt(), isSSL)
    }


    fun setTransactionRrn(rrn : String){
        this.mRrn = rrn
    }
    fun setRollbackRequest(request : String){
        this.rollbackRequest = request
    }

    open fun setDeviceStatusObserver(){

        LiveDataReactiveStreams.fromPublisher(mEmvInteractor.observeStatus()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()))
                .observe({ lifecycle }){
                    it?.let{
                        val intent = Intent()
                        intent.putExtra("state", it.state)

                        when {
                            it.state == DeviceState.DECLINED -> {
                                mHostInteractor.rollBackTransaction().subscribe({
                                    Log.d("OkHRollback", it.toString())
                                    intent.putExtra("rrn", it.RRN)

                                    setResult(Activity.RESULT_OK, intent)
                                    finish()
                                },{
                                    it.printStackTrace()
                                })
                            }
                            it.state == DeviceState.FAILED -> {
                                //  toast("Failed")
                                intent.putExtra("rrn", mRrn)

                                setResult(Activity.RESULT_OK, intent)
                                finish()
                            }

                            it.state == DeviceState.AWAITING_ONLINE_RESPONSE -> {
                                Log.d("OkH", "Awaiting online response")
                            }

                            it.state == DeviceState.APPROVED -> {
//                                intent.putExtra("rollbackrequest", rollbackRequest)
                                intent.putExtra("rrn", mRrn)
                                setResult(Activity.RESULT_OK, intent)
                                finish()
                            }
                        }

                        Log.d("UI_STATE", it.state.toString())

                    }
                }


    }
}
package com.iisysgroup.payviceforagents.models;

/**
 * Created by Itex-PC on 15/11/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("nameValuePairs")
    @Expose
    private AllProducts products;

    public AllProducts getproduct() {
        return products;
    }


}


